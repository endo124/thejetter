<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/','Front\HomeController@home')->name('home');
//  Route::get('/home','Front\HomeController@home');
 Route::get('contact','Front\HomeController@contact')->name('contact');
 Route::get('faq','Front\HomeController@faq')->name('faq');
 Route::get('contactus','Front\HomeController@overview')->name('contactus');
 Route::get('vendor','Front\VendorController@index')->name('vendor');

/*******************frontend routes**************************/

Route::group(['namespace'=>'Front'], function () {

    Route::resource('Restaurants', 'VendorController');
    // Route::resource('dishes', 'HomeController');

    // Route::group(['middleware' => ['customer']], function () {
            // Route::resource('dish', 'DishController');
            // Route::resource('cooker', 'CookerController');
            Route::resource('profile', 'CustomerFrontController');
            // Route::resource('addDish/{lang}', 'CartController');
            Route::post('addDish/{lang}', 'CartController@store');
            Route::get('orders', 'CustomerFrontController@orders');
            Route::post('fav/{lang}', 'FavController@fav');
            Route::get('messages', 'CustomerFrontController@messages');
            Route::get('change-password', 'CustomerFrontController@changePassword');
            Route::put('change-password', 'LoginController@changePass')->name('change-password');
            Route::get('reviews', 'CustomerFrontController@reviews');

            // Route::resource('orders', 'OrderController');
            // Route::post('/filter', 'FilterController@filter')->name('filter');

    // });
});

Route::get('/user/reset/{token}', 'API\AuthController@showForgetForm');
Route::get('/user/reset/{token}',function(){


    return view('frontend.auth.passwords.reset');
});
Route::post('/password/reset', 'Front\LoginController@checkreset')->name('reset');




Route::post('/login', 'Front\LoginController@checklogin');
Route::get('/logout/{id}', 'Front\LoginController@logout');
Route::get('/register', 'Front\LoginController@register');
Route::post('/checkregister', 'Front\LoginController@checkregister')->name('checkregister');


// Route::get('FAQS',function(){
//     return view('front.faqs');
// });
// Route::get('Safety&Trust',function(){
//     return view('front.safetyandtrust');
// });


// Route::resource('dish/order', 'Backend\OrderController');










// Route::get('hyperpay/checkout', 'HyperPayPaymentController@checkout')->name('checkout');
// Route::post('hyperpay/payment', 'HyperPayPaymentController@payment')->name('payment');
// Route::post('hyperpay/payment-status', 'HyperPayPaymentController@paymentStatus')->name('payment-status');
// Route::get('hyperpay/finalize', 'HyperPayPaymentController@finalize')->name('finalize');
