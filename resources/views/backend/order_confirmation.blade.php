<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
		<meta name="color-scheme" content="light dark">
		<meta name="supported-color-schemes" content="light dark">

		<style type="text/css">
			:root
			{
				Color-scheme: light dark;
				supported-color-schemes:light dark;
			}
		</style>
		<style type="text/css">
			@media (prefers-color-scheme: dark )
			{
				body
				{
					background-color: white !important;;
				}

				.wrapper
				{
					background-color: white !important;;
				}

				h2 , p
				{
					color: black !important;
				}
			}

			@media (prefers-color-scheme: light )
			{
				body
				{
					background-color: white !important;
				}

				.wrapper
				{
					background-color: white !important;
				}

				h2 , p
				{
					color: black !important;
				}
			}

			@media only screen and (min-width: 480px)
			{
				.webkit
				{
					max-width: 360px;
				}
			}
		</style>
	</head>
	<body style="font-family: Verdana, sans-serif;margin: 0;padding: 0;background-color: white" dir="ltr">
		<center class="wrapper" style="width: 100%;">
			<div class="webkit" dir="ltr">
				<img style="border: 0;" src="https://i.imgur.com/Lev1m7s.jpg" width="100%"/>
				<img style="border: 0;margin-top: 12px;" src="https://i.imgur.com/7L0LkqY.png" width="74"/>
				@php
                    $user=App\Models\User::find($order->user_id);
                    $restaurant=App\Models\User::find($order->cook_id);
                    $cook=App\Models\UserTranslation::where('user_id',$order->cook_id)->where('locale',$lang)->first();//cook work from sat and sat is index (4)date('w')+1
                @endphp
                {{-- @dump( $restaurant, $restaurantlang) --}}
                <h2 style="font-weight: bold;margin: 8px;" >Dear {{  $user->name ?? ''}}</h2>
				<p style="margin: 0 12px;">
					Great choice, your order {{ $order->id }} has been confirmed,<br />
					the chefs are cooking and flights attendant on the move on.<br />
					“Get ready for our planes to land”<br />
				</p>
				<div style="background-color: #1abaff;color: white;margin: 8px 12px 0 12px;">User Information</div>
				<p class="SmallTxt" style="text-align: left;font-size: 0.75em;margin: 0 12px;">
					username: {{ $user->name ?? ''}} <br />
					{{-- usermail: <br /> --}}
					userphone: {{ $user->phone ?? '' }}<br />
					location: {{ $order->address ?? ''}}<br />
				</p>
				<div style="background-color: #1abaff;color: white;margin: 8px 12px 0 12px;">Oder Details</div>
				<p class="SmallTxt" style="text-align: left;font-size: 0.75em;margin: 0 12px;">
					Order Number: {{ $order->id }}<br />
					Order Type: {{ $order->delivery_type == '0' ? 'Pickup' : 'Delivery' }}<br />
					Payment Method:{{ $order->payment_method == '0' ? 'Cash' : 'Credit' }} <br />
					Placed Order:{{ $order->created_at }} <br />
					Accepted time from restaurant: {{$order->status == '2'? $order->updated_at : 'not accepted yet'  }}<br />
				</p>
				<div style="background-color: #1abaff;color: white;margin: 8px 12px 0 12px;">Delivery Information</div>
				<p class="SmallTxt" style="text-align: left;font-size: 0.75em;margin: 0 12px;">
					House No: {{ $customer->address[0]->building ?? ' ' }}
				</p>
				<table style="width: calc(100% - 24px);margin: 8px 12px 38px 12px;border-spacing: 0;table-layout: fixed;text-align: center;align-content: center;">
					<thead style="color: white;background-color: #1abaff;">

						<tr style="direction:{{ $lang =='ar' ? 'rtl' : 'ltr' }} ">
							<th style="font-weight: 400;">Product</th>
							<th style="font-weight: 400;">Price</th>
							<th style="font-weight: 400;">Quantity</th>
							<th style="font-weight: 400;">Total</th>
						</tr>
					</thead>
					<tbody style="width: 100%;">
                        {{-- @dump($order,$order['orderEntry'] ) --}}
                        @foreach ($order['orderEntry'] as $item)
                        <tr style="direction:{{ $lang =='ar' ? 'rtl' : 'ltr' }} ">
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">{{ $order->order_lang == 'en'? $item['name_en'] : $item['name_ar'] }}</td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">{{ $item['price'] / $item['quantity'] }}</td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">{{ $item['quantity'] }}</td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">{{ $item['price'] }}</td>
						</tr>


                        @endforeach

						{{-- <tr style="background-color:gray;">
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*products*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*price for each*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*Quantity*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*Total if more than one*</td>
						</tr> --}}
						{{-- <tr>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*products*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*price for each*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*Quantity*</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;">*Total if more than one*</td>
						</tr> --}}
					</tbody>
					<tfoot>
						<tr><td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;border-top: 1px solid #000000;" colspan="4"></td></tr>
						<tr >
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;text-align: right;" colspan="3">Sub total: </td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="1">{{ $order->sub_total}}</td>
						</tr>
						<tr>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;text-align: right;" colspan="3">Tax: </td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="1">{{  $cook->taxes ?? 'taxes included' }}</td>
						</tr>
						<tr>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;text-align: right;" colspan="3">Delivery charge: </td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="1">{{ $order->delivery_fees  }}</td>
						</tr>
                        <tr>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;text-align: right;" colspan="3">Delivery charge tax: </td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="1">{{ ($order->delivery_taxes)}}</td>
						</tr>
						<tr style="font-size: 0.5em;"><td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;"><br/></td></tr>
						<tr style="background-color: #1abaff;color: white;margin-top: 8px;">
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;text-align: right;" colspan="3"> Grand total: </td>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="1">{{ $order->total_price}} </td>
						</tr>
						<tr style="font-size: 0.5em;"><td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;"><br/></td></tr>
						<tr>
							<td style="font-size: 0.75em;padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;" colspan="4">Estimated delivery time :  {{ $order->estimated_time + $order->delivery_estimated_time  }} min</td>
						</tr>
						<tr style="font-size: 0.5em;"><td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;"><br/></td></tr>
						<tr >
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;font-size: 0.75em;-moz-text-decoration-line: underline;-moz-text-decoration-color: #06005a !important;text-decoration: underline;text-decoration-color: #06005a !important;" colspan="2">{{ $restaurant->name }}</td>
							<td style="padding: 0;text-align: center;align-content: center;word-wrap: break-word;-ms-word-wrap: break-word;font-size: 0.75em;-moz-text-decoration-line: underline;-moz-text-decoration-color: #06005a !important;text-decoration: underline;text-decoration-color: #06005a !important;" colspan="2">{{ $restaurant->phone }}</td>
						</tr>
					</tfoot>
				</table>

				<div style="background-color: #06005a;padding: 12px;">
					<table style="width: 100%;">
						<thead>
							<tr>
								<th><a href="https://facebook.com/profile.php?id=106474654609586&ref=content_filter"><img src="https://i.imgur.com/W5VGJyX.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
								<th><a href="https://www.instagram.com/thejet.sa/"><img src="https://i.imgur.com/sxhrnVJ.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
								<th><a href="https://twitter.com/SaThejet"><img src="https://i.imgur.com/M31yTaD.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
							</tr>
						</thead>
					</table>
					<a href="https://thejet.com.sa" style="color: #1abaff;text-decoration: none;margin-top: 8px;display: block;font-size: 0.75em;">www.thejet.com.sa</a>
				</div>
			</div>
		</center>

	</body>
</html>
