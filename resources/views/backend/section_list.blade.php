@extends('backend.layouts.app')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>

<style>
    .hide{
        display: none;
    }
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.List of sections')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/shabab">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item active">@lang('site.section')</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                <div class="col-sm-12 col">
                    <a href="#Add_section" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" >@lang('site.Add')</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.List of sections')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table" class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.Name')</th>
                                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                        <th>@lang('site.Vendor Name')</th>
                                        @endif
                                        <th> @lang('site.From')</th>
                                        <th> @lang('site.To')</th>
                                        <th>@lang('site.Action')</th>
                                    </tr>
                                </thead>
                                <tbody  id="tablecontents">
                                    @isset ($sections)
                                        @foreach ($sections as $index=>$section)
                                        @if (auth()->guard('admin')->user()->roles[0]->name != 'cook')
                                        <tr   class="row1 " data-id="{{ $section->id }}">
                                        @else
                                        <tr   class="row1 {{  !in_array($section->id,(array)$sections_id) ? 'hide' : ''}}" data-id="{{ $section->id }}">

                                        @endif
                                            <td>
                                                {{ $index }}
                                            </td>

                                            <td>{{ $section->name }}</td>
                                            @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                            <td>{{ $section->users[0]->name ?? '' }}</td>
                                            @endif
                                            <td>{{ date('h:i a',strtotime( $section->available_from)) }}</td>
                                            <td>{{ date('h:i a',strtotime( $section->available_to)) }}</td>

                                            <td >
                                                <div class="actions">
                                                    <a id="{{ $section->id }}" class="link_update bg-success-light mr-2  {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal"  href="#edit_sections_details{{ $section->id }}" >
                                                        <i class="fe fe-pencil"></i> @lang('site.Edit')
                                                    </a>
                                                    <a id="{{ $section->id }}" class="link_delete  {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> @lang('site.Delete')
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                         <!-- Edit Details Modal -->
                                        <div class="modal fade" id="edit_sections_details{{ $section->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">@lang('site.Edit sections')</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group errors" style="display: none" >
                                                            <div class="alert alert-danger">
                                                              <ul class="list">

                                                              </ul>
                                                            </div>
                                                        </div>
                                                        <form id="formUpdate" method="post" action="{{ route('section.update',$section->id) }}" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('put')
                                                            <div class="row form-row">
                                                                @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                                                <div class="col-12 col-sm-12">
                                                                    <label>@lang('site.Vendors') : </label>

                                                                    <select id="Vendors" name="Vendor"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                        <option value="">@lang('site.select vendor')</option>
                                                                        @foreach ($cooks as $cook)
                                                                            <option value="{{ $cook->id }}"  {{ $cook->id == $section->users->first()->id ? 'selected' : '' }}>{{ $cook->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            @else
                                                            <input name="Vendor" value="{{ auth()->guard('admin')->user()->id }}" type="text" hidden>
                                                            @endif
                                                                @foreach (config('translatable.locales') as $locale)
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.name')</label>
                                                                        @isset($section)
                                                                        <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $section->translate($locale)->name }}">
                                                                        @endisset
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>@lang('site.From')</label>
                                                                    {{-- @dump($section['available_from'] ,$section['available_to'] ) --}}
                                                                    <input type="time" name="from" class="form-control"  style="display: inline-block; width:43%" value="{{ date("H:i", strtotime( $section['available_from']))   ?? old('from') }}">
                                                                    <label>@lang('site.To')</label>
                                                                    <input type="time" name="to" class="form-control"  style="display: inline-block; width:43%" value="{{ date("H:i", strtotime( $section['available_to'])) ??old('to') }}">
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary btn-block">@lang('site.Save Changes')</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Edit Details Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_section" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('site.Add section')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                <div class="form-group errors" style="display: none" >
                    <div class="alert alert-danger">
                      <ul class="list">

                      </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('section.store') }}">
                    @csrf
                    <div class="row form-row">
                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                        <div class="col-12 col-sm-12">
                            <label>@lang('site.Vendors') : </label>
                            <select id="Vendors" name="Vendor"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                <option value="">@lang('site.select vendor')</option>

                                @foreach ($cooks as $cook)

                                    <option value="{{ $cook->id }}">{{ $cook->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                    <input name="Vendor" value="{{ auth()->guard('admin')->user()->id }}" type="text" hidden>
                    @endif
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>{{ trans('site.'.$locale.'.name') }}</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>
                        </div>

                        @endforeach
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.From')</label>
                                <input type="time" name="from" class="form-control"  style="display: inline-block; width:43%" value="{{ old('from') }}">
                                <label>@lang('site.To')</label>
                                <input type="time" name="to" class="form-control"  style="display: inline-block; width:43%" value="{{ old('to') }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">@lang('site.Save')</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('site.Delete')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">@lang('site.Delete')</h4>
                        <p class="mb-4">@lang('site.Are you sure want to delete?')</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">@lang('site.Delete') </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('site.Close')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript">
      $(function () {
        $("#table").DataTable( {
        "paging":   false,

        } );

        $( "#tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });
          });

          $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://thejet.com.sa/{{ Config::get("app.locale") }}/shabab/sections/reorder",
                data: {
              order: order,
              "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        }
      });
    </script>
    <script>
       var err= <?php echo json_encode(session()->get('message_update')); ?>;
                var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

                if(err !=null) {
                    let id= <?php echo json_encode(session()->get('section_id')); ?>;
                    let modal_edit='#edit_sections_details'+id;
                    for (x in err) {
                        var err='<li>'+ err[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');
                    $(modal_edit).modal('show');
                }

                else if(errors_add != null)
                {
                    for (x in errors_add) {
                        var err='<li>'+ errors_add[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');

                    $('#Add_section').modal('show');

                }

                $(".modal").on("hidden.bs.modal", () => {
                    $('.errors').css('display','none');
                });

        // $('.link_update').on('click',function(){
        //     var section_id=$(this).attr('id');

        //     $('#formUpdate').attr('action',')

        // })
        $('.link_delete').on('click',function(){
            var section_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/section/'+section_id)

        })
    </script>
@endpush
