@extends('backend.layouts.app')

@push('style')
    <style>
.sub tr th,.sub tr td{
    border: 0 !important
}
.scroll{

    max-height: 400px;
    overflow: scroll;
}

</style>
@endpush
@section('content')


<!-- Page Wrapper -->
<div class="page-wrapper">

    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.Welcome') {{ auth()->guard('admin')->user()->name }}!</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">@lang('site.shabab')</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            @if (auth()->guard('admin')->user()->roles[0]->name != 'cook')
            <div class="col-xl-3 col-sm-6 col-12">
                <a href="{{ url('/shabab/cook') }}">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                    <i class="fe fe-users"></i>
                                </span>
                                <div class="dash-count">
                                    <h3>{{ count($cooks) }}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">@lang('site.Vendors')</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary" style="width: {{ count($cooks) }}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endif
            {{-- <div class="col-xl-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        @isset($customers)
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fa fa-users"></i>
                            </span>
                            <div class="dash-count">

                                    <h3>{{ count($customers) }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted">Customers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width: {{ count($customers) }}%"></div>
                            </div>
                        </div>
                        @endisset
                    </div>
                </div>
            </div> --}}

           @isset($orders)
           <div class="col-xl-3 col-sm-6 col-12">
             <a href="{{ url('/shabab/order') }}">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-warning border-warning">
                                <i class="fe fe-folder"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($orders_accepted) ?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info" >

                            <h6 class="text-muted">@lang('site.Accepted Orders')</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-warning" style="width: {{ count($orders_accepted) ?? 0}}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
             </a>
            </div>

        </div>

        <fieldset>
            <legend>@lang('site.Monthly')</legend>
            <div class="row">


                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #4e9cc9 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $month_total_profit ?? ' ' }} @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #4e9cc9">@lang('site.Total Profit')</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>
                                        @if($month_total_profit && $month_total_profit >0)
                                            <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:{{  $month_total_profit / 1000  ?? 0}}% ;background-color:#E9ECEF"></div>
                                        @endif
                                        <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:0% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @isset($discount)
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #3799d1 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $discount ?? ' ' }}  @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #3799d1">@lang('site.Total Discount')</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #3799d1 !important;  width:{{ $discount / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endisset --}}

                    {{-- <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-danger border-danger" style="color: #3C8DBC !important;border :3px solid #3C8DBC !important">
                                        <i class="fe fe-money" style="color: #3C8DBC" ></i>
                                    </span>
                                    @isset($delivery_fees)
                                    <div class="dash-count">
                                        <h3>{{$delivery_fees ??' ' }}</h3>
                                    </div>
                                    @endisset
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted" style="color: #3C8DBC">Delivery Fees</h6>
                                    <div class="progress progress-sm">
                                        @isset($delivery_fees)
                                            <div class="progress-bar bg-danger" style="background-color : #3C8DBC !important;width: {{ $delivery_fees/100 ?? 0 }}%;"></div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning"  style="color: #1374e2 !important;border :3px solid #1374e2 !important">
                                        <i class="fe fe-money" style="color: #1374e2"></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $month_total_revenue ?? ' ' }}  @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted">@lang('site.Revenue')</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning"></div>
                                        @if ($month_total_revenue && $month_total_revenue >0 )
                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:{{ $month_total_revenue / 1000  ?? ' '}}%"></div>

                                        @endif
                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:0%"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>@lang('site.Annual')</legend>
            <div class="row">

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #4e9cc9 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $annual_total_profit ?? ' ' }}  @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #4e9cc9">@lang('site.Total Profit') </h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>
                                        @if ($annual_total_profit && $annual_total_profit > 0)
                                            <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:{{ $annual_total_profit / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>
                                        @endif
                                        <div class="progress-bar" style="background-color : #4e9cc9 !important;  width:0% ;background-color:#E9ECEF"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @isset($discount)

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning" style="color: #3799d1 !important;border :3px solid #4e9cc9 !important">
                                        <i class="fe fe-money" ></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $discount ?? ' ' }}  @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted"  style="color: #3799d1">@lang('site.Total Discount')</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning "></div>

                                        <div class="progress-bar" style="background-color : #3799d1 !important;  width:{{ $discount / 1000  ?? ' '}}% ;background-color:#E9ECEF"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endisset --}}
                    {{-- <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-danger border-danger" style="color: #3C8DBC !important;border :3px solid #3C8DBC !important">
                                        <i class="fe fe-money" style="color: #3C8DBC" ></i>
                                    </span>
                                    @isset($delivery_fees)
                                    <div class="dash-count">
                                        <h3>{{$delivery_fees ??' ' }}</h3>
                                    </div>
                                    @endisset
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted" style="color: #3C8DBC">Delivery Fees</h6>
                                    <div class="progress progress-sm">
                                        @isset($delivery_fees)
                                            <div class="progress-bar bg-danger" style="background-color : #3C8DBC !important;width: {{ $delivery_fees/100 ?? 0 }}%;"></div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dash-widget-header">
                                    <span class="dash-widget-icon text-warning border-warning"  style="color: #1374e2 !important;border :3px solid #1374e2 !important">
                                        <i class="fe fe-money" style="color: #1374e2"></i>
                                    </span>
                                    <div class="dash-count">
                                        <h3>{{ $annual_total_revenue ?? ' ' }}  @lang('site.'.$currency)</h3>
                                    </div>
                                </div>
                                <div class="dash-widget-info">

                                    <h6 class="text-muted">@lang('site.Revenue')</h6>
                                    <div class="progress progress-sm">

                                        <div class="progress-bar bg-warning"></div>
                                        @if ($annual_total_revenue  && $annual_total_revenue  > 0)
                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:{{ $annual_total_revenue / 1000  ?? ' '}}%"></div>

                                        @endif
                                        <div class="progress-bar" style="background-color : #1374e2 !important;width:0%"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </fieldset>

        @endisset


    </div>
</div>
<!-- /Page Wrapper -->


@endsection
