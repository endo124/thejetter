@extends('backend.layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    ">
@endpush

<style>
    thead input {
        width: 100%;
    }
</style>
@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.List of reports')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item active">@lang('site.reports')</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                {{-- <div class="col-sm-12 col">
                    <a href="#Add_feast" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div> --}}
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.Reports List')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('site.Restaurant')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Customer')</th>
                                        @endif
                                        <th>@lang('site.Date')</th>
                                        <th>@lang('site.Time')</th>
                                        <th>@lang('site.Notes')</th>
                                        <th>@lang('site.Status')</th>
                                        <th>@lang('site.Reason')</th>
                                        <th>@lang('site.Coupon')</th>

                                        <th>@lang('site.Subtotal')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                        <th>@lang('site.Admin Profit')</th>
                                        @endif
                                        <th>@lang('site.Vendor Profit')</th>

                                        {{-- <th>Commission</th> --}}
                                        <th>@lang('site.Vendor Tax')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                        <th>@lang('site.Delivery Fees Tax')</th>

                                            <th>@lang('site.Delivery Fees')</th>
                                        @endif
                                        <th>@lang('site.Discount price')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                        <th>@lang('site.Total price')</th>
                                        @endif

                                        <th>@lang('site.Delivery Type')</th>
                                        {{-- <th>Discount</th> --}}
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Payment Method')</th>
                                            <th>@lang('site.Driver Name')</th>
                                            <th>@lang('site.Admin notes')</th>
                                        @endif
                                        {{-- <th>Total profit</th>
                                        <th>Revenue</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $index=> $order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;

                                        $customer=\App\Models\User::where('id',$order->user_id)->first();
                                        $customer_name=$customer->name;


                                    $fees=0;
                                    $discount=0;
                                    if($order->delivery_fees){
                                        $fees=$order->delivery_fees;
                                    }

                                    if(isset($order->coupon['discount_type'])){
                                        if ($order->coupon['discount_type'] =="%") {
                                            // $discount= ($order->coupon['total']*$order->total_price)/100;
                                        }
                                        else if ($order->coupon['discount_type'] == $currency) {
                                            $discount= $order->coupon['total'];
                                        }
                                    }

                                    $delivery_tax=((float)$order->delivery_fees*15)/100;
                                    @endphp
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $cook_name }}</td>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <td>{{ $customer_name }}</td>
                                        @endif
                                        <td>{{ $order->date }}</td>
                                        <td>{{date('h:i a ', strtotime($order->time) ) }}</td>
                                        <td>{{ $order->note ?? '' }}</td>
                                        <td>
                                            @if ($order['status'] == 1 )
                                                 @lang('site.Pending')
                                                 @elseif($order['status'] == 2)
                                                 @lang('site.Accepted')
                                                 @elseif($order['status'] == 3)
                                                 @lang('site.In progress')
                                                 @elseif($order['status'] == 4)
                                                 @lang('site.Shipped')
                                                 @elseif($order['status'] == 5)
                                                 @lang('site.Delivered')
                                                 @elseif($order['status'] == 6)
                                                 @lang('site.Completed')
                                                 @elseif($order['status'] == 0)
                                                 @lang('site.Rejected')
                                                 @elseif($order['status'] == 9)
                                                 @lang('site.Cancelled')
                                                 @endif
                                        </td>
                                        <td>{{ $order->reason ?? '' }}</td>
                                        @php
                                        if(isset($order->discount_id)){
                                            $coupon=App\Models\Discount::find($order->discount_id);
                                        }else{
                                            $coupon=null;
                                        }
                                        @endphp
                                        <td><a href="{{ route('discount.index') }}"> {{ $coupon !=null  ? $coupon->name : '' }}</a></td>

                                        <td>{{$order->sub_total ?? 0 }} @lang('site.'.$currency )</td>

                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                        <td>{{$order->admin_profit ?? 0 }} @lang('site.'.$currency )</td>
                                        @endif
                                        <td>{{$order->vendor_profit ?? 0 }} @lang('site.'.$currency )</td>

                                        <td>{{ $order->vendor_taxes ?? 0 }} @lang('site.'.$currency )</td>
                                        {{-- <td>{{ $commission ?? '0'}} {{ $currency }}</td> --}}
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                        <td>{{ $delivery_tax  ?? 0 .' ' }} @lang('site.'.$currency )</td>

                                            <td class="fees">{{ $order->delivery_fees ?? '0' }} @lang('site.'.$currency )</td>
                                        @endif
                                        <td>{{ $order->discount_price ?? 0 }} @lang('site.'.$currency )</td>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                        <td class="total">{{ $order->total_price }} @lang('site.'.$currency )</td>
                                        @endif

                                        <td>{{ $order->delivery_type == '1' ? 'delivery' : 'pickup' }}</td>

                                        {{-- @if($order->coupon)
                                          <td class="discount">{{ $order->coupon['discount_type'] =="%" ? ($order->coupon['total']*$order->total_price)/100 : $order->coupon['total'] }} {{ $currency }}</td>
                                        @else
                                        <td class="discount">0 {{ $currency }}</td>

                                        @endif --}}
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <td>{{ $order->payment_method == 1 ? 'Credit' : 'Cash' }}</td>
                                            <td class="">{{ $order->driver_name??' '}} </td>
                                            <td class="" style="max-width:200px">{{ $order->admin_note?? '' }}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->



@endsection


@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
    <script>

        $( document ).ready(function() {
            $('.revenue').html()
        });


        $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder=" '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        });
    });
    var lang='{{ Config::get('app.locale') }}';
    if (lang == 'ar') {
        var url = '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json';
    }
    var table = $('#example').DataTable( {

        language: {
            url: url
        },
        orderCellsTop: true,
        fixedHeader: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

} );

    </script>
@endpush
