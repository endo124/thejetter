@extends('backend.layouts.app')

@push('style')
    <style>


input[type=checkbox] + label {
  display: block;
  margin: 0.2em;
  cursor: pointer;
  padding: 0.2em;
}

input[type=checkbox] {
  display: none;
}

input[type=checkbox] + label:before {
  content: "\2714";
  border: 0.1em solid #000;
  border-radius: 0.2em;
  display: inline-table;
  width: 10px;
  height: 10px;
  padding-left: 0.2em;
  padding-bottom: 0.3em;
  margin-right: 0.2em;
  vertical-align: bottom;
  color: transparent;
  transition: .2s;
}

input[type=checkbox] + label:active:before {
  transform: scale(0);
}

input[type=checkbox]:checked + label:before {
  background-color: gold;
  border-color: gold;
  color: #fff;
}

input[type=checkbox]:disabled + label:before {
  transform: scale(1);
  border-color: #aaa;
}

input[type=checkbox]:checked:disabled + label:before {
  transform: scale(1);
  background-color: #bfb;
  border-color: #bfb;
}
</style>
@endpush
@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of customers</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/shabab">shabab</a></li>
                        <li class="breadcrumb-item active">customer</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('customer')) --}}
                {{-- <div class="col-sm-12 col"> --}}
                    {{-- <a href="#Add_customers" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('customer') ? 'disabled' : '' }}" >Add</a> --}}
                    {{-- <a href="#Add_customers" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   " >Add</a>
                </div> --}}
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">customers List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Addresses</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($customers)
                                        @foreach ($customers as $customer)
                                                <tr>
                                                    <td>
                                                        <h2 class="table-avatar">
                                                            {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="Customer Image"></a> --}}
                                                            {{-- <span id="customer_{{ $customer->id }}" hidden>{{ $customer->id }}</span> --}}
                                                            <input type="checkbox" class="vip_{{ $customer->id }}"  id="customer_{{ $customer->id }}" name="vip{{ $customer->id }}" value="{{ $customer->VIP =='1' ? $customer->VIP :'0'  }}"{{ $customer->VIP =='1' ? 'checked' :' '  }} onchange="vip({{ $customer->id }},this)">
                                                            <label for="customer_{{ $customer->id }}">
                                                            <a href="#" style="color:{{ $customer->VIP =='1' ? 'gold' :' '  }}">{{ $customer->name }}</a>
                                                        </h2>
                                                    </td>
                                                    <td>
                                                        <a class="btn" href="#edit_customer_address_{{ $customer->id }}"  data-toggle="modal" >Addresses</a>
                                                    </td>
                                                    <td>{{ $customer->email }}</td>
                                                    <td>{{ $customer->phone }}</td>
                                                    <td >
                                                        <div class="actions">
                                                            <a id="{{ $customer->id }}" class="link_edit"  class="btn btn-sm btn-success" href="{{ $customer->active == 0 ?  url(Config::get('app.locale').'/shabab/cook/active/'.$customer->id ) : url(Config::get('app.locale').'/shabab/cook/deactive/'.$customer->id) }} " >
                                                                <i class="fe fe-edit"></i> {{ $customer->active == 0 ?'Active' : 'Deactive' }}
                                                            </a>
                                                            <a class="link_update bg-success-light mr-2" data-toggle="modal"  href="#edit_customers_details{{ $customer->id }}" >
                                                                <i class="fe fe-pencil"></i> Edit
                                                            </a>
                                                            <a id="{{$customer->id}}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- Edit Details Modal -->
                                                <div class="modal fade   edit_customer" id="edit_customers_details{{ $customer->id }}" aria-hidden="true" role="dialog">
                                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Edit customer</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                @include('backend.partials.errors')

                                                                <form id="formUpdate" method="post" action="{{ route('customer.update',$customer->id) }}" enctype="multipart/form-data">
                                                                    @csrf
                                                                    @method('put')
                                                                        @csrf

                                                                        {{-- <h1>{{ $customer->id }}</h1> --}}
                                                                        <div class="row form-row">
                                                                            <div class="col-12 col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label>Name</label>
                                                                                    <input type="text" name="name" class="form-control" value=" {{ $customer->name ?? old('name') }}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-12 col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label>phone</label>
                                                                                    <input type="text" name="phone" class="form-control" value="{{ $customer->phone ?? old('phone') }}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-12 col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label>E-mail</label>
                                                                                    <input type="text" name="email" class="form-control" value="{{ $customer->email ??  old('email') }}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-12 col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label>password</label>
                                                                                    <input type="password" name="password" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button type="submit" class="btn btn-otbokhly btn-block mt-4">Update Changes</button>
                                                                    </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /Edit Details Modal -->

                                                <!-- Edit Address Modal -->
                                                <div style="width: " class="modal fade  edit_customer_address" id="edit_customer_address_{{ $customer->id }}" aria-hidden="true" role="dialog">
                                                    <div class="modal-dialog modal-dialog-centered modal-xl" role="document" >
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Customer Addresses</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body table-body-address">
                                                                <div>
                                                                    <div class="row">
                                                                        <div class="col-2">Name</div>
                                                                        <div class="col-6">Address</div>
                                                                        <div class="col-1">Phone</div>
                                                                        <div class="col-1">Building</div>
                                                                        <div class="col-2">Action</div>
                                                                    </div>
                                                                    @foreach ($customer->address as $address)

                                                                    <div class="row">
                                                                        <div class="col-2">{{ $address->name }}</div>
                                                                        <div class="col-6" style="overflow: scroll">{{ $address->address }}</div>
                                                                        <div class="col-1">{{ $address->phone }}</div>
                                                                        <div class="col-1">{{ $address->building }}</div>
                                                                        <div class="col-2">
                                                                            <a class="link_update bg-success-light mr-2"   href="{{ url(Config::get("app.locale").'/shabab/customer/edit/addresses/'.$address->id) }}" >
                                                                                <i class="fe fe-pencil"></i> Edit
                                                                            </a>
                                                                            <a id="{{$customer->id}}" class="link_delete_address" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                                <i class="fe fe-trash"></i> Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- / Edit Address Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->


      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')




    <script>

        $('.link_delete_address').click(function(){
            $('.modal-xl').parent().modal("hide");
        })

        @if (count($errors) > 0)
                $('.edit_customer').modal('show');
        @endif
        $('.link_delete').on('click',function(){
            var customer=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/customer/'+customer)

        })
        $('.link_delete_address').on('click',function(){
            var customer=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/customer/address/delete/'+customer)

        })
        function vip(customer_id,checkbox){
            var value=document.getElementById('customer_'+customer_id);

            if(value.checked == true){

                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/shabab/customer/vip/'+customer_id,

                    success:function(data){
                        value='1';

                    },
                    error:function(data){
                    }
                });


            }else{
                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/shabab/customer/reset/'+customer_id,

                    success:function(data){
                        value='0';

                    },
                    error:function(data){
                    }
                });
            }
        };


    </script>
@endpush
