
@extends('backend.layouts.app')
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css"> --}}

{{-- <link rel="stylesheet" href="https://openlayers.org/en/v4.3.1/css/ol.css" type="text/css"> --}}
    <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
    {{-- <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script> --}}
    {{-- <script src="https://openlayers.org/en/v4.3.1/build/ol.js"></script> --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script> --}}

<style>
    #map {
  height: 200px;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}

#description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
}

#infowindow-content .title {
  font-weight: bold;
}

#infowindow-content {
  display: none;
}

#map #infowindow-content {
  display: inline;
}

.pac-card {
  margin: 10px 10px 0 0;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  background-color: #fff;
  font-family: Roboto;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
}

#target {
  width: 345px;
}
    .dropzone.dz-clickable{
    cursor:pointer;
    text-align: center;
}
.dropzone {
    background-color: #fbfbfb;
    border: 2px dashed rgba(0, 0, 0, 0.1);
    min-height: 150px;
    border: 2px solid rgba(0,0,0,0.3);
    background: white;
    padding: 20px 20px;
}
.dropzone .dz-message{
    margin: 3em 0;

}
.upload-image{
position: relative;
    width: 80px;
    margin-right: 20px;
    display: none
}
.upload-image img {
    border-radius: 4px;
    height: 80px;
    width: 80px;
}





</style>


@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    {{-- <h3 class="page-title">@lang('site.Profile of Cooks')</h3> --}}
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item active">{{ $customer->name }}  address</li>
                    </ul>
                </div>

            </div>

        </div>
        <!-- /Page Header -->

    <!-- Page Content -->
    <div class="content mt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-lg-8 col-xl-9">
                    <form action="{{ route('updare_customer_address',$address->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')



                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12" style="width: 100%">
                                    @php
                                    if(isset($address)){

                                       $lat=$address['lat'];
                                       $lng=$address['lon'];
                                    }
                                    @endphp
                                    <input type="text" class="form-control "  name="lat" id="lat" value="{{ $lat ??' ' }}"   hidden>
                                    <input type="text" class="form-control "  name="lng" id="lng" value="{{ $lng ?? ''}}"  hidden>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label>Name</label>
                                            <input name="name"  type="text" value="{{ $address->name ?? old('name') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label>Address</label>
                                            <input name="address"  type="text" value="{{ $address->address ?? old('address') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label>Phone</label>
                                            <input name="phone"  type="text" value="{{ $address->phone?? old('phone')  }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label>Buliding</label>
                                            <input name="building"  type="text" value="{{ $address->building?? old('building')  }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div id="map"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn"> <i class="fas fa-edit"></i> @lang('site.Save Changes')</button>
                        </div>
                    </form>

                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.css" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ $googleapikey ?? '' }}&libraries=places,drawing&callback=initMap" async></script>

    <script>

function reset(elem){
    $(elem).parent().find('.from').find('.from_input').val('');
    $(elem).parent().find('.to').find('.to_input').val('');

}


function initMap() {


var lat=30.0444;
var lng=31.2357;
var pastlat=document.getElementById('lat').value;
var  pastlng=document.getElementById('lng').value;
var myLatLng = {lat: pastlat, lng: pastlng}; // center US

    if(pastlat){
    lat=pastlat;
    lng=pastlng;

    }
    var position = new google.maps.LatLng(lat,lng);


  const map = new google.maps.Map(document.getElementById("map"), {
    center: position,
    zoom: 4,
  });


  const input = document.getElementById("input-1");
  const autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.setComponentRestrictions({
    country: 'sa',
  });

  // Set the data fields to return when the user selects a place.
//   const infowindow = new google.maps.InfoWindow();
//   const infowindowContent = document.getElementById("infowindow-content");
//   infowindow.setContent(infowindowContent);
//   alert(position)
  const marker = new google.maps.Marker({
    //   pos:position,
    map,
    draggable:true,
    // anchorPoint: new google.maps.Point(0, -29),
  });
//   infowindow.open(map, marker);


  marker.setPosition(position);
    marker.setVisible(true);


  autocomplete.addListener("place_changed", () => {
    // infowindow.close();
    marker.setVisible(false);
    const place = autocomplete.getPlace();
    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17); // Why 17? Because it looks good.
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    let address = "";
    document.getElementById('lat').value=marker.getPosition().lat();
    document.getElementById('lng').value=marker.getPosition().lng();

    if (place.address_components) {
      address = [
        (place.address_components[0] &&
          place.address_components[0].short_name) ||
          "",
        (place.address_components[1] &&
          place.address_components[1].short_name) ||
          "",
        (place.address_components[2] &&
          place.address_components[2].short_name) ||
          "",
      ].join(" ");
    }
    // infowindowContent.children["place-icon"].src = place.icon;
    // infowindowContent.children["place-name"].textContent = place.name;
    // infowindowContent.children["place-address"].textContent = address;
    // infowindow.open(map, marker);
  });
  google.maps.event.addListener(marker, 'dragend', function()
{
    geocodePosition(marker.getPosition());
    document.getElementById('lat').value=marker.getPosition().lat();
   document.getElementById('lng').value=marker.getPosition().lng();
})
function geocodePosition(pos)
{
   geocoder = new google.maps.Geocoder();

   geocoder.geocode
    ({
        latLng: pos
    },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $("#mapSearchInput").val(results[0].formatted_address);
                $("#mapErrorMsg").hide(100);
            }
            else
            {
                $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}
}

    </script>



@endsection




