@extends('backend.layouts.app')


@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.cuisine restaurant list')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item "><a href="{{ url('/shabab/cusine') }}">@lang('site.cusine')</a></li>
                        <li class="breadcrumb-item active">@lang('site.cuisine restaurant')</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                <div class="col-sm-12 col">
                    <a href="#Add_cusine" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >@lang('site.Add')</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.Vendors List') @lang('site.of') {{ $cusine->name }} </h4>
                        <div class="{{ $cusine->id }}" id="cusine_id" style="visibility: hidden"></div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table" class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.Restaurant')</th>
                                        {{-- <th>@lang('site.Action')</th> --}}
                                    </tr>
                                </thead>
                                <tbody  id="tablecontents">
                                    @isset($restaurants)
                                   @foreach ($restaurants as $index=>$restaurant)

                                   <tr  class="row1 " data-id="{{ $restaurant->id }}">
                                       <td>
                                           {{ $index+1 }}
                                       </td>

                                       <td>

                                        {{ $restaurant->name }}

                                       </td>

                                       {{-- <td >
                                           <div class="actions">
                                               <a id="{{ $restaurant->id }}" class="link_update bg-success-light mr-2" data-toggle="modal"  href="#edit_cusines_details{{ $restaurant->id }}" >
                                                   <i class="fe fe-pencil"></i> @lang('site.Edit')
                                               </a>
                                               <a id="{{ $restaurant->id }}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                   <i class="fe fe-trash"></i> @lang('site.Delete')
                                               </a>
                                           </div>
                                       </td> --}}
                                   </tr>

                                   @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->





@endsection


@push('scripts')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#table").DataTable( {
            "paging":   false,
            "searching":   false,

            } );

            $( "#tablecontents" ).sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                sendOrderToServer();
            }
            });

            function sendOrderToServer() {
            var order = [];
            var token = $('meta[name="csrf-token"]').attr('content');
            var cusine_id=$('#cusine_id').attr('class');
            $('tr.row1').each(function(index,element) {
                order.push({
                id: $(this).attr('data-id'),
                position: index+1,
                });
            });

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "https://thejet.com.sa/{{ Config::get("app.locale") }}/shabab/cusines/reorder",
                    data: {
                order: order,
                cusine:cusine_id,
                "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    if (response.status == "success") {
                    console.log(response);
                    } else {
                    console.log(response);
                    }
                }
            });
            }
        });
    </script>
    <script>



    </script>
@endpush
