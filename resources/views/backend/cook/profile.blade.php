
@extends('backend.layouts.app')
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css"> --}}

{{-- <link rel="stylesheet" href="https://openlayers.org/en/v4.3.1/css/ol.css" type="text/css"> --}}
    <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
    {{-- <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script> --}}
    {{-- <script src="https://openlayers.org/en/v4.3.1/build/ol.js"></script> --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    {{-- <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script> --}}

<style>
    #map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}

#description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
}

#infowindow-content .title {
  font-weight: bold;
}

#infowindow-content {
  display: none;
}

#map #infowindow-content {
  display: inline;
}

.pac-card {
  margin: 10px 10px 0 0;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  background-color: #fff;
  font-family: Roboto;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
}

#target {
  width: 345px;
}
    .dropzone.dz-clickable{
    cursor:pointer;
    text-align: center;
}
.dropzone {
    background-color: #fbfbfb;
    border: 2px dashed rgba(0, 0, 0, 0.1);
    min-height: 150px;
    border: 2px solid rgba(0,0,0,0.3);
    background: white;
    padding: 20px 20px;
}
.dropzone .dz-message{
    margin: 3em 0;

}
.upload-image{
position: relative;
    width: 80px;
    margin-right: 20px;
    display: none
}
.upload-image img {
    border-radius: 4px;
    height: 80px;
    width: 80px;
}





</style>


@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.Profile of Cooks')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item active">@lang('site.Cook profile')</li>
                    </ul>
                </div>

            </div>

        </div>
        <!-- /Page Header -->

    <!-- Page Content -->
    <div class="content mt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-lg-8 col-xl-9">

                    <form action="{{ route('cook.update',$cook->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <!-- Basic Information -->
                        <div class="card">
                            <div class="card-body">
                                @include('backend.partials.errors')

                               <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="card-title">@lang('site.Basic Information')</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="change-avatar">
                                                <div class="profile-img mb-3">
                                                    <img class="w-100" id="hidden_img_cover_src" src="{{$cook->profile_cover_image ? asset('/backend/img/cook/cover/'.$cook->profile_cover_image) : '' }}" alt="Vendor Cover" width="150" style="border-radius: 25%;" >
                                                </div>
                                                <div class="upload-img">
                                                    <div class="change-photo-btn">
                                                        <span style="cursor: pointer;"><i class="fa fa-upload"></i> <label for="file">@lang('site.Upload Photo')</label></span>
                                                        <input id="file" type="file" name="profile_cover_image" class="upload" onchange="hidden_img_cover_src.src=window.URL.createObjectURL(this.files[0])" style="display:none">
                                                    </div>
                                                    <small class="form-text text-muted">@lang('site.Allowed JPG, GIF or PNG. Max size of 2MB')</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                               <div class="row">
                                <div class="col-md-12" style="text-align: center;margin-top:50px">
                                    <div class="custom-control custom-switch">
                                        <input name="availability" type="checkbox" class="custom-control-input" id="customSwitches" value="{{ $cook->availability }}" {{ $cook->availability == 1 ? 'checked' : ''}} >
                                        <label class="custom-control-label" for="customSwitches">@lang('site.Status')</label>
                                    </div>
                                </div>
                               </div>
                                <div class="row form-row">


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="change-avatar">
                                                <div class="profile-img mb-3">
                                                    <img id="hidden_img_src" src="{{ asset('/backend/img/cook/'.$cook->images) }}" alt="Cook Image" width="150" style="border-radius: 25%;" >
                                                </div>
                                                <div class="upload-img">
                                                    <div class="change-photo-btn">
                                                        <span style="cursor: pointer;"><i class="fa fa-upload"></i> <label for="fileProfile">@lang('site.Upload Photo')</label></span>
                                                        <input id="fileProfile" type="file" name="profile_image" class="upload" onchange="hidden_img_src.src=window.URL.createObjectURL(this.files[0])" style="display:none">
                                                    </div>
                                                    <small class="form-text text-muted">@lang('site.Allowed JPG, GIF or PNG. Max size of 2MB')</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.Username') </label>
                                            <input type="text" name="name" class="form-control" value="{{ $cook->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.Email') </label>
                                            <input type="email" name="email" class="form-control"value="{{ $cook->email }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.Phone Number')</label>
                                            <input type="text" name="phone" value="{{ $cook->phone }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label>@lang('site.Date of Birth')</label>
                                            <input name="birth_date"  type="date" value="{{$cook->date_of_birth ?? old('birth_date') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="width: 100%">
                                        @lang('site.Address') :
                                        <div class="col-sm-12 mb-3">
                                            <div class="row pb-3 map-location pt-3">
                                                <input type="text" class="form-control " id="coordinatold" value="{{ $cook->address[0]->coordinates ?? ' ' }}" hidden >
                                                {{-- @dump($cook->address) --}}
                                                {{-- <input type="text" class="form-control "  name="coordinates" id="coordinates" value="{{ $cook->address[0]->coordinates ?? ' ' }}" hidden > --}}
                                                <input type="text"  class="form-control "  name="address"  value="{{ $cook->address[0]->address ?? ' ' }}"  >
                                                {{-- <div id="map"  style="width: 800px;height:200px "></div> --}}
                                                {{-- <div id="location" class="marker"><i class="fa fa-arrow"></i></div> --}}
                                                @php
                                                if(isset($cook->address[0])){
                                                   $add= explode(",",$cook->address[0]->coordinates) ;

                                                   $lat=$add[0];
                                                   $lng=$add[1];
                                                }
                                                @endphp
                                                <input type="text" class="form-control "  name="lat" id="lat" value="{{ $lat ??' ' }}" hidden >
                                                <input type="text" class="form-control "  name="lng" id="lng" value="{{ $lng ?? ''}}"  hidden>

                                            </div>

                                        </div>

                                        {{-- <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Title</label>
                                                <input name="title"  type="text" value="{{ $cook->address[0]->title ?? old('title') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>State</label>
                                                <input name="state"  type="text" value="{{ $cook->address[0]->state ?? old('state') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>City</label>
                                                <input name="city"  type="text" value="{{ $cook->address[0]->city ?? old('city')  }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Area</label>
                                                <input name="area"  type="text" value="{{$cook->address[0]->area ?? old('area') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Floor</label>
                                                <input name="floor"  type="text" value="{{$cook->address[0]->floor ?? old('floor') }}" class="form-control">
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div class="col-md-12" style="min-height: 200px">
                                        <input
                                        id="input-1"
                                        class="controls"
                                        type="text"
                                        placeholder="Search Box"
                                      />
                                      <div id="map"></div>
                                      <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon" />
                                        <span id="place-name" class="title"></span><br />
                                        <span id="place-address"></span>
                                      </div>
                                        <input hidden type="button" id="track" value="trackme"/>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-12 mt-3 ">
                                            @lang('site.schedule') :
                                        </div>
                                        <div class="col-md-12 mt-3 ">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    @php
                                                        $from=json_decode($cook->work_from,true);
                                                        $to=json_decode($cook->work_to,true);
                                                        $from2=json_decode($cook->work_from2,true);
                                                        $to2=json_decode($cook->work_to2,true);
                                                        $days=['Sat','Sun','Mon','Tue','Wed','Thu','Fri']
                                                    @endphp
                                                    <thead>
                                                        <tr>
                                                            <th> #</th>
                                                            <th> @lang('site.From')</th>
                                                            <th> @lang('site.To')</th>
                                                            <th> @lang('site.From')</th>
                                                            <th> @lang('site.To')</th>
                                                            <th> @lang('site.Holiday')</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        
                                                        @for ($i = 0; $i < count($days); $i++)
                                                        <tr>
                                                            <td>@lang('site.'.$days[$i] )</td>
                                                            @if (isset($from) && $from[$i] != null  )
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value="{{ date('H:i:s', strtotime($from[$i])) }}"   ></td>
                                                            @else
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value=""   ></td>
                                                            @endif
                                                            @if (isset($to) && $to[$i] != null )
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value="{{date('H:i:s', strtotime($to[$i])) ?? old('work_to') }}"  > </td>
                                                            @else
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value=""  > </td>
                                                            @endif


                                                            @if (isset($from2[$i]) && $from2[$i] != null  )
                                                                <td class="from"> <input class="from_input" name="work_from2[]"  type="time" value="{{ date('H:i:s', strtotime($from2[$i])) }}"   ></td>
                                                            @else
                                                                <td class="from"> <input class="from_input" name="work_from2[]"  type="time" value=""   ></td>
                                                            @endif
                                                            @if (isset($to2[$i]) &&  $to2[$i] != null )
                                                                <td class="to"> <input class="to_input" name="work_to2[]"   type="time" value="{{date('H:i:s', strtotime($to2[$i])) ?? old('work_to') }}"  > </td>
                                                            @else
                                                                <td class="to"> <input class="to_input" name="work_to2[]"   type="time" value=""  > </td>
                                                            @endif

                                                            <td class="text-center" onclick="reset(this)">
                                                                <input  name="holiday[]" type="checkbox" id="holiday{{ $i }}"   value="{{ $i }}"
                                                                @if (isset($from) && $from[$i] == '' && isset($from2 ) && $from2[$i]== '' )
                                                                    checked
                                                                @endif
                                                                >
                                                 
                                                            </td>
                                                        </tr>
                                                        @endfor


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Basic Information -->

                        <!-- About Me -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">@lang('site.About Me')</h4>
                                <div class="form-group mb-0">
                                    <label>@lang('site.info')</label>
                                    <textarea name="info" class="form-control"  rows="5" >{{$cook->info ?? old('info') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /About Me -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">@lang('site.terms and conditions') <p style="font-size: 10px"> *@lang('site.by submit that mean you are  agree with the terms and conditions')</p></h4>
                                <div class="form-group mb-0" style="height: 200px;overflow-y: scroll;">
                                    <p>{{ $terms ?? ' ' }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn"> <i class="fas fa-edit"></i> @lang('site.Save Changes')</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.css" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ $googleapikey ?? '' }}&libraries=places,drawing&callback=initMap" async></script>

    <script>

function reset(elem){
    $(elem).parent().find('.from').find('.from_input').val('');
    $(elem).parent().find('.to').find('.to_input').val('');

}


function initMap() {


var lat=30.0444;
var lng=31.2357;
var pastlat=document.getElementById('lat').value;
var  pastlng=document.getElementById('lng').value;
var myLatLng = {lat: pastlat, lng: pastlng}; // center US

    if(pastlat){
    lat=pastlat;
    lng=pastlng;

    }
    var position = new google.maps.LatLng(lat,lng);


  const map = new google.maps.Map(document.getElementById("map"), {
    center: position,
    zoom: 4,
  });


  const input = document.getElementById("input-1");
  const autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.setComponentRestrictions({
    country: 'sa',
  });

  // Set the data fields to return when the user selects a place.
//   const infowindow = new google.maps.InfoWindow();
//   const infowindowContent = document.getElementById("infowindow-content");
//   infowindow.setContent(infowindowContent);
//   alert(position)
  const marker = new google.maps.Marker({
    //   pos:position,
    map,
    draggable:true,
    // anchorPoint: new google.maps.Point(0, -29),
  });
//   infowindow.open(map, marker);


  marker.setPosition(position);
    marker.setVisible(true);


  autocomplete.addListener("place_changed", () => {
    // infowindow.close();
    marker.setVisible(false);
    const place = autocomplete.getPlace();
    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17); // Why 17? Because it looks good.
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    let address = "";
    document.getElementById('lat').value=marker.getPosition().lat();
    document.getElementById('lng').value=marker.getPosition().lng();

    if (place.address_components) {
      address = [
        (place.address_components[0] &&
          place.address_components[0].short_name) ||
          "",
        (place.address_components[1] &&
          place.address_components[1].short_name) ||
          "",
        (place.address_components[2] &&
          place.address_components[2].short_name) ||
          "",
      ].join(" ");
    }
    // infowindowContent.children["place-icon"].src = place.icon;
    // infowindowContent.children["place-name"].textContent = place.name;
    // infowindowContent.children["place-address"].textContent = address;
    // infowindow.open(map, marker);
  });
  google.maps.event.addListener(marker, 'dragend', function()
{
    geocodePosition(marker.getPosition());
    document.getElementById('lat').value=marker.getPosition().lat();
   document.getElementById('lng').value=marker.getPosition().lng();
})
function geocodePosition(pos)
{
   geocoder = new google.maps.Geocoder();

   geocoder.geocode
    ({
        latLng: pos
    },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                $("#mapSearchInput").val(results[0].formatted_address);
                $("#mapErrorMsg").hide(100);
            }
            else
            {
                $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}
}

    </script>



@endsection




