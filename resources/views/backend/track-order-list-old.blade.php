@extends('backend.layouts.app')

@push('style')
    <style>


        .avatar-sm {
         width: 4.5rem !important;
        }
        .sub tr th,.sub tr td{
            border: 0 !important
        }
        .card{
            min-height: 200px !important;
        }
        .action-btn{
            padding: 5px 8px;
            margin: 0 3px !important;
            border: 1px solid
        }
.hidden-table:hover {
     background-color: white !important;
 }



    .link_reject{
        border: 1px solid #f00;
    padding: 3px 5px ;
    margin: 0 8px 0 0 !important;
    }
    .null,.undefined{
        visibility: hidden;
    }
.vr{
     border-left: 6px solid green;
  height: 100px;
}

 table .head tr td{
    color:#161819;
    /* text-align: left;
    font: normal normal normal 13px/14px Comfortaa; */
 }
.head  tr th{
    text-align:center;
    width:10%
}

.form-control:focus {
    border-color: #3C8DBC !important;
    }

    .animation-menu img{
        padding-right:1px;
        width: 7px;
    height: 4px;
    }
.card-table .card-body .table > thead > tr > th {
    border-top: 0;
    color: #A5A5A5;
    font-weight: 400;
}
    </style>
      {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    "> --}}


      <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-auth.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-database.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-storage.js"></script>
      <script src="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js"></script>
      <script src = "https://cdnjs.cloudflare.com/ajax/libs/node-uuid/1.4.8/uuid.min.js"></script>
      <!------ Include the above in your HEAD tag ---------->

      @endpush

@section('content')




 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of orders</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">order</li>
                    </ul>
                </div>

            </div>

        </div>

        {{-- <div class="row">



            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($orders) }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">orders</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($orders) }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($onduty) ?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">on duty drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($onduty) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($available)?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">available drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($available) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
         --}}

        <div class="row" >

            <div class="col-md-12 d-flex">
                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.orders List')</h4>

                    </div>
                    <div class="card-body m-" >

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @php
                                 $tabs= array('InProgress','Completed','Unassigned','Assigned','Shipped','Delivered','Cancelled');
                           @endphp
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" id="Pending-tab" href="#Pending" role="tab" aria-controls="Pending"
                                aria-selected="true">Pending</a>
                            </li>

                            @foreach ($tabs as $tab)
                            <li class="nav-item">
                                <a class="nav-link" id="{{ $tab }}.'-tab'" data-toggle="tab" href="#{{ $tab }}" role="tab" aria-controls="{{ $tab }}"
                                  aria-selected="false">{{ $tab }}</a>
                              </li>
                            @endforeach
                    </ul>
                    <div class="tab-content" id="myTabContent" style="padding:0">
                        <div class="tab-pane fade show active" id="Pending" role="tabpanel" aria-labelledby="Pending-tab">

                            @if(isset($currentActivities))


                                <div class="table-responsive">
                                    <table class="table  table-center mb-0">

                                        <tbody id="table_body">


                                        </tbody>
                                    </table>

                                </div>
                            @else
                            <div class="row">
                                There is no data
                            </div>
                            @endif

                        </div>
                        @foreach ($tabs as $tab)
                        <div class="tab-pane fade" id="{{ $tab }}" role="tabpanel" aria-labelledby="{{ $tab }}.'-tab'">

                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


  <!-- view order Modal -->

<div class="modal fade" id="edit_orders_details" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('site.Order Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body edit_orders_details" id="view">

            </div>
        </div>
    </div>
</div>


<!-- /Page Wrapper -->


    <!-- Delete Modal -->

    <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->



    <!-- Reject Modal -->
      <div class="modal fade" id="reject_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reject</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Reject</h4>
                        <p class="mb-4">Are you sure want to Reject?</p>

                        <div class="text-center">
                            <form  id="formReject" action="" method="post" style="display: inline">
                                @csrf
                                @method('post')
                                <input type="text" name="id" id="hiddenid" hidden>
                                <input class="form-control mb-2" type="text" name="note" placeholder="please enter the reason">
                                <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Yes </button>
                            </form>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Reject Modal -->


@endsection


@push('scripts')

<script src='/order/js/app.js'></script>




<script>

</script>

<script>

        const addOrder = data =>{
            let order_key=data.key;


                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/shabab/orderDetails/'+order_key,



                    success:function(order){

                        var order =order.order;

                        if(order != null){




                            var neworder= `<tr style="background: #21252917 0% 0% no-repeat padding-box; ">
                                            <td style="    padding-top: 4.3%;width: 10%;background:#FFF">
                                                <div class="row"   >
                                                    <a style="display:block " target="_blank" href="{{url('shabab/order/customerorders/${order.user_id}')}}" class="ml-3">
                                                        <div class="row" style="       position: relative; justify-content: center;">
                                                            <img class="arounded-circle " style="width:40%" src="{{asset('backend/img/profile.png')}}" alt="User Image" >
                                                            <img src="{{asset('backend/img/order/${order.image_name}')}}" style="position:absolute;    bottom: -7px;">
                                                        </div>
                                                    </a>

                                                </div>
                                                <div class="row" style="justify-content: center;">
                                                    <a id="customer_${order.id}"class="col-12" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  target="_blank" href="{{url('shabab/order/customerorders/${order.user_id}')}}">${order.customer.name}</a>
                                                    <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">${order.customer.phone}</span>
                                                </div>
                                            </td>

                                            <td style="background:#FFF">
                                                <table class="table table-light order_details">
                                                    <thead class=" head" style="background: #fff 0% 0% no-repeat padding-box;">
                                                        <tr>
                                                            <th style ="width:1%">#</th>
                                                            <th  style ="width:15%">Restaurant</th>
                                                            <th>Payment Method</th>
                                                            <th>Payment Status</th>
                                                            <th>Order Type</th>
                                                            <th>Quantity</th>
                                                            <th>Total Price</th>
                                                            <th>Coupon</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="    background: #ebebeb52;">
                                                        <tr style="text-align:center" >
                                                            <td>${order.id}</td>
                                                            <td style="width:10%">
                                                                <div class="row" style=" justify-content: center;">
                                                                    <a class="col-12" id="cook_${order.id}" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  >${order.cook_name}</a>
                                                                    <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">${order.cook.phone}</span>
                                                                </div>
                                                            </td>

                                                            <td id="payment_method_${order.id }">

                                                            </td>
                                                            <td id="payment_status_${order.id }">

                                                            </td>

                                                            <td id="order_type_${order.id }">

                                                            </td>
                                                            <td>${order.qty}</td>
                                                            <td>${order.total_price}  ${order.currency}</td>
                                                            <td style="display:${order.display_auth_type}">
                                                                ${order.admin_note}
                                                                <a href="{{ route('discount.index') }}"> ${order.coupon.name}
                                                            </td>

                                                            <td style="padding-right:20px ;position:relative" >
                                                                <div class="row" style="justify-content: flex-end;margin-right: 0px;">
                                                                    <p class="animation-menu" style="  cursor: pointer;  float: right;"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"></p>
                                                                </div>
                                                                <div class="fade-in-left"  style=" z-index:22;   width: 136px;height: 82px;position: absolute;display: none; background: #fff;top: 30px; right: 40px;">
                                                                    <ul style="list-style: none;padding:5px 0;    text-align: left;">

                                                                        <li style="padding: 8px 20px 8px;display:${order.display_auth_type}" >
                                                                            <a  style="color: #4C93FA;" id="${order.id}" class="link_update  mr-2" data-toggle="modal"  href="#edit_order_details${order.id}" ><img src="{{asset('backend/img/order/Icon material-edit.svg')}}" style="padding:0 0px 5px">  Edit</a>
                                                                        </li>

                                                                        <li style="padding: 8px 20px 8px ; text-decoration-line:underline;   border-top: 1px solid #eee;"><a id="${order.id}" onclick="link_show(${order.id})" class="link_show  mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details"><img style="padding:0px 0 4px 0px" src="{{asset('backend/img/order/Icon ionic-ios-eye.svg')}}"> View Details</a></li>
                                                                    <ul>
                                                                </div>
                                                                <button  style="display:${order.display_auth_type}; background: #3F8DFD 0% 0% no-repeat padding-box;box-shadow: 0px 1px 2px #161819;border-radius: 13px;   display: block;color:#fff ;float:right;width: 99px;height: 25px;">@lang('site.unAssigned')</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                    </tbody>
                                                    <tfoot style="background:#ebebeb52">
                                                        <tr>
                                                            <th style="padding:5px" colspan="9">
                                                                <div class="row">
                                                                    <div class="col-8" style ="display: flex; align-items: center;">
                                                                        <div style="position:relative" id="footer_address_${order.id}">


                                                                        </div>
                                                                    </div>
                                                                    <div class="col-2 " id="footer_order_type_${order.id}" style="">


                                                                    </div>
                                                                    <div class=" col-2"  style="padding-right: 30px;  display: flex;align-items: center;flex-direction: row-reverse;">

                                                                        <select id="my-select-${order.id}" class="form-control" onchange="status(${order.id})" id="${order.id}" name="order_status" style="     border-radius: 40px; background:#FFF2BC 0% 0% no-repeat padding-box; height: 32px; width: 83%;float: right;">
                                                                            <option value="1" selected> @lang('site.Pending')</option>
                                                                            <option value="2"> @lang('site.Accepted')</option>
                                                                            <option value="3"> @lang('site.In progress')</option>
                                                                            <option value="4"> @lang('site.Shipped')</option>
                                                                            <option value=""> @lang('site.on the way')</option>
                                                                            <option value="5"> @lang('site.Delivered')</option>
                                                                            <option value="6"> @lang('site.Completed')</option>
                                                                            <option value="0"> @lang('site.Rejected')</option>
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>

                                            </td>

                                            <td id="order_status_${order.id}" status="${order.status}" hidden>
                                                ${order.order_status}
                                            </td>



                                        </tr>`;



                            $('#table_body').prepend(neworder);

                            var payment_method='';
                            if(order.payment_method == 0){
                                payment_method= '@lang('site.Cash')';
                            }else if(order.payment_method == 1){
                                payment_method= '@lang('site.Credit')';

                            }
                            $('#payment_method_'+order.id).append(payment_method);


                            var payment_status='';
                            if(order.payment_status != 1){
                                payment_status= '@lang('site.Pending')';

                            }
                            $('#payment_status_'+order.id).append(payment_status);

                            var delivery_type='';
                            if(order.delivery_type != 1){
                                delivery_type= '@lang('site.Pickup')';

                            }else{
                                delivery_type='@lang('site.Delivery')';

                            }
                            $('#order_type_'+order.id).append(delivery_type);



                            if(order.delivery_type == 0){
                                $('#footer_order_type_'+order.id).css({'display':'flex','align-items': 'center'})
                            }else{
                                var order_type=` <p style="margin:0">
                                    <img src="{{asset('backend/img/order/Icon material-timer.svg')}}">
                                    <span style="color:#A5B3CA">
                                        Within
                                        ${order.delivery_estimated_time}
                                        min
                                    </span>
                                </p>
                                <p style="margin:0">
                                    <img src="{{asset('backend/img/order/Icon ionic-md-time.svg')}}">
                                    <span>${order.date}</span>
                                    <span>${order.time}</span>
                                </p>`;
                                $('#footer_order_type_'+order.id).append(order_type);

                            }

                            if(order.delivery_type == 0){
                                var address_img= `<img style="width:12px;margin-left:25px" src="{{asset('backend/img/order/Icon material-location-on.svg')}}">
                                <span style="position:absolute ;top:2}}px;left:50px;">${order.cook.address[0].address}</span>
                                `;
                            }else{
                                var address_type='';
                                if((order.addresses).includes(order.address) || (order.address_ids).includes(order.customer_address_id)){


                                        address_type=" (@lang('site.old') @lang('site.Address'))";
                                }else{

                                    address_type="(@lang('site.new') @lang('site.Address'))";
                                }
                                var address_img= `
                                <span style="position:absolute ;top:-3}}px;left:50px;">${order.cook.address[0].address}</span>
                                <img style="width:20px;margin-left:25px" src="{{asset('backend/img/order/location.png')}}">
                                <span style="position:absolute ; bottom: -6px;">
                                    ${order.address}
                                    <span style="color:#A5A5A5;padding:5px">
                                        ${address_type}
                                    </span>
                                </span>
                                `;
                            }


                            $('#footer_address_'+order.id).append(address_img)
                            $('.animation-menu').click(function(){
                                $(this).parent().parent().find('.fade-in-left').css('display','block');
                                $(document).mouseup(function(e){

                                    var container =  $(document).find('.fade-in-left');

                                    if(!container.is(e.target) && container.has(e.target).length === 0){
                                        container.hide();
                                    }
                                });
                            });



                        }

                    },
                    error:function(data){
                    }


                });

            }

            let user_key = 'aVuMsFcc72VzayNmpBAeBr851uA2';
            const db = firebase.database();
            function init(){



                db.ref(`/users/${user_key}/tasks`).on('child_added', addOrder);



            }

        document.addEventListener('DOMContentLoaded',init);






        @if (count($errors) > 0)
                $('#Add_orders').modal('show');
        @endif

        $('.link_delete').on('click',function(){
             var order=$(this).attr('id');
            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/order/'+order)

        });



        $('.link_reject').on('click',function(){
             var order=$(this).attr('id');
             $('#hiddenid').val(order);
            $('#formReject').attr('action','/{{ Config::get('app.locale') }}/shabab/order/reject')

        });




        function status(order_id){
                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/shabab/order/'+order_id,

                    success:function(data){
                        //    document.getElementById(order_id).innerHTML= data.status;

                        if (data.status == 2) {
                            $('.alert-success').text('Order Accepted ')
                        }else if (data.status == 3){
                            $('.alert-success').text('Order In Progressive ')
                        }
                        $('.alert-success').css('display','block');


                        $('#my-select-'+order_id).parent().parent().parent().parent().parent().parent().parent().parent().css('display','none')
                        //9 parent
                        // setTimeout(function() {
                        //     $('.alert').fadeOut('fast');
                        //     location.reload();
                        // }, 2000);
                    },
                    error:function(data){
                    }
                });
                // location.reload();

        };


        function link_show(id){
            $('#view').html('');
            $('#body').html('');
            var order_id=id;
            var customer=$('#customer_'+order_id).html()
            var cook=$('#cook_'+order_id).html()
            var order_status=$('#order_status_'+order_id).html()
            $.ajax({url: '/{{ Config::get('app.locale') }}/shabab/order/orderdetails/'+order_id,
                success: function(result){
                var data= result.order.orderEntry;
                var details='<div class="row form-row">'+
                                '<div class="col-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<label>Number : '+result.order.id+'</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<label>Customer Name : '+customer+'</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<label>Cook Name : '+cook+'</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<table class="table table-borderd table-hover">'+
                                            '<thead>'+
                                                '<tr>'+
                                                    '<th>Dish Name</th>'+
                                                    '<th>Dish price</th>'+
                                                    '<th>Amount</th>'+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody id="body">'+

                                            '</tbody>'+
                                        '</table>'+

                                        '<table class="table table-borderd table-hover">'+
                                            '<thead>'+
                                                '<tr>'+
                                                    '<th>Addon Name</th>'+
                                                    '<th>Addon price</th>'+
                                                    '<th>Amount</th>'+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody id="body_addon">'+

                                            '</tbody>'+
                                        '</table>'+


                                    '</div>'+
                                '</div>'+
                                '<div class="col-12 col-sm-12" id="location">'+

                                '</div>'+

                                '<div class="col-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<label>Order Status : '+order_status+'</label>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                            var location='<div class="form-group">'+
                                        '<label>Location : '+result.order.address+'</label>'+
                                    '</div>';
                            var fees='<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                                '<div class="form-group">'+
                                    '<label>Delivery Fees : '+ (Number(result.order.delivery_fees)).toFixed(2)+'</label>'+
                                '</div>'+
                            '</div>';
                            // '<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                            //     '<div class="form-group">'+
                            //         '<label>Delivery Taxes : '+ (Number(result.order.delivery_taxes)).toFixed(2)+'</label>'+
                            //     '</div>'+
                            // '</div>'

                            var discount='<div class="col-12 col-sm-12"  style="    padding: 0;">'+
                                '<div class="form-group">'+
                                    '<label>Discount value : '+ (Number(result.order.discount_price)).toFixed(2)+'</label>'+
                                '</div>'+
                            '</div>';
                            var total='<div class="col-12 col-sm-12" style=" padding: 0;display :{{ auth()->guard('admin')->user()->roles[0]->name !='cook' ? 'block' : 'none' }}">'+
                                '<div class="form-group">'+
                                    '<label >Total Price : <span id="price_total">'+(Number(result.order.total_price)).toFixed(2)+'</sapn></label>'+
                                '</div>'+
                            '</div>';
                            $('#view').append(details);

                            if( result.order.address != null){
                                $('#location').append(location);
                            }

                            var role=$('#role').val();
                            if(role != 'cook' && result.order.delivery_fees > 0){
                                $('#view').append(fees);

                            }
                            if(role != 'cook' && result.order.discount_price > 0){
                                $('#view').append(discount);

                            }
                            $('#view').append(total);


                    for (let i = 0; i < data.length; i++) {


                    for (let j = 0; j < data[i].addons.length; j++) {

                        var body_Addon= '<tr>'+
                                '<td>'+data[i].addons[j].add_{{ Config::get('app.locale') }}+'</td>'+
                                // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                '<td>'+data[i].addons[j].add_price+'</td>'+
                                '<td>'+data[i].addons[j].qty+'</td>'+

                            '</tr>';
                        $('#body_addon').append(body_Addon)  ;

                    }


                    var body= '<tr>'+
                        '<td>'+data[i].name_{{ Config::get('app.locale') }}+'</td>'+
                        // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                        '<td>'+data[i].price+'</td>'+
                        '<td>'+data[i].quantity+'</td>'+

                    '</tr>';
                    $('#body').append(body)  ;


                    }


            }
            });
        }


    </script>
@endpush
