@extends('backend.layouts.app')

@push('style')
    <style>
/* quick reset
* {
  margin: 0;
  padding: 0;
  border: 0;
}

/* relevant styles */
.img__wrap__text {
  position: relative;
  width: 450px;
  /* height:  50px; */
}
.img__wrap{

    cursor: pointer;
    display: block

}

.img__description {
  top: 0;
  bottom: -21px;
  left: 0;
  right: 90px;
  background: rgba(29, 106, 154, 0.72);
  color: #fff;
  margin: 0;


  transition: opacity .2s, visibility .2s;
}
.img__wrap__text{
    right: 95px;
    /* right: 12px; */
    /* top:  18px; */
    /* visibility: hidden;
    opacity: 0; */
    display: none;
}
.hover {
    visibility: visible;
  opacity: 1;
  display: block;

}
/*
.img__wrap:hover .img__wrap__text {
  visibility: visible;
  opacity: 1;
} */


        .avatar-sm {
         width: 4.5rem !important;
        }
        .sub tr th,.sub tr td{
            border: 0 !important
        }
        .card{
            min-height: 200px !important;
        }
        .action-btn{
            padding: 5px 8px;
            margin: 0 3px !important;
            border: 1px solid
        }
.hidden-table:hover {
     background-color: white !important;
 }



    .link_reject{
        border: 1px solid #f00;
    padding: 3px 5px ;
    margin: 0 8px 0 0 !important;
    }
    .null,.undefined{
        visibility: hidden;
    }
.vr{
     border-left: 6px solid green;
  height: 100px;
}

 table .head tr td{
    color:#161819;
    /* text-align: left;
    font: normal normal normal 13px/14px Comfortaa; */
 }
.head  tr th{
    text-align:center;
    width:10%
}

.form-control:focus {
    border-color: #3C8DBC !important;
    }

    .animation-menu img{
        padding-right:1px;
        width: 7px;
    height: 4px;
    }
.card-table .card-body .table > thead > tr > th {
    border-top: 0;
    color: #A5A5A5;
    font-weight: 400;
}
    </style>
      {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    "> --}}

{{--
      <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-auth.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-database.js"></script>
      <script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-storage.js"></script>
      <script src="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js"></script>
      <script src = "https://cdnjs.cloudflare.com/ajax/libs/node-uuid/1.4.8/uuid.min.js"></script> --}}
      <!------ Include the above in your HEAD tag ---------->


<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

      @endpush

@section('content')




 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of orders</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/shabab') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">order</li>
                    </ul>

                </div>

            </div>

        </div>

        {{-- <div class="row">



            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($orders) }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">orders</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($orders) }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($onduty) ?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">on duty drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($onduty) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-primary border-primary">
                            </span>
                            <div class="dash-count">
                                <h3>{{ count($available)?? ' ' }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <h6 class="text-muted">available drivers</h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary" style="width: {{ count($available) ?? ' ' }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
         --}}

        <div class="row" >

            <div class="col-md-12 d-flex">
                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.orders List')</h4>

                    </div>
                    <div class="card-body " >

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @php
                                 $tabs= array('Accepted','InProgress','Shipped','Unassigned','Assigned','On_The_Way','Delivered','Cancelled','Rejected');
                           @endphp
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" id="Pending-tab" href="#Pending" role="tab" aria-controls="Pending"
                                aria-selected="true">Pending</a>
                            </li>

                            @foreach ($tabs as $tab)
                            <li class="nav-item">
                                <a class="nav-link" id="{{ $tab }}.'-tab'" data-toggle="tab" href="#{{ $tab }}" role="tab" aria-controls="{{ $tab }}"
                                  aria-selected="false">{{ $tab == 'On_The_Way' ? 'On the way' : $tab }}</a>
                              </li>
                            @endforeach
                    </ul>
                    <div class="tab-content" id="myTabContent" style="padding:0">
                        <div class="tab-pane fade show active" id="Pending" role="tabpanel" aria-labelledby="Pending-tab">

                            @if(isset($currentActivities))
                                <h1 name="{{ auth('admin')->user()->name }}" id="auth" hidden></h1>

                                <div class="table-responsive">
                                    <table class="table  table-center mb-0">

                                        <tbody id="table_body_Pending">
                                        @foreach ($currentActivities as $index=>$order)
                                            @php
                                                $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                                $cook_name=$cook->name;
                                                $customer=\App\Models\User::find($order->user_id);
                                                $customer_orders=\App\Models\Order::where('user_id',$customer->id)->get();


                                                if(count($customer_orders)<3 && $customer->VIP!= 1  ){
                                                    $image_name='Group 126.png';
                                                }

                                                if( count($customer_orders)>=3  ){
                                                    $image_name='Group 127.png';
                                                }

                                                if($customer->VIP == 1){

                                                    $image_name='Group 125.png';

                                                }
                                                $orderEntry=$order->orderEntry;
                                                $qty=0;
                                                foreach ($orderEntry as $item){
                                                    $qty+=$item['quantity'];
                                                }
                                                if(isset($order->discount_id)){
                                                 $coupon=App\Models\Discount::find($order->discount_id);
                                             }else{
                                                 $coupon=null;
                                             }

                                            @endphp
                                            <tr style="background: #21252917 0% 0% no-repeat padding-box; ">
                                                <td style="    padding-top: 4.3%;width: 10%;background:#FFF">
                                                    <div class="row"   >
                                                        <a style="display:block " target="_blank" href="{{url('shabab/order/customerorders/'.$order->user_id)}}" class="ml-3">
                                                            <div class="row" style="       position: relative; justify-content: center;">
                                                                <img class="arounded-circle " style="width:40%" src="{{asset('backend/img/profile.png')}}" alt="User Image" >
                                                                <img src="{{asset('backend/img/order/'.$image_name)}}" style="position:absolute;    bottom: -7px;">
                                                            </div>
                                                        </a>

                                                    </div>
                                                    <div class="row" style="justify-content: center;">
                                                        <a id="customer_{{ $order->id }}"class="col-12" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  target="_blank" href="{{url('shabab/order/customerorders/'.$order->user_id)}}">{{ $customer->name }}</a>
                                                        <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">{{ $customer->phone}}</span>
                                                    </div>
                                                </td>

                                                <td style="background:#FFF">
                                                    <table class="table table-light order_details">
                                                        <thead class=" head" style="background: #fff 0% 0% no-repeat padding-box;">
                                                            <tr>
                                                                <th style ="width:1%">#</th>
                                                                <th  style ="width:15%">Restaurant</th>
                                                                @if (in_array('Pending',$tabs))
                                                                    <th>Driver</th>
                                                                @endif
                                                                <th>Payment Method</th>
                                                                <th>Payment Status</th>
                                                                <th>Order Type</th>
                                                                <th>Quantity</th>
                                                                <th>Total Price</th>
                                                                <th>Coupon</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="    background: #ebebeb52;">
                                                            <tr style="text-align:center" >
                                                                <td>{{  $order->id }}</td>
                                                                <td style="width:10%">
                                                                    <div class="row" style=" justify-content: center;">
                                                                        <a class="col-12" id="cook_{{ $order->id }}" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  >{{ $cook_name }}</a>
                                                                        <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">{{ $cook->phone}}</span>
                                                                    </div>
                                                                </td>
                                                                @if (in_array('Pending',$tabs))
                                                                    <td>{{ $order->driver_name ?? '' }}</td>
                                                                @endif
                                                                <td id="payment_method_{{ $order->id }}">
                                                                    @if ($order['payment_method'] == 0 )
                                                                    @lang('site.Cash')
                                                                    @elseif($order['payment_method'] == 1)
                                                                    @lang('site.Credit')
                                                                    @endif
                                                                </td>
                                                                <td id="payment_status_{{ $order->id }}">
                                                                    @if ($order['payment_status'] == 0 )
                                                                    @lang('site.Unpaid')
                                                                    @elseif($order['payment_status'] == 1)
                                                                    @lang('site.Paid')
                                                                    @endif
                                                                </td>

                                                                <td id="order_type_{{ $order->id }}">
                                                                    @if ($order['delivery_type'] == 0 )
                                                                    @lang('site.Pickup')
                                                                    @elseif($order['delivery_type'] == 1)
                                                                    @lang('site.Delivery')
                                                                    @endif
                                                                </td>
                                                                <td>{{ $qty }}</td>
                                                                <td>{{ $order['total_price'] }} {{ $currency }}</td>
                                                                @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                                    <td>
                                                                        {{-- {{ $order->admin_note ?? ' ' }} --}}
                                                                        <a href="{{ route('discount.index') }}"> {{ $coupon !=null  ? $coupon->name : '' }}
                                                                    </td>
                                                                @endif

                                                                <td style="padding-right:20px ;position:relative" >
                                                                    <div class="row" style="justify-content: flex-end;margin-right: 0px;">
                                                                        <p class="animation-menu" style="  cursor: pointer;  float: right;"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"></p>
                                                                    </div>
                                                                    <div class="fade-in-left"  style=" z-index:22;   width: 136px;height: 82px;position: absolute;display: none; background: #fff;top: 30px; right: 40px;">
                                                                        <ul style="list-style: none;padding:5px 0;    text-align: left;">

                                                                            @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                                            <li style="padding: 8px 20px 8px">
                                                                                <a  style="color: #4C93FA;" id="{{ $order->id }}" class="link_update  mr-2" data-toggle="modal"  href="#edit_order_details{{ $order->id }}" ><img src="{{asset('backend/img/order/Icon material-edit.svg')}}" style="padding:0 0px 5px">  Edit</a>
                                                                            </li>
                                                                            @endif

                                                                            <li style="padding: 8px 20px 8px ; text-decoration-line:underline;   border-top: 1px solid #eee;"><a id="{{ $order->id }}" onclick="link_show({{ $order->id }})" class="link_show  mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details"><img style="padding:0px 0 4px 0px" src="{{asset('backend/img/order/Icon ionic-ios-eye.svg')}}"> View Details</a></li>
                                                                        <ul>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12" >
                                                                            <button  id="{{  $order->id }}" onclick="assign(this)" class="assign" style="z-index:9999999999;margin-bottom:5px;display:{{auth()->guard('admin')->user()->roles[0]->name == 'admin' ? 'block' : 'hidden'}}; background: #3F8DFD 0% 0% no-repeat padding-box;box-shadow: 0px 1px 2px #161819;border-radius: 13px;   display: block;color:#fff ;float:right;width: 99px;height: 25px;">

                                                                                    {{ $order->assign ?? __('site.unAssigned')  }}
                                                                            </button>
                                                                        </div>


                                                                        @if ($order->admin_note)
                                                                        <div class="col-12">
                                                                            <span   style="    float: right; margin-right: 13px;" class="img__wrap img__img d-block">Admin Note</span>

                                                                            <div class="img__wrap__text">
                                                                                <p class="img__description">{{ $order->admin_note }}</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    @endif


                                                                </td>
                                                            </tr>
                                                            <tr>
                                                        </tbody>
                                                        <tfoot style="background:#ebebeb52">
                                                            <tr>
                                                                <th style="padding:5px" colspan="9">
                                                                    <div class="row">
                                                                        <div class="col-8" style ="display: flex; align-items: center;">
                                                                            <div style="position:relative">
                                                                                @if ($order['delivery_type'] == 0 )
                                                                                    <img style="width:12px;margin-left:25px" src="{{asset('backend/img/order/Icon material-location-on.svg')}}">
                                                                                @endif

                                                                                <span style="position:absolute ;top:{{$order['delivery_type'] == 0 ? '2' : '-3'}}px;left:50px;">{{$cook->address[0]->address}}</span>

                                                                                @if ($order['delivery_type'] == 1 )
                                                                                    <img style="width:20px;margin-left:25px" src="{{asset('backend/img/order/location.png')}}">
                                                                                    <span style="position:absolute ; bottom: -6px;">
                                                                                        {{$order->address}}
                                                                                        <span style="color:#A5A5A5;padding:5px">
                                                                                            @php
                                                                                                $tmp1 = array_count_values((array)$address_ids );
                                                                                                $cut1=$tmp1[$order['customer_address_id']];

                                                                                                $tmp2 = array_count_values((array)$addresses );
                                                                                                $cut2=$tmp2[$order['address']];
                                                                                            @endphp
                                                                                            @if ($cut1 > 1 || $cut2 > 1)
                                                                                                (@lang('site.old') @lang('site.Address'))
                                                                                                @else
                                                                                                (@lang('site.new') @lang('site.Address'))
                                                                                            @endif
                                                                                        </span>
                                                                                    </span>

                                                                                @endif

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-2" style="{{$order['delivery_type'] ==0 ? 'display: flex;align-items: center;' : '' }} ">
                                                                            @if ($order['delivery_type'] == 1 )

                                                                                <p style="margin:0">
                                                                                    <img src="{{asset('backend/img/order/Icon material-timer.svg')}}">
                                                                                    <span style="color:#A5B3CA">
                                                                                        Within
                                                                                        @if (isset($order->delivery_estimated_time))
                                                                                                {{$order->delivery_estimated_time   }}
                                                                                            @else
                                                                                                45
                                                                                        @endif
                                                                                        min
                                                                                    </span>
                                                                                </p>
                                                                            @endif
                                                                            <p style="margin:0">
                                                                            <img src="{{asset('backend/img/order/Icon ionic-md-time.svg')}}">
                                                                            <span>{{$order->date  }}</span>
                                                                            <span>{{date('h:i A', strtotime($order->time ))  }}</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class=" col-2"  style="      padding-right: 30px;  display: flex;align-items: center;flex-direction: row-reverse;">
                                                                            <input type="text" hidden id="pending_order" value="1">
                                                                            <select id="my-select-{{$order->id}}" class="form-control" onchange="status({{ $order->id }} )"  name="order_status" style="     border-radius: 40px; background:#FFF2BC 0% 0% no-repeat padding-box; height: 32px; width: 83%;float: right;">
                                                                                <option value="1" {{ $order->status == 1 ? 'selected' :'' }}> @lang('site.Pending')</option>
                                                                                <option value="2" {{ $order->status == 2 ?'selected' :'' }}>  @lang('site.Accepted')</option>
                                                                                <option value="3" {{ $order->status == 3 ? 'selected' :'' }}> @lang('site.In progress')</option>
                                                                                <option value="4" {{ $order->status == 4 ? 'selected' :'' }}> @lang('site.Shipped')</option>
                                                                                <option value="8" {{ $order->status == 8 ? 'selected' :'' }}> @lang('site.un assigned')</option>
                                                                                <option value="11"  {{ $order->status == 11 ? 'selected' :'' }}> @lang('site.Assigned')</option>

                                                                                <option value="6" {{ $order->status == 6 ? 'selected' :'' }}> @lang('site.on the way')</option>
                                                                                <option value="5" {{ $order->status == 5 ? 'selected' :'' }}> @lang('site.Delivered')</option>
                                                                                {{-- <option value="6"> @lang('site.Completed')</option> --}}
                                                                                <option value="0"> @lang('site.Rejected')</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </td>


                                                <td id="order_status_{{ $order->id }}" status="{{$order['status']}}" hidden>
                                                    @if ($order['status'] == 1 )
                                                    @lang('site.Pending')
                                                    @elseif($order['status'] == 2)
                                                    @lang('site.Accepted')
                                                    @elseif($order['status'] == 3)
                                                    @lang('site.In progress')
                                                    @elseif($order['status'] == 4)
                                                    @lang('site.Shipped')
                                                    @elseif($order['status'] == 5)
                                                    @lang('site.Delivered')
                                                    {{-- @elseif($order['status'] == 6)
                                                    @lang('site.Completed') --}}
                                                    @elseif($order['status'] == 6)
                                                    @lang('site.on the way')
                                                    {{-- @elseif($order['status'] == 7)
                                                    @lang('site.Delivered') --}}
                                                    @elseif($order['status'] == 8)
                                                    @lang('site.un assigned')
                                                    @elseif($order['status'] == 0)
                                                    @lang('site.Rejected')
                                                    @elseif($order['status'] == 9)
                                                    @lang('site.Cancelled')
                                                    @endif



                                                </td>
                                            </tr>



                                             <!-- Edit Details Modal -->
                                            <div class="modal fade " id="edit_order_details{{ $order->id }}" aria-hidden="true" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered" role="document" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Order</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form id="formUpdate" method="post" action="{{ route('order.updatedata',$order->id) }}" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('put')
                                                                <div class="row form-row">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Total Price</label>
                                                                            <input type="text" name="total_price" class="form-control" value="{{$order->total_price }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Admin Note</label>
                                                                            <input type="text" name="admin_note" class="form-control" value="{{$order->admin_note }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Edit Details Modal -->

                                        @endforeach
                                        <input type="hidden" id="ids" name="ids[]" value="{{ json_encode($current_ids) }}">

                                        </tbody>
                                    </table>

                                </div>

                            @endif

                        </div>
                        @foreach ($tabs as $tab)
                        <div class="tab-pane fade" id="{{ $tab }}" role="tabpanel" aria-labelledby="{{ $tab }}-tab">
                            @if(isset(${$tab.'_orders'}))
                                <h1 name="{{ auth('admin')->user()->name }}" id="auth" hidden></h1>
                                <div class="table-responsive">
                                    <table class="table  table-center mb-0">

                                        <tbody id="table_body_{{ $tab =='On_The_Way'?'On_The_Way': $tab }}">
                                        @foreach (${$tab.'_orders'} as $index=>$order)
                                            @php
                                                $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                                $cook_name=$cook->name;
                                                $customer=\App\Models\User::find($order->user_id);
                                                $customer_orders=\App\Models\Order::where('user_id',$customer->id)->get();


                                                if(count($customer_orders)<3 && $customer->VIP!= 1  ){
                                                    $image_name='Group 126.png';
                                                }

                                                if( count($customer_orders)>=3  ){
                                                    $image_name='Group 127.png';
                                                }

                                                if($customer->VIP == 1){

                                                    $image_name='Group 125.png';

                                                }
                                                $orderEntry=$order->orderEntry;
                                                $qty=0;
                                                foreach ($orderEntry as $item){
                                                    $qty+=$item['quantity'];
                                                }
                                                if(isset($order->discount_id)){
                                                 $coupon=App\Models\Discount::find($order->discount_id);
                                             }else{
                                                 $coupon=null;
                                             }

                                            @endphp
                                            <tr id="TR_{{ $order->id }}" style="background: #21252917 0% 0% no-repeat padding-box; ">
                                                <td style="    padding-top: 4.3%;width: 10%;background:#FFF">
                                                    <div class="row"   >
                                                        <a style="display:block " target="_blank" href="{{url('shabab/order/customerorders/'.$order->user_id)}}" class="ml-3">
                                                            <div class="row" style="       position: relative; justify-content: center;">
                                                                <img class="arounded-circle " style="width:40%" src="{{asset('backend/img/profile.png')}}" alt="User Image" >
                                                                <img src="{{asset('backend/img/order/'.$image_name)}}" style="position:absolute;    bottom: -7px;">
                                                            </div>
                                                        </a>

                                                    </div>
                                                    <div class="row" style="justify-content: center;">
                                                        <a id="customer_{{ $order->id }}"class="col-12" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  target="_blank" href="{{url('shabab/order/customerorders/'.$order->user_id)}}">{{ $customer->name }}</a>
                                                        <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">{{ $customer->phone}}</span>
                                                    </div>
                                                </td>

                                                <td style="background:#FFF">
                                                    <table class="table table-light order_details">
                                                        <thead class=" head" style="background: #fff 0% 0% no-repeat padding-box;">
                                                            <tr>
                                                                <th style ="width:1%">#</th>
                                                                <th  style ="width:15%">Restaurant</th>
                                                                @if (in_array('Pending',$tabs))
                                                                    <th>Driver</th>
                                                                @endif
                                                                <th>Payment Method</th>
                                                                <th>Payment Status</th>
                                                                <th>Order Type</th>
                                                                <th>Quantity</th>
                                                                <th>Total Price</th>
                                                                <th>Coupon</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="    background: #ebebeb52;">
                                                            <tr style="text-align:center" >
                                                                <td>{{  $order->id }}</td>
                                                                <td style="width:10%">
                                                                    <div class="row" style=" justify-content: center;">
                                                                        <a class="col-12" id="cook_{{ $order->id }}" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  >{{ $cook_name }}</a>
                                                                        <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">{{ $cook->phone}}</span>
                                                                    </div>
                                                                </td>
                                                                @if (in_array('Pending',$tabs))
                                                                    <td>{{ $order->driver_name ?? '' }}</td>
                                                                @endif
                                                                <td id="payment_method_{{ $order->id }}">
                                                                    @if ($order['payment_method'] == 0 )
                                                                    @lang('site.Cash')
                                                                    @elseif($order['payment_method'] == 1)
                                                                    @lang('site.Credit')
                                                                    @endif
                                                                </td>
                                                                <td id="payment_status_{{ $order->id }}">
                                                                    @if ($order['payment_status'] == 0 )
                                                                    @lang('site.Unpaid')
                                                                    @elseif($order['payment_status'] == 1)
                                                                    @lang('site.Paid')
                                                                    @endif
                                                                </td>

                                                                <td id="order_type_{{ $order->id }}">
                                                                    @if ($order['delivery_type'] == 0 )
                                                                    @lang('site.Pickup')
                                                                    @elseif($order['delivery_type'] == 1)
                                                                    @lang('site.Delivery')
                                                                    @endif
                                                                </td>
                                                                <td>{{ $qty }}</td>
                                                                <td>{{ $order['total_price'] }} {{ $currency }}</td>
                                                                @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                                    <td>
                                                                        {{-- {{ $order->admin_note ?? ' ' }} --}}
                                                                        <a href="{{ route('discount.index') }}"> {{ $coupon !=null  ? $coupon->name : '' }}
                                                                    </td>
                                                                @endif

                                                                <td style="padding-right:20px ;position:relative" >
                                                                    <div class="row" style="justify-content: flex-end;margin-right: 0px;">
                                                                        <p class="animation-menu" style="  cursor: pointer;  float: right;"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"></p>
                                                                    </div>
                                                                    <div class="fade-in-left"  style=" z-index:22;   width: 136px;height: 82px;position: absolute;display: none; background: #fff;top: 30px; right: 40px;">
                                                                        <ul style="list-style: none;padding:5px 0;    text-align: left;">

                                                                            @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                                            <li style="padding: 8px 20px 8px">
                                                                                <a  style="color: #4C93FA;" id="{{ $order->id }}" class="link_update  mr-2" data-toggle="modal"  href="#edit_order_details{{ $order->id }}" ><img src="{{asset('backend/img/order/Icon material-edit.svg')}}" style="padding:0 0px 5px">  Edit</a>
                                                                            </li>
                                                                            @endif

                                                                            <li style="padding: 8px 20px 8px ; text-decoration-line:underline;   border-top: 1px solid #eee;"><a id="{{ $order->id }}" onclick="link_show({{ $order->id }})" class="link_show  mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details"><img style="padding:0px 0 4px 0px" src="{{asset('backend/img/order/Icon ionic-ios-eye.svg')}}"> View Details</a></li>
                                                                        <ul>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12" >
                                                                            <button  id="{{  $order->id }}" onclick="assign(this)" class="assign" style="z-index:9999999999;margin-bottom:5px;display:{{auth()->guard('admin')->user()->roles[0]->name == 'admin' ? 'block' : 'hidden'}}; background: #3F8DFD 0% 0% no-repeat padding-box;box-shadow: 0px 1px 2px #161819;border-radius: 13px;   display: block;color:#fff ;float:right;width: 99px;height: 25px;">

                                                                                    {{ $order->assign ?? __('site.unAssigned')  }}
                                                                            </button>
                                                                        </div>


                                                                        @if ($order->admin_note)
                                                                        <div class="col-12">
                                                                            <span   style="    float: right; margin-right: 13px;" class="img__wrap img__img d-block">Admin Note</span>

                                                                            <div class="img__wrap__text">
                                                                                <p class="img__description">{{ $order->admin_note }}</p>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    @endif


                                                                </td>
                                                            </tr>
                                                            <tr>
                                                        </tbody>
                                                        <tfoot style="background:#ebebeb52">
                                                            <tr>
                                                                <th style="padding:5px" colspan="9">
                                                                    <div class="row">
                                                                        <div class="col-8" style ="display: flex; align-items: center;">
                                                                            <div style="position:relative">
                                                                                @if ($order['delivery_type'] == 0 )
                                                                                    <img style="width:12px;margin-left:25px" src="{{asset('backend/img/order/Icon material-location-on.svg')}}">
                                                                                @endif

                                                                                <span style="position:absolute ;top:{{$order['delivery_type'] == 0 ? '2' : '-3'}}px;left:50px;">{{$cook->address[0]->address}}</span>

                                                                                @if ($order['delivery_type'] == 1 )
                                                                                    <img style="width:20px;margin-left:25px" src="{{asset('backend/img/order/location.png')}}">
                                                                                    <span style="position:absolute ; bottom: -6px;">
                                                                                        {{$order->address}}
                                                                                        <span style="color:#A5A5A5;padding:5px">
                                                                                            @if (in_array($order['address'], $addresses) || in_array($order['customer_address_id'],$address_ids))
                                                                                                (@lang('site.old') @lang('site.Address'))
                                                                                                @else
                                                                                                (@lang('site.new') @lang('site.Address'))
                                                                                            @endif
                                                                                        </span>
                                                                                    </span>

                                                                                @endif

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-2" style="{{$order['delivery_type'] ==0 ? 'display: flex;align-items: center;' : '' }} ">
                                                                            @if ($order['delivery_type'] == 1 )

                                                                                <p style="margin:0">
                                                                                    <img src="{{asset('backend/img/order/Icon material-timer.svg')}}">
                                                                                    <span style="color:#A5B3CA">
                                                                                        Within
                                                                                        @if (isset($order->delivery_estimated_time))
                                                                                                {{$order->delivery_estimated_time   }}
                                                                                            @else
                                                                                                45
                                                                                        @endif
                                                                                        min
                                                                                    </span>
                                                                                </p>
                                                                            @endif
                                                                            <p style="margin:0">
                                                                            <img src="{{asset('backend/img/order/Icon ionic-md-time.svg')}}">
                                                                            <span>{{$order->date  }}</span>
                                                                            <span>{{date('h:i A', strtotime($order->time ))  }}</span>
                                                                            </p>
                                                                        </div>
                                                                        <div class=" col-2"  style="      padding-right: 30px;  display: flex;align-items: center;flex-direction: row-reverse;">

                                                                            <select id="my-select-{{$order->id}}"  {{ $order->status == 0  || $order->status == 11  || $order->status == 9 ? 'disabled' :'' }} class="form-control" onchange="status({{ $order->id }} )"name="order_status" style="     border-radius: 40px; background:#FFF2BC 0% 0% no-repeat padding-box; height: 32px; width: 83%;float: right;">
                                                                                <option value="1" {{ $order->status == 1 ? 'selected' :'' }}> @lang('site.Pending')</option>
                                                                                <option value="2" {{ $order->status == 2 ?'selected' :'' }}>  @lang('site.Accepted')</option>
                                                                                <option value="3" {{ $order->status == 3 ? 'selected' :'' }}> @lang('site.In progress')</option>
                                                                                <option value="4" {{ $order->status == 4 ? 'selected' :'' }}> @lang('site.Shipped')</option>
                                                                                <option value="8" {{ $order->status == 8 ? 'selected' :'' }}> @lang('site.un assigned')</option>
                                                                                <option value="11"  {{ $order->status == 11 ? 'selected' :'' }}> @lang('site.Assigned')</option>

                                                                                <option value="6" {{ $order->status == 6 ? 'selected' :'' }}> @lang('site.on the way')</option>
                                                                                <option value="5" {{ $order->status == 5 ? 'selected' :'' }}> @lang('site.Delivered')</option>
                                                                                {{-- <option value="6"> @lang('site.Completed')</option> --}}
                                                                                @if( $order->status == 0)
                                                                                <option value="0" {{ $order->status == 0 ? 'selected' :'' }}> @lang('site.Rejected')</option>
                                                                                @endif
                                                                                <option value="9" {{ $order->status == 9 ? 'selected' :'' }}> @lang('site.Cancelled')</option>



                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </td>


                                                <td id="order_status_{{ $order->id }}" status="{{$order['status']}}" hidden>
                                                    @if ($order['status'] == 1 )
                                                    @lang('site.Pending')
                                                    @elseif($order['status'] == 2)
                                                    @lang('site.Accepted')
                                                    @elseif($order['status'] == 3)
                                                    @lang('site.In progress')
                                                    @elseif($order['status'] == 4)
                                                    @lang('site.Shipped')

                                                    @elseif($order['status'] == 8)
                                                    @lang('site.un assigned')

                                                    @elseif($order['status'] == 6)
                                                    @lang('site.on the way')
                                                    {{-- @elseif($order['status'] == 7)
                                                    @lang('site.Delivered') --}}

                                                    @elseif($order['status'] == 5)
                                                    @lang('site.Delivered')
                                                    {{-- @elseif($order['status'] == 6)
                                                    @lang('site.Completed') --}}
                                                    @elseif($order['status'] == 0)
                                                    @lang('site.Rejected')
                                                    @elseif($order['status'] == 9)
                                                    @lang('site.Cancelled')
                                                    @endif

                                                </td>
                                            </tr>



                                             <!-- Edit Details Modal -->
                                            <div class="modal fade " id="edit_order_details{{ $order->id }}" aria-hidden="true" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered" role="document" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Order</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form id="formUpdate" method="post" action="{{ route('order.updatedata',$order->id) }}" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('put')
                                                                <div class="row form-row">
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Total Price</label>
                                                                            <input type="text" name="total_price" class="form-control" value="{{$order->total_price }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Admin Note</label>
                                                                            <input type="text" name="admin_note" class="form-control" value="{{$order->admin_note }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Edit Details Modal -->

                                        @endforeach
                                        {{-- <input type="hidden" id="ids" name="ids[]" value="{{ json_encode($current_ids) }}"> --}}

                                        </tbody>
                                    </table>

                                </div>

                            @endif
                        </div>
                        @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
</div>


  <!-- view order Modal -->

<div class="modal fade" id="edit_orders_details" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('site.Order Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="view">

            </div>
        </div>
    </div>
</div>


<!-- /Page Wrapper -->


      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->



    <!-- Reject Modal -->
      <div class="modal fade" id="reject_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reject</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Reject</h4>
                        <p class="mb-4">Are you sure want to Reject?</p>

                        <div class="text-center">
                            <form  id="formReject" action="" method="post" style="display: inline">
                                @csrf
                                @method('post')
                                <input type="text" name="id" id="hiddenid" hidden>
                                <input class="form-control mb-2" type="text" name="note" placeholder="please enter the reason">
                                <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Yes </button>
                            </form>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Reject Modal -->


@endsection


@push('scripts')




    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        var pusher = new Pusher('e18a602fc8a8c1d5a939', {
        cluster: 'eu'
        });
        var channel = pusher.subscribe('my-channel');
        channel.bind('new-order', function(data) {
            var order_id=data.orderId;
            var order_status=data.order.status;

            if(order_status == 1){
                appendRow(order_id);
            }else{
                status_from_event(order_id ,order_status);
            }
        });
    </script>

    <script>
        function appendRow(order_id){
            $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/shabab/orderDetails/'+order_id,
                    success:function(order){
                        var order =order.order;
                        if(order != null){
                            var neworder= `<tr style="background: #21252917 0% 0% no-repeat padding-box; ">
                                            <td style="    padding-top: 4.3%;width: 10%;background:#FFF">
                                                <div class="row"   >
                                                    <a style="display:block " target="_blank" href="{{url('shabab/order/customerorders/${order.user_id}')}}" class="ml-3">
                                                        <div class="row" style="       position: relative; justify-content: center;">
                                                            <img class="arounded-circle " style="width:40%" src="{{asset('backend/img/profile.png')}}" alt="User Image" >
                                                            <img src="{{asset('backend/img/order/${order.image_name}')}}" style="position:absolute;    bottom: -7px;">
                                                        </div>
                                                    </a>

                                                </div>
                                                <div class="row" style="justify-content: center;">
                                                    <a id="customer_${order.id}"class="col-12" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  target="_blank" href="{{url('shabab/order/customerorders/${order.user_id}')}}">${order.customer.name}</a>
                                                    <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">${order.customer.phone}</span>
                                                </div>
                                            </td>

                                            <td style="background:#FFF">
                                                <table class="table table-light order_details">
                                                    <thead class=" head" style="background: #fff 0% 0% no-repeat padding-box;">
                                                        <tr>
                                                            <th style ="width:1%">#</th>
                                                            <th  style ="width:15%">Restaurant</th>
                                                            <th>Payment Method</th>
                                                            <th>Payment Status</th>
                                                            <th>Order Type</th>
                                                            <th>Quantity</th>
                                                            <th>Total Price</th>
                                                            <th>Coupon</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="    background: #ebebeb52;">
                                                        <tr style="text-align:center" >
                                                            <td>${order.id}</td>
                                                            <td style="width:10%">
                                                                <div class="row" style=" justify-content: center;">
                                                                    <a class="col-12" id="cook_${order.id}" style="text-align: center;line-height:3;font:normal normal normal 18px/40px Comfortaa;"  >${order.cook_name}</a>
                                                                    <span style="display:block ;font: normal normal normal Comfortaa;color: #707070; ">${order.cook.phone}</span>
                                                                </div>
                                                            </td>

                                                            <td id="payment_method_${order.id }">

                                                            </td>
                                                            <td id="payment_status_${order.id }">

                                                            </td>

                                                            <td id="order_type_${order.id }">

                                                            </td>
                                                            <td>${order.qty}</td>
                                                            <td>${order.total_price}  ${order.currency}</td>
                                                            <td style="display:${order.display_auth_type}">
                                                                ${order.admin_note}
                                                                <a href="{{ route('discount.index') }}"> ${order.coupon.name}
                                                            </td>

                                                            <td style="padding-right:20px ;position:relative" >
                                                                <div class="row" style="justify-content: flex-end;margin-right: 0px;">
                                                                    <p class="animation-menu" style="  cursor: pointer;  float: right;"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"><img src="{{asset('backend/img/order/Ellipse 15.svg')}}"></p>
                                                                </div>
                                                                <div class="fade-in-left"  style=" z-index:22;   width: 136px;height: 82px;position: absolute;display: none; background: #fff;top: 30px; right: 40px;">
                                                                    <ul style="list-style: none;padding:5px 0;    text-align: left;">

                                                                        <li style="padding: 8px 20px 8px;display:${order.display_auth_type}" >
                                                                            <a  style="color: #4C93FA;" id="${order.id}" class="link_update  mr-2" data-toggle="modal"  href="#edit_order_details${order.id}" ><img src="{{asset('backend/img/order/Icon material-edit.svg')}}" style="padding:0 0px 5px">  Edit</a>
                                                                        </li>

                                                                        <li style="padding: 8px 20px 8px ; text-decoration-line:underline;   border-top: 1px solid #eee;"><a id="${order.id}" onclick="link_show(${order.id})" class="link_show  mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details"><img style="padding:0px 0 4px 0px" src="{{asset('backend/img/order/Icon ionic-ios-eye.svg')}}"> View Details</a></li>
                                                                    <ul>
                                                                </div>
                                                                <button onclick="assign(this)" style="display:${order.display_auth_type}; background: #3F8DFD 0% 0% no-repeat padding-box;box-shadow: 0px 1px 2px #161819;border-radius: 13px;   display: block;color:#fff ;float:right;width: 99px;height: 25px;">@lang('site.unAssigned')</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                    </tbody>
                                                    <tfoot style="background:#ebebeb52">
                                                        <tr>
                                                            <th style="padding:5px" colspan="9">
                                                                <div class="row">
                                                                    <div class="col-8" style ="display: flex; align-items: center;">
                                                                        <div style="position:relative" id="footer_address_${order.id}">


                                                                        </div>
                                                                    </div>
                                                                    <div class="col-2 " id="footer_order_type_${order.id}" style="">


                                                                    </div>
                                                                    <div class=" col-2"  style="padding-right: 30px;  display: flex;align-items: center;flex-direction: row-reverse;">

                                                                        <select id="my-select-${order.id}" class="form-control" onchange="status(${order.id})"name="order_status" style="     border-radius: 40px; background:#FFF2BC 0% 0% no-repeat padding-box; height: 32px; width: 83%;float: right;">
                                                                            <option value="1" selected> @lang('site.Pending')</option>
                                                                            <option value="2"> @lang('site.Accepted')</option>
                                                                            <option value="3"> @lang('site.In progress')</option>
                                                                            <option value="4"> @lang('site.Shipped')</option>
                                                                            <option value="6"> @lang('site.on the way')</option>
                                                                            <option value="5"> @lang('site.Delivered')</option>
                                                                            <option value="0"> @lang('site.Rejected')</option>
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>

                                            </td>

                                            <td id="order_status_${order.id}" status="${order.status}" hidden>
                                                ${order.order_status}
                                            </td>
                                        </tr>`;


                            var neworder_deit= `<div class="modal fade " id="edit_order_details${order.id}" aria-hidden="true" role="dialog">
                                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Edit Order</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form id="formUpdate" method="post" action="{{ url('shabab/order/updatedata/${order.id}') }}" enctype="multipart/form-data">
                                                                    @csrf
                                                                    @method('put')
                                                                    <div class="row form-row">
                                                                        <div class="col-12 col-sm-12">
                                                                            <div class="form-group">
                                                                                <label>Total Price</label>
                                                                                <input type="text" name="total_price" class="form-control" value="${ order.total_price }">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 col-sm-12">
                                                                            <div class="form-group">
                                                                                <label>Admin Note</label>
                                                                                <input type="text" name="admin_note" class="form-control" value="${order.admin_note}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>`;


                            $('#table_body_Pending').prepend(neworder);
                            $('#table_body_Pending').prepend(neworder_deit);
                            var payment_method='';
                            if(order.payment_method == 0){
                                payment_method= '@lang('site.Cash')';
                            }else if(order.payment_method == 1){
                                payment_method= '@lang('site.Credit')';

                            }
                            $('#payment_method_'+order.id).append(payment_method);
                            var payment_status='';
                            if(order.payment_status != 1){
                                payment_status= '@lang('site.Unpaid')';
                            }else if(order.payment_status == 1){
                                payment_status= '@lang('site.Paid')';

                            }


                            $('#payment_status_'+order.id).append(payment_status);
                            var delivery_type='';
                            if(order.delivery_type != 1){
                                delivery_type= '@lang('site.Pickup')';
                            }else{
                                delivery_type='@lang('site.Delivery')';
                            }
                            $('#order_type_'+order.id).append(delivery_type);
                            if(order.delivery_type == 0){
                                $('#footer_order_type_'+order.id).css({'display':'flex','align-items': 'center'})
                            }else{
                                var order_type=` <p style="margin:0">
                                                    <img src="{{asset('backend/img/order/Icon material-timer.svg')}}">
                                                    <span style="color:#A5B3CA">
                                                        Within
                                                        ${order.delivery_estimated_time}
                                                        min
                                                    </span>
                                                </p>
                                                <p style="margin:0">
                                                    <img src="{{asset('backend/img/order/Icon ionic-md-time.svg')}}">
                                                    <span>${order.date}</span>
                                                    <span>${order.time}</span>
                                                </p>`;
                                $('#footer_order_type_'+order.id).append(order_type);
                            }
                            if(order.delivery_type == 0){
                                var address_img= `<img style="width:12px;margin-left:25px" src="{{asset('backend/img/order/Icon material-location-on.svg')}}">
                                <span style="position:absolute ;top:2}}px;left:50px;">${order.cook.address[0].address}</span>
                                `;
                            }else{
                                var address_type='';
                                const count1 = (order.addresses).filter(item => item == order.address).length;

                                console.log(count1,'111111111111');
                                const count2 = (order.address_ids).filter(item => item == order.customer_address_id).length;

                                console.log(count2,'2222222222222');
                                if(count1 > 1 || count2 > 1){
                                        address_type=" (@lang('site.old') @lang('site.Address'))";
                                }else{
                                    address_type="(@lang('site.new') @lang('site.Address'))";
                                }
                                var address_img= `<span style="position:absolute ;top:-3}}px;left:50px;">${order.cook.address[0].address}</span>
                                                        <img style="width:20px;margin-left:25px" src="{{asset('backend/img/order/location.png')}}">
                                                        <span style="position:absolute ; bottom: -6px;">
                                                            ${order.address}
                                                            <span style="color:#A5A5A5;padding:5px">
                                                                ${address_type}
                                                            </span>
                                                        </span> `;
                            }
                            $('#footer_address_'+order.id).append(address_img)
                            $('.animation-menu').click(function(){
                                $(this).parent().parent().find('.fade-in-left').css('display','block');
                                $(document).mouseup(function(e){

                                    var container =  $(document).find('.fade-in-left');

                                    if(!container.is(e.target) && container.has(e.target).length === 0){
                                        container.hide();
                                    }
                                });
                            });
                        }
                    },
                    error:function(data){
                    }
            });
        }
    </script>

    <script>

        function assign(elem){
            var order=$(elem).attr('id');
            var auth=$('#auth').attr('name');

            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/shabab/order/assign/'+order,

                success:function(data){

                    $()

                }

            });

            $(elem).text(auth);
            $(elem).prop("disabled", true);

        }

        $('.animation-menu').click(function(){
            $(this).parent().parent().find('.fade-in-left').css('display','block');
            $(document).mouseup(function(e){

                var container =  $(document).find('.fade-in-left');

                if(!container.is(e.target) && container.has(e.target).length === 0){
                    container.hide();
                }
            });
        });
        $(".modal").on("hidden.bs.modal", function(){
            $("#view").html("");
            $("#body").html("");
        });

        function link_show(id){

                $('#view').html('');
                $('#body').html('');
                var order_id=id;
                // var order_id=$(this).attr('id');
                var customer=$('#customer_'+order_id).html()
                var cook=$('#cook_'+order_id).html()
                var payment_method=$('#payment_method_'+order_id).html()
                var payment_status=$('#payment_status_'+order_id).html()
                var delivery_status=$('#delivery_status_'+order_id).html()
                var order_status=$('#order_status_'+order_id).html()
                $.ajax(
                    {url: '/{{ Config::get('app.locale') }}/shabab/order/orderdetails/'+order_id,
                        success: function(result){
                        var data= result.order.orderEntry;
                        console.log(data)
                        var details='<div class="row form-row">'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Number : '+result.order.id+'</label>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Customer Name : '+customer+'</label>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Cook Name : '+cook+'</label>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<table class="table table-borderd table-hover">'+
                                                    '<thead>'+
                                                        '<tr>'+
                                                            '<th>Dish Name</th>'+
                                                            '<th>Dish Note</th>'+
                                                            '<th>Dish price</th>'+
                                                            '<th>Amount</th>'+
                                                        '</tr>'+
                                                    '</thead>'+
                                                    '<tbody id="body">'+

                                                    '</tbody>'+
                                                '</table>'+


                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-12 col-sm-12" id="location">'+

                                        '</div>'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Subtotal Price : '+(Number(result.order.sub_total)).toFixed(2)+'</label>'+
                                            '</div>'+
                                        '</div>'+

                                        // '<div class="col-12 col-sm-12">'+
                                        //     '<div class="form-group">'+
                                        //         '<label>Delivery Status : '+delivery_status+'</label>'+
                                        //     '</div>'+
                                        // '</div>'+
                                        '<div class="col-12 col-sm-12">'+
                                            '<div class="form-group" style="margin:0">'+
                                                '<label>Order Status : '+order_status+'</label>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';

                                    var location='<div class="form-group" style="margin:0">'+
                                                '<label>Location : '+result.order.address+'</label>'+
                                            '</div>';
                                    var fees='<div class="col-12 col-sm-12"   style="margin:0;  padding: 0;">'+
                                        '<div class="form-group"  style="margin:0;  padding: 0;">'+
                                            '<label>Delivery Fees : '+ (Number(result.order.delivery_fees)).toFixed(2)+'</label>'+
                                        '</div>'+
                                    '</div>'+


                                    '<div class="col-12 col-sm-12"  style=" margin:0;    padding: 0;">'+
                                        '<div class="form-group"  style="margin:0;  padding: 0;">'+
                                            '<label>Delivery Taxes : '+ (Number(result.order.delivery_taxes)).toFixed(2)+'</label>'+
                                        '</div>'+
                                    '</div>';

                                    var discount='<div class="col-12 col-sm-12"  style="  margin:0;   padding: 0;">'+
                                        '<div class="form-group" style="margin:0">'+
                                            '<label>Discount value : '+ (Number(result.order.discount_price)).toFixed(2)+'</label>'+
                                        '</div>'+
                                    '</div>';
                                    var total='<div class="col-12 col-sm-12" style=" padding: 0;display :{{ auth()->guard('admin')->user()->roles[0]->name !='cook' ? 'block' : 'none' }}">'+
                                        '<div class="form-group" style="margin:0">'+
                                            '<label >Total Price : <span id="price_total">'+(Number(result.order.total_price)).toFixed(2)+'</sapn></label>'+
                                        '</div>'+
                                    '</div>';
                                    if(result.order.note != null ){
                                        var note='<div class="col-12 col-sm-12" style=" padding: 0;">'+
                                                        '<div class="form-group" style="margin:0">'+
                                                            '<label >Note : <span id="note">'+(result.order.note)+'</sapn></label>'+
                                                        '</div>'+
                                                    '</div>';
                                    }else{
                                        var note='<span></span>';
                                    }

                                    $('#view').append(details);

                                    if( result.order.address != null){
                                        $('#location').append(location);
                                    }

                                    var role=$('#role').val();
                                    if(role != 'cook' && result.order.delivery_fees > 0){
                                        $('#view').append(fees);

                                    }
                                    if(role != 'cook' && result.order.discount_price > 0){
                                        $('#view').append(discount);

                                    }
                                    $('#view').append(total);
                                    $('#view').append(note);



                                    for (let i = 0; i < data.length; i++) {
                                        if(data[i].addons.length > 0){
                                            var body= '<tr>'+
                                                            '<td>'+data[i].name_{{ Config::get('app.locale') }}+'</td>'+
                                                            '<td class="'+data[i].notes+'">'+data[i].notes+'</td>'+

                                                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                                            '<td>'+(data[i].price)+'</td>'+
                                                            '<td>'+data[i].quantity+'</td>'+

                                                        '</tr>';
                                                $('#body').append(body)  ;
                                            for (let j = 0; j < data[i].addons.length; j++) {
                                                var add_price =data[i].addons[j].add_price;
                                                var add_name=data[i].addons[j].add_{{ Config::get('app.locale') }};
                                                var add= '<tr style="border:0">'+
                                                            '<td style="border: 0;padding: 0 0.75rem;">'+add_name+'</td>'+
                                                            '<td style="border: 0;padding: 0 0.75rem;"></td>'+

                                                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                                            '<td style="border: 0;padding: 0 0.75rem;">'+add_price+'</td>'+
                                                            '<td style="border: 0;padding: 0 0.75rem;">'+data[i].quantity+'</td>'+

                                                        '</tr>';
                                                $('#body').append(add)  ;
                                            }

                                        }else{

                                            var body= '<tr>'+
                                                            '<td>'+data[i].name_{{ Config::get('app.locale') }}+'</td>'+
                                                            '<td class="'+data[i].notes+'">'+data[i].notes+'</td>'+

                                                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                                            '<td>'+(data[i].price)/data[i].quantity+'</td>'+
                                                            '<td>'+data[i].quantity+'</td>'+

                                                        '</tr>';
                                        $('#body').append(body)  ;
                                        }
                                    }


                    }
                });
        }

        @if (count($errors) > 0)
                $('#Add_orders').modal('show');
        @endif

        $('.link_delete').on('click',function(){
             var order=$(this).attr('id');
            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/order/'+order)

        });

        function status(order_id){
                var status =$('#my-select-'+order_id).val();
                if(status != null){
                    $.ajax({
                        type: 'GET',
                        url: '/{{ Config::get('app.locale') }}/shabab/order/'+order_id+'/'+status,
                        // url: '/{{ Config::get('app.locale') }}/shabab/order/'+order_id,

                        success:function(data){
                            //    document.getElementById(order_id).innerHTML= data.status;

                            // if (data.status == 2) {
                            //     $('.alert-success').text('Order Accepted ')
                            // }else if (data.status == 3){
                            //     $('.alert-success').text('Order In Progressive ')
                            // }
                            // $('.alert-success').css('display','block');


                            //    var Row= $('#my-select-'+order_id).parent().parent().parent().parent().parent().parent().parent().parent().css('display','none')
                            //     var pending=$('#pending_order').val();
                            //     if(pending != 1){
                            //         appendRow(order_id)
                            //     }
                            var tabs= new Array('Rejected','Pending','Accepted','InProgress','Shipped','Delivered','','','','Cancelled');
                            var Row= $('#my-select-'+order_id).parent().parent().parent().parent().parent().parent().parent().parent()

                            if(status == '8' ){
                                Row.insertBefore('#table_body_Unassigned');
                            }else if( status == '6'){
                                $('#my-select-'+order_id).val(6);
                                Row.insertBefore('#table_body_On_The_Way');
                            }else if(status == '5'){
                                var pay_status="<?php echo __('site.Paid'); ?>";
                                $('#payment_status_'+order_id).html(pay_status)
                                Row.insertBefore('#table_body_Delivered');
                            }else{
                                if(status == '4' ){

                                $('#table_body_Unassigned').find('#TR_'+order_id).empty()

                                    var unassigned_Row=Row.clone();
                                    Row.insertBefore('#table_body_'+tabs[status]);

                                    unassigned_Row.insertBefore('#table_body_Unassigned');
                                    $('#Unassigned' ).find('#TR_'+order_id).find('#my-select-'+order_id ).val(4);

                                }else{
                                    Row.insertBefore('#table_body_'+tabs[status]);

                                }
                                $('#order_status_'+order_id).html(tabs[status]);
                                $('#my-select-'+order_id).val(status);
                                $('#Unassigned' ).find('#TR_'+order_id).find('#my-select-'+order_id ).val(status);

                            }

                        },
                        error:function(data){
                        }
                    });
                }

                // location.reload();

        };

        $('.link_reject').on('click',function(){
             var order=$(this).attr('id');
             $('#hiddenid').val(order);
            $('#formReject').attr('action','/{{ Config::get('app.locale') }}/shabab/order/reject')

        });



        function status_from_event(order_id, status){
            // alert(status)
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/shabab/order/'+order_id+'/'+status,
                // url: '/{{ Config::get('app.locale') }}/shabab/order/'+order_id,

                success:function(data){
                    //    document.getElementById(order_id).innerHTML= data.status;
                    // console.log(data.status)
                    /*if (data.status == 2) {
                        $('.alert-success').text('Order Accepted ')
                    }else if (data.status == 3){
                        $('.alert-success').text('Order In Progressive ')
                    }
                    $('.alert-success').css('display','block');*/


                    var Row= $('#my-select-'+order_id).parent().parent().parent().parent().parent().parent().parent().parent()
                    var tabs= new Array('Rejected','Pending','Accepted','InProgress','Shipped','Delivered','','','','Cancelled');
                    // alert(status)
                    if(status == '11' ){
                        Row.insertBefore('#table_body_Assigned');
                        $('#my-select-'+order_id ).val(11).change();
                        $('#Unassigned' ).find('#TR_'+order_id).empty();

                    }else if( status == '6'){
                        Row.insertBefore('#table_body_On_The_Way');
                        $('#my-select-'+order_id ).val(6).change();

                    }else if(status == '5'){
                        var pay_status="<?php echo __('site.Paid'); ?>";
                        $('#payment_status_'+order_id).html(pay_status)
                        Row.insertBefore('#table_body_Delivered');
                        $('#my-select-'+order_id ).val(5).change();

                    }else{
                        if(status == '4' ){
                            $('#table_body_Unassigned').find('#TR_'+order_id).empty()
                            var unassigned_Row=Row.clone();
                            Row.insertBefore('#table_body_'+tabs[status]);
                            unassigned_Row.insertBefore('#table_body_Unassigned');
                            $('#Unassigned' ).find('#TR_'+order_id).find('#my-select-'+order_id ).val(4);

                        }else{
                            Row.insertBefore('#table_body_'+tabs[status]);
                            $('#Unassigned' ).find('#TR_'+order_id).find('#my-select-'+order_id ).val(status);

                        }
                        $('#my-select-'+order_id).val(status).change();

                    }
                    $('#order_status_'+order_id).html(tabs[status])

                },
                error:function(data){
                }
            });
            // location.reload();

        };


        $('.img__img').hover(function(){
            $(this).parent().find('.img__wrap__text').toggleClass( "hover" );
        });
    </script>
@endpush
