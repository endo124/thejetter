@extends('backend.layouts.app')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    ">
<style>
.dish-type  li{
    list-style: none;
    float: left;
    margin-left:10px
}

#addRow{
    display: hidden;
}
#addRow:first-child {
display: block;

}
#new_addon2{
    margin-left: 61px;
    margin-top: 6px;
    width: 100%;
}
.fa-trash-alt{
    margin-left: 5px
}
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.List of Dishes')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/shabab">@lang('site.shabab')</a></li>
                        <li class="breadcrumb-item active">@lang('site.Dishes')</li>
                    </ul>
                </div>
                <div class="col-sm-12 col">
                    <a href="#Add_Dish" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" >@lang('site.Add')</a>
                </div>
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">

                    <div class="card-header" style="display: flex;justify-content: space-between;">
                        <h4 class="card-title">@lang('site.dish List')</h4>
                        {{-- <div class="form-group" style="    width: 20%;">
                            <form action="{{ url('/shabab/dish') }}" method="get" style="display: flex;">
                                @csrf
                                <input type="text" name="search" class="form-control" value="{{request()->search}}" >
                                <button class="btn btn-otbokhly" type="submit" style="margin: 0;border-radius: 0 37px 37px 0;padding: 9px;min-width: 80px;">@lang('site.search')</button>
                            </form>

                        </div> --}}
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0" id="example">
                                <thead>
                                    <tr>

                                        <th style="width: 200px">
                                            @lang('site.Name')
                                            {{-- <input type="text" id="search_dish" name="search_dish" class="d-block search_data" style="padding:0;margin:0"> --}}
                                        </th>
                                        <th> @lang('site.Images')</th>
                                        <th> @lang('site.Vendor')</th>
                                        <th style="width: 200px">
                                            @lang('site.Section')
                                            {{-- <input type="text"  id="search_section" name="search_section" class="d-block search_data" style="padding:0;margin:0"> --}}
                                        </th>
                                        <th>@lang('site.cusine')</th>
                                        <th>@lang('site.Price')</th>
                                        <th>@lang('site.Calories')</th>
                                        {{-- <th>Reviews</th> --}}
                                        @if (is_null(auth()->guard('admin')->user()->parent))
                                             <th>@lang('site.Action')</th>
                                        @else
                                             <th>@lang('site.Availability<')/th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>


                                  @if($dishes )
                                    @foreach ($dishes as $dish)

                                        <tr>
                                            <td>
                                                <h2 >{{ $dish->name }}</h2>
                                            </td>
                                            <td>

                                                @php
                                                $images= explode("__",$dish->images);
                                                @endphp
                                                @foreach ($images as $img)
                                                    <img src=" {{ asset('/backend/img/dishes/'.$img) }}" width=50 alt="">
                                                @endforeach
                                            </td>
                                            <td>{{ $dish->users->name}}</td>
                                            <td>{{ $dish->sections->name ?? ' ' }}</td>
                                            <td>{{ $dish->cusines[0]->name ??  ' ' }}</td>
                                            @php
                                                $price=$dish->portions_price;
                                                    $min=$price[0];
                                                    foreach ($price as $pr) {
                                                    if($pr !=null){
                                                        if ($min > $pr) {
                                                            $min =$pr;
                                                        }
                                                    }
                                                }
                                            @endphp
                                            <td>@lang('site.start from') : {{ $min}}</td>
                                            {{-- <td>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                            </td> --}}
                                            <td>{{ $dish->calories??  ' ' }}</td>

                                            <td >
                                                {{-- @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                                    <div class="actions">
                                                        <a  class="link_update bg-success-light mr-2 disabled" data-toggle="modal"  href="#edit_dishes_details" >
                                                            <i class="fe fe-pencil"></i> Edit
                                                        </a>
                                                        <a id="{{ $dish->id }}" class="link_delete disabled" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00" >
                                                            <i class="fe fe-trash"></i> Delete
                                                        </a>
                                                    </div>
                                                @else --}}
                                                @if (is_null(auth()->guard('admin')->user()->parent))
                                                    <div class="actions">
                                                        <a id="{{ $dish->id }}" class="link_update bg-success-light mr-2" href="{{ route('dish.edit', $dish->id) }}" >
                                                            <i class="fe fe-pencil"></i> @lang('site.Edit')
                                                        </a>
                                                        <a id="{{ $dish->id }}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                            <i class="fe fe-trash"></i> @lang('site.Delete')
                                                        </a>
                                                    </div>
                                                @else
                                                <div class="custom-control custom-switch">
                                                    @php
                                                       $available= $dish['availabilities'];
                                                       if (count($available )==0) {
                                                          $status=1;
                                                       }
                                                    @endphp

                                                    @if (isset($status))

                                                        <input name="availability" type="checkbox" class="custom-control-input" id="{{$dish->id }}" onclick="changeStatus(this)" value="1" checked >
                                                    @else
                                                        @php
                                                            $branches=[];
                                                            foreach($available as $av){
                                                                array_push($branches,$av['user_id']);
                                                            }
                                                        @endphp

                                                        @if (in_array(auth()->guard('admin')->user()->id ,  $branches))
                                                            <input name="availability" type="checkbox" class="custom-control-input" id="{{$dish->id }}" onclick="changeStatus(this)" value="0" >
                                                        @else
                                                            <input name="availability" type="checkbox" class="custom-control-input" id="{{$dish->id }}" onclick="(this)" value="1"  checked>
                                                        @endif

                                                            {{-- @if ($av['user_id'] == auth()->guard('admin')->user()->id)

                                                                <input name="availability" type="checkbox" class="custom-control-input" id="{{$dish->id }}" onclick="changeStatus(this)" value="0" >
                                                            @elseif()
                                                            @else
                                                                <input name="availability" type="checkbox" class="custom-control-input" id="{{$dish->id }}" onclick="changeStatus(this)" value="1"  checked>
                                                            @endif --}}

                                                    @endif

                                                    <label class="custom-control-label" for="{{ $dish->id }}">@lang('site.Status')</label>changeStatus                                              </div>
                                                @endif
                                                {{-- @endif --}}
                                            </td>
                                        </tr>
                                        <!-- Edit Details Modal -->
                                        {{-- <div class="modal fade" id="edit_dishes_details{{ $dish->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">@lang('site.Edit dishes')</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="formUpdate" method="post" action="" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('put')


                                                            <div class="row form-row">
                                                                @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                                                <div class="col-12 col-sm-12 mb-4">

                                                                    <label>@lang('site.Vendors') : </label>

                                                                    <select  name="cook_name"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                        @foreach ($cooks as $cook)
                                                                        <option {{ $cook->id== $dish->users->id? 'selected' :' ' }}  value="{{ $cook->id }}">{{ $cook->name }}</option>
                                                                    @endforeach
                                                                    </select>
                                                                </div>
                                                                @endif
                                                                @foreach (config('translatable.locales') as $locale)
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.dishname')</label>
                                                                        <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $dish->translate($locale)->name ??old($locale.'.name') }}">
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div  class="dropzone dz-clickable" >
                                                                            <div class="dz-default dz-message">
                                                                            <label>@lang('site.Dish Images') : </label>
                                                                                <br>
                                                                                <input class="box__file" type="file" name="files[]" id="file1" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  />
                                                                            </div>
                                                                        </div>
                                                                        <div class="upload-wrap">
                                                                            <div class="upload-image float-left" id="upload-image" style="display:inline-block">
                                                                                @php
                                                                                    $dishes_img=explode('__',$dish->images)
                                                                                @endphp
                                                                            @foreach ($dishes_img as $img)
                                                                            <img id='img_src' width=50 src="{{ asset('/backend/img/dishes/'.$img) }}">
                                                                            @endforeach
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-12 col-sm-12 mt-4">
                                                                    <div class="form-group row">
                                                                        <div class="col-9 col-sm-9">
                                                                        <label>@lang('site.cusines') : </label>
                                                                            <select name="cusine"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                                                @foreach ($cusines as $cusine)
                                                                                    <option {{ (in_array($cusine->id, $dish->cusines->pluck('id')->toArray()))? 'selected' :' ' }}  value="{{ $cusine->id }}" >{{ $cusine->name }}</option>
                                                                                @endforeach

                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group row">
                                                                        <div class="col-9 col-sm-9">
                                                                        <label>Categories : </label>
                                                                            <select id="category" name="category"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                                                <option value="">@lang('site.select')</option>
                                                                                @foreach ($categories as $category)
                                                                                    <option {{ $category->id== $dish->categories->id ? 'selected' :' ' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group row" id="addon_section2"  >
                                                                        @if (count($dish->addons) >= 1)
                                                                        @for ($i = 0; $i < count($dish->addons); $i++)
                                                                            <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                                                <label>@lang('site.Addons') : </label>
                                                                                    <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                                                    <a onclick="addRoow()"  class="float-right mt-2" style="font-size: medium;">+</a>
                                                                                <select class="addons select_addon"   name="addonsection[]"    style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                    <option >@lang('site.select')</option>
                                                                                    @foreach ($addons as $addon)
                                                                                        <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <div  id="new_addon2" style="margin-left: 60px; ">
                                                                                    <select  name="addon_conditions[]"      style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                                                                        <option value="1" {{ $dish->addons[$i]->pivot->condition == '1' ? 'selected' : '' }} >@lang('site.required')</option>
                                                                                        <option value="0"  {{ $dish->addons[$i]->pivot->condition == '0' ? 'selected' : '' }}>@lang('site.optional')</option>
                                                                                    </select>
                                                                                    <p>
                                                                                        <span >@lang('site.max')</span><input type="text" value="{{ $dish->addons[$i]->pivot->max }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                        <span>@lang('site.Min')</span><input type="text" value="{{ $dish->addons[$i]->pivot->min }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                    </p>

                                                                                </div>
                                                                            </div>
                                                                        @endfor
                                                                        @else
                                                                        <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                                            <label>@lang('site.Addons') : </label><a onclick="removeDiv(this)" class="float-right mt-2" style=";margin-right:3px;font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a><a onclick="addRoow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                                                            <select class="addons select_addon"    name="addonsection[]"      style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                <option >@lang('site.select')</option>
                                                                                @foreach ($addons as $addon)
                                                                                    <option value="{{ $addon->id }}" {{ (in_array($addon->id, $dish->addons->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $addon->name }}</option>
                                                                                @endforeach
                                                                            </select>

                                                                            <div  id="new_addon2" >
                                                                                <select  name="addon_conditions[]"  required style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                                                    <option value="1">@lang('site.required')</option>
                                                                                    <option value="0">@lang('site.optional')</option>
                                                                                </select>
                                                                                <p>
                                                                                    <span >@lang('site.max')</span><input type="text" value="{{ old('max[]') }}" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                    <span>@lang('site.Min')</span><input type="text" value="{{ old('min[]') }}" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                                                                </p>

                                                                            </div>
                                                                        </div>
                                                                        @endif

                                                                    </div>
                                                                </div>

                                                                 <div class="col-12 col-sm-12" id="AddonSection">
                                                                 </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.portions available') : </label>
                                                                        <ul class="nav nav-tabs" >
                                                                            <li class="nav-item">
                                                                              <a class="nav-link active"  data-toggle="tab" href="#hoome{{ $dish->id }}" >@lang('site.Large')</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                              <a class="nav-link"  data-toggle="tab" href="#proofile{{ $dish->id }}" >@lang('site.Medium')</a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                              <a class="nav-link"  data-toggle="tab" href="#coontact{{ $dish->id }}" >@lang('site.Small')</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">

                                                                            <div class="tab-pane fade show active" id="hoome{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control "  value="{{ $dish->portions_available[0] }}" style="width: 20%;display:inline"> <span class="mr-5"> @lang('site.persons')</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[0] }}" style="width: 20%;display:inline"> <span>@lang('site.'.$currency) </span>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="proofile{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[1] }}" style="width: 20%;display:inline"> <span  class="mr-5"> @lang('site.persons')</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[1] }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency)</span>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="coontact{{ $dish->id }}" >
                                                                                <input type="text" name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[2]  }}" style="width: 20%;display:inline"> <span class="mr-5"> @lang('site.persons')</span>
                                                                                <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[2] }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency)</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <label>Section : </label>
                                                                    <ul class="dish-type">
                                                                    @foreach ($sections as $section)
                                                                    <li>
                                                                        <input type="radio"  name="section" value="{{ $section->id }}" {{$dish->section_id == $section->id ?'checked':' ' }} >
                                                                        <label for="f-option">{{ $section->name }}</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    @endforeach
                                                                    </ul>
                                                                </div>
                                                                <div class="col-12 col-sm-12 mb-4">
                                                                    <label>availability : </label>
                                                                    <ul class="dish-type">
                                                                    <li>
                                                                        <input type="radio" id="f-option" name="dish_available" value="1" {{$dish->available ==1 ? 'checked':'' }}>
                                                                        <label for="f-option">availabe</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    <li>
                                                                        <input type="radio" id="" name="dish_available" value="0" {{$dish->available ==0 ? 'checked':'' }}>
                                                                        <label for="f-option">not available</label>
                                                                        <div class="check"></div>
                                                                    </li>
                                                                    </ul>
                                                                </div>

                                                                @foreach (config('translatable.locales') as $locale)

                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                                                        <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{$dish->translate($locale)->main_ingredients ?? old($locale.'.main_ingredients') }}">
                                                                    </div>
                                                                </div>
                                                                @endforeach

                                                                @foreach (config('translatable.locales') as $locale)

                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.description')</label>
                                                                        <textarea name="{{ $locale }}[info]" style="width: 90%;display: block;"  rows="3">{{$dish->translate($locale)->info ?? old($locale.'.info') }}</textarea>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                            <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div> --}}


                                        <!-- /Edit Details Modal -->
                                    @endforeach
                                  @endif

                                </tbody>
                            </table>
                            <div class="row justify-content-center pagination" >
                                {{-- {{ $dishes->appends(request()->query())->links() }} --}}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_Dish" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('site.Add Dish')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')

                <form method="POST" action="{{ route('dish.store')}}" enctype="multipart/form-data">
                    @csrf

                    <div class="row form-row">
                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                            <div class="col-12 col-sm-12 mb-4">

                                <label>@lang('site.Vendors') : </label>
                                <select id="Vendors" name="Vendor_id" class="selectpicker"  data-live-search="true"   style="width:50%;paddind:2px">
                                    <option value="" disabled selected readonly>@lang('site.select vendor')</option>
                                    @foreach ($cooks as $cook)
                                    <option value="{{ $cook->id }}">{{ $cook->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        @endif
                        <input id="Vendors" type="text" value="{{auth()->guard('admin')->user()->id}}" hidden>
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.dishname')</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>
                        </div>
                        @endforeach
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <div  class="dropzone dz-clickable" >
                                    <div class="dz-default dz-message">
                                    <label>@lang('site.Dish Images') : </label>

                                        <input class="box__file" type="file" name="files[]" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  hidden/>
                                        <label for="file"><strong>@lang('site.Choose a file')</strong></label>
                                    </div>
                                </div>
                                <div class="upload-wrap">


                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>@lang('site.cusines') : </label>

                                    <select name="cusine[]" class="selectpicker" multiple data-live-search="true" style="width:50%;paddind:2px">
                                        <option disabled  readonly selected value="">@lang('site.select')</option>

                                        @foreach ($cusines as $cusine)
                                            <option value="{{ $cusine->id }}">{{ $cusine->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>@lang('site.Categories') : </label>

                                    <select id="category1" class="selectpicker"  name="category" data-live-search="true" style="width:50%;paddind:2px" >
                                        <option disabled selected readonly value="">@lang('site.select')</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row" id="addon_section"  style="display: none">
                                <div class="col-9 col-sm-9 mt-3" id="newRow">
                                    <!-- <label>Addons : </label><a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                    <select class="addons1"   name="addonsection[]"     style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                        <option  value="">select</option>
                                    </select>
                                    <div  id="new_addon" style="margin-left: 73px; margin-top:5px">
                                        <select  name="addon_conditions[]"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                            <option value="1">required</option>
                                            <option value="0">optional</option>
                                        </select>
                                        <p>
                                            <span >max</span><input type="text" name="max[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                            <span>min</span><input type="text" name="min[]" style="width: 20%; border: #CCB 1px solid; margin: 5px;" >
                                        </p>

                                    </div> -->
                                    <label>@lang('site.Addon Section ') </label><a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>

                                    <select class="addons1"   name="addonsection[]"     style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                        <option  value="">@lang('site.select')</option>
                                    </select>
                                    <div  id="new_addon" style="margin-left: 73px; margin-top:5px">
                                        <!-- <select  name="addon_conditions[]"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" required>
                                            <option value="1">required</option>
                                            <option value="0">optional</option>
                                        </select> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>Allergens : </label>
                                    <select id="allergens" name="allergens[]" class="selectpicker" multiple data-live-search="true"   style="width:50%;paddind:2px">
                                        @foreach ($allergens as $allergen)
                                        <option value="{{ $allergen->id }}">{{ $allergen->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>price : </label>
                                <input type="text" name="price" class="form-control" placeholder=" 10  " value="{{ old('price') }}" style="width: 20%;display:inline"> <span> $</span>
                            </div>
                        </div> --}}
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.portions available') : </label>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="home-tab1" data-toggle="tab" href="#home1" role="tab" aria-controls="home1"
                                        aria-selected="true">@lang('site.Large')</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="profile-tab1" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1"
                                        aria-selected="false">@lang('site.Medium')</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="contact-tab1" data-toggle="tab" href="#contact1" role="tab" aria-controls="contact1"
                                        aria-selected="false">@lang('site.Small')</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab1">
                                        <input type="number" min="1" name="portions_available[]" class="form-control " placeholder=" 5 " value="{{ old('portions_lg_available') }}" style="width: 20%;display:inline"> <span class="mr-5"> @lang('site.persons')</span>
                                        <input  type="text"   name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency )</span>
                                    </div>
                                    <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab1">
                                        <input  type="number" min="1"  name="portions_available[]" class="form-control" placeholder=" 3 " value="{{ old('portions_md_available') }}" style="width: 20%;display:inline"> <span  class="mr-5"> @lang('site.persons')</span>
                                        <input  type="text"  name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency )</span>
                                    </div>
                                    <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact-tab1">
                                        <input  type="number" min="1"  name="portions_available[]" class="form-control" placeholder=" 2 " value="{{ old('portions_sm_available') }}" style="width: 20%;display:inline"> <span class="mr-5"> @lang('site.persons')</span>
                                        <input  type="text"  name="portions_price[]" class="form-control"  value="{{ old('portions_lg_price') }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency )</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12" id="dish_section" style="display:{{auth()->guard('admin')->user()->roles[0]->name !='cook' ? 'none' : ''}}">
                            <label>@lang('site.Section') : </label>

                            @if(auth()->guard('admin')->user()->roles[0]->name == 'cook')
                                <ul class="dish-type"   >
                                    @foreach ($sections as $section)
                                    <li>
                                    <input type="radio"  name="section" value="{{ $section->id }}">
                                    <label for="f-option">{{ $section->name }}</label>
                                    <div class="check"></div>
                                </li>
                                    @endforeach
                                </ul>
                            @endif
                            <ul class="dish-type" id="sections_checkboxes">
                               {{-- @foreach ($sections as $section)
                               <li>
                                <input type="radio"  name="section" value="{{ $section->id }}">
                                <label for="f-option">{{ $section->name }}</label>
                                <div class="check"></div>
                              </li>
                               @endforeach --}}
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 mb-4">
                            <label>@lang('site.availability') : </label>
                            <ul class="dish-type">
                               <li>
                                <input type="radio" id="f-option" name="dish_available" value="1" checked>
                                <label for="f-option">@lang('site.availabe')</label>
                                <div class="check"></div>
                                {{-- <input name="available_count" type="number" style="width: 20%"> dish --}}
                              </li>
                              <li>
                                <input type="radio" id="" name="dish_available" value="0">
                                <label for="f-option">@lang('site.not available')</label>
                                <div class="check"></div>
                              </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group row">
                                <div class="col-9 col-sm-9">
                                <label>@lang('site.Calories')  </label>
                                    <input class="form-control" min="0" type="number" name="calories" >
                                </div>
                            </div>
                        </div>


                        @foreach (config('translatable.locales') as $locale)

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{ old($locale.'.main_ingredients') }}">
                            </div>
                        </div>
                        @endforeach
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.description')</label>
                                <textarea name="{{ $locale }}[info]" style="width: 90%"  rows="3">{{ old($locale.'.info') }}</textarea>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">@lang('site.Save Changes')</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('site.Delete')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-contendropdown bootstrap-selectlete">
                        <p class="mb-4">@lang('site.Are you sure want to delete?')</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">@lang('site.Delete') </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('site.Close')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
    <script>

$(document).ready(function() {
    // Setup - add a text input to each footer cell
    // $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder=" '+title+'" />' );

        // $( 'input', this ).on( 'keyup change', function () {
        //     if ( table.column(i).search() !== this.value ) {
        //         table
        //             .column(i)
        //             .search( this.value )
        //             .draw();
        //     }
        // });
    });
    var lang='{{ Config::get('app.locale') }}';
    if (lang == 'ar') {
        var url = '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json';
    }
    var table = $('#example').DataTable( {

        language: {
            url: url
        },
        orderCellsTop: true,
        fixedHeader: true,
        pageLength : 10,
        "ordering": false,
        lengthMenu: [[10,  30, 50,80,100], [10, 30, 50,80,100]]

    } );

} );

        // $('.search_data').keyup(function(){
        //     var dish_name= $('#search_dish').val();
        //     var section_name= $('#search_section').val();

        //     $.ajax({
        //         type:'Get',
        //         url: '/{{ Config::get('app.locale') }}/shabab/dish',

        //         data:{dish_name:dish_name ,section_name:section_name},

        //         success: function(data) {
        //             if(dish_name || section_name){
        //                 var dishes=data.data.dishes;

        //                 if(dishes){


        //                     // alert(dishes.length)
        //                     console.log(dishes)
        //                 }


        //                 $('tbody').hide()
        //                 $('.pagination').hide()
        //             }else{
        //                 $('tbody').show()
        //                 $('.pagination').show()
        //             }

        //         }
        //     });

        // });


    @if (count($errors) > 0)
            $('#Add_Dish').modal('show');
    @endif

function removeDiv(elem){
    $(elem).parent('div').remove();
}

function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

    var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
                "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
                // '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
            '</div>';

    $(".upload-wrap").append(img);

    $('.upload-image').css('display','block');
};
};

        // $('#category').change(function(){
        //     $('#addon_section').show();
        //     var category_id = document.getElementById('category').value;
        //     $('.addons').find('option').remove().end();

        //     $.ajax({
        //         type: 'GET',
        //         url: '/{{ Config::get('app.locale') }}/shabab/addons/'+ category_id,
        //         success: function(data) {
        //             for (var i = 0; i < data.success.length; i++) {
        //                 $(".addons").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');
        //                 // $(".addons1").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');

        //             }
        //             // $('.addons').selectpicker('refresh');
        //         }
        //     });

        // });

        $('#addon_section').hide();

        $('#category1').change(function(){
            $('#addon_section').show();

            var cook_id = document.getElementById('Vendors').value;
            $('.addons1').find('option').remove().end();
            // $('.addons1').find('.dropdown-menu').remove();
            // alert('a')
            $.ajax({
                type: 'GET',
                // url: '/{{ Config::get('app.locale') }}/shabab/addons/'+ category_id,
                url: '/{{ Config::get('app.locale') }}/shabab/addons_section/'+ cook_id,

                success: function(data) {
                    for (var i = 0; i < data.success.length; i++) {
                         $(".addons1").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');;
                        }
                    }
            });

        });

         $('#Vendors').change(function(){
            //  alert('a')
            $('#sections_checkboxes').html('');

            var cook_id = document.getElementById('Vendors').value;

            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/shabab/section/'+ cook_id,

                success: function(data) {
                    // console.log(data)
                    for (var i = 0; i < data.success.length; i++) {
                            console.log(data.success[i].name)
                         $('#sections_checkboxes').append('<li><input type="radio"  name="section" value="'+ data.success[i].id +'"><label for="f-option">' + data.success[i].name + '</label><div class="check"></div></li>');
                        }
                    }


            });
            $('#dish_section').css('display','block');

        });

    $('.link_delete').on('click',function(){
        var dish=$(this).attr('id');
        $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/shabab/dish/'+dish)

    });


    // $("#addRow").click(function () {
    //     alert('tessssssst')
    //         let test=$("#newRowww").clone().appendTo("#addon_section");
    //         test.css('display','block')
    //         // $("#newRow").selectpicker();
    //         // $('.table-responsive').find().('#newRow').css('display','none')

    // });
    function addRow () {
        let test=$("#newRow").clone().appendTo("#addon_section");
            test.css('display','block')
        // $('#newRow').selectpicker();
        // $('.table-responsive').find().('#newRow').css('display','none')

    };
    // function addRooow (elem) {
    //     let test=$("#newRooow").clone().appendTo("#addon_section2");
    //         // test.css('display','block')
    //     // $('#newRow').selectpicker();
    //     // $('.table-responsive').find().('#newRow').css('display','none')

    // };

    $('.select_addon').change(function(){
        var selectedaddons = [];
        // var addon=$(this).val;
        // alert(addon);
        // selectedaddons.push(addon);
        $.each($(this).parent().parent().find('.select_addon'), function(){
            selectedaddons.push($(this).val());
            });
    });
    function changeStatus(elem){
        var id=$(elem).attr('id');
        var status=$(elem).val();
        var checkbox=$(elem);
        $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/shabab/dish/'+id,

                success:function(data){
                    if(data.success ==0){
                        console.log(checkbox.prop('checked'))
                    }else{

                    }
                },
                error:function(data){
                }
            });


    }


    // function addRoow () {

    //     var parent = $('#addon_section2');
    //     parent.children(':last').clone().appendTo('#addon_section2');

    //     // $('#newRowedit').clone(true).appendTo('#addon_section2');
    //     // var form= $('#formUpdate ');

    //     // $('#newRow').selectpicker();
    //     // $('.table-responsive').find().('#newRow').css('display','none')

    // };


        // $('.link_update').on('click',function(){
        //     var dish=$(this).attr('id');

        //     $('#formUpdate').attr('action','/{{ Config::get('app.locale') }}/shabab/dish/'+dish+'/edit')

        // })
    </script>
@endpush


