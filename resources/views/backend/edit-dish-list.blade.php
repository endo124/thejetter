@extends('backend.layouts.app')
<style>
.dish-type  li{
    list-style: none;
    float: left;
    margin-left:10px
}

/* #addRow{
    display: hidden;
}
#addRow:first-child {
display: block;

} */
#new_addon2{
    margin-left: 61px;
    margin-top: 6px;
    width: 100%;
}
.fa-trash-alt{
    margin-left: 5px
}
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container">

        <div class="row" style="justify-content: center" >
            <div class="col-md-8 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.Edit Dish')</h4>

                    </div>
                    <div class="card-body" style="padding: 20px 50px">
                        @include('backend.partials.errors')

                            <form id="formUpdate" method="post" action="{{ route('dish.update',$dish->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('put')

                                    <input id="dish_section_id" type="text" value="{{$dish->section_id}}" hidden>
                            
                                <div class="row form-row">
                                    @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                    <div class="col-12 col-sm-12 mb-4">

                                        <label>@lang('site.Vendors') : </label>

                                        <select  id="Vendors"  name="cook_name"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                            @foreach ($cooks as $cook)
                                            <option {{ $cook->id== $dish->users->id? 'selected' :' ' }}  value="{{ $cook->id }}">{{ $cook->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    <input id="Vendors" type="text" value="{{auth()->guard('admin')->user()->id}}" hidden>

                                    @foreach (config('translatable.locales') as $locale)
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.dishname')</label>
                                            <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $dish->translate($locale)->name ??old($locale.'.name') }}">
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <div  class="dropzone dz-clickable" >
                                                <div class="dz-default dz-message">
                                                    <label>@lang('site.Dish Images') : </label>
                                                        <input class="box__file" type="file" name="files[]" id="file" onchange="showImage(this)"   data-multiple-caption="{count} files selected"  multiple  hidden/>
                                                        <label for="file"><strong>@lang('site.Choose a file')</strong></label>
                                                    </div>
                                            </div>
                                            <div class="upload-wrap">
                                                <div class="upload-image float-left" id="upload-image" style="display:inline-block">
                                                    @php
                                                        $dishes=explode('__',$dish->images)
                                                    @endphp
                                                @foreach ($dishes as $img)
                                                <img id='img_src' width=50 src="{{ asset('/backend/img/dishes/'.$img) }}">
                                                {{-- <a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a> --}}
                                                @endforeach
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 mt-4">
                                        <div class="form-group row">
                                            <div class="col-9 col-sm-9">
                                            <label>@lang('site.cusines') : </label>
                                                <select name="cusine"    style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                    @foreach ($cusines as $cusine)
                                                        <option {{ (in_array($cusine->id, $dish->cusines->pluck('id')->toArray()))? 'selected' :' ' }}  value="{{ $cusine->id }}" >{{ $cusine->name }}</option>
                                                    @endforeach

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-9 col-sm-9">
                                            <label>@lang('site.Categories') : </label>
                                                <select id="category" name="category"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;" >
                                                    <option value="">@lang('site.select')</option>
                                                    @foreach ($categories as $category)
                                                        <option {{ $category->id== $dish->categories->id ? 'selected' :' ' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row" id="addon_section2"  >
                                            @if (count($dish->addonsections) >= 1)
                                            @for ($i = 0; $i < count($dish->addonsections); $i++)
                                                <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                    <label>@lang('site.addon section') </label>
                                                    @if ($i==0)
                                                        <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                        <a onclick="addRow()"  class="float-right mt-2" style="font-size: medium;">+</a>
                                                    @else
                                                    <a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a>
                                                    @endif
                                                    <select class="addons select_addon"   name="addonsection[]"    style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                        <option value=" " >@lang('site.select')</option>
                                                        @foreach ($dish->addonsections as $section)
                                                            <option value="{{ $section->id }}" {{ $section->id== $dish->addonsections[$i]->id ? 'selected' :' ' }}>{{ $section->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div  id="new_addon2" style="margin-left: 60px; ">


                                                    </div>
                                                </div>
                                            @endfor
                                            @else
                                            <div class="col-9 col-sm-9 mt-3" id="newRowedit">
                                                <label>@lang('site.addon section') : </label><a onclick="removeDiv(this)" class="float-right mt-2" style=";margin-right:3px;font-size: medium;margin-right:3px"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
                                                <select class="addons select_addon"    name="addonsection[]"      style="padding: 10px;background: #E0E0E0;border: none;margin-left: 6px;width: 64%;">
                                                    <option value=" ">@lang('site.select')</option>
                                                    @foreach ($addonsections as $section)
                                                        <option value="{{ $section->id }}" >{{ $section->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif

                                        </div>
                                    </div>

                                        <div class="col-12 col-sm-12" id="AddonSection">
                                        </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.portions available') </label>
                                            <ul class="nav nav-tabs" >
                                                <li class="nav-item">
                                                    <a class="nav-link active"  data-toggle="tab" href="#hoome{{ $dish->id }}" >@lang('site.Large')</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"  data-toggle="tab" href="#proofile{{ $dish->id }}" >@lang('site.Medium')</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"  data-toggle="tab" href="#coontact{{ $dish->id }}" >@lang('site.Small')</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">

                                                <div class="tab-pane fade show active" id="hoome{{ $dish->id }}" >
                                                    <input type="number" min="1"  name="portions_available[]" class="form-control "  value="{{ $dish->portions_available[0] }}" style="width: 20%;display:inline"> <span class="mr-5"> @lang('site.persons')</span>
                                                    <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[0] }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency)</span>
                                                </div>
                                                <div class="tab-pane fade" id="proofile{{ $dish->id }}" >
                                                    <input type="number" min="1"  name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[1] }}" style="width: 20%;display:inline"> <span  class="mr-5">  @lang('site.persons')</span>
                                                    <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[1] }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency)</span>
                                                </div>
                                                <div class="tab-pane fade" id="coontact{{ $dish->id }}" >
                                                    <input type="number" min="1"  name="portions_available[]" class="form-control"  value="{{ $dish->portions_available[2]  }}" style="width: 20%;display:inline"> <span class="mr-5">  @lang('site.persons')</span>
                                                    <input type="text" name="portions_price[]" class="form-control"  value="{{ $dish->portions_price[2] }}" style="width: 20%;display:inline"> <span> @lang('site.'.$currency)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12" id="dish_section" >
                                        <label>@lang('site.Section') : </label>
                                        <ul class="dish-type"  id="sections_checkboxes">
                                        @foreach ($sections as $section)
                                        <li>
                                            <input type="radio"  name="section"  value="{{ $section->id }}" {{$dish->section_id == $section->id ?'checked':' ' }} >
                                            <label for="f-option">{{ $section->name }}</label>
                                            <div class="check"></div>
                                        </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-12 mb-4">
                                        <label>@lang('site.availability') : </label>
                                        <ul class="dish-type">
                                        <li>
                                            <input type="radio" id="f-option" name="dish_available" value="1" {{$dish->available ==1 ? 'checked':'' }}>
                                            <label for="f-option">@lang('site.availabe')</label>
                                            <div class="check"></div>
                                            {{-- <input name="available_count" type="number" style="width: 20%" value="{{$dish->available_count??old('available_count')  }}"> dish --}}
                                        </li>
                                        <li>
                                            <input type="radio" id="" name="dish_available" value="0" {{$dish->available ==0 ? 'checked':'' }}>
                                            <label for="f-option">@lang('site.not available')</label>
                                            <div class="check"></div>
                                        </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-9 col-sm-9">
                                            <label>@lang('site.Calories')  </label>
                                                <input class="form-control" style="display:inline-block" min="0"  type="number" name="calories" id="calories_{{ $dish->id }}" type="text" value="{{ $dish->calories ?? old('calories') }}">
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @foreach (config('translatable.locales') as $locale)
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.ingredients')</label>
                                            <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{ $dish->main_ingredients??old($locale.'.ingredients') }}">
                                        </div>
                                    </div>
                                    @endforeach --}}
                                    @foreach (config('translatable.locales') as $locale)

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.time_of_preparation')</label>
                                            <input type="text" name="{{ $locale }}[main_ingredients]" class="form-control" value="{{$dish->translate($locale)->main_ingredients ?? old($locale.'.main_ingredients') }}">
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach (config('translatable.locales') as $locale)

                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.'.$locale.'.description')</label>
                                            <textarea name="{{ $locale }}[info]" style="width: 90%;display: block;"  rows="3">{{$dish->translate($locale)->info ?? old($locale.'.info') }}</textarea>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <button type="submit" class="btn btn-otbokhly btn-block">@lang('site.Save Changes')</button>
                            </form>


                        </div>


                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<div class="col-9 col-sm-9 my-3" id="newRow" style="display: none;padding-left:0">
    <label>@lang('site.addon section') </label><a onclick="removeDiv(this)" class="float-right mt-2" style="font-size: medium;"><i class="far fa-trash-alt "></i></a><a onclick="addRow()" class="float-right mt-2" style="font-size: medium;">+</a>
    <select class="addons1"   name="addonsection[]"     style="padding: 10px;background: #E0E0E0;color: #000;border: none;width: 64%;">
        <option  value=" ">@lang('site.select')</option>
        @foreach ($addonsections as $section)
            <option value="{{ $section->id }}" >{{ $section->name }}</option>
         @endforeach
    </select>

</div>

@endsection


@push('scripts')
    <script>

function removeDiv(elem){
    $(elem).parent('div').remove();
}

function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

    var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
                "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
                // '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
            '</div>';

    $(".upload-wrap").append(img);

    $('.upload-image').css('display','block');
};
};

$('#category').change(function(){
    $('#addon_section').show();
    var cook_id = document.getElementById('Vendors').value;
            $('.addons1').find('option').remove().end();
            // $('.addons1').find('.dropdown-menu').remove();
            $.ajax({
                type: 'GET',
                // url: '/{{ Config::get('app.locale') }}/shabab/addons/'+ category_id,
                url: '/{{ Config::get('app.locale') }}/shabab/addons_section/'+ cook_id,

                success: function(data) {
                    for (var i = 0; i < data.success.length; i++) {
                         $(".addons1").append('<option value="' + data.success[i].id + '" >' + data.success[i].name + '</option>');;
                        }
                    }
            });
});


$('#Vendors').change(function(){
            //  alert('a')
            $('#sections_checkboxes').html('');

            var cook_id = document.getElementById('Vendors').value;
            var dish_section_id = document.getElementById('dish_section_id').value;
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/shabab/section/'+ cook_id,

                success: function(data) {
                    for (var i = 0; i < data.success.length; i++) {
                            console.log(data.success[i].name) 
                            if(dish_section_id == data.success[i].id ){
                               $('#sections_checkboxes').append('<li><input type="radio" name="section" checked value="'+ data.success[i].id +'"><label for="f-option">' + data.success[i].name + '</label><div class="check"></div></li>');
                            }else{

                                $('#sections_checkboxes').append('<li><input type="radio" name="section" value="'+ data.success[i].id +'"><label for="f-option">' + data.success[i].name + '</label><div class="check"></div></li>');
                            }
                         
                        }
                    }
            });
            $('#dish_section').css('display','block');

        });



    function addRow (){
      var test= $('#newRow').clone();
      test.appendTo('#AddonSection');
      test.css('display','block')


    };

    </script>
@endpush


