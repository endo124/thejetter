@if ($lang == 'en')
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr">
        <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
            <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
            <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
            <meta name="color-scheme" content="light dark">

            <meta name="supported-color-schemes" content="light dark">

            <style type="text/css">
            :root
            {
                Color-scheme: light dark;
                supported-color-schemes:light dark;
            }
            </style>
            <style type="text/css">

                @media (prefers-color-scheme: dark )
                {
                    body
                    {
                        background-color: white !important;;
                    }

                    .wrapper
                    {
                        background-color: white !important;;
                    }

                    h2 , p
                    {
                        color: black !important;
                    }
                }

                @media (prefers-color-scheme: light )
                {
                    body
                    {
                        background-color: white !important;
                    }

                    .wrapper
                    {
                        background-color: white !important;
                    }

                    h2 , p
                    {
                        color: black !important;
                    }
                }

                @media only screen and (min-width: 480px)
                {
                    .webkit
                    {
                        max-width: 360px;
                    }
                }
            </style>
        </head>
        <body style="font-family: Verdana, sans-serif;margin: 0;padding: 0;" dir="ltr">
            <center class="wrapper" style="width: 100%;">
                <div class="webkit" dir="ltr">
                    <img style="border: 0;" src="https://i.imgur.com/Bwht5FJ.jpeg" width="100%"/>
                    <img style="border: 0;margin-top: 12px;" src="https://i.imgur.com/EuCDdqm.png" width="74"/>
                    <h2 style="font-weight: bold;margin: 8px;">Thank you....</h2>
                    <p style="margin: 0 12px;">
                        Hello {{ $user->name }},<br />
                        this is the jet speaking ;We’d like to thank you for joining us on this trip and we are looking forward to seeing you on board again in the near future.<br />
                        Have a nice day<br />
                    </p>
                    <a href="http://onelink.to/thejet"><button style="cursor: pointer;background-color: #1abaff;border: none;color: white;padding: 8px 32px;font-size: 16px;border-radius: 50px;margin: 38px 0px;"> Open the app</button></a>
                    <div style="background-color: #06005a;padding: 12px;">
                        <table style="width: 100%;border-spacing: 0;">
                            <thead>
                                <tr>
                                    <th><a href="https://facebook.com/profile.php?id=106474654609586&ref=content_filter"><img src="https://i.imgur.com/W5VGJyX.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
                                    <th><a href="https://www.instagram.com/thejet.sa/"><img src="https://i.imgur.com/sxhrnVJ.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
                                    <th><a href="https://twitter.com/SaThejet"><img src="https://i.imgur.com/M31yTaD.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
                                </tr>
                            </thead>
                        </table>
                        <a href="https://thejet.com.sa" style="color: #1abaff;text-decoration: none;margin-top: 8px;display: block;font-size: 0.75em;">www.thejet.com.sa</a>
                    </div>
                </div>
            </center>
        </body>
    </html>
@else
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//AR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl" lang="ar">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
		<meta name="color-scheme" content="light dark">

		<meta name="supported-color-schemes" content="light dark">

		<style type="text/css">
		:root
		{
			Color-scheme: light dark;
			supported-color-schemes:light dark;
		}
		</style>
		<style type="text/css">

			@media (prefers-color-scheme: dark )
			{
				body
				{
					background-color: white !important;;
				}

				.wrapper
				{
					background-color: white !important;;
				}

				h2 , p
				{
					color: black !important;
				}
			}

			@media (prefers-color-scheme: light )
			{
				body
				{
					background-color: white !important;
				}

				.wrapper
				{
					background-color: white !important;
				}

				h2 , p
				{
					color: black !important;
				}
			}

			@media only screen and (min-width: 480px)
			{
				.webkit
				{
					max-width: 360px;
				}
			}
		</style>
	</head>
	<body style="font-family: Verdana, sans-serif;margin: 0;padding: 0;" dir="rtl">
		<center class="wrapper" style="width: 100%">
			<div class="webkit" dir="rtl">
				<img style="border: 0;" src="https://i.imgur.com/jmDYQBw.jpeg" width="100%"/>
				<img style="border: 0;margin-top: 12px;" src="https://i.imgur.com/EuCDdqm.png" width="74"/>
				<h2 style="font-weight: bold;margin: 8px;">شكرا لك ....</h2>
				<p style="margin: 0 12px;">
					اهلا  {{ $user->name }}<br />
					ذا جيت تتحدث ؛ نود ان نشكرك لإنضمامك معنا في هذه الرحلة ونتمنى أن نراك على خطوط ذا جيت قريبا.<br />
					نتمنى لك يوم سعيدا.<br />
				</p>
                <a href="http://onelink.to/thejet"><button style="cursor: pointer;background-color: #1abaff;border: none;color: white;padding: 8px 32px;font-size: 16px;border-radius: 50px;margin: 38px 0px;"> فتح التطبيق</button></a>
				<div style="background-color: #06005a;padding: 12px;">
					<table style="width: 100%;border-spacing: 0;">
						<thead>
							<tr>
								<th><a href="https://facebook.com/profile.php?id=106474654609586&ref=content_filter"><img src="https://i.imgur.com/W5VGJyX.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
								<th><a href="https://www.instagram.com/thejet.sa/"><img src="https://i.imgur.com/sxhrnVJ.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
								<th><a href="https://twitter.com/SaThejet"><img src="https://i.imgur.com/M31yTaD.png" style="border: 0;background-color: transparent;" width="28"/></a></th>
							</tr>
						</thead>
					</table>
					<a href="https://www.thejet.com.sa" style="color: #1abaff;text-decoration: none;margin-top: 8px;display: block;font-size: 0.75em;">www.thejet.com.sa</a>
				</div>
			</div>
		</center>
	</body>
</html>
@endif





