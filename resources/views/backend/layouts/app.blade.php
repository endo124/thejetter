<!DOCTYPE html>
<html dir="ltr">
{{-- <html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}"> --}}

<!-- Mirrored from dreamguys.co.in/demo/doccure/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:10 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>theJet _ shabab</title>
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

		<!-- Bootstrap CSS -->



<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-storage.js"></script>
<script src="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/node-uuid/1.4.8/uuid.min.js"></script>


        <link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/feathericon.min.css') }}">

        {{-- <link rel="stylesheet" href="{{ asset('backend/plugins/morris/morris.css') }}"> --}}
          <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Google Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
        <!-- Bootstrap core CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">

        <script src="https://maps.googleapis.com/maps/api/js?key={{ $googleapikey ?? '' }}&callback=initMap&libraries=&v=weekly"defer></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

        <!-- Your custom styles (optional) -->
        {{-- <link href="{{ asset('/front/css/simple-sidebar.css') }}" rel="stylesheet"> --}}

        @stack('style')

		<!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">

    <style>
        .disabled{
            pointer-events: none;
        }
        .actions{
                display: flex;
        }
        /* .dropdown-menu{
            top:inherit !important;
        }
        .dropdown-menu:active{
            top:inherit !important;
        } */
        .notifications{
            top:0 !important;

        }
    </style>

    </head>
    <body>

		<!-- Main Wrapper -->
        <div class="main-wrapper">


            @include('backend.partials.header')


            @include('backend.partials.sidebar')


            @yield('content')


        </div>
		<!-- /Main Wrapper -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>


        <!-- Latest compiled JavaScript -->
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}

		<!-- Bootstrap Core JS -->
        <script src="{{ asset('backend/js/popper.min.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>

		<!-- Slimscroll JS -->
        <script src="{{ asset('backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

		<script src="{{ asset('backend/plugins/raphael/raphael.min.js') }}"></script>
		{{-- {{-- <script src="{{ asset('backend/plugins/morris/morris.min.js') }}"></script> --}}
		{{-- <script src="{{ asset('backend/js/chart.morris.js') }}"></script>؟ --}}

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


        @stack('scripts')




		<!-- Custom JS -->
        <script  src="{{ asset('backend/js/script.js') }}"></script>


        <script>
             const base_Url = `{{ Config::get('app.App_URL') }}`;
        </script>

    <script src='/chat/js/app.js'></script>

    {{-- <script src='/chat/js/vendor.php'></script> --}}

    {{-- <script src='/chat/js/admin.php'></script> --}}

@if(!auth()->guard('admin')->user()->hasRole('cook'))

    <script>//admin



        $('.noti-dropdown').click(function(){
                console.log('a');
            var  uid =<?php echo json_encode(auth('admin')->user()->id); ?>;
            var user_ref= db.ref(`/new_user`);
            user_ref.once('value', function(user_snapshot) {
                user_snapshot.forEach((user_data) => {
                console.log(user_data,'hgh')
                var ref = db.ref(`/msgs/${user_data.key}`);
                array=[];

                    ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                        snapshot.forEach((data) => {

                            if(!array.includes(data.key)&&data.val().email != 'admin@gmail.com'){


                                var name=data.val().name;
                                var msg=data.val().text;
                                var date=data.val().date;
                                date=new Date(parseInt(date)).toLocaleString()
                                var notify=`<a href="#" id="`+data.key+`">
                                                <div class="media">

                                                    <div class="media-body">
                                                        <p class="noti-details"><span class="noti-title">`+name+`</span>   massage <span class="noti-title">`+msg+`</span></p>
                                                        <p class="noti-time">`+date+`<span class="notification-time"></span></p>
                                                    </div>
                                                </div>
                                            </a>`;

                                $('.notification-message').append(notify);


                                data.ref.update({
                                    'read':1
                                });
                                $('#notif').html(0);

                            }

                        });
                    });


            });
            });
        });




        let email = "";
        let name = "";
        const msgScreen = document.getElementById("messages");
        const userScreen = document.getElementById("users");
        const msgForm = document.getElementById("messageForm");
        const msgInput = document.getElementById("msg-input");
        const msgBtn = document.getElementById("msg-btn");
        const userName = document.getElementById("user-name");
        const db = firebase.database();
        const msgRef = db.ref("/msgs"); //save in msgs folder in database


        let user_id = window.location.hash.substr(1);
        let uid=<?php echo json_encode(auth('admin')->user()->id); ?>;
        function init(){


                // User is signed in. Get their name.

                email =<?php echo json_encode(auth('admin')->user()->email); ?>;
                if(user_id)
                {
                    db.ref(`/msgs/${user_id}`).on('child_added', updateMsgs);
                    document.getElementById('type_msgInput').style.display='block';

                }
                else
                {
                    // document.getElementById('type_msgInput').style.display='none';

                }
                // document.getElementById("chat-window").scrollTop = document.getElementById("chat-window").scrollHeight;



                db.ref('/new_user').orderByChild('date').on('value', updateUser);


                db.ref(`/new_user`).on('value', updatNotifi);




            msgForm.addEventListener('submit', sendMessage);
        }


        const updatNotifi = data =>{
            let array=[];
            data.forEach(element => {
                var ref = db.ref(`/msgs/${element.key}`);
            ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                snapshot.forEach((data) => {

                if(!array.includes(data.key)&&data.val().email != 'admin@gmail.com'){

                    $('#notif').html(array.length+1);
                    array.push(data.key);
                }
                });
            });
            });
        };


        function updateId(id)
        {

            var uri='https://thejet.com.sa/en/shabab/support#'+id;

            // let route=<?p#p echo json_encode(\Request::url()); ?>

            // window.open(route+'#'+id,'_self')
            // alert(route+'#'+id)
            // window.location =actual_link+'#'+id;
            window.location =uri;
            window.location.reload()

        }

        const updateMsgNotify = data =>{
        console.log(data.val(),'lllllllllll');
        }

        const updateUser = data =>{
            userScreen.innerHTML=''
            let dataValue=data.val()
            let array_data=[];




            for (var prop in dataValue )

            {

                array_data.push({'uid':dataValue[prop].uid,'date':dataValue[prop].date,'text':dataValue[prop].text,'name':dataValue[prop].name,'email':dataValue[prop].email,'read':0})
                // var counter = $('#notif').html();
                // $('#notif').html(counter+1);
            }


            let test=  array_data.sort(function($a,$b) {
            return $b['date'] - $a['date'];
            });

            array_data.forEach(prop => {
                var encryptMode = fetchJson();
                    var outputText =prop.text;

                    if(encryptMode == "nr"){
                    outputText = normalEncrypt(outputText);
                    }else if(encryptMode == "cr"){
                    outputText = crazyEncrypt(outputText);
                    }

                    var user =  ` <a onclick="updateId('${prop.uid}')"><div class="chat_list">
                    <div class="chat_people">
                    <div class="chat_img"> </div>
                    <div class="chat_ib">
                    <h5>ID:${prop.uid} ${prop.email}</h5>
                        <h5> ${prop.name}</h5>
                        <p>${outputText}</p>
                    </div>
                    </div>
                </div></a>`

                userScreen.innerHTML += user;
            });


        }
        const updateMsgs = data =>{
        const {email: userEmail , name, text,date} = data.val();

        //Check the encrypting mode
        var encryptMode = fetchJson();
        var outputText = text;

        if(encryptMode == "nr"){
            outputText = normalEncrypt(outputText);
        }else if(encryptMode == "cr"){
            outputText = crazyEncrypt(outputText);
        }


            // alert(date)
        if(email == userEmail)
        {
            //   alert(new Date(date).toLocaleString())
                var msg = ` <div class="incoming_msg">

                <div class="received_msg">
                <div class="received_withd_msg">
                <i class = "name">Admin: </i>
                    <p>${outputText}</p>
                    <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}  </span>
                </div>
                </div>
            </div>`


        }
        else
        {
            var msg = ` <div class="outgoing_msg">

            <div class="received_msg">
            <div class="sent_msg">
            <i class = "name">${name}: </i>
                <p>${outputText}</p>
                <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}   </span>
            </div>
            </div>
            </div>`


        }

        msgScreen.innerHTML += msg;
        document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;


        }

        function sendMessage(e){
            e.preventDefault();
            const text = msgInput.value;

            if(!text.trim()) return alert('Please type your message.'); //no msg submitted
            const msg = {
                email,
                uid,
                name,
                text: text,
                read:0,
                date:Date.now()
            };

            db.ref(`/msgs/${user_id}`).push(msg);
            msgInput.value = "";



        }

        function fetchJson(){
            var settings = JSON.parse(localStorage.getItem('settings'));
            return settings;
        }


        function crazyEncrypt(text){
        var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
        var newWord = '';
        var newArr =[];

        words.map(function(w) {
            if(w.length > 1){
            w.split('').map(function() {
                var hash = Math.floor(Math.random() * w.length);
                newWord += w[hash];
                w = w.replace(w.charAt(hash), '');
            });
            newArr.push(newWord);
            newWord = '';

            }else{
            newArr.push(w);
            }
        });
        text = newArr.join(' ');
        return text;
        }

        //Normal encryption - first and last letter fixed position
        function normalEncrypt(text){
        var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
        var newWord = '';
        var newArr =[];

        words.map(function(w) {
            if(w.length > 1){
            var lastIndex = w.length-1;
            var lastLetter = w[lastIndex];

            //add the first letter
            newWord += w[0];
            w = w.slice(1,lastIndex);

            //scramble only letters in between the first and last letter
            w.split('').map(function(x) {
                var hash = Math.floor(Math.random() * w.length);
                newWord += w[hash];
                w = w.replace(w.charAt(hash), '');
            });

            //add the last letter
            newWord+=lastLetter;
            newArr.push(newWord);
            newWord = '';
            }else{
            newArr.push(w);
            }
        });
        text = newArr.join(' ');
        return text;
        }
        document.addEventListener('DOMContentLoaded',init);
    </script>
@else

    <script>


            $('.noti-dropdown').click(function(){
                console.log('a');
            var  uid =<?php echo json_encode(auth('admin')->user()->id); ?>;
                var ref = db.ref(`/msgs/${uid}`);
                array=[];
                    ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                        snapshot.forEach((data) => {
                            if(!array.includes(data.key)&&data.val().email == 'admin@gmail.com'){

                                var name='admin';
                                var msg=data.val().text;
                                var date=data.val().date;
                                date=new Date(parseInt(date)).toLocaleString()
                                var notify=`<a href="#" id="`+data.key+`">
                                                <div class="media">

                                                    <div class="media-body">
                                                        <p class="noti-details"><span class="noti-title">`+name+`</span>   massage <span class="noti-title">`+msg+`</span></p>
                                                        <p class="noti-time">`+date+`<span class="notification-time"></span></p>
                                                    </div>
                                                </div>
                                            </a>`;

                                $('.notification-message').append(notify);
                            }
                        });
                    });






                db.ref(`/msgs/${uid}`).once('value', function(snapshot) {
                snapshot.forEach((data) => {

                    data.ref.update({
                        'read':1
                    });
                });

                $('#notif').html(0);



            });
            });





            let email = "";
            let name = "";
            let uid=""
            const msgScreen = document.getElementById("messages");
            const msgForm = document.getElementById("messageForm");
            const msgInput = document.getElementById("msg-input");
            const msgBtn = document.getElementById("msg-btn");
            const userName = document.getElementById("user-name");
            const db = firebase.database();
            const msgRef = db.ref("/msgs"); //save in msgs folder in database

            function init(){


                    // User is signed in. Get their name.
                    email =<?php echo json_encode(auth('admin')->user()->email); ?>;
                    uid =<?php echo json_encode(auth('admin')->user()->id); ?>;

                    db.ref(`/msgs/${uid}`).on('child_added', updateMsgs);
                    db.ref(`/msgs/${uid}`).on('child_added', updatNotifi);







                msgForm.addEventListener('submit', sendMessage);
            }


            const updatNotifi = data =>{
                let array=[];
                var ref = db.ref(`/msgs/${uid}`);
                ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                    snapshot.forEach((data) => {
                    if(!array.includes(data.key)&&data.val().email == 'admin@gmail.com'){

                        $('#notif').html(array.length+1);
                        array.push(data.key);
                    }
                    });
                });
                }


            const updateMsgs = data =>{
            const {email: userEmail , name, text,date} = data.val();

            //Check the encrypting mode
            var encryptMode = fetchJson();
            var outputText = text;

            if(encryptMode == "nr"){
                outputText = normalEncrypt(outputText);
            }else if(encryptMode == "cr"){
                outputText = crazyEncrypt(outputText);
            }



            if(email == userEmail)
            {
                    var msg = ` <div class="incoming_msg">

                    <div class="received_msg">
                    <div class="received_withd_msg">
                    <i class = "name">${email}: </i>
                        <p>${outputText}</p>
                        <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}   </span>
                    </div>
                    </div>
                </div>`


            }
            else
            {
                var msg = ` <div class="outgoing_msg">

                <div class="received_msg">
                <div class="sent_msg">
                <i class = "name">Admin </i>
                    <p>${outputText}</p>
                    <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}   </span>
                </div>
                </div>
            </div>`





            }

            msgScreen.innerHTML += msg;
            document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;

            }

            function sendMessage(e){
            e.preventDefault();
            const text = msgInput.value;

                if(!text.trim()) return alert('Please type your message.'); //no msg submitted
                const msg = {
                    email,
                    name:email,
                    text: text,
                    read:0,
                    date:(Date.now())
                };

                db.ref(`/msgs/${uid}`).push(msg);
                db.ref(`/new_user/${uid}`).update({'name':email,'email':email, 'text': text, 'date':String(Date.now()),'uid':uid,'read':0})
                msgInput.value = "";

            }

            //Get encryption settings
            function fetchJson(){
            var settings = JSON.parse(localStorage.getItem('settings'));
            return settings;
            }


            function crazyEncrypt(text){
            var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
            var newWord = '';
            var newArr =[];

            words.map(function(w) {
                if(w.length > 1){
                w.split('').map(function() {
                    var hash = Math.floor(Math.random() * w.length);
                    newWord += w[hash];
                    w = w.replace(w.charAt(hash), '');
                });
                newArr.push(newWord);
                newWord = '';

                }else{
                newArr.push(w);
                }
            });
            text = newArr.join(' ');
            return text;
            }

            //Normal encryption - first and last letter fixed position
            function normalEncrypt(text){
            var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
            var newWord = '';
            var newArr =[];

            words.map(function(w) {
                if(w.length > 1){
                var lastIndex = w.length-1;
                var lastLetter = w[lastIndex];

                //add the first letter
                newWord += w[0];
                w = w.slice(1,lastIndex);

                //scramble only letters in between the first and last letter
                w.split('').map(function(x) {
                    var hash = Math.floor(Math.random() * w.length);
                    newWord += w[hash];
                    w = w.replace(w.charAt(hash), '');
                });

                //add the last letter
                newWord+=lastLetter;
                newArr.push(newWord);
                newWord = '';
                }else{
                newArr.push(w);
                }
            });
            text = newArr.join(' ');
            return text;
            }
            document.addEventListener('DOMContentLoaded',init);
    </script>
@endif





    <script>
    $('.dropdown-menu').css('top','inhirt');


    </script>
    </body>

</html>
