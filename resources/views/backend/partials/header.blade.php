<!-- Header -->
<div class="header">

    <!-- Logo -->
    <div class="header-left mt-1">
        <a href="#" class="logo" style="color: #fff">
			<img class="img-fluid" src="{{ asset('frontend/images/logo.png') }}" alt="Logo">
            {{-- <img src="{{ asset('front/images/msol-logo.png') }}" width="150" alt="Logo"> --}}
        </a>
        <a href="#" class="logo logo-small">
            {{-- <img src="{{ asset('front/images/msol-logo.png') }}" alt="Logo" width="30" height="30"> --}}
        </a>
    </div>
    <!-- /Logo -->

    <a href="javascript:void(0);" id="toggle_btn" style="color: #fff">
        <i class="fe fe-text-align-left" ></i>
    </a>

    {{-- <div class="top-nav-search">
        <form>
            <input type="text" class="form-control" placeholder="Search here">
            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div> --}}

    <!-- Mobile Menu Toggle -->
    <a class="mobile_btn" id="mobile_btn">
        <i class="fa fa-bars"></i>
    </a>
    <!-- /Mobile Menu Toggle -->

    <!-- Header Right Menu -->
    <ul class="nav user-menu">
        <ul style="list-style: none;align-items: center;display: flex;">
            {{-- @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties) --}}
                @if(LaravelLocalization::getCurrentLocale() == 'ar')
                    <li style="display: block">
                        <a rel="alternate" hreflang="en" href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}" style="color: #fff">
                            EN
                        </a>
                    </li>
                @else
                    <li style="display: block">
                        <a rel="alternate" hreflang="ar" href="{{ LaravelLocalization::getLocalizedURL('ar', null, [], true) }}" style="color: #fff">
                            عربي
                        </a>
                    </li>
                @endif

            {{-- @endforeach --}}
        </ul>
        <!-- Notifications -->
        <li class="nav-item dropdown noti-dropdown">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <i class="fe fe-bell"></i> <span class="badge badge-pill " id="notif"></span>
            </a>
            <div class="dropdown-menu notifications" style="top:inherit !important">
                <div class="topnav-dropdown-header">
                    <span class="notification-title">Notifications</span>
                    {{-- <a href="javascript:void(0)" class="clear-noti"> Clear All </a> --}}
                </div>
                <div class="noti-content">

                    <ul class="notification-list">

                        <li class="notification-message">

                        </li>

                    </ul>
                </div>
                {{-- <div class="topnav-dropdown-footer">
                    <a href="#">View all Notifications</a>
                </div> --}}
            </div>
        </li>
        <!-- /Notifications -->

        <!-- User Menu -->
        <li class="nav-item dropdown has-arrow">
            @if (auth()->guard('admin')->user()->hasRole('cook'))
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <span class="user-img">
                        {{-- <img class="rounded-circle" src="{{ asset('backend/img/cook/'.auth()->guard('admin')->user()->images) }}" width="31"> --}}
                        <h4 style="color: #fff">{{ auth()->guard('admin')->user()->name }}</h4>
                    </span>
                </a>
            @else
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <span class="user-img">
                    {{-- <img class="rounded-circle" src="{{ asset('backend/img/profiles/avatar-01.jpg') }}" width="31" alt="Ryan Taylor"> --}}
                    <h4 style="color: #fff">{{ auth()->guard('admin')->user()->name }}</h4>

                </span>
            </a>
            @endif

            <div class="dropdown-menu">
                <div class="user-header" style="display: flex; align-items: center;}">
                    @if (auth()->guard('admin')->user()->hasRole('cook'))
                    <div class="avatar avatar-sm">
                        <img src="{{ asset('backend/img/cook/'.auth()->guard('admin')->user()->images) }}" alt="User " class="avatar-img rounded-circle">
                    </div>

                    @else
                        <div class="avatar avatar-sm">
                            <img src="{{ asset('frontend/images/logo.png') }}" alt="User Image" class="avatar-img rounded-circle">
                        </div>
                    @endif
                    <div class="user-text">
                        <h6>{{ auth()->guard('admin')->user()->name }}</h6>
                        {{-- <p class="text-muted mb-0">{{ auth()->guard('admin')->user()->roles[0]->display_name }}</p> --}}
                    </div>
                </div>
                @if (auth()->guard('admin')->user()->hasRole('cook'))

                    <a class="dropdown-item" href="{{ auth()->guard('admin')->user()? url('shabab/cook/{id}/edit') : '' }}">@lang('site.My Profile')</a>
                    {{-- <a class="dropdown-item" href="settings.html">Settings</a> --}}
                @endif
                <form action="{{ url('shabab/logout') }}" method="post">
                    @csrf
                    <button type="submit" class="dropdown-item" >@lang('site.Logout')</button>
                </form>
            </div>
        </li>
        <!-- /User Menu -->

    </ul>
    <!-- /Header Right Menu -->

</div>
<!-- /Header -->
