<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>The Jet</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/intlTelInput.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/reset.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/plugins.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/color.css')}}">
        <link rel="stylesheet" href="{{ asset('frontend/css/owl-carousel.css') }}">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="{{ asset('frontend/images/LOGO 3.png') }}">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <!--loader end-->

        @include('frontend.partials.header')

        <!-- main start  -->
        <div id="main">



            @yield('content')


            <!--map-modal -->
            <div class="map-modal-wrap">
                <div class="map-modal-wrap-overlay"></div>
                <div class="map-modal-item">
                    <div class="map-modal-container fl-wrap">
                        <div class="map-modal fl-wrap">
                            <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
                        </div>
                        <h3><span>Location for : </span><a href="#">Listing Title</a></h3>
                        <div class="map-modal-close"><i class="fal fa-times"></i></div>
                    </div>
                </div>
            </div>
            <!--map-modal end -->
            <!--register form -->
            <div class="main-register-wrap modal">
                <div class="reg-overlay"></div>
                <div class="main-register-holder tabs-act">
                    <div class="main-register fl-wrap  modal_main">
                        <div class="main-register_title">Welcome to <span><strong>The</strong>Jetter<strong>.</strong></span></div>
                        <div class="close-reg"><i class="fal fa-times"></i></div>
                        <ul class="tabs-menu fl-wrap no-list-style">
                            <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Login</a></li>
                            <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Register</a></li>
                        </ul>
                        <!--tabs -->
                        <div class="tabs-container">
                            <div class="tab">
                                <!--tab -->
                                <div id="tab-1" class="tab-content first-tab">
                                    <div class="custom-form">
                                        <form method="post" action="{{ url('/login') }}"  name="registerform">
                                            @csrf
                                            <label>Email Address <span>*</span> </label>
                                            <input name="email" type="text"   onClick="this.select()" value="">
                                            <label >Password <span>*</span> </label>
                                            <input name="password" type="password"   onClick="this.select()" value="" >
                                            <button type="submit"  class="btn float-btn color2-bg"> Log In <i class="fas fa-caret-right"></i></button>
                                            <div class="clearfix"></div>
                                            {{-- <div class="filter-tags">
                                                <input id="check-a3" type="checkbox" name="check">
                                                <label for="check-a3">Remember me</label>
                                            </div> --}}
                                        </form>
                                        {{-- <div class="lost_password">
                                            <a href="#">Lost Your Password?</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <!--tab end -->
                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-2" class="tab-content">
                                        <div class="custom-form">
                                            <form method="post"  action="{{ route('checkregister') }}"  name="registerform" class="main-register-form" id="main-register-form2">
                                                @csrf
                                                <label >Full Name <span>*</span> </label>
                                                <input name="name" type="text"   onClick="this.select()" value="">
                                                <label >Phone No. <span>*</span> </label>
                                                <input name="phone" type="text"   onClick="this.select()" value="">
                                                <label>Email Address <span>*</span></label>
                                                <input name="email" type="text"  onClick="this.select()" value="">
                                                <label >Password <span>*</span></label>
                                                <input name="password" type="password"   onClick="this.select()" value="" >
                                                <label >Password Confirmation<span>*</span></label>
                                                <input class="form-control" name="password_confirmation" type="password" onClick="this.select()" value="" >
                                                {{-- <div class="filter-tags ft-list">
                                                    <input id="check-a2" type="checkbox" name="check">
                                                    <label for="check-a2">I agree to the <a href="#">Privacy Policy</a></label>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="filter-tags ft-list">
                                                    <input id="check-a" type="checkbox" name="check">
                                                    <label for="check-a">I agree to the <a href="#">Terms and Conditions</a></label>
                                                </div> --}}


                                                <input type="tel" id="demo" placeholder="" id="telephone">
                                                <input type="text" id="code" />

                                                <!-- Add two buttons to submit the inputs -->
                                                <a id="sign-in-button" onclick="submitPhoneNumberAuth()">
                                                  SIGN IN WITH PHONE
                                                </a>
                                                <a id="confirm-code" onclick="submitPhoneNumberAuthCode()">
                                                  ENTER CODE
                                                </a>
                                                  <!-- Add a container for reCaptcha -->
                                                <div id="recaptcha-container"></div>
                                                <div class="clearfix"></div>
                                                <button type="submit"     class="btn float-btn color2-bg"> Register  <i class="fas fa-caret-right"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!--tab end -->
                            </div>
                            <!--tabs end -->
                            {{-- <div class="log-separator fl-wrap"><span>or</span></div>
                            <div class="soc-log fl-wrap">
                                <p>For faster login or register use your social account.</p>
                                <a href="#" class="facebook-log"> Facebook</a>
                            </div> --}}
                            <div class="wave-bg">
                                <div class='wave -one'></div>
                                <div class='wave -two'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--register form end -->
            <a class="to-top"><i class="fas fa-caret-up"></i></a>

        @include('frontend.partials.footer')

        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        {{-- <script src="{{asset('frontend/js/intlTelInput.min.js')}}"></script> --}}

        <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
        {{-- <script src="{{asset('frontend/js/intlTelInput-jquery.min.js')}}"></script> --}}

        <script src="{{ asset('frontend/js/owl-carousel.js') }}"></script>

        <script src="{{asset('frontend/js/plugins.js')}}"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
        <script src="{{asset('frontend/js/scripts.js')}}"></script>
        {{-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY_HERE&libraries=places&callback=initAutocomplete"></script> --}}
        {{-- <script src="{{asset('frontend/js/map-single.js')}}"></script> --}}
        @stack('scripts')



    {{-- <!-- Add the latest firebase dependecies from CDN -->
    <script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-auth.js"></script>
        <script>
// Vanilla Javascript
var input = document.querySelector("#telephone");
window.intlTelInput(input,({
  // options here
}));

// jQuery
$("#telephone").intlTelInput({
  // options here
});
// destroy
instance.destroy();

// Get the extension part of the current number
var extension = instance.getExtension();

// Get the current number in the given format
var intlNumber = instance.getNumber();

// Get the type (fixed-line/mobile/toll-free etc) of the current number.
var numberType = instance.getNumberType();

// Get the country data for the currently selected flag.
var countryData = instance.getSelectedCountryData();

// Get more information about a validation error.
var error = instance.get<a href="https://www.jqueryscript.net/tags.php?/Validation/">Validation</a>Error();

// Vali<a href="https://www.jqueryscript.net/time-clock/">date</a> the current number
var isValid = instance.isValidNumber();

// Change the country selection
instance.selectCountry("gb");

// Insert a number, and update the selected flag accordingly.
instance.setNumber("+44 7733 123 456");

// Change the placeholderNumberType option.
instance..setPlaceholderNumberType("FIXED_LINE");

// Load the utils.js script (included in the lib directory) to enable formatting/validation etc.
window.intlTelInputGlobals.loadUtils("build/js/utils.js");

// Get all the country data
var countryData = window.intlTelInputGlobals.getCountryData();
input.addEventListener("countrychange", function() {
  // do something with iti.getSelectedCountryData()
});

input.addEventListener("open:countrydropdown", function() {
  // triggered when the user opens the dropdown
});

input.addEventListener("close:countrydropdown", function() {
  // triggered when the user closes the dropdown
});
        </script>

    <script>
        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        var firebaseConfig = {
        apiKey: "AIzaSyDUwgwD98FloIpnnu-b6HNyO1xhsZwmPqk",
        authDomain: "otpproject-a5c0e.firebaseapp.com",
        projectId: "otpproject-a5c0e",
        storageBucket: "otpproject-a5c0e.appspot.com",
        messagingSenderId: "666656215817",
        appId: "1:666656215817:web:0cc07797f7e654cb13b9d8",
        measurementId: "G-12Z22EC2G1"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    ​
    </script>
    <script>
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
        "recaptcha-container",
        {
        size: "normal",
        callback: function(response) {
            submitPhoneNumberAuth();
        }
        }
    );
    ​
    function submitPhoneNumberAuth() {
        let tel=document.getElementById('phoneNumber').value
        // We are using the test phone numbers we created before
        // var phoneNumber = document.getElementById("phoneNumber").value;
        var phoneNumber =tel;
        var appVerifier = window.recaptchaVerifier;
        firebase
        .auth()
        .signInWithPhoneNumber(phoneNumber, appVerifier)
        .then(function(confirmationResult) {
            window.confirmationResult = confirmationResult;
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    ​
    // This function runs when the 'confirm-code' button is clicked
    // Takes the value from the 'code' input and submits the code to verify the phone number
    // Return a user object if the authentication was successful, and auth is complete
    function submitPhoneNumberAuthCode() {
        // We are using the test code we created before
        // var code = document.getElementById("code").value;
        let code=document.getElementById('code').value
        confirmationResult
        .confirm(code)
        .then(function(result) {
            var user = result.user;
            console.log(user);
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    </script> --}}
    </body>
</html>
