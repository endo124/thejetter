<!-- Sidebar -->
<div class="sidebar" id="sidebar" style="overflow: auto;">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                {{-- <li class="menu-title">
                    <span>Main</span>
                </li> --}}
                <li class="">
                    <a href="/shabab"><i class="fe fe-home"></i> <span>shabab</span></a>
                </li>

                @if (auth()->guard('admin')->user()->hasRole('super_admin') ||auth()->guard('admin')->user()->hasRole('admin'))
                    @if (auth()->guard('admin')->user()->hasPermission('admins_read'))
                    <li class="{{ Request::segment(3) == 'admin' ? 'active' : null }}">
                        <a href="{{ url('/shabab/admin') }}"><i class="fe fe-users"></i><span>Admins</span></a>
                    </li>
                    @endif
                    <li class="submenu">
                        <a href="#"><i class="fa fa-database " aria-hidden="true"></i><span> Catalog </span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            @if (auth()->guard('admin')->user()->hasPermission('cusines_read'))
                                <li class="{{ Request::segment(3) == 'cusine' ? 'active' : null }}">
                                    <a href="{{ url('/shabab/cusine') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>cusine</span></a>
                                </li>
                            @endif
                            @if (auth()->guard('admin')->user()->hasPermission('sections_read'))
                                <li class="{{ Request::segment(3) == 'section' ? 'active' : null }}">
                                    <a href="{{ url('/shabab/section') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Sections</span></a>
                                </li>
                            @endif

                            @if (auth()->guard('admin')->user()->hasPermission('allergenes_read'))
                                <li class="{{ Request::segment(3) == 'allergen' ? 'active' : null }}">
                                    <a href="{{ url('/shabab/allergen') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Allergen</span></a>
                                </li>
                            @endif
                            @if (auth()->guard('admin')->user()->hasPermission('categories_read'))
                                <li class="{{ Request::segment(3) == 'category' ? 'active' : null }}">
                                    <a href="{{ url('/shabab/category') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Categories</span></a>
                                </li>
                            @endif
                        </ul>
                    </li>


                    @if (auth()->guard('admin')->user()->hasPermission('cooks_read'))
                    <li class="{{ Request::segment(3) == 'cook' ? 'active' : null }}">
                        <a href="{{ url('/shabab/cook') }}"><i class="fe fe-user-plus"></i> <span>Vendors</span></a>
                    </li>
                    @endif
                    @if (auth()->guard('admin')->user()->hasPermission('coupons_read'))
                    <li class="{{ Request::segment(3) == 'discount' ? 'active' : null }}">
                        <a href="{{ url('/shabab/discount') }}"><i class="fa fa-gift" aria-hidden="true"></i><span>Coupons</span></a>
                    </li>
                    @endif
                    @if (auth()->guard('admin')->user()->hasPermission('customers_read'))
                    <li class="{{ Request::segment(3) == 'customer' ? 'active' : null }}">
                        <a href="{{ url('/shabab/customer') }}"><i class="fa fa-users" aria-hidden="true"></i><span>Customers</span></a>
                    </li>
                    @endif
                    @if (auth()->guard('admin')->user()->hasPermission('cities_read'))
                    <li class="{{ Request::segment(3) == 'city' ? 'active' : null }}">
                        <a href="{{ url('/shabab/city') }}"><i class="fa fa-university" aria-hidden="true"></i><span>Cities</span></a>
                    </li>
                    @endif
                    @if (auth()->guard('admin')->user()->hasPermission('settings_read'))
                    <li class="{{ Request::segment(3) == 'setting' ? 'active' : null }}">
                        <a href="{{ url('/shabab/setting') }}"><i class="fe fe-vector"></i> <span>Settings</span></a>
                    </li>
                    @endif
                    @if (auth()->guard('admin')->user()->hasPermission('VIP_read'))
                    <li class="{{ Request::segment(3) == 'vipCustomers' ? 'active' : null }}">
                        <a href="{{ url('/shabab/vipCustomers') }}"><i class="fe fe-user"></i> <span>VIP Customer</span></a>
                    </li>
                    @endif
                    {{-- @if (auth()->guard('admin')->user()->hasPermission('VIP_read'))
                    <li class="{{ Request::segment(1) == 'vipCooks' ? 'active' : null }}">
                        <a href="{{ url('/shabab/vipCooks') }}"><i class="fe fe-user"></i> <span>VIP Cooks</span></a>
                    </li>
                    @endif --}}
                    <li class="{{ Request::segment(3) == 'drivers' ? 'active' : null }}">
                        <a href="{{ url('/shabab/drivers') }}"><i class="fa fa-motorcycle" aria-hidden="true"></i><span> Driver Filter</span></a>
                    </li>
                    <li class="{{ Request::segment(3) == 'drivers' ? 'active' : null }}">
                        <a href="{{ url('/shabab/tracking-drivers') }}"><i class="fa fa-motorcycle" aria-hidden="true"></i><span>Tracking Drivers</span></a>
                    </li>
                @endif
                @if (auth()->guard('admin')->user()->hasRole('cook'))
                    <li class="{{ Request::segment(3) == 'addons' ? 'active' : null }}">
                        <a href="{{ url('/shabab/addons') }}"><i class="fa fa-plus-square" aria-hidden="true"></i><span>Addons</span></a>
                    </li>
                    <li class="{{ Request::segment(3) == 'addons_section' ? 'active' : null }}">
                        <a href="{{ url('/shabab/addons_section') }}"><i class="fa fa-plus-square" aria-hidden="true"></i><span>Addons Section</span></a>
                    </li>
                @endif
                @if (auth()->guard('admin')->user())
                <li class="{{ Request::segment(3) == 'order' ? 'active' : null }}">
                    <a href="{{ url('/shabab/order') }}"><i class="fa fa-bars" aria-hidden="true"></i><span>Orders</span></a>
                </li>
                @endif
                @if (auth()->guard('admin')->user())
                <li class="{{ Request::segment(3) == 'dish' ? 'active' : null }}">
                    <a href="{{ url('/shabab/dish') }}"><i class="fas fa-pizza-slice" style="font-size: 15px"></i> <span>Dishes</span></a>
                </li>
                @endif



                {{-- <li>
                    <a href="#"><i class="fe fe-star-o"></i> <span>Reviews</span></a>
                </li>
                <li>
                    <a href="#"><i class="fe fe-activity"></i> <span>Transactions</span></a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fe fe-document"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="invoice-report.html">Invoice Reports</a></li>
                    </ul>
                </li> --}}

            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
