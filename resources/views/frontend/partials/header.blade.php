 <!-- header -->
 <header class="main-header">
 
    <!-- logo-->
    <a href="#" class="logo-holder"><img width="400px" src="{{asset('frontend/images/LOGO 3.png')}}" alt=""></a>
    <!-- logo end-->
    <!-- header-search_btn-->
    <div class="header-search_btn show-search-button"><i class="fal fa-search"></i><span>Search</span></div>
    <!-- header-search_btn end-->
    <!-- header opt -->
    {{-- <a href="shabab-add-listing.html" class="add-list color-bg">Add Listing <span><i class="fal fa-layer-plus"></i></span></a> --}}
    @if (Auth::guard('customer')->check() )

    <div class="cart-btn   show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist"><i class="fal fa-heart"></i><span class="cart-counter green-bg"></span> </div>
        <div class="header-user-menu">
            <div class="header-user-name">
                <span><img src="{{asset(Auth::guard('customer')->user()->images ? 'backend/images/customers/'.Auth::guard('customer')->user()->images :  'backend/img/user.jpeg')  }}" alt=""></span>
                Hello , {{ Auth::guard('customer')->user()->name }}
            </div>
            <ul>
                <li><a href="{{ url('profile/') }}">profile</a></li>

                <li><a href="{{ url('logout/'.auth()->guard('customer')->user()->id) }}">Log Out</a></li>
            </ul>
        </div>
    @else
        <div class="show-reg-form modal-open avatar-img" data-srcav="images/avatar/3.jpg"><i class="fal fa-user"></i>Sign In</div>
    @endif
    <!-- header opt end-->
    <!-- lang-wrap-->
    <div class="lang-wrap">
        <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>En</strong></span><i class="fa fa-caret-down arrlan"></i></div>
        <ul class="lang-tooltip lang-action no-list-style">
            <li><a href="#" class="current-lan" data-lantext="En">English</a></li>
            <li><a href="#" data-lantext="AR">Arabic</a></li>

        </ul>
    </div>
    <!-- lang-wrap end-->
    <!-- nav-button-wrap-->
    <div class="nav-button-wrap color-bg">
        <div class="nav-button">
            <span></span><span></span><span></span>
        </div>
    </div>
    <!-- nav-button-wrap end-->
    <!--  navigation -->
    <div class="nav-holder main-menu">
        <nav>
            <ul class="no-list-style">
                <li>
                    <a href="{{ route('home') }}"  class="{{ Request::segment(1) == ''?  'act-link' : '' }}">Home</a>
                    <!--second level -->
                    {{-- <ul>
                        <li><a href="index.html">Parallax Image</a></li>
                        <li><a href="index2.html">Slider</a></li>
                        <li><a href="index3.html">Slideshow</a></li>
                        <li><a href="index4.html">Video</a></li>
                        <li><a href="index5.html">Map</a></li>
                    </ul> --}}
                    <!--second level end-->
                </li>
                {{-- <li>
                    <a href="#">Listings <i class="fa fa-caret-down"></i></a>
                    <!--second level -->
                    <ul>
                        <li><a href="listing.html">Column map</a></li>
                        <li><a href="listing2.html">Column map 2</a></li>
                        <li><a href="listing3.html">Fullwidth Map</a></li>
                        <li><a href="listing4.html">Fullwidth Map 2</a></li>
                        <li><a href="listing5.html">Without Map</a></li>
                        <li><a href="listing6.html">Without Map 2</a></li>
                        <li>
                            <a href="#">Single <i class="fa fa-caret-down"></i></a>
                            <!--third  level  -->
                            <ul>
                                <li><a href="listing-single.html">Style 1</a></li>
                                <li><a href="listing-single2.html">Style 2</a></li>
                                <li><a href="listing-single3.html">Style 3</a></li>
                                <li><a href="listing-single4.html">Style 4</a></li>
                            </ul>
                            <!--third  level end-->
                        </li>
                    </ul>
                    <!--second level end-->
                </li> --}}
                {{-- <li>
                    <a href="blog.html">News</a>
                </li> --}}
                <li>
                    <a href="{{ route('Restaurants.index') }}"  class="{{ Request::segment(1) == 'Restaurants'?  'act-link' : '' }}" >Restaurants</a>
                    <!--second level -->
                    {{-- <ul>
                        <li>
                            <a href="#">Shop<i class="fa fa-caret-down"></i></a>
                            <!--third  level  -->
                            <ul>
                                <li><a href="shop.html">Products</a></li>
                                <li><a href="product-single.html">Product single</a></li>
                                <li><a href="cart.html">Cart</a></li>
                            </ul>
                            <!--third  level end-->
                        </li>

                        <li><a href="author-single.html">User single</a></li>
                        <li><a href="help.html">How it Works</a></li>
                        <li><a href="booking.html">Booking</a></li>
                        <li><a href="pricing-tables.html">Pricing</a></li>
                        <li><a href="shabab.html">User Dasboard</a></li>
                        <li><a href="blog-single.html">Blog Single</a></li>
                        <li><a href="shabab-add-listing.html">Add Listing</a></li>
                        <li><a href="invoice.html">Invoice</a></li>
                        <li><a href="404.html">404</a></li>
                    </ul> --}}
                    <li><a href=" {{ Request::segment(1) == ''?  '#contactus' : route('contactus') }}" class="custom-scroll-link">How It Work</a></li>
                    <li><a href="{{ route('contact') }}" class="{{ Request::segment(1) == 'contact'?  'act-link' : '' }}">Contact Us</a></li>
                    <li><a href="{{ route('faq') }}" class="{{ Request::segment(1) == 'faq'?  'act-link' : '' }}">FAQ</a></li>
                    {{-- <li><a href="#">Cart</a></li> --}}

                    <!--second level end-->
                </li>
            </ul>
        </nav>
    </div>
    <!-- navigation  end -->
    <!-- header-search_container -->
    <div class="header-search_container header-search vis-search">
        <div class="container small-container">
            <div class="header-search-input-wrap fl-wrap">
                <!-- header-search-input -->
                <div class="header-search-input">
                    <label><i class="fal fa-keyboard"></i></label>
                    <input type="text" placeholder="What are you looking for ?"   value=""/>
                </div>
                <!-- header-search-input end -->
                <!-- header-search-input -->
                <div class="header-search-input location autocomplete-container">
                    <label><i class="fal fa-map-marker"></i></label>
                    <input type="text" placeholder="Location..." class="autocomplete-input" id="autocompleteid2" value=""/>
                    <a href="#"><i class="fal fa-dot-circle"></i></a>
                </div>
                <!-- header-search-input end -->
                <!-- header-search-input -->
                <div class="header-search-input header-search_selectinpt ">
                    <select data-placeholder="Category" class="chosen-select no-radius" >
                        <option>All Restaurants</option>
                        <option>Restaurants</option>
                        <option>Restaurants1</option>
                        <option>Restaurants2</option>
                        <option>Restaurants3</option>
                        <option>Restaurants4</option>
                        <option>Restaurants5</option>
                    </select>
                </div>
                <!-- header-search-input end -->
                <button class="header-search-button green-bg" onclick="window.location.href='#'"><i class="far fa-search"></i> Search </button>
            </div>
            <div class="header-search_close color-bg"><i class="fal fa-long-arrow-up"></i></div>
        </div>
    </div>
    <!-- header-search_container  end -->
    <!-- wishlist-wrap-->
    @if (auth('customer')->user())
    <div class="header-modal novis_wishlist">
        <!-- header-modal-container-->
        <div class="header-modal-container scrollbar-inner fl-wrap" data-simplebar>
            <!--widget-posts-->
            <div class="widget-posts  fl-wrap">
                <ul class="no-list-style">
                    @isset(auth('customer')->user()->favitems)
                        @foreach (auth('customer')->user()->favitems as $item)
                        @php
                            $vendor=App\Models\User::find($item->user_id);
                            $price=$dish->portions_price;
                            $min=$price[0];
                            foreach ($price as $pr) {
                                if($pr !=null){
                                    if ($min > $pr) {
                                    $min =$pr;
                                    }
                                }
                            }
                        @endphp
                            <li>
                                <div class="widget-posts-img"><a href="#"><img src={{asset('backend/img/dishes/'.$dish->images) }} alt=""></a>
                                </div>
                                <div class="widget-posts-descr">
                                    <h4><a href="#">{{ $item->name }}</a></h4>
                                    <div class="geodir-category-location fl-wrap"><a href="#"> {{ $vendor->name }}</a></div>
                                    <div class="widget-posts-descr-link"><a href="#" >{{ $min }} </a></div>
                                    {{-- <div class="widget-posts-descr-score">4.1</div> --}}
                                    <div title="{{ $item->id }}" class="clear-wishlist whishlist"><i class="fal fa-times-circle"></i></div>
                                </div>
                            </li>
                        @endforeach
                    @endisset


                </ul>
            </div>
            <!-- widget-posts end-->
        </div>
        <!-- header-modal-container end-->
        <div class="header-modal-top fl-wrap">
            <h4>Your Wishlist : <span><strong  id="totlafavitem"></strong> Dishes</span></h4>
            <div class="close-header-modal"><i class="far fa-times"></i></div>
        </div>
    </div>
    @endif

    <!--wishlist-wrap end -->
</header>
<!-- header end-->


@push('scripts')
    <script>
            //==================add to wishlist================//
    var totalfavs=$('#totlafavitem').html()

    $(".whishlist").click(function(e){
			e.preventDefault();

			var id = $(this).attr("title");

			$.ajax({
			type:'POST',
			url:'/fav/{{ Config::get('app.locale') }}',
			data:{
                "_token": "{{ csrf_token() }}",

                id:id },

			success:function(data){
                console.log(data);
					// var res=jQuery.inArray(id,localStorage);
					// var key='thejet_product_'+data.id;
					// var storage=localStorage['cart'];
					// if(storage.includes(key)){
					// 	$('#alert-cart').show();
					// 	$('#alert-cart').html(data.name+' already in cart ');
					// 	setTimeout(function() {
					// 			$('#alert-cart').fadeOut('fast');
					// 	}, 2000);
					// }else{
					// 	addToCart(data);
					// 	$('#alert-cart').show();
					// 	$('#alert-cart').html('added to cart succssfully');
					// 	setTimeout(function() {
					// 			$('#alert-cart').fadeOut('fast');
					// 	}, 2000);
					// 	$('.total-count').html('');
					// 	var total=JSON.parse(localStorage.getItem("cart"));
					// 	$('.total-count').html(JSON.parse(total.length));
					// }
				}
			});
            // $(this).parent().parent().remove();
            // totalfavs =totalfavs-1;
            // $('#totlafavitem').html(totalfavs);
		});
    </script>
@endpush
