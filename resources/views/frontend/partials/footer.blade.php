     <style>
         .footer-contacts li{
             float: left;
             margin-right: 10px !important;

         }
         .subfooter-nav ul li i{
            font-size: 19px
         }
     </style>
     <!--footer -->
     <footer class="main-footer fl-wrap">
        {{-- <!-- footer-header-->
        <div class="footer-header fl-wrap grad ient-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div  class="subscribe-header">
                            <h3>Subscribe For a <span>Newsletter</span></h3>
                            <p>Whant to be notified about new locations ?  Just sign up.</p>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="subscribe-widget">
                            <div class="subcribe-form">
                                <form id="subscribe">
                                    <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="Enter Your Email" spellcheck="false" type="text">
                                    <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fal fa-envelope"></i></button>
                                    <label for="subscribe-email" class="subscribe-message"></label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-header end--> --}}
        <!--footer-inner-->
        <div class="footer-inner   fl-wrap">
            <div class="container">
                <div class="row">
                    <!-- footer-widget-->
                    <div class="col-md-4">
                        <div class="footer-widget fl-wrap">
                            <div class="footer-logo"><a href="#"><img src="{{ asset('/frontend/images/LOGO 3.png') }}" style="width:100px;height:100px" alt=""></a></div>
                            <div class="footer-contacts-widget fl-wrap">
                                {{-- <p>In ut odio libero, at vulputate urna. Nulla tristique mi a massa convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida.   </p> --}}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul  class="footer-contacts fl-wrap no-list-style">
                            <li><span><i class="fal fa-envelope"></i> Mail :</span><a href="mailto:info@thejet.com.sa" >info@thejet.com.sa</a></li>
                            <li> <span><i class="fal fa-map-marker"></i> Adress :</span><a href="#" >Saudi Arabia  - Jeddah </a></li>
                            <li><span><i class="fal fa-phone"></i> Phone :</span><a href="tel:+996533595962">+996533595962</a></li>
                        </ul>

                    </div>

                    <!-- footer-widget end-->
                    <!-- footer-widget-->
                    {{-- <div class="col-md-4">
                        <div class="footer-widget fl-wrap">
                            <h3>Our Last News</h3>
                            <div class="footer-widget-posts fl-wrap">
                                <ul class="no-list-style">
                                    <li class="clearfix">
                                        <a href="#"  class="widget-posts-img"><img src="images/all/1.jpg" class="respimg" alt=""></a>
                                        <div class="widget-posts-descr">
                                            <a href="#" title="">Vivamus dapibus rutrum</a>
                                            <span class="widget-posts-date"><i class="fal fa-calendar"></i> 21 Mar 09.05 </span>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="#"  class="widget-posts-img"><img src="images/all/1.jpg" class="respimg" alt=""></a>
                                        <div class="widget-posts-descr">
                                            <a href="#" title=""> In hac habitasse platea</a>
                                            <span class="widget-posts-date"><i class="fal fa-calendar"></i> 7 Mar 18.21 </span>
                                        </div>
                                    </li>
                                    <li class="clearfix">
                                        <a href="#"  class="widget-posts-img"><img src="images/all/1.jpg" class="respimg" alt=""></a>
                                        <div class="widget-posts-descr">
                                            <a href="#" title="">Tortor tempor in porta</a>
                                            <span class="widget-posts-date"><i class="fal fa-calendar"></i> 7 Mar 16.42 </span>
                                        </div>
                                    </li>
                                </ul>
                                <a href="blog.html" class="footer-link">Read all <i class="fal fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- footer-widget end-->
                    <!-- footer-widget  -->
                    <div class="col-md-4">
                        <div class="footer-widget fl-wrap ">
                            <h3>Our  Twitter</h3>
                            <div class="twitter-holder fl-wrap scrollbar-inner2" data-simplebar data-simplebar-auto-hide="false">
                                <div id="footer-twiit"></div>
                            </div>
                            <a href="#" class="footer-link twitter-link" target="_blank">Follow us <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                    </div> --}}
                    <!-- footer-widget end-->
                </div>
            </div>
            <!-- footer bg-->
            <div class="footer-bg" data-ran="4"></div>
            <div class="footer-wave">
                <svg viewbox="0 0 100 25">
                    <path fill="#fff" d="M0 30 V12 Q30 17 55 12 T100 11 V30z" />
                </svg>
            </div>
            <!-- footer bg  end-->
        </div>
        <!--footer-inner end -->
        <!--sub-footer-->
        <div class="sub-footer  fl-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="copyright " > &#169; <a href="http://techzonelabs.com/" style="color: #4DB7FE">  Talha Al-Khair  .</a> 2021 .  All rights reserved.</div>
                    </div>

                <div class="col-sm-6">
                    <div class="">
                        <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>En</strong></span><i class="fa fa-caret-down arrlan"></i></div>
                        <ul class="lang-tooltip lang-action no-list-style">
                            <li><a href="#" class="current-lan" data-lantext="En">English</a></li>
                            <li><a href="#" data-lantext="Ar">Arabic</a></li>

                        </ul>
                    </div>
                    <div class="subfooter-nav " style="top: 8">
                        {{-- <span style="color: #fff">Find  us on: </span> --}}
                        <ul class="no-list-style">
                            <li><a href="https://facebook.com/profile.php?id=106474654609586&ref=content_filter" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://twitter.com/SaThejet" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/thejet.sa/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            {{-- <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li> --}}
                        </ul>

                    </div>
                </div>
                </div>

            </div>
        </div>
        <!--sub-footer end -->
    </footer>
    <!--footer end -->
