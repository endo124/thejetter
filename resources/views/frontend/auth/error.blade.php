@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<p class="alert alert-danger">{!! $message !!}</p>
	</div>
</div>
@endsection
