<?php

namespace App\Models;

use App\Observers\OrderRefresh;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class Order extends Model
{
    protected $fillable=['admin_profit','vendor_profit','delivery_taxes','vendor_taxes','payment_status','reason','user_id','vendor','sub_total','discount_id','discount_price','lateVendor','latePrep','lateDriver','assign','cook_id','estimated_time','delivery_estimated_time','driver_name','logista_order_id','customer_address_id','change','note','payment_method','admin_note','delivery_type','date','time','delivery_fees','orderEntry','total_price','address','status','order_lang','accepted_at','shipped_at'];

    public function users(){
        return $this->belongsTo('App\Models\User','user_id');

    }


    public function coupon(){
        return $this->belongsTo('App\Models\Discount','discount_id');
    }

    public $append=['vendor' ,'lateVendor','latePrep','lateDriver'];
    public $attribute=['vendor'];

    protected $casts = [
        'orderEntry' => 'array',
    ];


    public function getVendorAttribute () {

        $user=UserTranslation::find($this->cook_id);
        return $user->name ;

    }

    public function getLateVendorAttribute () {

        if($this->status == 1){

            if(strtotime("now") - strtotime($this->updated_at) >= (5*60)){
                return 1;
            }

        }


    }

    public function getLatePrepAttribute () {

        if($this->status == 2){
            if(strtotime("now") - strtotime($this->updated_at) >= (15*60)){
                return 1;
            }

        }


    }

    public function getLateDriverAttribute () {

        if($this->status == 4){
            if(strtotime("now") - strtotime($this->updated_at) >= (45*60)){
                return 1;
            }

        }


    }



    public function getDeliveryEstimatedTimeAttribute($val){
       return ceil($val/ 60);
    }



    // public static function boot(){
    //     parent::boot();
    //     Order::observe(OrderRefresh::class);
    // }


}
