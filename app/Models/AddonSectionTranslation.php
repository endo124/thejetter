<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonSectionTranslation extends Model
{
    public $timestamps = false;

    public $table="addonsection_translations";
    protected $fillable = ['name'];
}
