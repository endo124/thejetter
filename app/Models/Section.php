<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Section extends Model implements TranslatableContract
{

    use Translatable;

    protected $guarded = [];
    public $fillable=['available_from','available_to','order','availability'];
    public $translatedAttributes = ['name'];

    protected $casts = [
        //  'available_from' => 'date:h:i:s a',
        //  'available_to' => 'date:h:i:s a',

    ];
    public $appends = ['availability'];


    public function getAvailabilityAttribute ()
    {

        $from=(strtotime($this->available_from));
        $to=(strtotime($this->available_to));
        $current=((time()));

        if ($to < $from) {
            $to=strtotime('+1 day', $to);
        }
        if($current >= $from && $current <= $to){
            $this->update(['availability'=>1]);
            return 1;
        }else{
            $this->update(['availability'=>0]);

            return 0;
        }
    }



    public function dishes(){
        return $this->hasMany('App\Models\Dish','section_id');
    }
    public function users(){
        return $this->belongsToMany('App\Models\User','cook_section','section_id','user_id');
    }


   



}
