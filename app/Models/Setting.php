<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Setting extends Model implements TranslatableContract
{
    use Translatable;

    protected $guarded = [];

    protected $fillable=['googleapikey','fees','currency','max_fees'];


    public $translatedAttributes = ['terms'];

}
