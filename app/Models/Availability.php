<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    public $table="availabilities";

    public $fillable=['available','user_id','dish_id'];


    public function dishes(){
        return $this->belongsTo('App\Models\Dish','dish_id');
    }

}
