<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    public $table='address';
    protected $fillable=['deleted_at','coordinates','address','user_id','name','phone','building','lat','lon'];

    public $appends = ['default'];
    // public $attribute=['default'];

    public function users(){
        return $this->belongsTo('App\Models\User','user_id');

    }

    // public function getLatitudeAttribute (){
    //     // dd(explode(",", $this->coordinates)[0]);
    //    return $lat=explode(",", $this->coordinates)[0];
    // }
    // public function getLongitudeAttribute (){
    //     return $lon=explode(",", $this->coordinates)[1];

    // }



    public function getDefaultAttribute ($default)
    {

        if (\Auth::check()) {

            $customer = auth('api_customer')->user();

            if(isset($customer)){
                if($customer['default_address'] !=null){
                    if($this->id == $customer->default_address){
                        return $this->default =1 ;
                    }else{
                        return $this->default =0 ;
                    }
                }else{
                    return $this->default = 1 ;

                }

            }
        }

    }

}
