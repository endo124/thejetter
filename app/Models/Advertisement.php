<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    public $table='advertisement';
    public $fillable=['advertisement','coupon'];
}
