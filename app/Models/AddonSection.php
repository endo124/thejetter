<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class AddonSection extends Model
{
    use Translatable;

    protected $fillable = ['min','max','conditions'];
    public $translatedAttributes = ['name'];
    public $table="addonsection";

    public function addons(){
        return $this->hasMany('App\Models\Addons','addonsection_id');
    }

    public function cooks(){
        return $this->belongsToMany('App\Models\User','cook_addonsection','addonsection_id','user_id');
    }

    public function dishes(){
        return $this->belongsToMany('App\Models\Dish','addonsection_dish','dish_id','addonsection_id');
    }
}
