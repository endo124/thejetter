<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkdeactivate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth('api')->user()->active == 0){
            // auth('api')->refresh();
            return response()->json(['error' => 'Unauthorized'], 401);



        }else{
            return $next($request);

        }




    }

}
