<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/chat-653d2-firebase-adminsdk-m0mo9-12658c17fb.json');
        // $firebase = (new Factory)
        // ->withServiceAccount($serviceAccount)
        // ->withDatabaseUri('https://chat-653d2-default-rtdb.firebaseio.com/')
        // ->create();

        // $database = $firebase->getDatabase();
        // $getusers = $database
        // ->getReference('users')
        // ->getSnapshot()->getValue();

        if(auth('admin')->user()->roles[0]->name == 'super_admin'){

         
            return view('backend.support');
        }
        else
        {
    
            return view('backend.user_support');
        }
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $user_name=auth('admin')->user()->name;
        $dateandtime=Carbon::now()->timestamp;
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/chat-653d2-firebase-adminsdk-m0mo9-12658c17fb.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-653d2-default-rtdb.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();
        $getusers = $database->getReference('users');

        if($getusers->getSnapshot()->getValue() != null){//override last message in exist user
            $users=$getusers->getSnapshot()->getValue();
            $arrUsers=[];
            // dd($getusers->getSnapshot());
            foreach ($users as $key => $user) {
                array_push($arrUsers ,$user['name'] );
            }

            if(!in_array($user_name, array_unique($arrUsers))){//push new user

                $nuewuser=$database->getReference('users')
                ->push([
                    'name' => $user_name,
                    'lastMessage' => $request->message,
                    'lastMessageTime'=>$dateandtime
                ]);

                $database->getReference('chat')
                    ->set($nuewuser->getKey());
                    $database->getReference('chat/'.$nuewuser->getKey())
                    ->push([
                        'Sender'=>$nuewuser->getKey(),
                        'Date' => $dateandtime,
                        'Message' => $request->message,
                    ]);
            }else{//override last message in exist user
                $getUserData=$database->getReference('users')->getSnapshot()->getValue()[$key];
                $data=[
                    'name' => $user_name,
                    'lastMessage' => $request->message,
                    'lastMessageTime'=>$dateandtime,
                ];
                $database->getReference('users')->update(array($key => $data));

                $database->getReference('chat/'.$key )
                ->push([
                    'Sender'=>$key,
                    'Date' => $dateandtime,
                    'Message' => $request->message,
                ]);
            }



        }else{//push new user

            $nuewuser=$database->getReference('users')
                ->push([
                    'name' => $user_name,
                    'lastMessage' => $request->message,
                    'lastMessageTime'=>$dateandtime
                ]);

            $database->getReference('chat')
            ->set($nuewuser->getKey());
            $database->getReference('chat/'.$nuewuser->getKey())
            ->push([
                'Sender'=>$nuewuser->getKey(),
                'Date' => $dateandtime,
                'Message' => $request->message,
            ]);
        }



        // dd($getusers);

        // $chat=Chat::create([
        //     'sender_id'=>auth('admin')->user()->id,
        //     'sender_name'=>auth('admin')->user()->name,
        //     'message'=>$request->message,
        //     'image'=>auth('admin')->user()->images
        // ]);

        // $this->broadcasts($request->message,auth('admin')->user()->name);
        return back();
    }


    // public function broadcasts($message , $sender_name){

    //     $url=route('support.index');
    //     $optionBuilder = new OptionsBuilder();
    //     $optionBuilder->setTimeToLive(60*20);

    //     $notificationBuilder = new PayloadNotificationBuilder('New Message From : '.$sender_name);
    //     $notificationBuilder->setBody($message)
	// 			    ->setSound('default')
    //                 ->setClickAction($url);

    //     $dataBuilder = new PayloadDataBuilder();
    //     $dataBuilder->addData(['sender_name' => $sender_name , 'message'=>$message]);

    //     $option = $optionBuilder->build();
    //     $notification = $notificationBuilder->build();
    //     $data = $dataBuilder->build();

    //     $token = auth('admin')->user()->fcm_token;
    //     // dd($token,auth('admin')->user()->id, $option, $notification, $data);
    //     $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

    //     $downstreamResponse->numberSuccess();


    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/chat-653d2-firebase-adminsdk-m0mo9-12658c17fb.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-653d2-default-rtdb.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();
        $getChat = $database
        ->getReference('chat/'.$id)
        ->getSnapshot()->getValue();
        return response()->json(['messages'=>$getChat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
