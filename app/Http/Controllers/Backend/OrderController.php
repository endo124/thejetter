<?php

namespace App\Http\Controllers\Backend;

use App\Events\NewOrder;
use App\Events\TrackOrder;
use App\Http\Controllers\Controller;
use App\Mail\OrderConfirmation;
use App\Models\Address;
use App\Models\Dish;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use App\Models\Discount;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use App\Models\DishTranslation;
use App\Models\UserTranslation;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        // if(auth()->guard('admin')->user()->roles[0]->name !='cook'){
        //     $orders=Order::where('status','!=',-1)->get();
        //     $currentActivities=Order::where('status',-1)->get();
        // }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){

        //     $orders=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('status','!=',-1)->get();

        //     $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('status',-1)->get();

        // }
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
            $orders=Order::whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

            $currentActivities=Order::where('status',1)
            ->where('status',1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

        }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

            $orders=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

            $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->where('status',1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

        }


        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
        ->create();
        $database = $firebase->getDatabase();

        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();

        $available=[];
        $onduty=[];

        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo("hesham.zaki.99@hotmail.com")
        ->getValue();
        // dd( $getUser);
        if(isset($getUser)){
            $user=array_keys($getUser);
            if ($user != []) {
                if (isset($getUser[$user[0]]['drivers'])) {
                    $drivers= $getUser[$user[0]]['drivers'];
                    if (isset($drivers)) {
                        foreach (array_keys($drivers) as $driver) {
                            if ($drivers[$driver]['driverStatus'] == -1) {
                                array_push($available, $driver);
                            } elseif (['driverStatus'] == 3) {
                                array_push($onduty, $driver);
                            }
                        }
                    }
                }
            }
        }
        $accepted_orders=Order::whereNotNull('status')->where('status', '!=', 1)->where('status','!=',-1)->where('status','!=',9)
        ->where('status','!=',0)->get();
        // $cooks=User::whereHas('roles',function($q){
        //     return $q->where('name','cook');
        // })->get();

        // $cook=User::find($orders->cook_id);
        $late_vendors=0;
        $latePreps=0;
        $lateDrivers=0;
        if($currentActivities){
            $late_vendors=$currentActivities->where('lateVendor',1);
        }
        if($accepted_orders){
            $latePreps=$accepted_orders->where('status',2)->where('latePrep',1);
            $lateDrivers=$accepted_orders->where('status',4)->where('lateDriver',1);
        }

        $addresses=array_filter(Order::pluck('address')->toArray());
        $address_ids=array_filter(Order::pluck('customer_address_id')->toArray());
        // dd($addresses,$address_ids);
        return view('backend.order-list',compact('orders','late_vendors','addresses','address_ids','latePreps','lateDrivers','currentActivities','available','onduty','accepted_orders','currency'));
    }

    public function trackorder(){

        $tabs= array('2'=>'Accepted','3'=>'InProgress','4'=>'Shipped','8'=>'Unassigned','11'=>'Assigned','5'=>'Delivered','9'=>'Cancelled','0'=>'Rejected','6'=>'On_The_Way');
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
            $orders=Order::whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

            $currentActivities=Order::where('status',1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();
            foreach ($tabs as $i => $tab) {
                if($i == '8'){
                    ${$tab . '_orders'}=Order::whereIn('status',[2,3,4,1])
                    ->orderBy('date', 'DESC')
                    ->orderBy('time', 'DESC')
                    ->get();

                }else{
                    ${$tab . '_orders'}=Order::where('status',$i)
                    ->orderBy('date', 'DESC')
                    ->orderBy('time', 'DESC')
                    ->get();
                }
                if(${$tab . '_orders'} == null){
                    ${$tab . '_orders'}=[];
                }

            }

        }

        if(isset($currentActivities)){
            $current_ids=$currentActivities->pluck('id');

        }else{
            $current_ids=[];
        }


        $addresses=array_filter(Order::pluck('address')->toArray());
        $address_ids=array_filter(Order::pluck('customer_address_id')->toArray());
        // dd($address_ids);

        return view('backend.track-order-list',compact('address_ids','Accepted_orders','InProgress_orders','Shipped_orders','On_The_Way_orders','Unassigned_orders','Rejected_orders','Assigned_orders','Delivered_orders','Cancelled_orders','orders','current_ids','currentActivities','currency','addresses'));

    }

    public function acceptorders(){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $orders=Order::whereNotNull('status')->where('status', '!=', 1)->where('status','!=',-1)->where('status','!=',9)
        ->where('status','!=',0)->get();
        return view('backend.order-accepted-list',compact('orders','currency'));
    }
    public function lateVendors(){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        $orders=Order::whereNotNull('status')->where('status', '!=', 1)->where('status','!=',-1)->where('status','!=',9)
        ->where('status','!=',0)->get();
        $orders=$orders->where('lateVendor',1);
        return view('backend.order-accepted-list',compact('orders','currency'));
    }
    public function latePreps(){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        $orders=Order::whereNotNull('status')->where('status', '!=', 1)->where('status','!=',-1)->where('status','!=',9)
        ->where('status','!=',0)->get();
        $orders=$orders->where('latePrep',1);
        return view('backend.order-accepted-list',compact('orders','currency'));
    }
    public function lateDrivers(){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        $orders=Order::whereNotNull('status')->where('status', '!=', 1)->where('status','!=',-1)->where('status','!=',9)
        ->where('status','!=',0)->get();
        $orders=$orders->where('lateDriver',1);
        return view('backend.order-accepted-list',compact('orders','currency'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function assign($id){

        $order=Order::find($id);
        $order->update([
            'assign'=>auth()->guard('admin')->user()->name
        ]);
        return response()->json(['name'=>auth()->guard('admin')->user()->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders=[];
        $validation=$request->validate([
            'qty'=>'required',
            'size'=>'required'
        ]);

       $orderEntry=array();
       $dish=Dish::find($request->dish_id);
        if($request->notes){
            $orderEntry['notes']=$request->notes;
        }
       $orderEntry['qty']=$request->qty;
       $orderEntry['notes']=$request->notes;
       $orderEntry['size']=$request->size;
       $orderEntry['name']=$dish->name;
       $orderEntry['price']=$request->price*$request->qty;
       $orderEntry['section']=$dish->sections->name;
       $orderEntry['images']=$dish->images;

       array_push($orders,$orderEntry);
       $order=Order::create([
            'user_id'=>auth()->guard('customer')->user()->id,
            'cook_id'=>$dish->user_id,
            // 'date'=>$request->date,
            // 'time'=>$request->time,
            // 'status'=>'0',
            'orderEntry'=>$orders,
            // 'address'=>$request->address,
        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function orderdetails($id){
        $order= Order::where('id',$id)->first();

        return response()->json(['success' => true,'order'=>$order]);

    }
    public function show($id)//when accept order
    {

       $order= Order::where('id',$id)->first();
       $date=$order->date;
       $time=$order->time;
       $date=(string)strtotime($date . " " . $time);

       $cook=User::find($order->cook_id);

       $customer= User::Find($order->user_id);


       $address=Address::find($order->customer_address_id);
       if(isset($address->phone)){
           $customer_phone=$address->phone;
       }else{
           $customer_phone=$customer->phone;
       }

       if($order->status == 1){
            if(isset($cook->parent)){
                $cook_parent=User::find($cook->parent);
            }
            $order->update(['status'=>2 ]);
            $customer= User::Find($order->user_id);
            $subtotal=doubleval($order->sub_total) ;
            if($cook_parent->taxes_type && $cook_parent->taxes_type == '%'){
                $subtotal=$subtotal+($subtotal*$cook_parent->taxes)/100;
            }elseif($cook_parent->taxes_type && $cook_parent->taxes_type != '%'){
                $subtotal=$subtotal+doubleval($cook_parent->taxes);
            }
            if($order->delivery_type == 1){

                if($cook_parent->commission_type == '%'){
                    $profit= $subtotal-( $subtotal*doubleval($cook_parent->commission)/100)+doubleval($cook_parent->profit);
                    $admin_commission=$subtotal*doubleval($cook_parent->commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook_parent->commission)+doubleval($cook_parent->profit);
                    $admin_commission=doubleval($cook_parent->commission);

                }
            }else{
                if($cook_parent->pickup_commission_type == '%'){

                    $profit= $subtotal-( $subtotal*doubleval($cook_parent->pickup_commission)/100)+doubleval($cook_parent->profit);
                    // $total_profit=$total_profit -($total_profit * $cook->pickup_commission) / 100;
                    $admin_commission=$subtotal*doubleval($cook_parent->pickup_commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook_parent->pickup_commission)+doubleval($cook_parent->profit);
                    // $total_profit=$total_profit-($cook->pickup_commission *$orders_count) ;
                    $admin_commission=doubleval($cook_parent->pickup_commission);

                }
            }
            $revenue =doubleval($cook->revenue)+ doubleval( $subtotal);
            $order->update(['admin_profit'=>$admin_commission,'vendor_profit'=>$subtotal-$admin_commission]);

            $cook->update(['revenue'=>$revenue,'profit'=>$profit]);
            $cook->save();
            $admin=User::first();
            $admin_rev=doubleval($admin->revenue) + doubleval( $subtotal);
            $admin_pro=$admin->profit + $admin_commission;
            $admin->update(['revenue'=>$admin_rev,'profit'=>$admin_pro]);
            $admin->save();

            Mail::to($customer->email)->send(new OrderConfirmation($order,app()->getLocale()));


            $admin=User::first();

        }else if($order->status == 2){
        $order->update([
            'status'=>3
        ]);
       }else if($order->status == 3){
           if($order->delivery_type != 1){
                $order->update([
                    'status'=>5,
                    'payment_status'=>1

                ]);
           }else{

                $details=$order->orderEntry;
                $orders=[];
                $notes=[];
                foreach ( $details as $ord) {
                    $item=[];
                    $item['id']=(string)$ord['id'];
                    $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
                    $item['price']=(string)$ord['price'];
                    $item['quantity']=(int)$ord['quantity'];


                    if (isset($ord['notes'])) {
                        $item['note']=(string)$ord['notes'];
                        array_push($notes, $item['note']);
                    }
                    array_push($orders, $item);

                }
                foreach ($details as $ord) {
                    foreach ( $ord['addons'] as $index=>$add) {
                    $item['id']=(string)$index;
                    $item['name']=$add['add_ar'];
                    $item['price']=(string)$add['add_price'];
                    $item['quantity']=1;
                    array_push($orders, $item);
                    }
                }

                $factory = (new Factory)->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
                $database = $factory->createDatabase();

                if($order->delivery_type == 1){
                    $coordinates=explode(",",$cook->address[0]->coordinates);
                    $customer_coordinates=explode(",",$address->coordinates);

                    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
                    $firebase = (new Factory)
                    ->withServiceAccount($serviceAccount)
                    ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
                    ->create();
                    $database = $firebase->getDatabase();

                    $getUser = $database
                    ->getReference('users')
                    ->orderByChild('email')
                    ->equalTo('hesham.zaki.99@hotmail.com')
                    ->getValue();
                    $user=array_keys($getUser);

                    $newOrder = $database
                    ->getReference('/users/'.$user[0].'/tasks')
                    ->push(
                        [
                        'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
                        'name'=>$address['address']],
                        'date' => $date,
                        'description' => '',
                        'email'=>$customer->email,
                        'name' => $customer->name,
                        'orderId' => (string)$order['id'],
                        'phone' =>  $customer_phone],
                        'distance'=>'',
                        'driverId'=>'',
                        'estTime'=>'',
                        'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
                        'date' => $date,
                        'description' => '',
                        'email'=>$customer->email,
                        'name' => $customer->name,
                        'orderId' => (string)$order['id'],
                        'phone' =>  $customer_phone],
                        'status'=>-1,
                        'orderItems' => $orders,
                        'type'=>'pickupOnDelivery',
                        'note'=>$order->note,
                        'notes'=>$notes,
                        'change'=>$order['change'],
                        'fees'=>(string)$order->delivery_fees,
                        'taken'=>false,
                        'vendor_taxes'=>$order->vendor_taxes ?? '',
                        'discount_price'=>$order->discount_price ?? '',
                        'delivery_taxes'=>$order->delivery_taxes ?? '',
                        'sub_total'=>$order->sub_total,
                        'total_price'=>$order->total_price,
                        'paid'=>$order->payment_method == 1 ? 1 :  0,

                    ]);

                    $logista_tasks = $database
                    ->getReference('/users/'.$user[0].'/tasks')
                    ->getSnapshot()->getValue();

                    $order->update([
                        'logista_order_id'=>array_key_last($logista_tasks)
                    ]);
                }
                $order->update([
                    'status'=>4
                ]);
            }

        }
        event(new NewOrder($order->id,$order));//for new order or any status change

        $this->sendWebNotification($order);

        return response()->json(['success' => true,'status'=>$order->status]);
    }




    public function updatestatus($id , $status){
        // dd((int)$status);
        $order= Order::where('id',$id)->first();
        $date=$order->date;
        $time=$order->time;
        $date=(string)strtotime($date . " " . $time);
        $cook=User::find($order->cook_id);
        $customer= User::Find($order->user_id);
        $address=Address::find($order->customer_address_id);
        if(isset($address->phone)){
            $customer_phone=$address->phone;
        }else{
            $customer_phone=$customer->phone;
        }


        if($status == 2){
            $cook=User::find($order->cook_id);

            $customer= User::Find($order->user_id);
            $subtotal=doubleval($order->sub_total) ;
            if($cook->taxes_type && $cook->taxes_type == '%'){
                $subtotal=$subtotal+($subtotal*$cook->taxes)/100;
            }elseif($cook->taxes_type && $cook->taxes_type != '%'){
                $subtotal=$subtotal+doubleval($cook->taxes);
            }
            if($order->delivery_type == 1){
                if($cook->commission_type == '%'){
                    $profit= $subtotal-( $subtotal*doubleval($cook->commission)/100)+doubleval($cook->profit);
                    $admin_commission=$subtotal*doubleval($cook->commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook->commission)+doubleval($cook->profit);
                    $admin_commission=doubleval($cook->commission);

                }
            }else{
                if($cook->pickup_commission_type == '%'){

                    $profit= $subtotal-( $subtotal*doubleval($cook->pickup_commission)/100)+doubleval($cook->profit);
                    // $total_profit=$total_profit -($total_profit * $cook->pickup_commission) / 100;
                    $admin_commission=$subtotal*doubleval($cook->pickup_commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook->pickup_commission)+doubleval($cook->profit);
                    // $total_profit=$total_profit-($cook->pickup_commission *$orders_count) ;
                    $admin_commission=doubleval($cook->pickup_commission);

                }
            }
            $revenue =doubleval($cook->revenue)+ doubleval( $subtotal);
            $order->update(['admin_profit'=>$admin_commission,'vendor_profit'=>$subtotal-$admin_commission]);

            $cook->update(['revenue'=>$revenue,'profit'=>$profit]);
            $cook->save();
            $admin=User::first();
            $admin_rev=doubleval($admin->revenue) + doubleval( $subtotal);
            $admin_pro=$admin->profit + $admin_commission;
            $admin->update(['revenue'=>$admin_rev,'profit'=>$admin_pro]);
            $admin->save();
            $lang=$order->order_lang;

            Mail::to($customer->email)->send(new OrderConfirmation($order , $lang ));

        }

         if($status == 4 && $order->delivery_type ==1){
            $details=$order->orderEntry;
            $orders=[];
            $notes=[];
            foreach ( $details as $ord) {
                $item=[];
                $item['id']=(string)$ord['id'];
                $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
                $item['price']=(string)$ord['price'];
                $item['quantity']=(int)$ord['quantity'];


                if (isset($ord['notes'])) {
                    $item['note']=(string)$ord['notes'];
                    array_push($notes, $item['note']);
                }
                array_push($orders, $item);

            }
            foreach ($details as $ord) {
                foreach ( $ord['addons'] as $index=>$add) {
                $item['id']=(string)$index;
                $item['name']=$add['add_ar'];
                $item['price']=(string)$add['add_price'];
                $item['quantity']=1;
                array_push($orders, $item);
                }
            }

            $factory = (new Factory)->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
            $database = $factory->createDatabase();
            if($order->delivery_type == 1){
                $coordinates=explode(",",$cook->address[0]->coordinates);
                $customer_coordinates=explode(",",$address->coordinates);

                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
                $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
                ->create();
                $database = $firebase->getDatabase();

                $getUser = $database
                ->getReference('users')
                ->orderByChild('email')
                ->equalTo('hesham.zaki.99@hotmail.com')
                ->getValue();
                $user=array_keys($getUser);

                $newOrder = $database
                ->getReference('/users/'.$user[0].'/tasks')
                ->push(
                    [
                    'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
                    'name'=>$address['address']],
                    'date' => $date,
                    'description' => '',
                    'email'=>$customer->email,
                    'name' => $customer->name,
                    'orderId' => (string)$order['id'],
                    'phone' =>  $customer_phone],
                    'distance'=>'',
                    'driverId'=>'',
                    'estTime'=>'',
                    'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
                    'date' => $date,
                    'description' => '',
                    'email'=>$customer->email,
                    'name' => $customer->name,
                    'orderId' => (string)$order['id'],
                    'phone' =>  $customer_phone],
                    'status'=>-1,
                    'orderItems' => $orders,
                    'type'=>'pickupOnDelivery',
                    'note'=>$order->note,
                    'notes'=>$notes,
                    'change'=>$order['change'],
                    'fees'=>(string)$order->delivery_fees,
                    'taken'=>false,
                    'vendor_taxes'=>$order->vendor_taxes ?? '',
                    'discount_price'=>$order->discount_price ?? '',
                    'delivery_taxes'=>$order->delivery_taxes ?? '',
                    'sub_total'=>$order->sub_total,
                    'total_price'=>$order->total_price,
                    'paid'=>$order->payment_method == 1 ? 1 :  0,

                ]);

                $logista_tasks = $database
                ->getReference('/users/'.$user[0].'/tasks')
                ->getSnapshot()->getValue();

                $order->update([
                    'logista_order_id'=>array_key_last($logista_tasks)
                ]);
            }
         }

        $order->update([
            'status'=>(int)$status,
        ]);
        if($status==5){
            $order->update([
                'payment_status'=>1
            ]);

        }

        return response()->json(['success' => true,'status'=>$order->status]);
    }

    public function reject(Request $request)
    {
       $order= Order::where('id',$request->id)->first();
        $order->update([
            'status'=>0,
            'reason'=>$request->note
        ]);
        return back();
        // return response()->json(['success' => true,'status'=>$order->status]);
    }

    public function order_details($order_id)
    {
        // $order=Order::where('logista_order_id',$order_key)->first();
        $order=Order::where('id',$order_id)->first();

        $cook=User::where('id',$order->cook_id)->with('address')->first();
        $cook_name=$cook->name;
        $customer=User::find($order->user_id);
        $customer_orders=Order::where('user_id',$customer->id)->get();


        if(count($customer_orders)<3 && $customer->VIP!= 1  ){
            $image_name='Group 126.png';
        }

        if(count($customer_orders)>=3  ){
            $image_name='Group 127.png';
        }

        if($customer->VIP == 1){

            $image_name='Group 125.png';

        }
        $orderEntry=$order->orderEntry;
        $qty=0;
        foreach ($orderEntry as $item){
            $qty+=$item['quantity'];
        }
        if(isset($order->discount_id)){
        $coupon=Discount::find($order->discount_id);
        }else{
            $coupon=['name'=>''];
        }
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        if($order->admin_note == null){
            $order->admin_note = '';
        }
        if($order->time !=null){
            $order->time=date('h:i A', strtotime($order->time ));
        }
        if($order->payment_status == null){
            $order->payment_status = 0;
        }

        $addresses=Order::pluck('address')->toArray();
        $address_ids=Order::pluck('customer_address_id')->toArray();


        $order_status='';
        if ($order['status'] == 1) {
            $order_status=__('site.Pending');
        }elseif($order['status'] == 2){
            $order_status=__('site.Accepted');
        }elseif($order['status'] == 3){
            $order_status=__('site.In progress');
        }elseif($order['status'] == 4){
            $order_status=__('site.Shipped');
        }elseif($order['status'] == 5){
            $order_status=__('site.Delivered');
        }elseif($order['status'] == 6){
            $order_status=__('site.Completed');
        }elseif($order['status'] == 0){
            $order_status=__('site.Rejected');
        }elseif($order['status'] == 9){
            $order_status=__('site.Cancelled');
        }

        $order['cook']=$cook;
        $order['cook_name']=$cook_name;
        $order['customer']=$customer;
        $order['customer_orders']=$customer_orders;
        $order['image_name']=$image_name;
        $order['qty']=$qty;
        $order['coupon']=$coupon;
        $order['currency']=$currency;
        $order['display_auth_type']=auth()->guard('admin')->user()->roles[0]->name !='cook' ? 'block': 'none';
        $order['delivery_estimated_time']=$order->delivery_estimated_time ?? '45';
        $order['addresses']=$addresses;
        $order['address_ids']=$address_ids;
        $order['order_status']=$order_status;
        return response()->json(['success' => true,'order'=>$order]);

    }



    public function feach(){

        event(new TrackOrder('hello world'));

        $ids=request()->ids;
        $ids=json_decode($ids);




        if(auth()->guard('admin')->user()->roles[0]->name !='cook'){// admin


            $order=Order::where('status',1)
            ->where('status',1)
            ->whereNotIn('id',$ids)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

            $current_orders=Order::where('status',1)
            ->where('status',1)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get();

        }

        $current_ids=$current_orders->pluck('id');

        $current_ids=json_encode($current_ids);
        return response()->json(['success' => true,'order'=>$order,'current_ids'=>$current_ids ]);


    }


    public function sendWebNotification($order)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey='AAAAxJAyh0Q:APA91bEWjkR_pj_iYBdOAyu4xvFFWKsfwMPm7LoD9c_gqcIGRCH7lSPLfMKt97Zjkb2xBiZPuo4jK-ByumcTaGlSIVhAaoUNLUyo0oBysz3wf4LIue36onIZTM76U4bXX2UwGQtbkK19';

        $user=User::find($order->user_id);
        $data = [
            "registration_ids" => $user->device_key,
            "notification" => [
                "id" => $order->id,
                "body" => $order->body,
                "time" =>$order->updated_at,
                "status"=> $order->status
            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        // FCM response
        return($result);
    }




    public function cancel($id)
    {
        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>9
        ]);
        return response()->json(['success' => true,'status'=>$order->status]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    // public function accept($id){
    //     $order=Order::find($id);
    //     $order->update([
    //         'status'=>1
    //     ]);
    //     return back();
    // }
    // public function reject($id){
    //     $order=Order::find($id);
    //     $order->update([
    //         'status'=>0
    //     ]);
    //     return back();
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order= Order::where('id',$id)->first();
        if($order->status ==null){
         $order->update([
             'status'=>'Accepted'
         ]);

        }else if($order->status == 'Accepted'){
         $order->update([
             'status'=>'In progress'
         ]);
        }
         return response()->json(['success' => true,'status'=>$order->status]);
    }

    public function updatedata($id ,Request $request){
        $order=Order::find($id);
        $order->update([
            'admin_note'=>$request->admin_note,
            'total_price'=>$request->total_price
        ]);
        return back();
    }


    public function customerorders($id){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
            $orders=Order::where('user_id',$id)->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();

            $currentActivities=Order::where('user_id',$id)->where('status',1)->get();
        }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

            $orders=Order::where('user_id',$id)->where('cook_id',auth()->guard('admin')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();

            $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->where('user_id',$id)
            ->where('status',1)
            ->orderBy('id', 'DESC')
            ->get();
        }

            return view('backend.customer-order-list',compact('orders','currentActivities','currency'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
