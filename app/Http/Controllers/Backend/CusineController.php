<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Cusine;
use App\Models\CusineTranslation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CusineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cusines=Cusine::all();

        return view('backend.cusine_list',compact('cusines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',Rule::unique('cusine_translations','name')]];
        }
        $request->validate( $rules);
        $cusine=Cusine::create( $request->all() );
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res_ids=[];
        $cusine=Cusine::find($id);
        $restaurant_ids = \DB::table('dishes')
            ->select('user_id')
            ->Join('cusine_dish', 'dishes.id', '=', 'cusine_dish.dish_id')
            ->where('cusine_dish.cusine_id', $id)
            ->distinct()

            ->get();


        foreach ($restaurant_ids as $restaurant_id) {
            array_push($res_ids ,$restaurant_id->user_id);
        }
        $restaurants=User::whereIn('users.id',$res_ids)
        ->select('users.*')
        ->leftJoin('cook_cusine', 'users.id' , 'cook_cusine.cook_id')
        ->orderBy('cook_cusine.order')->get();

        return view('backend.vendor_cuisine',compact('restaurants','cusine'));

    }

    public function reorder(Request $request)
    {

        $cusines = Cusine::all();

        foreach ($cusines as $cusine) {

            foreach ($request->order as $order) {

                if($cusine->id == $request->cusine){
                    // dd($order['id'] , $cusine->id ,$order['position']);

                    \DB::table('cook_cusine')

                    ->where(['cusine_id' => $cusine->id , 'cook_id'=>$order['id']])->delete();

                    \DB::table('cook_cusine')->Insert([
                        'cusine_id'=>$cusine->id,
                        'cook_id'=>$order['id'],
                        'order'=>$order['position']
                    ]);
                }

            }
        }

        return response('Update Successfully.', 200);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */





    public function update(Request $request, $id)
    {

        $cusine=Cusine::find($id);
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.name' => ['required', Rule::unique('cusine_translations', 'name')->ignore($cusine->id, 'cusine_id')]];
        }//end of for each
        $request->validate( $rules);

        $cusine->update(
            $request->all()
        );
        $cusine->save();
        return redirect()->route('cusine.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cusine_trans=CusineTranslation::where('cusine_id',$id)->delete();
        $cusine=cusine::find($id);
        if( ! empty($cusine->dishes[0])){
            $cusine->dishes()->detach();
        }

        $cusine->delete();
        return back();
    }
}
