<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use App\Models\Setting;
use Kreait\Firebase\ServiceAccount;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $factory = (new Factory)
		// ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json')
		// ;
		// $database = $factory->createDatabase();
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
        ->create();
        $database = $firebase->getDatabase();

        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo('hesham.zaki.99@hotmail.com')
        ->getValue();
        $user=array_keys($getUser);
        // dd($user);
        $drivers=( $getUser[$user[0]]['drivers']);

        $setting=Setting::first();
        if(isset($setting->googleapikey)){
            $googleapikey=$setting->googleapikey;
        }
        return view('backend.driver',compact('drivers','googleapikey'));
    }


    public function getDataDriver(request $request)
    {

		// $factory = (new Factory)
		// ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json')
		// ;
		// $database = $factory->createDatabase();
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
        ->create();
        $database = $firebase->getDatabase();

        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo('hesham.zaki.99@hotmail.com')
        ->getValue();
        $user=array_keys($getUser);
        $getDriversData = $database
        ->getReference('/users/'.$user[0].'/drivers/'.$request->id)

        ->getValue();

        return response()->json(['success' => true, 'data' => $getDriversData]);


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
