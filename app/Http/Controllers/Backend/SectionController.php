<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\Section;
use App\Models\User;
use App\Models\SectionTranslation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule as Rule;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guard('admin')->user()->roles[0]->name != 'cook'){
        $sections=Section::with('users')->orderBy('order', 'asc')->get();
        $cooks=User::whereHas('roles',function($q){
            $q->where('name','cook');
        })->where('active',1)->get();
        return view('backend.section_list',compact('sections','cooks'));
        }else{
            $sections=Section::with('users')->orderBy('order', 'asc')->get();

            if (auth()->guard('admin')->user()->parent != null) {
                $cook=User::find(auth()->guard('admin')->user()->parent );
                $sections_id=$cook->sections->pluck('id')->toArray();
            }else{
                $cook=User::find(auth()->guard('admin')->user()->id);
                $sections_id=$cook->sections->pluck('id')->toArray();
             }

            //  dd( $sections[0]);
            return view('backend.section_list',compact('sections_id','sections'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $rules=[
            'from'=>'required',
            'to'=>'required',
            'Vendor'=>'required'

        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required'
            // ,Rule::unique('section_translations','name')
            ]];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{

            $from=(strtotime($request->from));
            $to=(strtotime($request->to));
            $current=((time()));
    
            if ($to < $from) {
                $to=strtotime('+1 day', $to);
            }
            $from= date("H:i:s", $from);
            $to=date("H:i:s",$to);

            $section=Section::create($request->all());
            $section->update([
                'available_from'=>$from,
                'available_to'=>$to
            ]);
            $cook=User::find($request->Vendor);
            $section->users()->attach($cook);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id);
        $cook=User::find($id);

        if ($cook->parent != null) {
            $cook=User::find($cook->parent);
            $sections=$cook->sections;
        }else{
            $sections=$cook->sections;
         }
        //  dd($sections);
         return response()->json(['success'=>$sections]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        

        $section=Section::find($id);

        $rules=[
            // 'name'=>'unique:section_translations,id,'.$id,
            'from'=>'required',
            'to'=>'required',
            'Vendor'=>'required'
        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required']];
        }

        $section=Section::find($id);
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',
            // Rule::unique('section_translations','name')->ignore($section->id,'section_id')
            ]];
        }

        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'section_id'=>$section->id]);
        }
        else{


            $from=(strtotime($request->from));
            $to=(strtotime($request->to));
            $current=((time()));
    
            if ($to < $from) {
                $to=strtotime('+1 day', $to);
            }
            $from= date("H:i:s", $from);
            $to=date("H:i:s",$to);

            $data=$request->except('available_from','available_to');
            $section->update($data);
            $section->update([
                'available_from'=>$from,
                'available_to'=>$to
            ]);
            // dd($section);
            $cook=User::find($request->Vendor);
            $section->users()->sync($cook);
            // $section->save();
            return  redirect()->route('section.index');}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section_trans=SectionTranslation::where('section_id',$id)->delete();
        $section=Section::find($id);

        $dishes=Dish::where('section_id',$section->id)->get();
        foreach ($dishes as $dish) {
            if($dish){
                $dish->update([
                    'section_id'=>null,
                ]);
                if( ! empty( $dish->cusines[0])){
                    $dish->cusines()->detach();

                }
                if( ! empty( $dish->allergens[0])){
                    $dish->allergens()->detach();
                }
                if( ! empty( $dish->addons[0])){
                    $dish->addons()->detach();

                }
                $dish_trans=DishTranslation::where('dish_id',$dish->id)->delete();
                $section->users()->detach();
                $dish->forceDelete();
            }

        }
        $section->delete();
        return back();
    }

    public function reorder(Request $request)
    {

        $sections = Section::all();
        // dd($request->order);

        foreach ($sections as $section) {

            foreach ($request->order as $order) {
                if ($order['id'] == $section->id) {
                    // dd($section->order , $order['position']);
                    $section->update(['order' => $order['position']]);
                    // dd($section->order);

                }
            }
        }

        return response('Update Successfully.', 200);


    }

}
