<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\Allergen;
use App\Models\Availability;
use App\Models\Category;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\Section;
use App\Models\SectionTranslation;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\ImageManager;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $allergens=Allergen::all();
        $addons=Addons::all();
        $sections=Section::all();
        $cusines=Cusine::all();
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $categories=Category::all();
            $sections=Section::all();
            $dishes=Dish::all();
            if(isset($request->dish_name) && $request->section_name ==null  ){//search
                $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->dish_name%")->pluck('dish_id');
                $dishes=Dish::whereIn('id',$dishes_id)->get();

                $data=['dishes'=>$dishes,'sections'=>null];
                return response()->json(['data'=>$data]);

            }elseif(isset($request->section_name) &&  $request->dish_name==null){
                $sections_id=SectionTranslation::where('name', 'LIKE', "%$request->section_name%")->pluck('section_id');
                $sections=Section::whereIn('id',$sections_id)->get();

                $data=['dishes'=>null,'sections'=>$sections];
                return response()->json(['data'=>$data]);
            }elseif($request->section_name && $request->dish_name){
                $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->dish_name%")->pluck('dish_id');
                $dishes=Dish::whereIn('id',$dishes_id)->get();

                $sections_id=SectionTranslation::where('name', 'LIKE', "%$request->section_name%")->pluck('section_id');
                $sections=Section::whereIn('id',$sections_id)->get();
                $data=['dishes'=>$dishes,'sections'=>$sections];
                return response()->json(['data'=>$data]);
            }
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->where('active',1)->get();


            return  view('backend.dish-list',compact('dishes','cusines','sections','addons','categories','allergens','cooks' ,'currency'));
        }
        else{
            $cook=User::find(auth()->guard('admin')->user()->id);
            $addons=$cook->addons;
            $sections=$cook->sections;
            $categories=$cook->categories;
            if (auth()->guard('admin')->user()->parent != null) {

                if($request->search){
                    $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->search%")->pluck('dish_id');
                    $dishes=Dish::where('user_id',auth()->guard('admin')->user()->parent)->whereIn('id',$dishes_id)->with('availabilities')->get();
                }else{
                    if($request->search){
                        $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->search%")->pluck('dish_id');
                        $dishes=Dish::where('user_id',auth()->guard('admin')->user()->id)->whereIn('id',$dishes_id)->get();

                        // $sections_id=SectionTranslation::where('name', 'LIKE', "%$request->search%")->pluck('section_id');
                        // $sections=Section::where('user_id',auth()->guard('admin')->user()->id)->whereIn('id',$sections_id)->paginate(10);

                    }else{
                        $dishes=Dish::where('user_id',auth()->guard('admin')->user()->parent)->with('availabilities')->paginate(800);
                    }
                }
            }else{
                // dd('aa');
                if($request->search){
                    $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->search%")->pluck('dish_id');
                    $dishes=Dish::where('user_id',auth()->guard('admin')->user()->id)->whereIn('id',$dishes_id)->get();

                    // $sections_id=SectionTranslation::where('name', 'LIKE', "%$request->search%")->pluck('section_id');
                    // $sections=$sections->whereIn('id',$sections_id);
                }else{
                    $dishes=Dish::where('user_id',auth()->guard('admin')->user()->id)->get();

                }
            }
            // dd('a');

            return view('backend.dish-list',compact('dishes','cusines','sections','addons','categories','allergens','currency'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules=[
            'files.*'=>'required|max:2000|mimes:jpeg,png,jpg,gif,svg',
            'cusine'=>'required',
            'category'=>'required',
            'section'=>'required',
            'portions_available'=>'required',
            'dish_available'=>'required',
            'portions_price	'=>'requires|numeric|regex:/^\d*\.?\d*$/',
            'calories'=>'required'

        ];
        if(isset($request->min[0])){
            foreach ($request->min as $index => $min) {
                $rules += [
                    'max.'.$index=>'regex:/^\d*\.?\d*$/',
                    'min.'.$index=>'regex:/^\d*\.?\d*$/',
                ];
            }

        }
        if(auth()->guard('admin')->user()->roles[0]->name =='cook'){
            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],

                ];
            }
            $request->validate( $rules);

            $cook_id=auth()->guard('admin')->user()->id;
        }
        else{
            $rules+=['Vendor_id'=>'required',];

                foreach (config('translatable.locales') as $locale) {
                    $rules += [
                        $locale.'.name'=>['required'],

                    ];
                }
                $request->validate( $rules);
                $cook_id=$request->Vendor_id;
        // dd($request->all());

            }
        if ($request->file('files')) {

            $dishes_images=array();
            foreach($request->file('files') as $index=>$img){
                $ext=$img->getClientOriginalExtension();
                $image_name=time().$index.'.'.$ext;
                $path='backend/img/dishes/';

                Image::make($img)->resize(300, null, function ($constraint) {
                    // dd('a');
                    $constraint->aspectRatio();
                })->save( $path.$image_name);
                // $img->move($path,$image_name);
                array_push($dishes_images,$image_name);
                }
                $dishes_images=implode('__',$dishes_images);
        }
        if($request->dish_type == 'main'){
            $request->dish_type =1;
        }else{
            $request->dish_type=0;
        }
        $dish_trans=$request->only(['en','ar']);



        $dish=Dish::create([
            // 'name'=>$request->name,
            'images'=>$dishes_images,
            'user_id'=>$cook_id,
            'calories'=>$request->calories,
'portions_available'=>json_encode($request->portions_available, true),
            'portions_price'=>json_encode($request->portions_price,
true),
            // 'available'=>$request->dish_available,
            'category_id'=>$request->category,
            'section_id'=>$request->section,
            // 'main_ingredients'=>$request->main_ingredients,
            // 'info'=>$request->info,
            // 'available_count'=>$request->available_count,
        ]);


        $branches=User::where('parent' ,auth()->guard('admin')->user()->id )->pluck('id');
        if(count($branches) > 0){//branch
            foreach($branches as $branch_id){
                $avalability=Availability::create(['user_id'=>$branch_id,'dish_id'=>$dish->id,'available'=>$request->dish_available]);
            }
        }//main
        $avalability=Availability::create(['user_id'=>$cook_id,'dish_id'=>$dish->id,'available'=>$request->dish_available]);




        if(isset($request->info)){
            $dish->update(['info'=>$request->info]);
        }
        // dd($dish_trans);
        $dish->update($dish_trans);

        foreach($request->cusine as $cusine){
            $dish->cusines()->attach($cusine);
        }

        if($request->addonsection){
        foreach($request->addonsection as $addonsection){
            $dish->addonsections()->attach($addonsection);
        }


        }
        // if($request->addons){
        //     for ($i=0; $i <count($request->addons) ; $i++) {
        //         if($request->addons[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>$request->min[$i],'max'=>$request->max[$i],'condition'=>$request->addon_conditions[$i]]);
        //         }
        //     }
        // }


        // foreach($request->addons as $addon){
        //     $dish->addons()->attach($addon);
        // }

        if(isset($request->allergens) > 0){
            foreach($request->allergens as $allergen){
                $dish->allergens()->attach($allergen);
            }
        }

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $avalability=Availability::where(['user_id'=>auth()->guard('admin')->user()->id,'dish_id'=>$id])->first();

        if($avalability->available == 0){
            $status=1;
        }else{
            $status=0;

        }

        $avalability->update([
            'dish_id'=>$id,
            'user_id'=>auth()->guard('admin')->user()->id,
            'available'=>$status
        ]);
        return response()->json(['success' => $status]);



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $allergens=Allergen::all();
        $addons=Addons::all();
        // $sections=Section::all();
        $cusines=Cusine::all();
        $categories=Category::all();
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $dish=Dish::find($id);
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->where('active',1)->get();
            $dish=Dish::find($id);
            $cook=User::find($dish->user_id);
            $sections=$cook->sections;

            $addonsections=$cook->addonsection;
        // $sections=Section::all();

        return view('backend.edit-dish-list',compact('dish','addonsections','cusines','sections','addons','categories','allergens','cooks','currency'));
        }
        else{
        // $sections=Section::all();

            $dish=Dish::find($id);
            $cook=User::find(auth()->guard('admin')->user()->id);
            $addons=$cook->addons;
            $addonsections=$cook->addonsection;
            $sections=$cook->sections;
            $categories=$cook->categories;

            return view('backend.edit-dish-list',compact('dish','addonsections','cusines','sections','addons','categories','allergens','currency'));
        }



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $dish=Dish::find($id);
         $rules=[
            'cusine'=>'required',
            'category'=>'required',
            // 'addons'=>'required',
            'calories'=>'required',
            'section'=>'required',
            'portions_available'=>'required',
            'dish_available'=>'required|min:1',
            // 'portions_price'=>'required|min:1'
            // 'allergens'=>'required',
            // 'available_count'=>'required',
            'portions_price.0'=>'required|min:1|regex:/^\d*\.?\d*$/',
        ];
        foreach ($request->portions_price as $index=>$portions_price) {
            if($portions_price != null){

                $rules += [
                    'portions_price.'.$index=>'regex:/^\d*\.?\d*$/',
                ];
            }


        }
        $request->validate($rules);
        // dd($rules);
        if(auth()->guard('admin')->user()->roles[0]->name =='cook'){
            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.info'=>['required'],
                ];
            }
            $request->validate( $rules);

            $cook_id=auth()->guard('admin')->user()->id;
        }
        else{

            $rules +=['cook_name'=>'required'];

            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.info'=>['required'],
                ];

            }
            // $validator=$request->validate($rules);


            $cook_id=$request->cook_name;

            if($request->dish_type == 'main'){
                $request->dish_type =1;
            }else{
                $request->dish_type=0;
            }
        }
        $dish_trans=$request->only(['en','ar']);


        $dish->update([
            // 'name'=>$request->name,
            // 'images'=>$dishes_images,
            'user_id'=>$cook_id,
            'calories'=>$request->calories,
            'portions_available'=>json_encode($request->portions_available, true),
            'portions_price'=>json_encode($request->portions_price,true),
            'available'=>$request->dish_available,
            'category_id'=>$request->category,
            'section_id'=>$request->section,
            'main_ingredients'=>$request->time_of_preparation,
            // 'info'=>$request->info,
            // 'available_count'=>$request->available_count,
        ]);

        $avalability=Availability::where(['user_id'=>$cook_id,'dish_id'=>$id])->first();
        $avalability->update(['available'=>$request->dish_available]);




        $dish->update($dish_trans);

        $dish->cusines()->sync($request->cusine);

        if($request->addonsection){
            $dish->addonsections()->detach();
            foreach ($request->addonsection as  $addonsection) {
                $dish->addonsections()->attach($addonsection);

            }
        }






        if ($request->file('files')) {

        $request->validate( ['files.*'=>'required|max:5000|image|mimes:png,jpg,gif,svg']);


            $dishes_images=array();
            foreach($request->file('files') as $index=>$img){

                $ext=$img->getClientOriginalExtension();
                $image_name=time().$index.'.'.$ext;
                $path='backend/img/dishes/';
                // $img->move($path,$image_name);

                Image::make($img)->resize(300, null, function ($constraint) {
                    // dd('a');
                    $constraint->aspectRatio();
                })->save( $path.$image_name);
                array_push($dishes_images,$image_name);
                }
                $dishes_images=implode('__',$dishes_images);
            $dish->update([
                'images'=>$dishes_images,

            ]);

        }


        // if($request->addons){
        //     $dish->addons()->detach();

        //     for ($i=0; $i <count($request->addons) ; $i++) {
        //         if($request->addons[$i] != null && $request->min[$i] != null && $request->max[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>$request->min[$i],'max'=>$request->max[$i],'condition'=>$request->addon_conditions[$i]]);
        //         }
        //         elseif($request->addons[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>1,'max'=>1,'condition'=>0]);

        //         }

        //     }

        // }else{
        //     $dish->addons()->detach();
        // }
        // foreach($request->addons as $addon){
        //     $dish->addons()->attach($addon);
        // }

        // foreach($request->allergens as $allergen){
        //     $dish->allergens()->attach($allergen);
        // }


        return redirect()->to('/shabab/dish');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dish_trans=DishTranslation::where('dish_id',$id)->delete();
        $dish=Dish::find($id);


            $dish->update([
                'section_id'=>null,
            ]);
            if( ! empty( $dish->cusines[0])){
                $dish->cusines()->detach();

            }
            if( ! empty( $dish->allergens[0])){
                $dish->allergens()->detach();
            }
            if( ! empty( $dish->addonsections[0])){
                $dish->addonsections()->detach();
            }
            // if( ! empty( $dish->addons[0])){
            //     $dish->addons()->detach();

            // }
            // if( asset( $dish->discounts)){
            //     dd('a');
            //     $dish->discounts()->detach();

            // }

        $dish_trans=DishTranslation::where('dish_id',$dish->id)->delete();


        $branches=User::where('parent' ,auth()->guard('admin')->user()->id )->pluck('id');
        if(count($branches) > 0){//branch
            foreach($branches as $branch_id){
                $avalability=Availability::where(['user_id'=>$branch_id,'dish_id'=>$dish->id])->delete();
            }
        }
        $avalability=Availability::where(['user_id'=>auth()->guard('admin')->user()->id,'dish_id'=>$dish->id])->delete();

        \DB::table('discount_dish')->where('dish_id', $dish->id)->delete();

        $dish->delete();

        return back();
    }
}

