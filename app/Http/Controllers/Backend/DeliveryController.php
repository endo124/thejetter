<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\DeliveryCharges;
use App\Models\User;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveryCharges=DeliveryCharges::all()->pluck('cook_id');

        $vendors=User::whereHas('roles',function($q){
            $q->where('name','cook');
        })->where('active',1)->get();

        $cooks=User::whereHas('roles',function($q){
            $q->where('name','cook');
        })->where('active',1)->whereIn('id' , $deliveryCharges)->with('deliveryCharges')->get();
        return view('backend.delivery_charges',compact('cooks','vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'Vendor'=>'required',
            'delivery_type'=>'required',
            'amount'=>'required|numeric'
        ];

        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{

            $delivery=DeliveryCharges::create([
                'cook_id'=>$request->Vendor,
                'amount'=>$request->amount,
                'from'=>$request->from,
                'to'=>$request->to,
                'orders'=>$request->orders,
                'delivery_type'=>$request->delivery_type
            ]);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $delivery=DeliveryCharges::find($id);

        $rules = [
            'Vendor'=>'required',
            'delivery_type'=>'required',
            'amount'=>'required|numeric'
        ];

        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'category_id'=>$delivery->id]);
        }else{

            $delivery->update([
                'cook_id'=>$request->Vendor,
                'amount'=>$request->amount,
                'from'=>$request->from,
                'to'=>$request->to,
                'orders'=>$request->orders,
                'delivery_type'=>$request->delivery_type
            ]);
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery=DeliveryCharges::find($id)->delete();
        return back();

    }
}
