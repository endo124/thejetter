<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Middleware\customer;
use App\Models\Address;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=User::whereHas('roles',function($q){
            $q->where('name','customer');
         })->where('active','!=','-1')->get();
         return view('backend.customer-list',compact('customers'));
    }

    public function vip_customer( $id)
    {
        // $customer=User::find($id);

        User::where('id',$id)->update([
            'VIP'=>'1'
        ]);
        return response()->json(['success' => true]);
    }

    public function vip_reset( $id)
    {

        User::where('id',$id)->update([
            'VIP'=>'0'
        ]);
       return response()->json(['success' => true]);
    }

    public function vip()
    {
        $customers=User::where('VIP','1')->whereHas('roles',function($q){
            $q->where('name','customer');
         })->where('active','!=','-1')->get();
         return view('backend.vip-customer-list',compact('customers'));
    }

    public function editAddress( $id){

        $address=Address::find($id);
        $customer=User::find($address->user_id);
        $googleapikey=Setting::first()->googleapikey;
        return view('backend.customeraddress',compact('address','customer','googleapikey'));
    }


    public function updateAddress(Request $request , $id){

        $validator = \Validator::make($request->all(), [
			'address' => 'required',
            'name' => 'required',
			'phone' => 'numeric',
            'building'=>'required'

		]);
        $address = Address::where('id', $id)->update([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'phone' => $request->get('phone'),
            'building'=>$request->building,
            'lat'=>$request->lat,
            'lon'=>$request->lng,
            'coordinates'=>$request->lat.','.$request->lng,

		]);
        return back();
    }


    public function deleteAddress($id){
        $address=Address::where('user_id',$id)->first();
        $customer=User::where('default_address', $id)->update(['default_address' => null]);
        $address->delete();
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // dd($id, $request->password);
        $user=User::find($id);
        $validate=$request->validate([
            'name'=>'required',
            'phone'=>'required|unique:users,id,'.$id,
            'email'=>'required|unique:users,id,'.$id,
            // 'password'=>'required',
        ]);
       $user->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>Hash::make($request->password)
        ]);

        $user->save();


        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cook=User::find($id);
        $cook->update(['active'=>-1]);
        return back();
    }
}
