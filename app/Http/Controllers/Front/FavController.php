<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FavController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fav($lang, Request $request)
    {

        $customer=User::find(auth('customer')->user()->id);
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '.$customer->token
        ];
        $res = $client->request('POST', 'http://thejet.com.sa/api/fav/'.$lang,[

                'headers'=>$headers,
                'json' => [
                            "dish_id"=>$request->id,
                        ],
        ]);

        $data=json_decode($res->getBody());
        if($data->msg == 'successfully added to wishList'){
            return  response()->json([
                'status' => true,
                'msg'=>'added'
            ]);
        }else{
            return  response()->json([
                'status' => true,
                'msg'=>'removed'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
