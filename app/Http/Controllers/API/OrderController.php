<?php

namespace App\Http\Controllers\API;

use App\Events\NewOrder;
use App\Http\Controllers\Controller;
use App\Mail\OrderDeliverd;
use App\Mail\OrderConfirmation;
use App\Models\Addons;
use App\Models\AddonsTranslation;
use App\Models\Address;
use App\Models\DeliveryCharges;
use App\Models\Discount;
use App\Models\Dish;
use App\Models\SectionTranslation;
use App\Models\DishTranslation;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use App\Models\UserTranslation;
use Illuminate\Http\Request;
use Validator;
use App\Traits\GeneralTrait;
use DateTime;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use GuzzleHttp\Client;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)// current orders
    {
        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();


        // $getUser = $database
        // ->getReference('users')
        // ->orderByChild('email')
        // ->equalTo('hesham.zaki.99@hotmail.com')
        // ->getValue();
        // $user=array_keys($getUser);
        // $orders = $database
        // ->getReference('/users/'.$user[0].'/tasks')
        // ->getSnapshot()
        // ->getValue();
        // $logistaorders=Order::whereIn('logista_order_id',array_keys($orders))->get();
        // dd($logistaorders);

        $orders=Order::where('orders.user_id',auth('api_customer')->user()->id)
        ->WhereNotNull('status')
        // ->where('status' , '!=' , '5')
        ->rightJoin('user_translations' ,'orders.cook_id' , '=' , 'user_translations.user_id')
        ->where('locale',$lang)
        ->rightJoin('users' ,'orders.cook_id' , '=' , 'users.id')
        ->rightJoin('address' ,'orders.cook_id' , '=' , 'address.user_id')
        ->select('orders.id',
        'orders.user_id',
        'orders.logista_order_id',
        'orders.delivery_estimated_time',
        'orders.cook_id','date','time','orderEntry',
        'total_price','delivery_fees','orders.address','payment_method',
        'payment_status','delivery_type','status','assign','user_translations.name',
        'images','taxes_type','taxes','note','orders.delivery_taxes','orders.discount_price','orders.sub_total','orders.vendor_taxes',
        'coordinates'
        )
        ->get();

        return $this->returnData('orders',$orders);

    }

    // public function pastorders($lang){
    //     $orders=Order::where('orders.user_id',auth('api_customer')->user()->id)
    //     ->WhereNotNull('status')
    //     ->where('status' , '==' , '5')
    //     ->rightJoin('user_translations' ,'orders.cook_id' , '=' , 'user_translations.user_id')
    //     ->where('locale',$lang)
    //     ->rightJoin('users' ,'orders.cook_id' , '=' , 'users.id')
    //     ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','delivery_fees','address','payment_method','payment_status','delivery_type','status','assign','name','images','taxes_type','taxes')
    //     ->get();

    //     return $this->returnData('orders',$orders);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $customer=User::where('id',auth('api_customer')->user()->id)->first();
        if($customer->active !=1 ){
            return response()->json(['error' => "sorry you can't order your account is inactive"], 401);
        }

        $allow = 0;
        $time= date("H:i");
        $date=date("Y-m-d",time());
        $cook=User::where('id',$request->cook_id)->first();//cook work from sat and sat is index (4)date('w')+1

        $from=(json_decode($cook->work_from));
        $to=(json_decode($cook->work_to));
        if(date('w') == '6'){
            $from=strtotime($from[0]);
            $to=strtotime($to[0]);
        }else{
            $from=strtotime($from[date('w')+1]);
            $to=strtotime($to[date('w')+1]);
        }

        $current=((time()));
        if ($to < $from) {
           $to=strtotime('+1 day', $to);
        }
        if($current >= $from && $current <= $to){
            $allow = 1;

        }

        if($cook->availability != 1 || $allow != 1){
            return response()->json(['error' => true, 'msg' => 'Vendor not available now']);
        }
        $address=Address::find($request->address);
        if(isset($address->phone)){
            $customer_phone=$address->phone;
        }else{
            $customer_phone=auth('api_customer')->user()->phone;
        }

        $coordinates=explode(",",$cook->address[0]->coordinates);


        if($coordinates[0] == ''){
            return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
        }

        $order=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();
        if( ! isset($order)){
            return response()->json(['error' => true, 'msg' => 'there is no dishes in cart']);
        }


        if ($request->delivery_type !='pickup' && $order->delivery_type == 1 ) {
            $customer=User::find(auth('api_customer')->user()->id);
            $key=Setting::first()->googleapikey;
            $client = new Client();

            $data = $client->request('GET', 'https://maps.googleapis.com/maps/api/directions/json?origin='.$cook->address[0]->lat.','.$cook->address[0]->lon.'&destination='.$address->lat.','.$address->lon.'&key='.$key);
            $data = json_decode($data->getBody());
            if ($data->status == 'OK') {
                $order->update(['delivery_estimated_time'=>$data->routes[0]->legs[0]->duration->value]);
            }
        }

        $details=$order->orderEntry;
        $orders=[];
        foreach ( $details as $or) {
            $item=[];
            $item['id']=(string)$or['id'];
            $item['name_ar']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            $item['name_en']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'en'])->first()->name;
            $item['price']=$or['price'];
            $item['quantity']=(int)$or['qty'];
            $item['notes']=$or['notes'];

            $item['addons']=[];

            $section_id=Dish::find($item['id'])->sections->id;
            $item['section_ar']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'ar'])->first()->name;
            $item['section_en']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'en'])->first()->name;
            $item['size_en']=$or['size'];
            if($or['size'] =='small'){
                $item['size_ar']='صغير';
            }elseif ($or['size'] =='large') {
                $item['size_ar']='كبير';
            }else{
                $item['size_ar']='متوسط';
            }
            if(isset($or['addons'])){
                $adds=[];

                foreach ($or['addons'] as  $addon) {
                    $add=[];
                    $add['add_ar']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'ar'])->first()->name;
                    $add['add_en']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'en'])->first()->name;
                    $add['add_price']=(string)(Addons::find($addon['id'])->price * $or['qty']);
                    array_push($adds,$add);
                }
                $item['addons']=$adds;
            }
            array_push($orders, $item);
        }

        if($request->delivery_type =='pickup'){
            $order->update([
                'vendor_taxes'=>$request->vendor_taxes,
                'total_price'=>round($request->total_price, 2) ,
                'time'=>$time,
                'date'=>$date,
                'orderEntry'=>$orders,
                'status'=>'1',
                'payment_method'=>$request->payment_method ==1 ? 1 : 0,//0 for cash and 1 for credit
                'delivery_type'=>0,
                'note'=>$request->note,
                'discount_price'=>round($request->discount_price, 2) ,
                'sub_total'=>$request->total_price,
                'payment_status'=>$request->payment_method ? 1 : 0

            ]);
            if($request->coupon_id != null){
                $order->update(['discount_id',$request->coupon_id]);
                $coupon=Discount::find($request->coupon_id);
                $order = $order->coupon()->associate($coupon);
                $order->save();
            }


            event(new NewOrder($order->id,$order));
            $this->sendWebNotification($order);
            $lang=$request->lang;


            // Mail::to($customer->email)->send(new OrderConfirmation($order , $lang ));

            return $this->returnData('msg','success');

        }

        $customer_coordinates=explode(",",$address->coordinates);
        $validator=\Validator::make($request->all(), [

            'address' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $admin=User::first();

        if($request->delivery_flag == 'Free' || $request->delivery_flag == 'Fixed Rate'){//delivery charge system
            $deliveryCharges=DeliveryCharges::where('cook_id',$request->cook_id)->first();
            $deliveryCharges->update(['used'=>$deliveryCharges->used+1]);
        }

        $order->update([
            'vendor_taxes'=>$request->vendor_taxes,
            'delivery_taxes'=>$request->delivery_taxes,
            'delivery_fees'=>$request->delivery_fees,
            'total_price'=>round($request->total_price, 2) ,
            'time'=>$time,
            'date'=>$date,
            'address'=>$address['address'],
            'customer_address_id'=>$request->address,
            'orderEntry'=>$orders,
            'status'=>'1',
            'payment_method'=>$request->payment_method ==1 ?? 0,//0 for cash and 1 for credit
            'delivery_type'=>1,
            'change'=>$request->change,
            'note'=>$request->note,
            'order_lang'=>$request->lang,
            'discount_price'=>round($request->discount_price, 2) ,
            'sub_total'=>$request->sub_total,
            'paid'=>$order->payment_method ?? 0,
            'payment_status'=>$request->payment_method  ==1 ?? 0




        ]);

        if($request->coupon_id != null){
            $coupon=Discount::find($request->coupon_id);
            $order = $order->coupon()->associate($coupon);
            $order->save();


        }


        $details=$order->orderEntry;
        $orders=[];
        foreach ( $details as $ord) {
            $item=[];
            $item['id']=(string)$ord['id'];
            $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            $item['price']=$ord['price'];
            $item['quantity']=(int)$ord['quantity'];

            $item['note']=$request->change;
            array_push($orders, $item);
        }
        $date=strtotime($date . " " . $time);
        $customer=User::find(auth()->guard('api_customer')->user()->id);


        event(new NewOrder($order->id,$order));
        $this->sendWebNotification($order);
        $lang=$request->lang;

        //$this->sendToLogista($order->id);

        // Mail::to($customer->email)->send(new OrderConfirmation($order , $lang ));
        return $this->returnData('msg','success');
    }


    public function sendToLogista($order_id ){
            $order=Order::Find($order_id);
            $cook=User::find($order->cook_id);
            $customer= User::Find($order->user_id);
            $date=$order->date;
            $time=$order->time;


            $date=strtotime($date . " " . $time);


            $details=$order->orderEntry;
            $orders=[];
            $notes=[];
            foreach ( $details as $ord) {
                $item=[];
                $item['id']=(string)$ord['id'];
                $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
                $item['price']=(string)$ord['price'];
                $item['quantity']=(int)$ord['quantity'];


                if (isset($ord['notes'])) {
                    $item['note']=(string)$ord['notes'];
                    array_push($notes, $item['note']);
                }
                array_push($orders, $item);

            }
            foreach ($details as $ord) {
                foreach ( $ord['addons'] as $index=>$add) {
                  $item['id']=(string)$index;
                  $item['name']=$add['add_ar'];
                  $item['price']=(string)$add['add_price'];
                  $item['quantity']=1;
                  array_push($orders, $item);
                }
            }
            if($order->delivery_type == 1){
                $address=Address::find($order->customer_address_id);
                if(isset($address->phone)){
                    $customer_phone=$address->phone;
                }else{
                    $customer_phone=$customer->phone;
                }
                $coordinates=explode(",",$cook->address[0]->coordinates);
                $customer_coordinates=explode(",",$address->coordinates);
            }else{
                $coordinates=["",""];
                $customer_coordinates=["",""];
                $address=['address'=>''];
                $customer_phone=$customer->phone;
            }





            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
            ->create();
            $database = $firebase->getDatabase();

            $getUser = $database
            ->getReference('users')
            ->orderByChild('email')
            ->equalTo('hesham.zaki.99@hotmail.com')
            ->getValue();
            $user=array_keys($getUser);

            $newOrder = $database
            ->getReference('/users/'.$user[0].'/tasks')
            ->push(
                [
                'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
                'name'=>$address['address']],
                'date' => (string)$date,
                'description' => '',
                'email'=>$customer->email,
                'name' => $customer->name,
                'orderId' => (string)$order['id'],
                'phone' =>  $customer_phone],
                'distance'=>'',
                'driverId'=>'',
                'estTime'=>'',
                'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
                'date' => (string)$date,
                'description' => '',
                'email'=>$customer->email,
                'name' => $customer->name,
                'orderId' => (string)$order['id'],
                'phone' =>  $customer_phone],
                'status'=>-1,
                'orderItems' => $orders,
                'type'=>'pickupOnDelivery',
                'note'=>$order->note,
                'notes'=>$notes,
                'change'=>$order['change'],
                'fees'=>(string)$order->delivery_fees,
                'taken'=>false,
                'vendor_taxes'=>$order->vendor_taxes ?? '',
                'discount_price'=>$order->discount_price ?? '',
                'delivery_taxes'=>$order->delivery_taxes ?? '',
                'sub_total'=>$order->sub_total,
                'total_price'=>$order->total_price,
            ]);

            $logista_tasks = $database
            ->getReference('/users/'.$user[0].'/tasks')
            ->getSnapshot()->getValue();

            $order->update([
                'logista_order_id'=>array_key_last($logista_tasks)
            ]);

    }



    public function get_paid(Request $request)
    {
        // $address=Address::find($request->address);
        // $coordinates=explode(",", $address->coordinates);
        // $cook=User::find($request->cook_id);


        // $cook_coordinates=explode(",", $cook->address[0]['coordinates']);
        // $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);


        // dd($distance ,$address->coordinates ,$cook->address[0]['coordinates']);
        $validator=\Validator::make($request->all(), [
            'cook_id' => 'required',
            'address' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
            // $factory = (new Factory)
            // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json') ;
            // $database = $factory->createDatabase();

            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
            ->create();
            $database = $firebase->getDatabase();
            // dd($database );
            //get value of all the unique ID

            $getUser = $database
                ->getReference('users')
                ->orderByChild('email')
                ->equalTo("hesham.zaki.99@hotmail.com")
                ->getValue();
            $user=array_keys($getUser);
            // dd($getUser,$user);

            $getZones = $database
                ->getReference('/users/'.$user[0].'/geoFences')

            ->getValue();
            $zones=(array_values($getZones));
            $arr_check=[];
            foreach($zones as $zo)
            {

                $arr_x=[];
                $arr_y=[];
                foreach($zo['cords'] as $zone)
                {
                    array_push($arr_x,$zone['lat']);
                    array_push($arr_y,$zone['lng']);
                }

                $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array

                // $cords=explode(",", str_replace('"', '', $request->coordinates_new));
                 // dd('fdhh');
                //  dd($cords);
                $address=Address::find($request->address);
                $coordinates=explode(",", $address->coordinates);
                // dd($coordinates);
                if($coordinates[0] !=''){
                    $check=$this->is_in_polygon($points_polygon, $arr_x, $arr_y,$coordinates[0],$coordinates[1]);

                }else{
                    return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
                }
                // dd($coordinates[0],$coordinates[1]);
                if ($check){
                    array_push($arr_check,$zo);
                }

            }
            if($arr_check==[])
            {
                return response()->json(['error' => true, 'msg' => 'Out Of Zones']);
            }
            $deliveryCharges=DeliveryCharges::where('cook_id',$request->cook_id)->first();
            // dd($deliveryCharges);
            $cook=User::find($request->cook_id);
            // return($cook);
            // dd($cook->default_address);
            if(isset($cook->default_address)){
                // dd('dd');
                $cook_address=Address::find( $cook->default_address);
                $cook_coordinates=explode(",", $cook_address->coordinates);
                // dd($cook_address);
                $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);
                $distance =round($distance,1);
                // dd($distance);
            }elseif (isset($cook->address)) {
                $cook_address=Address::find( $cook->address);
                // dd($cook_address);
                $cook_coordinates=explode(",", $cook->address[0]['coordinates']);
                if($cook_coordinates[0] == ''){
                     return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
                }
                $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);
                // dd('dd');
                $distance =round($distance,1);
            }else{
                return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
            }
            $initialfees=Setting::first()->fees;
            $maxfees=Setting::first()->max_fees;
            if(isset($deliveryCharges)){//check if this cook in delivery charge [time or order or both]




                if(isset($deliveryCharges->from) && isset($deliveryCharges->to) ){
                    $current = date('Y-m-d');
                    $from = date('Y-m-d', strtotime($deliveryCharges->from));

                        $to = date('Y-m-d', strtotime($deliveryCharges->to));


                        if (($current >= $from) && ($current <= $to)){
                            $in_range =1;

                            if(isset($deliveryCharges->orders)) {
                                if($deliveryCharges->orders > $deliveryCharges->used   ){
                                    if($deliveryCharges->delivery_type == 0){
                                                $data=['cost'=>"0" ,'flag' =>'Free' ];
                                    }else{
                                        $data=['cost'=>$deliveryCharges->amount ,'flag' =>'Fixed Rate' ];
                                    }

                                    // $deliveryCharges->update(['used'=>$deliveryCharges->used+1]);
                                    // dd($deliveryCharges->used+1);
                                    return response()->json([
                                        'success' => true,
                                        'msg' => 'Get Orders successfully.',
                                        'data' => [$data],
                                        'distance'=>1,
                                        'initial delivery fees'=>"0"
                                    ], 200);
                                }else {
                                    $distance =round($distance,1);
                                return response()->json([
                                    'success' => true,
                                    'msg' => 'Get Orders successfully.',
                                    'data' => array_values($arr_check[0]['flags']),
                                    'distance'=>$distance,
                                    'initial delivery fees'=>$initialfees,
                                    'max delivery fees'=>$maxfees


                                ], 200);
                                }
                            }else {
                                if($deliveryCharges->delivery_type == 0){
                                    $data=['cost'=>"0" ,'flag' =>'Free' ];
                        }else{
                            $data=['cost'=>$deliveryCharges->amount ,'flag' =>'Fixed Rate' ];
                        }

                        // $deliveryCharges->update(['used'=>$deliveryCharges->used+1]);
                        // dd($deliveryCharges->used+1);
                        return response()->json([
                            'success' => true,
                            'msg' => 'Get Orders successfully.',
                            'data' => [$data],
                            'distance'=>1,
                            'initial delivery fees'=>"0"
                        ], 200);
                            }




                        }else{
                            $in_range =0;
                            $distance =round($distance,1);
                            return response()->json([
                                'success' => true,
                                'msg' => 'Get Orders successfully.',
                                'data' => array_values($arr_check[0]['flags']),
                                'distance'=>$distance,
                                'initial delivery fees'=>$initialfees,
                                'max delivery fees'=>$maxfees


                            ], 200);
                        }


                }else { // from - to == null
                    if($deliveryCharges->orders != null && $deliveryCharges->orders > $deliveryCharges->used   ){
                        if($deliveryCharges->delivery_type == 0){
                                    $data=['cost'=>"0" ,'flag' =>'Free' ];
                        }else{
                            $data=['cost'=>$deliveryCharges->amount ,'flag' =>'Fixed Rate' ];
                        }

                        // $deliveryCharges->update(['used'=>$deliveryCharges->used+1]);
                        // dd($deliveryCharges->used+1);
                        return response()->json([
                            'success' => true,
                            'msg' => 'Get Orders successfully.',
                            'data' => [$data],
                        ], 200);
                    }else {
                        $distance =round($distance,1);
                    return response()->json([
                        'success' => true,
                        'msg' => 'Get Orders successfully.',
                        'data' => array_values($arr_check[0]['flags']),
                        'distance'=>$distance,
                        'initial delivery fees'=>$initialfees,
                        'max delivery fees'=>$maxfees


                    ], 200);
                    }
                }






            }else{
                $distance =round($distance,1);

                return response()->json([
                    'success' => true,
                    'msg' => 'Get Orders successfully.',
                    'data' => array_values($arr_check[0]['flags']),
                    'distance'=>$distance,
                    'initial delivery fees'=>$initialfees,
                    'max delivery fees'=>$maxfees

                ], 200);
            }

    }

    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
		if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
		 ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
		   $c = !$c;
      }
	  return $c;
	}

    function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
      {
        if (($latitudeFrom == $latitudeTo) && ($longitudeFrom == $longitudeTo)) {
            return 0;
        }else{
            $customer=User::find(auth('api_customer')->user()->id);
            $client = new Client();
            $data = $client->request('GET', 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$latitudeFrom.','.$longitudeFrom.'&destinations='.$latitudeTo.','.$longitudeTo.'&key=AIzaSyArUJhAM_MY5imJh7g6l6-rgcBOMyOS63s' );
            $data = json_decode($data->getBody());
            // if( collect(collect($data->rows[0])['elements'][0])['status'] == 'ZERO_RESULTS'){
            //     return
            // }
            $miles =floatval(collect(collect(collect($data->rows[0])['elements'][0])['distance'])['text']);

        // $theta = $longitudeFrom - $longitudeTo;
        // $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        // $dist = acos($dist);
        // $dist = rad2deg($dist);
        // $miles = $dist * 60 * 1.1515;
        $km = $miles * 1.609344;
        // dd($km);
        return $km;
        }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $currency=Setting::first();
        // if(isset($currency->currency)){
        //     $currency=$currency->currency;
        // }else{
        //     $currency='EGP';
        // }

        // if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
        //     $orders=Order::where('user_id',$id)->whereNotNull('status')
        //     ->where('status', '!=', 1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        //     $currentActivities=Order::where('user_id',$id)->where('status',1)->get();
        // }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

        //     $orders=Order::where('user_id',$id)->where('cook_id',auth()->guard('admin')->user()->id)
        //     ->whereNotNull('status')
        //     ->where('status', '!=', 1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        //     $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('user_id',$id)->
        //     ->where('status',1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        // }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function updateFcnToken(Request $request)
    {
        $devices[$request->device_id] = $request->token;
        // dd($devices);
        $user=auth('api_customer')->user();
        // dd((array)(json_decode($user->device_key)));
        $devs=(array)(json_decode($user->device_key));
        foreach ($devs as  $key=>$dev) {
            if( $key!= $request->device_id){
                $devices[$key] = $dev;
            }
        }

        $user->update(['device_key'=>json_encode($devices)]);
        return $this->returnData('msg','token updated successfully');

    }


    public function sendWebNotification($order)
    {

        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey='AAAAxJAyh0Q:APA91bEWjkR_pj_iYBdOAyu4xvFFWKsfwMPm7LoD9c_gqcIGRCH7lSPLfMKt97Zjkb2xBiZPuo4jK-ByumcTaGlSIVhAaoUNLUyo0oBysz3wf4LIue36onIZTM76U4bXX2UwGQtbkK19';

        $user=User::find($order->cook_id);
        // dd($user->device_key );
       $devices= ( array)(json_decode($user->device_key));
       $devs=[];
       foreach ($devices as $device) {
           array_push($devs, $device);
       }

        $data = [
            "registration_ids" =>array_values( $devs),
            "notification" => [
                // "id" => $order->id,
                // "body" => $order->body,
                // "time" =>$order->updated_at,
                // "status"=> $order->status
                "body" => $order->status,
                "title" =>$order->id,
                "sound" => "notify.mp3",

            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        // FCM response
        return($result);
    }


    public function driver(Request $request , $logista_order_id){

        $order=Order::where('logista_order_id',$logista_order_id)->first();
        $order->update(['driver_name'=>$request->driver_name ,'status'=>11]);

        event(new NewOrder($order->id,$order));
        return $this->returnData('msg','driver added successfully');

    }


    public function onTheWay($logista_order_id){
        $order=Order::where('logista_order_id',$logista_order_id)->first();
        $order->update([
            'status'=>6
        ]);
        event(new NewOrder($order->id,$order));

        return $this->returnSuccessMessage('order onTheWay successfully');

    }
    public function logista_delivered($logista_order_id){
        $order=Order::where('logista_order_id',$logista_order_id)->first();
        $order->update([
            'status'=>5,
            'payment_status'=>1
        ]);
        event(new NewOrder($order->id,$order));

        return $this->returnSuccessMessage('order delivered successfully');

    }


}
