<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Billing\HyperPayBilling;
use Devinweb\LaravelHyperpay\Facades\LaravelHyperpay;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Order;
use App\Traits\GeneralTrait;
use Carbon\Carbon;

class HyperPayPaymentController extends Controller
{
    use GeneralTrait;

    /**
     * Create a new PaymentController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function payment(Request $request)
    {
    

        $HYPERPAY_BASE_URL='https://oppwa.com/';
        $ACCESS_TOKEN='OGFjZGE0Y2M3ZDJkYzQ0MzAxN2Q0MWIwZmJhMTVkZmR8VzQ3M1NOMlA5Ng==';
        $ENTITY_ID_MADA='8acda4cc7d2dc443017d41b1f8df5e0b';
        $ENTITY_ID_CREDIT='8acda4cc7d2dc443017d41b17e3a5e07';
        $ENTITY_ID_STC='8acda4cc7d2dc443017d41b17e3a5e07';
        $ENTITY_ID_AMEX='8acda4cc7d2dc443017d41b17e3a5e07';
        $CURRENCY='SAR';


        $user = User::find(auth('api')->user()->id);
        $order=Order::where('user_id',auth('api_customer')->user()->id)
        //->where('status',null)
        ->first();
        // $amount = 1;
        $entity_id='';
        // dd($order);

        if($request->brand=="MADA"){
            $entity_id=env('ENTITY_ID_MADA') ?? $ENTITY_ID_MADA;
        }elseif($request->brand=="STC_Pay"){
            $entity_id=env('ENTITY_ID_STC') ?? $ENTITY_ID_STC;
        }elseif($request->brand=="AMEX"){
            $entity_id=env('ENTITY_ID_AMEX') ?? $ENTITY_ID_AMEX;
        }else{
            $entity_id=env('ENTITY_ID_CREDIT') ??  $ENTITY_ID_CREDIT;
        }
        // $data = 'entityId='.$entity_id .
        // "&amount=92.00" .
        // "&currency=SAR" .
        // "&paymentType=DB";
        // dd( $entity_id,$request->brand,env('ENTITY_ID_MADA'),env('ENTITY_ID_CREDIT'));
        $data = "entityId=".$entity_id .

        "&currency=SAR" .
        "&paymentType=DB".
        "&merchantTransactionId=".$order->id.time().//your unique id from the dataBase


        "&billing.street1=riyadh".
        "&billing.city=riyadh".
        "&billing.state=riyadh".
        "&billing.country=SA".
        "&billing.postcode=123456" .
        "&customer.email=".$user->email.
        "&customer.givenName=".$user->name.
        "&customer.surname=".$user->name
        ;
        if($request->brand=="MADA"){
            $data=$data."&amount=".number_format((float)$request->amount, 2, '.', '');

        }else{


            $data=$data."&amount=".number_format((float)$request->amount, 2, '.', '');

        }




        $url = env("HYPERPAY_BASE_URL")."/v1/checkouts";

        // $url .= "?entityId=".$entityId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                       'Authorization: Bearer '.$ACCESS_TOKEN));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
// dd($responseData);

        $order->update(['payment_method'=>1]);
        return $this->returnData('responseData',(array)json_decode($responseData));
    }

    public function paymentStatus(Request $request)
    {

        $HYPERPAY_BASE_URL='https://oppwa.com/';
        $ACCESS_TOKEN='OGFjZGE0Y2M3ZDJkYzQ0MzAxN2Q0MWIwZmJhMTVkZmR8VzQ3M1NOMlA5Ng==';
        $ENTITY_ID_MADA='8acda4cc7d2dc443017d41b1f8df5e0b';
        $ENTITY_ID_CREDIT='8acda4cc7d2dc443017d41b17e3a5e07';
        $ENTITY_ID_STC='8acda4cc7d2dc443017d41b17e3a5e07';
        $ENTITY_ID_AMEX='8acda4cc7d2dc443017d41b17e3a5e07';
        $CURRENCY='SAR';

        if($request->brand=="MADA"){
            $entity_id=env('ENTITY_ID_MADA') ?? $ENTITY_ID_MADA;
        }elseif($request->brand=="STC_Pay"){
            $entity_id=env('ENTITY_ID_STC') ?? $ENTITY_ID_STC;
        }elseif($request->brand=="AMEX"){
            $entity_id=env('ENTITY_ID_AMEX') ?? $ENTITY_ID_AMEX;
        }else{
            $entity_id=env('ENTITY_ID_CREDIT') ??  $ENTITY_ID_CREDIT;
        }

        //$entity_id=($request->brand=="MADA")?env('ENTITY_ID_MADA'):env('ENTITY_ID_CREDIT');

        Log::debug($request['id'].'payment unique id'.Carbon::now());
        $url = env("HYPERPAY_BASE_URL")."v1/checkouts/".$request['id']."/payment";
        $url .= "?entityId=".$entity_id;

        // dd($url);
        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                           'Authorization: Bearer '.env('ACCESS_TOKEN')));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            if(curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
            $final_result= (array)json_decode($responseData,true);
            Log::debug($final_result['result']['code'].'final_result'.Carbon::now());

            return $this->returnData('final_result',$final_result);
        }




}
