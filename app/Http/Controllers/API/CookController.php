<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class CookController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang ,$items)
    {

        $cooks=User::with('dishes.cusines')->with('address')
        ->whereRoleIs('cook')->where('active','1')->paginate($items);

        return $this->returnData('cooks',$cooks);

    }

    public function vipvendor($lang){
        $cooks=User::with('dishes.cusines')->with('address')
        ->whereRoleIs('cook')->get();
        return $this->returnData('cooks',$cooks);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
        $cook=User::whereRoleIs('cook')
        ->where('id',$id)->first();
        if($cook->parent){

            $dishes= Dish:: with('sections')
            ->join('availabilities', 'availabilities.dish_id', 'dishes.id')
            ->where('availabilities.user_id',$cook->parent)
            ->select('dishes.*', 'availabilities.available as available')->get();
            $cook_id=$cook->parent;
            $cook=User::whereRoleIs('cook')
            ->where('id', $id)
            ->with('address')

            ->first();

                // $dishes=[];
                // foreach ($cook->dishes_availabilities as $index=>$dish) {
                //     $dish=$dish;
                //     $dish->sections=$cook->users->dishes[$index]->sections;
                //     array_push($dishes,$dish);
                // }
                $cook->dishes =$dishes;
                $cook->sections = $cook->users->sections;
        }else{
            // dd(date('H:i:s', time()) );
            $cook=User::whereRoleIs('cook')
            ->where('id',$id)
            ->with('sections')
            ->with(['sections'=>function($q){
                $q->where('availability',1);
            }])
            ->with(['dishes.sections'=>function($q){
                $q->where('availability',1);
            }])
            ->with('dishes','address','dishes.sections')



            ->with(['dishes'=>function($q) use($id){
                $q->join('availabilities', 'availabilities.dish_id', 'dishes.id');
                $q->where('availabilities.user_id',$id);
                $q->select('dishes.*', 'availabilities.available as available');
                $q->orderBy('dishes.section_id');
            }])

            ->first();

        }

        $customers=[];
        if(isset($cook->customers)){
            foreach($cook->customers as $customer){
                array_push($customers , $customer->id);
            }
        }
        $cook['custs']=$customers;

        return response()->json($cook,200);
    }

    function sortByOrder($param1, $param2) {
        return strcmp($param1->order, $param2->order);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
