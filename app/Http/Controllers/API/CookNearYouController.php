<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class CookNearYouController extends Controller
{
    use GeneralTrait;
    /**

     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    public function getaddress($items, $lang, $location){


        $lat = explode(",", $location)[0];
        $lon = explode(",", $location)[1];

        $cooks=User::whereRoleIs('cook')
                ->where('active','1')
                ->with('dishes')
                ->with('address')
                ->join('address','users.id','=','address.user_id')->select('users.*', \DB::raw("
                ( 3959 * acos( cos( radians('$lat') )
                * cos( radians( address.lat ) )
                * cos( radians( address.lon )
                - radians('$lon') )
                + sin( radians('$lat') )
                * sin( radians( address.lat ) ) ) ) AS distance"))
                ->orderBy('distance')
                ->paginate($items);


        return $this->returnData('cooks',$cooks);

    }


    // public function index($items,$lang ,$governorate)
    // {
    //     dd($governorate);
    //     $city=CityTranslation::where('name',$governorate)->first();
    //     if(isset($city)){
    //         $cooks=User::with('dishes')
    //         ->whereRoleIs('cook')
    //         ->where('active','1')
    //         ->where('city_id',$city->city_id)->paginate($items)->get();
    //         return $this->returnData('cooks',$cooks);

    //     }else{
    //         return $this->returnData('cooks',[]);
    //     }


    // }



    // function getaddress($items,$lang,$location)
    // {

    //    $key=Setting::first()->googleapikey;
    //    $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key='.$key;
    //    $json = @file_get_contents($url);
    //    $data=json_decode($json);
    //    $status = $data->status;
    //    if($status=="OK")
    //    {
    //     $address= $data->results;
    //     $address_components= $address[0]->address_components;
    //     // dd(  $address_components );
    //     for ($i = 0; $i < count($address_components); $i++) {
    //         $address= $data->results;
    //         $address_components= $address[0]->address_components;
    //         $addressType = $address_components[$i]->types[0];

    //         if ($addressType == 'administrative_area_level_1') {
    //             $governorate= $address[0]->address_components[$i]->short_name;
    //             return $this->index($items,$lang,$governorate);
    //         }
    //     }

    //    }
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
