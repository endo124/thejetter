<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\User;
use App\Traits\GeneralTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CusineController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $cusines=Cusine::all();
        return $this->returnData('cusines',$cusines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id , $lang , $items)
    {

        $res_ids=[];


        $restaurant_ids = DB::table('dishes')
            ->select('user_id')
            ->Join('cusine_dish', 'dishes.id', '=', 'cusine_dish.dish_id')
            ->where('cusine_id', $id)
            ->distinct()
            ->get();
        foreach ($restaurant_ids as $restaurant_id) {
            array_push($res_ids ,$restaurant_id->user_id);
        }

        $restaurants=User::whereIn('users.id',$res_ids)->where('active',1)
        ->select('users.*')
        ->leftJoin('cook_cusine', 'users.id' , 'cook_cusine.cook_id')
        ->orderBy('cook_cusine.order')->paginate($items);
        // $restaurants=User::whereIn('id',$res_ids)->paginate($items);

        return $this->returnData('restaurants',$restaurants);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
