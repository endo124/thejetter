<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use App\Models\Availability;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class DishController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {


        if (isset(auth('api')->user()->parent)) {
            $cook_id=auth('api')->user()->parent;
            $dishes=Dish::with('sections')
            // ->join('users', 'users.id', 'dishes.user_id')
            ->join('availabilities', 'availabilities.dish_id', 'dishes.id')
            ->where('availabilities.user_id',auth('api')->user()->id)
            ->select('dishes.*', 'availabilities.available as available')->get();
        }else{
            $dishes=Dish::with('sections')->where('dishes.user_id', auth('api')->user()->id)
            ->join('availabilities', 'availabilities.dish_id', 'dishes.id')
            ->where('availabilities.user_id',auth('api')->user()->id)

            ->select('dishes.*', 'availabilities.available as available')->get();
        }




//andrew
        // if(isset(auth('api')->user()->parent)){
        //     $cook_id=auth('api')->user()->parent;
            // $cook=User::whereRoleIs('cook')->where('id',$cook_id);
            // $dishes=[];
            // foreach ($cook->users->dishes as $index=>$dish) {
            //     $dish=$dish;
            //     $dish->sections=$cook->users->dishes[$index]->sections;
            //     array_push($dishes,$dish);
            // }

            // $cook->dishes =$dishes;

            // $cook->sections = $cook->users->sections;
            // $dishes=Dish::with('sections')->with('availabilities')->where('user_id',$cook_id)->get();

        // }else{
        //     $dishes=Dish::with('sections')->with('availabilities')->where('user_id',auth()->guard('api_cook')->user()->id)->get();
        // }

        // dd($dishes);

        return $this->returnData('dishes',$dishes);

    }


    public function status(Request $request)
    {



        // $dish=Dish::find($request->id);

        $avalability=Availability::where(['user_id'=>auth()->guard('api')->user()->id,'dish_id'=>$request->id])->first();

        if( $avalability->available == 1){
            // $dish->update(['available'=>0]);
            $avalability->update(['available'=>0]);

            return $this->returnData('dish','dish is unavailable now');

        }else{
            // $dish->update(['available'=>1]);
            $avalability->update(['available'=>1]);

            return $this->returnData('dish','dish is available now');

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
