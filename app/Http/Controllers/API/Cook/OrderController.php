<?php

namespace App\Http\Controllers\API\Cook;

use App\Events\NewOrder;
use App\Http\Controllers\Controller;
use App\Mail\OrderConfirmation;
use App\Mail\OrderDeliverd;
use App\Models\Address;
use App\Models\DishTranslation;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Traits\GeneralTrait;
use Kreait\Firebase\Factory;
use Illuminate\Support\Facades\Mail;
use Kreait\Firebase\ServiceAccount;

class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();
        return $this->returnData('orders',$orders);
    }

    public function currentorder(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status',1)
        ->orderBy('id', 'DESC')
        ->with('users')
        ->get();
        return $this->returnData('currentorder',$currentActivities);

    }

    public function accept(Request $request,$id){

        $order= Order::where('id',$id)->first();
       if($order->status == 1){
            $order->update([
                'status'=>2,
                'estimated_time'=>$request->time
            ]);

            $cook=User::find($order->cook_id);



            $subtotal=doubleval($order->sub_total) ;
            if($cook->taxes_type && $cook->taxes_type == '%'){
                $subtotal=$subtotal+($subtotal*$cook->taxes)/100;
            }elseif($cook->taxes_type && $cook->taxes_type != '%'){
                $subtotal=$subtotal+doubleval($cook->taxes);
            }
            if($order->delivery_type == 1){

                if($cook->commission_type == '%'){
                    $profit= $subtotal-( $subtotal*doubleval($cook->commission)/100)+doubleval($cook->profit);
                    $admin_commission=$subtotal*doubleval($cook->commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook->commission)+doubleval($cook->profit);
                    $admin_commission=doubleval($cook->commission);

                }
            }else{
                if($cook->pickup_commission_type == '%'){

                    $profit= $subtotal-( $subtotal*doubleval($cook->pickup_commission)/100)+doubleval($cook->profit);
                    // $total_profit=$total_profit -($total_profit * $cook->pickup_commission) / 100;
                    $admin_commission=$subtotal*doubleval($cook->pickup_commission)/100;

                }
                else{
                    $profit= $subtotal-doubleval($cook->pickup_commission)+doubleval($cook->profit);
                    // $total_profit=$total_profit-($cook->pickup_commission *$orders_count) ;
                    $admin_commission=doubleval($cook->pickup_commission);

                }
            }
            $revenue = doubleval( $subtotal)+doubleval($cook->revenue);

            $order->update(['admin_profit'=>$admin_commission,'vendor_profit'=>$subtotal-$admin_commission]);

            $cook->update(['revenue'=>$revenue,'profit'=>$profit]);
            $cook->save();
            $admin=User::first();
            $admin_rev=$admin->revenue + $revenue;
            $admin_pro=$admin->profit + $admin_commission;
            $admin->update(['revenue'=>$admin_rev,'profit'=>$admin_pro]);
            $admin->save();


            $lang=$order->order_lang;
            $admin=User::first();
            $admin->update(['revenue'=>doubleval($admin->revenue) + $order->total_price + doubleval($order->delivery_fees) ]);

        }
       ////////////////////////////////////logista\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        $date=$order->date;
        $time=$order->time;
        $date=(string)strtotime($date . " " . $time);
        $cook=User::find($order->cook_id);
        $customer= User::Find($order->user_id);


        $address=Address::find($order->customer_address_id);
        if(isset($address->phone)){
            $customer_phone=$address->phone;
        }else{
            $customer_phone=$customer->phone;
        }
        // return($address);

        $coordinates=explode(",",$cook->address[0]->coordinates);
        $order_address=Address::find($order->customer_address_id);
        $customer_coordinates=explode(",",$order_address->coordinates);
        // dd($customer_coordinates);
        $details=$order->orderEntry;
        $orders=[];
        $notes=[];

        foreach ( $details as $ord) {
            $item=[];
            $item['id']=(string)$ord['id'];
            $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            // $item['name_en']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'en'])->first()->name;
            $item['price']=(string)$ord['price'];
            $item['quantity']=(int)$ord['quantity'];
            // $item['addons']=[];
            // if(isset($order['addons'])){
            //     $adds=[];

            //     foreach ($order['addons'] as  $addon) {
            //         return $addon;
            //         $add=[];
            //         $add['add_ar']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'ar'])->first()->name;
            //         $add['add_en']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'en'])->first()->name;
            //         $add['add_price']=Addons::find($addon['id'])->price;
            //         array_push($adds,$add);
            //     }
            //     array_push($item['addons'],$adds);
            // }
            // $item['note']=(string)$order['change'];
                if (isset($ord['notes'])) {
                    $item['note']=(string)$ord['notes'];
                    array_push($notes, $item['note']);
                }
            array_push($orders, $item);
        }
        foreach ($details as $ord) {
            foreach ( $ord['addons'] as $index=>$add) {
            $item['id']=(string)$index;
            $item['name']=$add['add_ar'];
            $item['price']=(string)$add['add_price'];
            $item['quantity']=1;
            array_push($orders, $item);
            }
        }

        if ($order->delivery_type == 1) {
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
            ->create();
            $database = $firebase->getDatabase();

            $getUser = $database
            ->getReference('users')
            ->orderByChild('email')
            ->equalTo('hesham.zaki.99@hotmail.com')
            ->getValue();
            $user=array_keys($getUser);
            // dd($user);
            $newOrder = $database
            ->getReference('/users/'.$user[0].'/tasks')
            ->push([
            'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
            'name'=>$address['address']],
                'date' => $date,
                'description' => '',
                'email'=>$customer->email,
                'name' => $customer->name,
                'orderId' => (string)$order['id'],
                'phone' =>  $customer_phone],
                'distance'=>'',
                'driverId'=>'',
                'estTime'=>'',
                'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
                'date' => $date,
                'description' => '',
                'email'=>$customer->email,
                'name' => $customer->name,
                'orderId' => (string)$order['id'],
                'phone' =>  $customer_phone],
                'status'=>-1,
                'orderItems' => $orders,
                'type'=>'pickupOnDelivery',
                'note'=>$order->note,
                'notes'=>$notes,
                'change'=>$order['change'],
                //  'note'=>'change : '.$order->change,
                'fees'=>(string)$order->delivery_fees,
                'taken'=>false,
                // 'restaurant_id'=>$cook->id
                'discount_price'=>$order->discount_price ?? '',

                'vendor_taxes'=>$order->vendor_taxes ?? '',
                'delivery_taxes'=>$order->delivery_taxes ?? '',
                'sub_total'=>$order->sub_total,
                'total_price'=>$order->total_price,

                'paid'=>$order->payment_method == 1 ? 1 :  0,

            ]);


            $logista_tasks = $database
            ->getReference('/users/'.$user[0].'/tasks')
            ->getSnapshot()->getValue();

            $order->update([
                'logista_order_id'=>array_key_last($logista_tasks)
            ]);
        }
       event(new NewOrder($order->id,$order));
       Mail::to($customer->email)->send(new OrderConfirmation($order , $lang ));


        return $this->returnSuccessMessage('order accepted successfully');



    }

    public function inprogress($id){

        $order= Order::where('id',$id)->first();
       if($order->status == 2){
        $order->update([
            'status'=>3
        ]);
        event(new NewOrder($order->id,$order));

        return $this->returnSuccessMessage('order now in progress ');

       }

    }
    public function shipped($id){
        $order= Order::where('id',$id)->first();

        if($order->status == 2){//after accept order  shipped from app there is no in progress
            $order->update([
                'status'=>4
            ]);
            event(new NewOrder($order->id,$order));
        }


        return $this->returnSuccessMessage('order shipped successfully');

    }

    public function reject(Request $request,$id){
        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>0,
            'reason'=>$request->reason

        ]);
        return $this->returnSuccessMessage('order rejected successfully');

    }

    public function accepted(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        // ->where('status', '!=', 1)//not pending
        // ->where('status', '!=', 0)// not rejected
        // // ->where('status', '!=', 4)//not shipped
        // ->where('status', '!=', 6)//not delivered
        // ->where('status', '!=', 5)//not delivered
        ->whereIn('status', [2,11])// accepted ,assigned
        ->orderBy('id', 'DESC')
        ->with('users')
        ->get();



        return $this->returnData('acceptedorder',$currentActivities);

    }
    public function delivered(){

        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();


        /*$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://chat-8e234-default-rtdb.firebaseio.com')
        ->create();
        $database = $firebase->getDatabase();

        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo('hesham.zaki.99@hotmail.com')
        ->getValue();
        $user=array_keys($getUser);
        $tasks=$database->getReference('/users/'.$user[0].'/tasks')
        ->orderByChild('status')
        ->equalTo(4)
        ->getValue();



        $deliveredorders=Order::where('cook_id',auth()
        ->guard('api_cook')->user()->id)
        ->whereIn('logista_order_id',array_keys($tasks))->with('users')->get();

        $currentorders=Order::where('cook_id',auth()
        ->guard('api_cook')->user()->id)
        ->whereIn('logista_order_id',array_keys($tasks))->update(['status'=>'5']);*/
        $deliveredorders=Order::where('cook_id',auth()
        ->guard('api_cook')->user()->id)
        ->where('status',5)->with('users')->get();

        $current_day=date('d');
        $sales=Order::where('cook_id',auth()->guard('api_cook')->user()->id)->where('status', '!=', 9)->where('status', '!=', 0)->where('status', '!=', 1)->whereDay('updated_at', '=', $current_day)->sum('sub_total');

        $accepted_orders=Order::where('cook_id',auth()->guard('api_cook')->user()->id)->where('status', '!=', 9)->where('status', '!=', 0)->where('status', '!=', 1)->whereDay('updated_at', '=', $current_day)->get();
        $accepted_orders= $accepted_orders->count();

        $delivered_orders=Order::where('cook_id',auth()->guard('api_cook')->user()->id)->where('status', 5)->whereDay('updated_at', '=', $current_day)->get();
        $delivered_orders= $delivered_orders->count();

        $data['deliveredorders']=$deliveredorders;
        $data['sales']=$sales;
        $data['accepted_orders']=$accepted_orders;
        $data['delivered_orders']=$delivered_orders;

        return $this->returnData('data',$data);

    }

    public function logistadelivered($logista_id){//logista api when driver deliverd order
        $order=Order::where('logista_order_id',$logista_id)->first();
        $order->update([
            'status'=>5
        ]);
        $customer=User::find($order->user_id);

        $lang=$order->order_lang;

        Mail::to($customer->email)->send(new OrderDeliverd($customer,$lang));

        return $this->returnSuccessMessage('order delivered successfully');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
