<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Dish;
use App\Models\Order;
use App\Models\SectionTranslation;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class ApplyCouponController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request )
    {

        if($request->name){
            $coupon=Discount::where('name',$request->name)->first();

            $customer_coupons_used_ids=Order::where('user_id',auth('api_customer')->user()->id)->pluck('discount_id');
            $customer_orders=Order::where('user_id',auth('api_customer')->user()->id)->whereNotNull('status')->get();
            if(isset($coupon->cooks)){
                $coupon_cooks=json_decode($coupon->cooks);

            }else{
                $coupon_cooks=[];
            }


            $cart=Order::where('user_id',auth('api_customer')->user()->id)
                ->whereNull('status')
                ->first();
            if(isset($coupon)){
                if($coupon->uses != null && $coupon->uses == 'once' && in_array($coupon->id ,  $customer_coupons_used_ids)){//uses limits
                    return $this->returnData('msg','this coupon used before');
                }elseif((  $coupon->cooks != null || $coupon->cooks != 'all' )&& !in_array($cart->cook_id ,$coupon_cooks) ){//vendors limits
                    return $this->returnData('msg','coupon not include this restaurant');
                }elseif($coupon->orders != 'all' && $request->sub_total < $coupon->orders ){//Orders limits
                    return $this->returnData('msg','subtotal should be over than '.$coupon->total.' SAR');
                }elseif(isset($coupon->customers)){//customers limits

                    switch ($coupon->customers) {

                        case 'repeat':
                            if(count($customer_orders) < 1){
                                return $this->returnData('msg','You can not use this coupon');
                            }
                            break;
                        case 'new':
                            if(count($customer_orders) > 1){
                                return $this->returnData('msg','You can not use this coupon');
                            }
                            break;
                        case 'vip':
                            if(auth('api_customer')->user()->VIP != 1){
                                return $this->returnData('msg','You cant use this coupon');
                            }
                            break;
                            default:

                            $section_ids=[];
                            $cusine_ids=[];
                            $dish_ids=[];

                            $cusine_discount=0;
                            $section_discount=0;
                            $dish_discount=0;
                            $coupon->cusines=json_decode($coupon->cusines);
                            $coupon->sections=json_decode($coupon->sections);
                            $coupon->dishes=json_decode($coupon->dishes);


                            foreach($cart['orderEntry'] as $item){

                                $dish_cusine_id=Dish::find($item['id'])->cusines[0]->id;
                                $dish_section_id=Dish::find($item['id'])->section_id;

                                array_push($cusine_ids , $dish_cusine_id );
                                array_push($dish_ids , $item['id'] );
                                array_push($section_ids ,  $dish_section_id);

                                if( is_array($coupon->cusines) && in_array($dish_cusine_id,$coupon->cusines)){
                                    $cusine_discount+=(float)$item['price'];
                                }

                                if( is_array($coupon->sections) && in_array($dish_section_id,$coupon->sections)){
                                    $section_discount+=(float)$item['price'];
                                }

                                if( is_array($coupon->dishes) && in_array($item['id'],$coupon->dishes)){
                                    $dish_discount+=(float)$item['price'];
                                }


                            }


                            if($coupon->cusines == 'all'  || $coupon->cusines == null ||  $coupon->sections == 'all'  || $coupon->sections == null || $coupon->dishes == 'all' || $coupon->dishes == null ){// all order
                                    // dd('aa');
                                if($coupon->discount_type == '%'){
                                    $discount=$request->sub_total * $coupon->total / 100 ;
                                }else{
                                    $discount= $coupon->total  ;
                                }
                                $coupon['final_disc']=$discount;
                                return $this->returnData('coupon',$coupon);
                            }

                            if( is_array($coupon->cusines) && $cusine_discount > 0){
                                //applay coupon on cusines
                                if($coupon->discount_type == '%'){
                                    $discount=$cusine_discount * $coupon->total / 100 ;
                                }else{
                                    $discount=$cusine_discount ;
                                }
                                $coupon['final_disc']=$discount;

                                return $this->returnData('coupon',$coupon);

                            }





                            if( is_array($coupon->sections) && $section_discount > 0){
                                //applay coupon on sections
                                if($coupon->discount_type == '%'){
                                    $discount=$section_discount * $coupon->total / 100 ;
                                }else{
                                    $discount=$section_discount ;
                                }
                                $coupon['final_disc']=$discount;

                                return $this->returnData('coupon',$coupon);

                            }

                            if(is_array($coupon->dishes) ){
                                //applay coupon on dishes
                                if($coupon->discount_type == '%'){
                                    $discount=$dish_discount * $coupon->total / 100 ;
                                }else{
                                    $discount=$dish_discount;
                                }
                                $coupon['final_disc']=$discount;

                                return $this->returnData('coupon',$coupon);

                            }

                            break;
                    }
                }
                // else{


                // }

            }
            else{
                return $this->returnData('msg','no match coupon');
            }
        }
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
