<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderDeliverd extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $lang;


    public function __construct($user,$lang)
    {
                $this->user =$user;
                $this->lang =$lang;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user=$this->user;
        $lang=$this->lang;
        return $this->from('care@thejet.com.sa')
                    ->view('backend.deliverd_order' ,compact('user','lang'));
    }
}
