<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $order;
    public $lang;

    public function __construct($order ,$lang)
    {
        $this->order =$order;
        $this->lang =$lang;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order=$this->order;
        $lang=$this->lang;
        // dd( $order->estimated_time,  $order->delivery_estimated_time,$order->delivery_estimated_time + $order->estimated_time);
        return $this->from('care@thejet.com.sa')

                    ->view('backend.order_confirmation' ,compact('order','lang'));
                    // ->text('mails.demo_plain')
    }
}
