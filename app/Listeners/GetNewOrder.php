<?php

namespace App\Listeners;

use App\Events\TrackOrder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GetNewOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TrackOrder  $event
     * @return void
     */
    public function handle(TrackOrder $event)
    {
;
        return view('backend.track-order-list');

    }
}
