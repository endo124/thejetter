<?php

namespace App\Observers;

use App\Models\Order;

class OrderRefresh
{
    /**
     * Handle the models order "created" event.
     *
     * @param  \App\Order  $modelsOrder
     * @return void
     */
    public function created(Order $modelsOrder)
    {
        // dd('aa');
    }

    /**
     * Handle the models order "updated" event.
     *
     * @param  \App\ModelsOrder  $modelsOrder
     * @return void
     */
    public function updated(Order $modelsOrder)
    {

        // return view('backend.order-list');
    }

    /**
     * Handle the models order "deleted" event.
     *
     * @param  \App\ModelsOrder  $modelsOrder
     * @return void
     */
    public function deleted(Order $modelsOrder)
    {
        // dd('aa');
    }

    /**
     * Handle the models order "restored" event.
     *
     * @param  \App\ModelsOrder  $modelsOrder
     * @return void
     */
    public function restored(Order $modelsOrder)
    {
        // dd('aa');
    }

    /**
     * Handle the models order "force deleted" event.
     *
     * @param  \App\ModelsOrder  $modelsOrder
     * @return void
     */
    public function forceDeleted(Order $modelsOrder)
    {
        // dd('aa');
    }
}
