   <script>

           
        $('.noti-dropdown').click(function(){
            console.log('a');
          var  uid =<?php echo json_encode(auth('admin')->user()->id); ?>;
            var ref = db.ref(`/msgs/${uid}`);
            array=[];
                ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                    snapshot.forEach((data) => {
                        if(!array.includes(data.key)&&data.val().email == 'admin@gmail.com'){

                            var name='admin';
                            var msg=data.val().text;
                            var date=data.val().date;
                            date=new Date(parseInt(date)).toLocaleString()
                            var notify=`<a href="#" id="`+data.key+`">
                                            <div class="media">

                                                <div class="media-body">
                                                    <p class="noti-details"><span class="noti-title">`+name+`</span>   massage <span class="noti-title">`+msg+`</span></p>
                                                    <p class="noti-time">`+date+`<span class="notification-time"></span></p>
                                                </div>
                                            </div>
                                        </a>`;

                            $('.notification-message').append(notify);
                        }
                    });
                });






            db.ref(`/msgs/${uid}`).once('value', function(snapshot) {
            snapshot.forEach((data) => {

                data.ref.update({
                    'read':1
                });
            });

            $('#notif').html(0);



        });
        });





        let email = "";
        let name = "";
        let uid=""
        const msgScreen = document.getElementById("messages");
        const msgForm = document.getElementById("messageForm");
        const msgInput = document.getElementById("msg-input");
        const msgBtn = document.getElementById("msg-btn");
        const userName = document.getElementById("user-name");
        const db = firebase.database();
        const msgRef = db.ref("/msgs"); //save in msgs folder in database

        function init(){


                  // User is signed in. Get their name.
                  email =<?php echo json_encode(auth('admin')->user()->email); ?>;
                  uid =<?php echo json_encode(auth('admin')->user()->id); ?>;

                  db.ref(`/msgs/${uid}`).on('child_added', updateMsgs);
                  db.ref(`/msgs/${uid}`).on('child_added', updatNotifi);







            msgForm.addEventListener('submit', sendMessage);
        }


        const updatNotifi = data =>{
            let array=[];
            var ref = db.ref(`/msgs/${uid}`);
            ref.orderByChild("read").equalTo(0).once('value', function(snapshot) {
                snapshot.forEach((data) => {
                if(!array.includes(data.key)&&data.val().email == 'admin@gmail.com'){

                    $('#notif').html(array.length+1);
                    array.push(data.key);
                }
                });
            });
            }


        const updateMsgs = data =>{
          const {email: userEmail , name, text,date} = data.val();

          //Check the encrypting mode
          var encryptMode = fetchJson();
          var outputText = text;

          if(encryptMode == "nr"){
            outputText = normalEncrypt(outputText);
          }else if(encryptMode == "cr"){
            outputText = crazyEncrypt(outputText);
          }



          if(email == userEmail)
          {
                var msg = ` <div class="incoming_msg">

                <div class="received_msg">
                  <div class="received_withd_msg">
                  <i class = "name">${email}: </i>
                    <p>${outputText}</p>
                    <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}   </span>
                  </div>
                </div>
              </div>`


          }
          else
          {
            var msg = ` <div class="outgoing_msg">

            <div class="received_msg">
              <div class="sent_msg">
              <i class = "name">Admin </i>
                <p>${outputText}</p>
                <span class="time_date"> ${new Date(parseInt(date)).toLocaleString()}   </span>
              </div>
            </div>
          </div>`





          }

          msgScreen.innerHTML += msg;
          document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;

        }

        function sendMessage(e){
          e.preventDefault();
          const text = msgInput.value;

            if(!text.trim()) return alert('Please type your message.'); //no msg submitted
            const msg = {
                email,
                name:email,
                text: text,
                read:0,
                date:(Date.now())
            };

            db.ref(`/msgs/${uid}`).push(msg);
            db.ref(`/new_user/${uid}`).update({'name':email,'email':email, 'text': text, 'date':String(Date.now()),'uid':uid,'read':0})
            msgInput.value = "";

        }

        //Get encryption settings
        function fetchJson(){
          var settings = JSON.parse(localStorage.getItem('settings'));
          return settings;
        }


        function crazyEncrypt(text){
          var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
          var newWord = '';
          var newArr =[];

          words.map(function(w) {
            if(w.length > 1){
              w.split('').map(function() {
                var hash = Math.floor(Math.random() * w.length);
                newWord += w[hash];
                w = w.replace(w.charAt(hash), '');
              });
              newArr.push(newWord);
              newWord = '';

            }else{
              newArr.push(w);
            }
          });
          text = newArr.join(' ');
          return text;
        }

        //Normal encryption - first and last letter fixed position
        function normalEncrypt(text){
          var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
          var newWord = '';
          var newArr =[];

          words.map(function(w) {
            if(w.length > 1){
              var lastIndex = w.length-1;
              var lastLetter = w[lastIndex];

              //add the first letter
              newWord += w[0];
              w = w.slice(1,lastIndex);

              //scramble only letters in between the first and last letter
              w.split('').map(function(x) {
                  var hash = Math.floor(Math.random() * w.length);
                  newWord += w[hash];
                  w = w.replace(w.charAt(hash), '');
              });

              //add the last letter
              newWord+=lastLetter;
              newArr.push(newWord);
              newWord = '';
            }else{
              newArr.push(w);
            }
          });
          text = newArr.join(' ');
          return text;
        }
        document.addEventListener('DOMContentLoaded',init);
    </script>