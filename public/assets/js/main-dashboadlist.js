// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCX54Ej_pzEV3lwwK9QNmpcbMUmc-mAxug",
    authDomain: "logista-282218.firebaseapp.com",
    databaseURL: "https://logista-282218.firebaseio.com",
    projectId: "logista-282218",
    storageBucket: "logista-282218.appspot.com",
    messagingSenderId: "747873953165",
    appId: "1:747873953165:web:ff0d3b01fa6dd2671c89c4",
    measurementId: "G-HE8VE9GSM3"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// make auth and firestore references
const auth = firebase.auth();
// const db = firebase.firestore();
const db = firebase.database();

// changing icon by OS theme

lightSchemeIcon = document.querySelector("#themeLightOsIconSite");
darkSchemeIcon = document.querySelector("#themeDarkOsIconSite");

function onUpdate() {
	if (matcher.matches) {
		lightSchemeIcon.remove();
		document.head.append(darkSchemeIcon);
	} else {
		document.head.append(lightSchemeIcon);
		darkSchemeIcon.remove();
	}
}

const uid = Cookies.get("uid");

const tasksQuery = `users/${uid}/tasks`
const driversQuery = `users/${uid}/drivers`

const getKey = async () => {
  const apiKey = (await (db.ref(`users/${uid}/settings`).child("api-key").once("value"))).val()
var script = document.createElement('script');
script.async = true;
script.defer = true;
    if(!apiKey)
    {
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAvKCQ5Ync-aLf0_fZcxvMhlST6o2MMh2U&libraries=places,drawing&callback=initMap';

    }
    else
    {
      script.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey + '&libraries=places,drawing&callback=initMap';
    }
    window.initMap = function(mapObject) {

        // ADDTASK MAP
        var addTaskMaps = new google.maps.Map(document.querySelector(".createTaskItemContainer_map"), {
            zoom: 8,
            center: {
                lat: 30.0444,
                lng: 31.2357,
            },
            disableDefaultUI: true,
        });
    
        // ADD MARKS ON ADD TASKS MAP
        const getDataAndSetMark = (input, autoComplete, map, title) => {
            google.maps.event.addListener(autoComplete, "place_changed", function () {
                var place = autoComplete.getPlace().geometry.location;
                const lat = place.lat();
                const lng = place.lng();
    
                var center = new google.maps.LatLng(lat, lng);
                map.panTo(center);
    
                var addMarker = new google.maps.Marker({
                    position: {
                        lat,
                        lng,
                    },
                    map,
                    title,
                });
            
                input.setAttribute("lng", lng);
                input.setAttribute("lat", lat);
            });
        };
    
        // AUTOCOMPLETE MAP SEARCH INPUT FOR PICKUP INPUT
        var pickupPoint = document.querySelector("#taskPickUpAddressSearchInput");
        var autocompletePickUp = new google.maps.places.Autocomplete(pickupPoint);
        autocompletePickUp.setComponentRestrictions({
            country: [country],
        });
    
        // AUTOCOMPLETE MAP SEARCH INPUT FOR DELIVERY INPUT
        var dropOffInput = document.querySelector("#taskDeliveryAddress");
        var autocompletedropOffInput = new google.maps.places.Autocomplete(dropOffInput);
    
        // RESTRICT SEARCH TO EGYPT ONLY
        autocompletedropOffInput.setComponentRestrictions({
            country: [country],
        });
    
        // RUN
        getDataAndSetMark(pickupPoint, autocompletePickUp, addTaskMaps, "Pick Up point");
        getDataAndSetMark(dropOffInput, autocompletedropOffInput, addTaskMaps, "Drop of point");
        if (mapObject) {
            if (mapObject.mode == "add") {
                if (mapObject.mapType == "popupSeeLocation") {
                    const bound = new google.maps.LatLngBounds();
                    const mapObjectPickup = mapObject.pickup;
                    const mapObjectDropoff = mapObject.dropOff;
                    bound.extend(new google.maps.LatLng(mapObjectPickup.lat, mapObjectPickup.lng));
                    bound.extend(new google.maps.LatLng(mapObjectDropoff.lat, mapObjectDropoff.lng));
                    
                    const centerLat = bound.getCenter().lat();
                    const centerLng = bound.getCenter().lng();
                    const directionsService = new google.maps.DirectionsService();
                    const directionsDisplay = new google.maps.DirectionsRenderer();
    
                    pickupLatLng = {
                        lat: parseFloat(mapObjectPickup.lat),
                        lng: parseFloat(mapObjectPickup.lng),
                    };
    
                    dropoffLatLng = {
                        lat: parseFloat(mapObjectDropoff.lat),
                        lng: parseFloat(mapObjectDropoff.lng),
                    };
                    const trafficLayer = new google.maps.TrafficLayer();
                    const seeTaskLocationMap = new google.maps.Map(
                        document.querySelector(".popupTaskMap"), {
                            zoom: 10,
                            center: {
                                lat: centerLat,
                                lng: centerLng,
                            },
                            disableDefaultUI: true,
                        }
                    );
                    //traffic layer
                    trafficLayer.setMap(seeTaskLocationMap);
                
                    const addMarker = (myLatLng, map, title, iconSrc, arrayToPush) => {
                        const marker = new google.maps.Marker({
                            position: myLatLng,
                            map,
                            title,
                            icon: !!iconSrc ? iconSrc : undefined,
                        });
    
                        return marker;
                    };
                    if(mapObject.driverLat!=0 && mapObject.driverLng!=0 && mapObject.mode=="add")
                    {
                        console.log(mapObject.mode,'add')
                        waypts=[{
                            location: { lat:mapObject.driverLat, lng: mapObject.driverLng },
                            stopover: true,
                            
                          }];
                        directionsService.route({
                                origin: pickupLatLng,
                                destination: dropoffLatLng,
                                waypoints: waypts,
                                travelMode: google.maps.TravelMode.DRIVING,
                                avoidTolls: false,
                                
                            },
                            (res, status) => {
                                if (status === "OK") {
                                    directionsDisplay.setMap(seeTaskLocationMap);
                                    // directionsDisplay.setOptions({
                                    // 	polylineOptions: {
                                    // 		strokeColor: "red",
                                    // 	},
                                    // });
                                    directionsDisplay.setDirections(res);
                                } else {
                                    directionsDisplay.setMap(null);
                                    directionsDisplay.setDirections(null);
                                }
                            }
                        );	
                    }
                    else
                    {
                        
                        directionsService.route({
                                origin: pickupLatLng,
                                destination: dropoffLatLng,
                                
                                travelMode: google.maps.TravelMode.DRIVING,
                                avoidTolls: true,
                            },
                            (res, status) => {
                                if (status === "OK") {
                                    directionsDisplay.setMap(seeTaskLocationMap);
                                    // directionsDisplay.setOptions({
                                    // 	polylineOptions: {
                                    // 		strokeColor: "red",
                                    // 	},
                                    // });
                                    directionsDisplay.setDirections(res);
                                } else {
                                    directionsDisplay.setMap(null);
                                    directionsDisplay.setDirections(null);
                                }
                            }
                        );
                    }
                    
                }
            }
            if (mapObject.mode == "track") {
                //
                
                const bound = new google.maps.LatLngBounds();
                const mapObjectPickupTrack = mapObject.pickupTrack;
                const mapObjectDropoffTrack = mapObject.dropOffTrack;
                console.log(mapObject);
                 bound.extend(new google.maps.LatLng(mapObjectPickupTrack.lat, mapObjectPickupTrack.lng));
                bound.extend(new google.maps.LatLng(mapObjectDropoffTrack.lat, mapObjectDropoffTrack.lng));
                
                const centerLat = bound.getCenter().lat();
                 const centerLng = bound.getCenter().lng();
                const seeTaskLocationMap = new google.maps.Map(
                    document.querySelector(".popupTaskMap"), {
                        zoom: 10,
                        center: {
                            lat: centerLat,
                            lng: centerLng,
                        },
                        disableDefaultUI: true,
                    }
                );
                const marker1 = new google.maps.Marker({
                    position: { lat:parseFloat(mapObject.pickupTrack.lat),lng:parseFloat(mapObject.pickupTrack.lng)},
                    map: seeTaskLocationMap,
                    label: "pickup"
                  });
                  const marker2 = new google.maps.Marker({
                    position: { lat:parseFloat(mapObject.dropOffTrack.lat),lng:parseFloat(mapObject.dropOffTrack.lng)},
                    map: seeTaskLocationMap,
                    label: "DropOff"
                  });
                  if(mapObject.driverLngTrack!=0)
                  {
                    const marker3 = new google.maps.Marker({
                        position: { lat:parseFloat(mapObject.driverLatTrack),lng:parseFloat(mapObject.driverLngTrack)},
                        map: seeTaskLocationMap,
                        label: "Driver",
                     
                      });  
                  }
            }
            if (mapObject.mode == "remove") {
                taskDetailsMapMarkersArray.forEach((marker) => marker.setMap(null));
                taskDetailsMapMarkersArray.splice(0, taskDetailsMapMarkersArray.length);
            }
            //// get eta
    if(mapObject.mode == "est")
    {
    
    var origin = { lat:parseFloat(mapObject.dropEST.lat), lng: parseFloat(mapObject.dropEST.lng) };

    var destination = { lat: parseFloat(mapObject.pickupEST.lat), lng: parseFloat(mapObject.pickupEST.lng) };
   
    
    const geocoder = new google.maps.Geocoder();
    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false,
      },
      (response, status) => {
        if (status !== "OK") {
          alert("Error was: " + status);
        } else {
          const originList = response.originAddresses;
          const destinationList = response.destinationAddresses;
          const outputDiv = document.getElementById('p'+mapObject.id);
          

          for (let i = 0; i < originList.length; i++) {
            const results = response.rows[i].elements;
            outputDiv.innerHTML =results[0].duration.text;
            
            
          }
        }
      }
    );
    }
        }
      // JS API is loaded and available
    };

     document.head.appendChild(script);
listViewTasksItemGenrator();
 return true;
};
const logoutBtn = document.querySelector("#logoutBtn");
logoutBtn.addEventListener("click", () => {
	
	auth.signOut().then(() => {
   
		Cookies.remove("logedin");
		Cookies.remove("uid");
		window.location.assign('signup.html')
	});
});
const getNotification = () => {

 let count=0;
  db.ref(`users/${uid}/notifications`).on("value", (snapshot) => {
      // document.querySelector("#notification").innerHTML = ""
      const numOfFences = snapshot.numChildren()
     
      
        document.querySelector("#notification").innerHTML = snapshot.val()

    count++;
    document.querySelector("#countNotification").innerHTML = count

    
  })
  
}


// function initialize() {
//   getKey();
// }
getNotification();
getKey();
