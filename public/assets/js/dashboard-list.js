const taskDetailsMapMarkersArray = [];
let country=Cookies.get('country')
addTask();
const datePicker = document.querySelector("#tasksTime")
datePicker.addEventListener("change", (e) => {
	listViewTasksItemGenrator(moment(datePicker.value).format("YYYY-MM-DD hh:mm A"))
})
const listViewTasksItemGenrator = async (time) => {

	db.ref(`users/${uid}/tasks`).orderByChild('deliver/orderId').on("value", (snapshot) => {
		task_container.innerHTML = "";
		// add taskitem for each task
		snapshot.forEach((taskData) => {
			if(typeof time  !==undefined)
			{
				 todayDate = moment(time).format("YYYY-MM-DD");
			
			}
			const taskDate = moment.unix(taskData.val().pickup.date).format("YYYY-MM-DD")
		if(todayDate==taskDate)
		{

			createTaskListElement(taskData);

		
			
			
			if( taskData.val().pickup.address &&  taskData.val().deliver.address)
			{
				
					initMap({
						mapType: "popupSeeLocation",
						pickupEST: taskData.val().pickup.address ,
						dropEST: taskData.val().deliver.address,
						id:taskData.key,
						mode:'est',
				
				
						});
				
				
			}
				// task item functions
		const viewTaskDetailsButtons = document.querySelectorAll(".taskDetails-popupBtn");
		openTaskDetailsPopup(viewTaskDetailsButtons);
		closePopupDefault(".closeBtnContianer", ".taskDetailsPopup_container", "#taskDetailsPopup_bgshade");
		viewTaskOnTheMapPopup();
		viewTaskTrackOnTheMapPopup()
		deleteTask();
		closePopupDefault("#closeTaskLocation", ".popupTaskMap_container", "#popupTaskMap_bgshade", () => {
			initMap({
				mapType: "popupSeeLocation",
				mode: "remove"
			});
		});
		}
		});

	
		
	});
};

// function initMap(mapObject) {
// 	console.log('ll');
// 	// ADDTASK MAP
// 	var addTaskMaps = new google.maps.Map(document.querySelector(".createTaskItemContainer_map"), {
// 		zoom: 8,
// 		center: {
// 			lat: 30.0444,
// 			lng: 31.2357,
// 		},
// 		disableDefaultUI: true,
// 	});

// 	// ADD MARKS ON ADD TASKS MAP
// 	const getDataAndSetMark = (input, autoComplete, map, title) => {
// 		google.maps.event.addListener(autoComplete, "place_changed", function () {
// 			var place = autoComplete.getPlace().geometry.location;
// 			const lat = place.lat();
// 			const lng = place.lng();

// 			var center = new google.maps.LatLng(lat, lng);
// 			map.panTo(center);

// 			var addMarker = new google.maps.Marker({
// 				position: {
// 					lat,
// 					lng,
// 				},
// 				map,
// 				title,
// 			});
		
// 			input.setAttribute("lng", lng);
// 			input.setAttribute("lat", lat);
// 		});
// 	};

// 	// AUTOCOMPLETE MAP SEARCH INPUT FOR PICKUP INPUT
// 	var pickupPoint = document.querySelector("#taskPickUpAddressSearchInput");
// 	var autocompletePickUp = new google.maps.places.Autocomplete(pickupPoint);
// 	autocompletePickUp.setComponentRestrictions({
// 		country: [country],
// 	});

// 	// AUTOCOMPLETE MAP SEARCH INPUT FOR DELIVERY INPUT
// 	var dropOffInput = document.querySelector("#taskDeliveryAddress");
// 	var autocompletedropOffInput = new google.maps.places.Autocomplete(dropOffInput);

// 	// RESTRICT SEARCH TO EGYPT ONLY
// 	autocompletedropOffInput.setComponentRestrictions({
// 		country: [country],
// 	});

// 	// RUN
// 	getDataAndSetMark(pickupPoint, autocompletePickUp, addTaskMaps, "Pick Up point");
// 	getDataAndSetMark(dropOffInput, autocompletedropOffInput, addTaskMaps, "Drop of point");
// 	if (mapObject) {
// 		if (mapObject.mode == "add") {
// 			if (mapObject.mapType == "popupSeeLocation") {
// 				const bound = new google.maps.LatLngBounds();
// 				const mapObjectPickup = mapObject.pickup;
// 				const mapObjectDropoff = mapObject.dropOff;
// 				bound.extend(new google.maps.LatLng(mapObjectPickup.lat, mapObjectPickup.lng));
// 				bound.extend(new google.maps.LatLng(mapObjectDropoff.lat, mapObjectDropoff.lng));
				
// 				const centerLat = bound.getCenter().lat();
// 				const centerLng = bound.getCenter().lng();
// 				const directionsService = new google.maps.DirectionsService();
// 				const directionsDisplay = new google.maps.DirectionsRenderer();

// 				pickupLatLng = {
// 					lat: parseFloat(mapObjectPickup.lat),
// 					lng: parseFloat(mapObjectPickup.lng),
// 				};

// 				dropoffLatLng = {
// 					lat: parseFloat(mapObjectDropoff.lat),
// 					lng: parseFloat(mapObjectDropoff.lng),
// 				};
// 				const trafficLayer = new google.maps.TrafficLayer();
// 				const seeTaskLocationMap = new google.maps.Map(
// 					document.querySelector(".popupTaskMap"), {
// 						zoom: 10,
// 						center: {
// 							lat: centerLat,
// 							lng: centerLng,
// 						},
// 						disableDefaultUI: true,
// 					}
// 				);
// 				//traffic layer
// 				trafficLayer.setMap(seeTaskLocationMap);
			
// 				const addMarker = (myLatLng, map, title, iconSrc, arrayToPush) => {
// 					const marker = new google.maps.Marker({
// 						position: myLatLng,
// 						map,
// 						title,
// 						icon: !!iconSrc ? iconSrc : undefined,
// 					});

// 					return marker;
// 				};
// 				if(mapObject.driverLat!=0 &&mapObject.driverLng!=0)
// 				{
// 					console.log(mapObject.driverLat)
// 					waypts=[{
// 						location: { lat:mapObject.driverLat, lng: mapObject.driverLng },
// 						stopover: true,
						
// 					  }];
// 					directionsService.route({
// 							origin: pickupLatLng,
// 							destination: dropoffLatLng,
// 							waypoints: waypts,
// 							travelMode: google.maps.TravelMode.DRIVING,
// 							avoidTolls: false,
							
// 						},
// 						(res, status) => {
// 							if (status === "OK") {
// 								directionsDisplay.setMap(seeTaskLocationMap);
// 								// directionsDisplay.setOptions({
// 								// 	polylineOptions: {
// 								// 		strokeColor: "red",
// 								// 	},
// 								// });
// 								directionsDisplay.setDirections(res);
// 							} else {
// 								directionsDisplay.setMap(null);
// 								directionsDisplay.setDirections(null);
// 							}
// 						}
// 					);	
// 				}
// 				else
// 				{
					
// 					directionsService.route({
// 							origin: pickupLatLng,
// 							destination: dropoffLatLng,
							
// 							travelMode: google.maps.TravelMode.DRIVING,
// 							avoidTolls: true,
// 						},
// 						(res, status) => {
// 							if (status === "OK") {
// 								directionsDisplay.setMap(seeTaskLocationMap);
// 								// directionsDisplay.setOptions({
// 								// 	polylineOptions: {
// 								// 		strokeColor: "red",
// 								// 	},
// 								// });
// 								directionsDisplay.setDirections(res);
// 							} else {
// 								directionsDisplay.setMap(null);
// 								directionsDisplay.setDirections(null);
// 							}
// 						}
// 					);
// 				}
				
// 			}
// 		}

// 		if (mapObject.mode == "remove") {
// 			taskDetailsMapMarkersArray.forEach((marker) => marker.setMap(null));
// 			taskDetailsMapMarkersArray.splice(0, taskDetailsMapMarkersArray.length);
// 		}
// 		//// get eta
// if(mapObject.mode == "est")
// {
// 	console.log(parseFloat(mapObject.pickupEST.lat),'lat')

// var origin = { lat:parseFloat(mapObject.dropEST.lat), lng: parseFloat(mapObject.dropEST.lng) };
// // var origin2 =  mapObject.pickupEST.name;
// // var destinationA = mapObject.dropEST.name;
//  var destination = { lat: parseFloat(mapObject.pickupEST.lat), lng: parseFloat(mapObject.pickupEST.lng) };
// //var origin = {lat:24.5484252,lng:46.9032182};
// // var destination = {lat:24.6458592,lng:46.7177128};
// console.log(origin,'origin',destination);

// const geocoder = new google.maps.Geocoder();
// const service = new google.maps.DistanceMatrixService();
// service.getDistanceMatrix(
//   {
// 	origins: [origin],
//       destinations: [destination],
// 	travelMode: google.maps.TravelMode.DRIVING,
// 	unitSystem: google.maps.UnitSystem.METRIC,
// 	avoidHighways: false,
// 	avoidTolls: false,
//   },
//   (response, status) => {
// 	if (status !== "OK") {
// 	  alert("Error was: " + status);
// 	} else {
// 	  const originList = response.originAddresses;
// 	  const destinationList = response.destinationAddresses;
// 	  const outputDiv = document.getElementById('p'+mapObject.id);
	  

// 	  console.log(response,'status')

// 	  for (let i = 0; i < originList.length; i++) {
// 		const results = response.rows[i].elements;
// 		outputDiv.innerHTML =results[0].duration.text;
		
		
// 	  }
// 	}
//   }
// );
// }
// 	}
	

// }


// ===============
// ===============
// ===============
// ===============
// ===============
// ===============
// ===============

const task_container = document.querySelector("#taskListContainer");
const createTaskListElement = (databaseTasksItemSnapshot,todayDate) => {
	// creat elements


	const taskItem = document.createElement("div");
	const locationFromEle = document.createElement("p");
	const locationToEle = document.createElement("p");
	const orderNoEle = document.createElement("p");

	const taskESTEle = document.createElement("p");
	const taskStatusEle = document.createElement("select");
	

	const DriverNameSelect = document.createElement("select");
	const openLocationButton = document.createElement("button");
	const moreTaskDetailsButton = document.createElement("button");
	const editTaskButton = document.createElement("button");
	const deleteTaskButton = document.createElement("button");
	const trackTaskButton = document.createElement("button");
	// organizing and ordering elements
	taskItem.appendChild(orderNoEle);
	

	taskItem.appendChild(locationFromEle);
	taskItem.appendChild(locationToEle);
	taskItem.appendChild(taskESTEle);
	taskItem.appendChild(taskStatusEle);
	taskItem.appendChild(DriverNameSelect);
	taskItem.appendChild(moreTaskDetailsButton);
	taskItem.appendChild(openLocationButton);

	// taskItem.appendChild(editTaskButton);
	// taskItem.appendChild(trackTaskButton);
	taskItem.appendChild(deleteTaskButton);

	//add classes
	locationFromEle.classList.add("list_item");
	locationToEle.classList.add("list_item");
	deleteTaskButton.classList.add("defaultButton", "defaultButton--delete");
	editTaskButton.classList.add("defaultButton", "defaultButton--edit");
	taskItem.classList.add("task_item-list");
	moreTaskDetailsButton.classList.add("list_buttons", "taskDetails-popupBtn", "defaultButton--edit");
	openLocationButton.classList.add("openLocationPopup", "list_buttons", "defaultButton--edit");
	trackTaskButton.classList.add("openTrackLocationPopup", "list_buttons", "defaultButton--edit");

	// retriving data from database
	const task = databaseTasksItemSnapshot.val();
	const taskId = databaseTasksItemSnapshot.key;
	const taskDriverId = task.driverId;
	const taskDriverTeam = task.driverTeam;
	const pickAddressName = task.pickup.address.name;
	let deliverAddressName = task.deliver.address.name;
	const status = task.status;
	const orderNo = task.deliver.orderId;
	taskStatusEle.setAttribute("id", "statusID"+taskId);
	if(taskDriverId)
	{
		taskStatusEle.setAttribute("driver-id", taskDriverId);
	}
	else
	{
		taskStatusEle.setAttribute("driver-id", 0);
	}
	
	
	taskStatusEle.onchange = function() { changeStatus(taskId,status); };
	if(task.deliver_points)
	{
		if(task.deliver_points.length==1)
		{
			deliverAddressName = task.deliver_points[0].address.name;
		}
		else
		{
			let i=task.deliver_points.length
			deliverAddressName = task.deliver_points[i-1].address.name;
		}
		
	}
	// filling elements with data
	locationFromEle.innerHTML = pickAddressName;
	locationToEle.innerHTML = deliverAddressName;

	if(task.status==4)
	{
		taskStatusEle.innerHTML += `<option  value="0" >Assigned</option>`;
		taskStatusEle.innerHTML += `<option disabled value="-1" >Unassigned</option>`;
		taskStatusEle.innerHTML += `<option value="1" >Preparing</option>`;
		taskStatusEle.innerHTML += `<option value="3"  >On The Way</option>`;
		taskStatusEle.innerHTML += `<option  selected value="4" >Completed</option>`;
	}
	if(task.status==3)
	{
		taskStatusEle.innerHTML += `<option  value="0" >Assigned</option>`;
		taskStatusEle.innerHTML += `<option disabled value="-1" >Unassigned</option>`;
		taskStatusEle.innerHTML += `<option value="1" >Preparing</option>`;
		taskStatusEle.innerHTML += `<option selected value="3"  >On The Way</option>`;
		taskStatusEle.innerHTML += `<option   value="4" >Completed</option>`;
	}
	if(task.status==1)
	{
		taskStatusEle.innerHTML += `<option  value="0" >Assigned</option>`;
		taskStatusEle.innerHTML += `<option disabled value="-1" >Unassigned</option>`;
		taskStatusEle.innerHTML += `<option selected value="1" >Preparing</option>`;
		taskStatusEle.innerHTML += `<option  value="3"  >On The Way</option>`;
		taskStatusEle.innerHTML += `<option   value="4" >Completed</option>`;
	}
	if(task.status==0)
	{
		taskStatusEle.innerHTML += `<option  selected value="0" >Assigned</option>`;
		taskStatusEle.innerHTML += `<option disabled value="-1" >Unassigned</option>`;
		taskStatusEle.innerHTML += `<option  value="1" >Preparing</option>`;
		taskStatusEle.innerHTML += `<option  value="3"  >On The Way</option>`;
		taskStatusEle.innerHTML += `<option   value="4" >Completed</option>`;
	}
	if(task.status==-1)
	{
		taskStatusEle.innerHTML += `<option  selected value="0" >Assigned</option>`;
		taskStatusEle.innerHTML += `<option selected disabled value="-1" >Unassigned</option>`;
		taskStatusEle.innerHTML += `<option  value="1" >Preparing</option>`;
		taskStatusEle.innerHTML += `<option  value="3"  >On The Way</option>`;
		taskStatusEle.innerHTML += `<option   value="4" >Completed</option>`;
	}
	



	// taskStatusEle.innerHTML = statusText(status);
	taskESTEle.setAttribute("id", 'p'+databaseTasksItemSnapshot.key);
	taskESTEle.innerHTML = ""

	orderNoEle.innerHTML=orderNo
	DriverNameSelect.innerHTML = "";
	moreTaskDetailsButton.innerHTML = "See More";
	moreTaskDetailsButton.dataset.id = taskId;
	if (taskDriverId != 0) {
		
		moreTaskDetailsButton.dataset.Did = taskDriverId;
		moreTaskDetailsButton.dataset.team = taskDriverTeam;
	}
	openLocationButton.innerHTML = "View on Map";
	deleteTaskButton.dataset.id = taskId;
	console.log(taskDriverId,'taskDriverId')
	if (taskDriverId != 0) {
		deleteTaskButton.dataset.driver = taskDriverId;

	}
	else
	{
		deleteTaskButton.dataset.driver = taskDriverId;

		

	}
	editTaskButton.innerHTML = "Edit";
	deleteTaskButton.innerHTML = "Delete";
	trackTaskButton.innerHTML = "track";
	taskItem.dataset.id = taskId;
	trackTaskButton.dataset.id = taskId;
	//load drivers in drivers select
	fillDriversSelect(taskDriverId, DriverNameSelect);

	// add task item to task container
	task_container.appendChild(taskItem);
};

 const changeStatus = async (driverTaskId,status) => {
	var today = new Date();
	
	
    var today_date =  today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
	console.log(today_date,'today_date');
	let value= document.getElementById("statusID"+driverTaskId).value
	let attribute =  document.getElementById("statusID"+driverTaskId).getAttribute('driver-id');
	console.log(attribute,'attribute')
if(status==4)
{
	confirm('Completed  Task cant be updated')	
	location.reload()
}
else
{
	if(attribute !=0)
	{
	  db.ref(`users/${uid}/tasks/${driverTaskId}`).update({
		  status: parseInt(value),
		})
	  db.ref(`ongoing_tasks/${uid}/${attribute}/${driverTaskId}`).update({status:  parseInt(value),
	  })
	  if( parseInt(value)==4)
	  {
		  const task = await db.ref(`users/${uid}/tasks/${driverTaskId}`)
		  const TasksData = await task.once("value", (snapshot) => snapshot)
		  db.ref(`delivered_tasks/${uid}/${attribute}/${today_date}/${driverTaskId}`).set(TasksData.val())
		  db.ref(`delivered_tasks/${uid}/${attribute}/${today_date}/${driverTaskId}`).update(
			  {
				  deliveredDate:today_date,
				  deliveredDateInMillis:moment().unix()*1000
			  }
		  )
		  
		  db.ref(`ongoing_tasks/${uid}/${attribute}/${driverTaskId}`).remove()
	  }
	  confirm('Done update status')
	}
	if(attribute ==0)
	{
	  confirm('Only Assigned Task can be updated')
	}
}

	  
	  
	 
	  
 }

// const fillDriversSelect = (driverTaskId, driverSelect) => {
	
// 	db.ref(`users/${uid}/drivers`).on("value", (snapshot) => {
// 		const driversByTeam = snapshot;
// 		driversByTeam.forEach((driversData) => {
// 			const drivers = driversData;
// 			driverSelect.innerHTML = "";
// 			drivers.forEach((driverData) => {
// 				const driver = driverData.val();
// 				const driverId = driverData.key;
// 				const driverTeam = driver.driverTeam;
// 				const driverFirstName = driver.driverFirstName;
// 				const driverLastName = driver.driverLastName;
// console.log('driverFirstName',driver)
// 				if (driverTaskId == driverId) {
// 					const DriverNameOption = `<option value="${driverId}" selected>${driverFirstName}</option>`;
// 					driverSelect.innerHTML += DriverNameOption;
// 				} else {
// 					const DriverNameOption = `<option value="${driverId}">${driverFirstName}</option>`;
// 					driverSelect.innerHTML += DriverNameOption;
// 				}
// 			});
// 		});
// 		driverSelect.innerHTML += `<option>Select A driver</option>`;
// 	});
// };

const fillDriversSelect =  async (driverTaskId, driverSelect) => {
	const dbConnect = await db.ref(`users/${uid}/drivers`)
	const driversData = await dbConnect.once("value", (snapshot) => snapshot)
	let allDrivers = []
	driversData.forEach((task) => {
		
		allDrivers.push(task)
	})

	driverSelect.innerHTML = "";
	allDrivers.forEach((driverData) => {
		const driver = driverData.val();
		const driverId = driverData.key;
		const driverTeam = driver.driverTeam;
		const driverFirstName = driver.driverFirstName;
		const driverLastName = driver.driverLastName;

		if (driverTaskId == driverId) {
			const DriverNameOption = `<option value="${driverId}" selected>${driverFirstName}</option>`;
			driverSelect.innerHTML += DriverNameOption;
		} else {
			const DriverNameOption = `<option value="${driverId}">${driverFirstName}</option>`;
			driverSelect.innerHTML += DriverNameOption;
		}
	});

};
const addDataInTaskDetailsContainer = (taskId, driverId, teamValue) => {
	
	const taskDetailsContainer = document.querySelector(".taskDetailsPopup_content");
	const taskDetailsContainerInputs = taskDetailsContainer.querySelectorAll(`[data-task-item]`);

	taskDetailsContainerInputs.forEach((input) => (input.innerHTML = ""));

	// General data selectors
	const taskDetailsPickupAddress = taskDetailsContainer.querySelector(`[data-task-item="from"]`);
	const taskDetailsDeliverAddress = taskDetailsContainer.querySelector(`[data-task-item="to"]`);

	// pickup data selectors
	const taskDetailsPickupName = taskDetailsContainer.querySelector(`[data-task-item="name_pickup"]`);
	const taskDetailsPickupEmail = taskDetailsContainer.querySelector(`[data-task-item="email_pickup"]`);
	const taskDetailsPickupPhoneNumber = taskDetailsContainer.querySelector(
		`[data-task-item="phoneNumebr_pickup"]`
	);
	const taskDetailsPickupBefore = taskDetailsContainer.querySelector(`[data-task-item="date_pickup"]`);
	const taskDetailsPickupOrderId = taskDetailsContainer.querySelector(`[data-task-item="orderId_pickup"]`);

	// deliver data selectors
	const taskDetailsDeliverName = taskDetailsContainer.querySelector(`[data-task-item="name_deliver"]`);
	const taskDetailsDeliverEmail = taskDetailsContainer.querySelector(`[data-task-item="email_deliver"]`);
	const taskDetailsDeliverPhoneNumber = taskDetailsContainer.querySelector(
		`[data-task-item="phoneNumebr_deliver"]`
	);
	const taskDetailsDeliverBefore = taskDetailsContainer.querySelector(`[data-task-item="date_deliver"]`);
	const taskDetailsDeliverOrderId = taskDetailsContainer.querySelector(`[data-task-item="orderId_deliver"]`);

	// driver data selectors
	const taskDetailsDriverName = taskDetailsContainer.querySelector(`[data-task-item="name_driver"]`);
	const taskDetailsDriverEmail = taskDetailsContainer.querySelector(`[data-task-item="email_driver"]`);
	const taskDetailsDriverUsername = taskDetailsContainer.querySelector(`[data-task-item="username_driver"]`);
	const taskDetailsDriverPhoneNumber = taskDetailsContainer.querySelector(
		`[data-task-item="phoneNumber_driver"]`
	);
	const taskDetailsDriverImage = taskDetailsContainer.querySelector(`[data-task-item="image_driver"]`);


	// fetching from database
	db.ref(`users/${uid}/tasks/${taskId}`).once("value", (snapshot) => {
		// database data to var
		const task = snapshot.val();
		const taskId = snapshot.key;
		const pickup = task.pickup;
		const deliver = task.deliver;

		const to = pickup.address.name;
		const from = deliver.address.name;

		const pickupBefore = pickup.pickupBefore;
		const pickupName = pickup.name;
		const pickupEmail = pickup.email;
		const pickupPhone = pickup.phone;
		const pickupOrderId = pickup.orderId;

		const deliverBefore = deliver.deliverBefore;
		const deliverName = deliver.name;
		const deliverEmail = deliver.email;
		const deliverPhone = deliver.phone;
		const deliverOrderId = deliver.orderId;

		// filling selectors with database data
		taskDetailsPickupAddress.innerHTML = to;
		taskDetailsDeliverAddress.innerHTML = from;

		taskDetailsPickupName.innerHTML = pickupName;
		taskDetailsPickupEmail.innerHTML = pickupEmail;
		taskDetailsPickupPhoneNumber.innerHTML = pickupPhone;
		taskDetailsPickupBefore.innerHTML = pickupBefore;
		taskDetailsPickupOrderId.innerHTML = pickupOrderId;

		taskDetailsDeliverName.innerHTML = deliverName;
		taskDetailsDeliverEmail.innerHTML = deliverEmail;
		taskDetailsDeliverPhoneNumber.innerHTML = deliverPhone;
		taskDetailsDeliverBefore.innerHTML = deliverBefore;
		taskDetailsDeliverOrderId.innerHTML = deliverOrderId;
	});

	//if it's assingend to driver
	if (!!driverId ) {
		
		db.ref(`users/${uid}/drivers/${driverId}`).once("value", (snapshot) => {
			// fetch from db
			const driver = snapshot.val();
			const name = `${driver.driverFirstName} ${driver.driverLastName}`;
			const email = driver.driverEmail;
			const username = driver.driverUsername;
			const phone = driver.driverPhoneNumber;
			//filling selectors
			taskDetailsDriverName.innerHTML = name;
			taskDetailsDriverEmail.innerHTML = email;
			taskDetailsDriverImage.src = driver.driverProfileImage
			taskDetailsDriverImage.setAttribute("alt", name)
			taskDetailsDriverUsername.innerHTML = username;
			taskDetailsDriverPhoneNumber.innerHTML = phone;
		});
	} else {
		// hide the input
		document.querySelector("#driverInfo").style.display = "none";
	}

	
};

const deleteTask = () => {
	const deleteButtons = document.querySelectorAll(".defaultButton--delete");
	deleteButtons.forEach((deleteButton) => {
		deleteButton.addEventListener("click", () => {
			const taskId = deleteButton.dataset.id;
			const driverId = deleteButton.dataset.driver;
			popupAreYouSure("Are You Sure You Want to Delete this Task", "Cancel", "Delete", () => {
				db.ref(`users/${uid}/tasks/${taskId}`).remove();
				if(driverId !=0)
				{
					db.ref(`ongoing_tasks/${uid}/${driverId}/${taskId}`).remove()
				}
			});
		});
	});
};

const defaultPopupOpenAnimation = (popupSelector, parentSelector) => {
	const popup = document.querySelector(popupSelector);

	popup.parentNode.style.display = "flex";
	anime({
		targets: parentSelector,
		opacity: 1,
		duration: 150,
		easing: "easeInOutQuad",
		complete: () => {
			popup.style.display = "flex";
			anime({
				targets: popupSelector,
				scale: [0.5, 1],
				opacity: 1,
				duration: 200,
				easing: "easeInOutQuad",
			});
		},
	});
};

const defaultPopupCloseAnimation = (popupSelector, parentSelector) => {
	const popup = document.querySelector(popupSelector);
	const parentPopup = document.querySelector(parentSelector);
	anime({
		targets: popupSelector,
		scale: [1, 0.5],
		opacity: 0,
		duration: 200,
		easing: "easeInOutQuad",
		complete: () => {
			popup.style.display = "none";
			anime({
				targets: parentSelector,
				opacity: 0,
				duration: 100,
				easing: "easeInOutQuad",
				complete: () => {
					parentPopup.style.display = "none";
				},
			});
		},
	});
};

const closePopupDefault = (buttonSelector, selector, parentSelector, callback) => {
	const button = document.querySelector(buttonSelector);
	button.addEventListener("click", () => {
		defaultPopupCloseAnimation(selector, parentSelector);
		if (typeof callback === "function") callback();
	});

	document.addEventListener("keydown", (e) => {
		if (document.querySelector(selector).parentNode.style.display === "none") return;
		if (e.key != "Escape") return;
		defaultPopupCloseAnimation(selector, parentSelector);
		if (typeof callback === "function") callback();
	});
};

const viewTaskOnTheMapPopup = () => {
	const openTaskMapButtons = document.querySelectorAll(".openLocationPopup");
	openTaskMapButtons.forEach((openTaskMapButton) => {
		openTaskMapButton.addEventListener("click", () => {
			const taskId = openTaskMapButton.parentNode.dataset.id;
			db.ref(`users/${uid}/tasks/${taskId}/pickup/address`).on("value", (snapshot) => {
				const pickupAddress = snapshot.val();
				db.ref(`users/${uid}/tasks/${taskId}/deliver/address`).on("value", (snapshot) => {
					const deliverAddress = snapshot.val();
					db.ref(`users/${uid}/tasks/${taskId}`).on("value", (snapshot) => {
						if(snapshot.val().whoTake)
						{
					db.ref(`users/${uid}/drivers/${snapshot.val().whoTake}`).on("value", (snapshot) => {
							if(snapshot.val().currantLongitude)
							{
							initMap({
							mapType: "popupSeeLocation",
							pickup: pickupAddress,
							dropOff: deliverAddress,
							mode: "add",
							driverLat:snapshot.val().currantLatitude,
							driverLng:snapshot.val().currantLongitude,

							});
							}
							else
							{
							initMap({
							mapType: "popupSeeLocation",
							pickup: pickupAddress,
							dropOff: deliverAddress,
							mode: "add",
							driverLat:0,
							driverLng:0,

							}); 
							}

							});
							}
							else
							{
							initMap({
								mapType: "popupSeeLocation",
								pickup: pickupAddress,
								dropOff: deliverAddress,
								mode: "add",
								driverLat:0,
								driverLng:0,
							});
							}

							});
							});
			});
			defaultPopupOpenAnimation(".popupTaskMap_container", "#popupTaskMap_bgshade");
			
		});
	});
};
////track
const  viewTaskTrackOnTheMapPopup= () => {
	const openTaskMapButtons = document.querySelectorAll(".openTrackLocationPopup");
	openTaskMapButtons.forEach((openTaskMapButton) => {
		openTaskMapButton.addEventListener("click", () => {
			const taskId = openTaskMapButton.parentNode.dataset.id;
			db.ref(`users/${uid}/tasks/${taskId}/pickup/address`).on("value", (snapshot) => {
				const pickupAddress = snapshot.val();
				db.ref(`users/${uid}/tasks/${taskId}/deliver/address`).on("value", (snapshot) => {
					const deliverAddress = snapshot.val();
					db.ref(`users/${uid}/tasks/${taskId}`).on("value", (snapshot) => {
						if(snapshot.val().whoTake)
						{
					db.ref(`users/${uid}/drivers/${snapshot.val().whoTake}`).on("value", (snapshot) => {
							if(snapshot.val().currantLongitude)
							{
								console.log(snapshot.val().currantLongitude,'l')
							initMap({
							mapType: "popupSeeLocation",
							pickupTrack: pickupAddress,
							dropOffTrack: deliverAddress,
							mode: "track",
							driverLatTrack:snapshot.val().currantLatitude,
							driverLngTrack:snapshot.val().currantLongitude,

							});
							}
							else
							{
							initMap({
							mapType: "popupSeeLocation",
							pickupTrack: pickupAddress,
							dropOffTrack: deliverAddress,
							mode: "track",
							driverLatTrack:0,
							driverLngTrack:0,

							}); 
							}

							});
							}
							else
							{
							initMap({
								mapType: "popupSeeLocation",
								pickupTrack: pickupAddress,
								dropOffTrack: deliverAddress,
								mode: "track",
								driverLat:0,
								driverLng:0,
							});
							}

							});
							});
			});
			defaultPopupOpenAnimation(".popupTaskMap_container", "#popupTaskMap_bgshade");
			
		});
	});
};
const openTaskDetailsPopup = (buttons) => {
	buttons.forEach((button) => {
		button.addEventListener("click", () => {
			defaultPopupOpenAnimation(".taskDetailsPopup_container", "#taskDetailsPopup_bgshade");
			const id = button.dataset.id;
			const driverId = button.dataset.Did;
			const teamValue = button.dataset.team;
			addDataInTaskDetailsContainer(id, driverId, teamValue);
			myMap(id);
		});
	});
};
function myMap(taskId) {
	
	db.ref(`users/${uid}/tasks/${taskId}/pickup/address`).on("value", (snapshot) => {
		const pickupAddress = snapshot.val();
		
		db.ref(`users/${uid}/tasks/${taskId}/deliver/address`).on("value", (snapshot) => {
			const deliverAddress = snapshot.val();
			map = new google.maps.Map(document.getElementById('TaskDetailMap'), {
				zoom: 10,
				center: {
					lat: 30.0444,
			lng: 31.2357,
				}
			});
		
			// 2. Put first marker in Bucaramanga
			markerA = new google.maps.Marker({
				position: {
					lat:parseFloat(deliverAddress.lat), 
					lng: parseFloat(deliverAddress.lng)
				},
				map: map,
				
				title: "A"
			});
		
			// 3. Put second marker in Bogota
			markerB = new google.maps.Marker({
				position: {
					lat:parseFloat(pickupAddress.lat), 
					lng: parseFloat(pickupAddress.lng)
				},
				map: map,
				
				title: "B"
			});
			var line = new google.maps.Polyline({
				path: [
					new google.maps.LatLng(deliverAddress.lat, deliverAddress.lng ), 
					new google.maps.LatLng(pickupAddress.lat, pickupAddress.lng)
				],
				strokeColor: "#FF0000",
				strokeOpacity: 1.0,
				strokeWeight: 10,
				map: map
			});
			var bounds = new google.maps.LatLngBounds();
			bounds.extend(markerA.getPosition());
			bounds.extend(markerB.getPosition());
			  
		});
	

	});
	}

const statusText = (status) => {
	if (status == -1) return "Unassigned"
	else if (status == 0) return "Assigned"
	else if (status == 2) return "Preparing"
	else if (status == 3) return "On The Way"
	else if (status == 4) return "Completed"
	return "Unassigned"
}
$(".navigation_hamburgerBtn").on('click', function(event){

	
	$('.hamburger_menu').addClass('hamburger_menu--active');
});
$(".hamburger_btn-back_container").on('click', function(event){

	
	$('.hamburger_menu').removeClass('hamburger_menu--active');
});
