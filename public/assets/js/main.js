// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCX54Ej_pzEV3lwwK9QNmpcbMUmc-mAxug",
  authDomain: "logista-282218.firebaseapp.com",
  databaseURL: "https://logista-282218.firebaseio.com",
  projectId: "logista-282218",
  storageBucket: "logista-282218.appspot.com",
  messagingSenderId: "747873953165",
  appId: "1:747873953165:web:ff0d3b01fa6dd2671c89c4",
  measurementId: "G-HE8VE9GSM3"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// make auth and firestore references
const auth = firebase.auth();
// const db = firebase.firestore();
const db = firebase.database();

// changing icon by OS theme

lightSchemeIcon = document.querySelector("#themeLightOsIconSite");
darkSchemeIcon = document.querySelector("#themeDarkOsIconSite");

function onUpdate() {
if (matcher.matches) {
  lightSchemeIcon.remove();
  document.head.append(darkSchemeIcon);
} else {
  document.head.append(lightSchemeIcon);
  darkSchemeIcon.remove();
}
}

const uid = Cookies.get("uid");

const tasksQuery = `users/${uid}/tasks`
const driversQuery = `users/${uid}/drivers`

const getKey = async () => {
const apiKey = (await (db.ref(`users/${uid}/settings`).child("api-key").once("value"))).val()
var script = document.createElement('script');
script.async = true;
script.defer = true;
  if(!apiKey)
  {
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAvKCQ5Ync-aLf0_fZcxvMhlST6o2MMh2U&libraries=places,drawing&callback=initMap';

  }
  else
  {
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey + '&libraries=places,drawing&callback=initMap';
  }

  document.head.appendChild(script);


};
const logoutBtn = document.querySelector("#logoutBtn");
logoutBtn.addEventListener("click", () => {
	
	auth.signOut().then(() => {
   
		Cookies.remove("logedin");
		Cookies.remove("uid");
		window.location.assign('signup.html')
	});
});
const getNotification = () => {

let count=0;
db.ref(`users/${uid}/notifications`).on("value", (snapshot) => {
    // document.querySelector("#notification").innerHTML = ""
    const numOfFences = snapshot.numChildren()
   
    
      document.querySelector("#notification").innerHTML = snapshot.val()

  count++;
  document.querySelector("#countNotification").innerHTML = count

  
})

}


// function initialize() {
//   getKey();
// }
getNotification();
window.onload = getKey();