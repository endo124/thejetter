//	TOGGLE HIDE AND SHOW
//
//		arguments 
//			=================================================
//				-> btn that will be clicked
//				-> contanier which will be visible
//				-> the class that add the vis or hiding option
//			 =================================================
const toggleHideAndShow = (btn, container, className, callback) => {

	const buttonItem = document.querySelector(btn);
	const containerItem = document.querySelector(container);

	buttonItem.addEventListener("click", () => {
		if (typeof callback === "function") {
			callback();
		}
		containerItem.classList.toggle(className);
	});
};


// ERROR MESSAGE FUNCTION
//
//		arguments 
//			=================================================
//				-> id of the elemebt
//				-> class that will be added to make the element visible
//				-> message will be displayed inside the element
//			 =================================================
const errMessageViblity = (id, addedClass, message) => {
	const el = document.querySelector(`#${id}`)
	el.innerHTML = message
	el.classList.add(addedClass)

	setTimeout(() => {
		el.classList.remove(addedClass)
	}, 5000);
}

const changeIcon = (select, oldIconClass, newIconClass) => {
	const iconContainer = document.querySelector(select);
	if (iconContainer.classList.contains(oldIconClass)) {
		iconContainer.classList.remove(oldIconClass);
		iconContainer.classList.add(newIconClass);
	} else {
		iconContainer.classList.add(oldIconClass);
		iconContainer.classList.remove(newIconClass);
	}
};

const tabSystem = (
	tabBtnsClass,
	containerClass,
	activeTabClass,
	activeContainerClass,
	dataName
) => {
	const allTabsBtns = document.querySelectorAll(tabBtnsClass);
	const allContainers = document.querySelectorAll(containerClass);

	const resetContainers = () => {
		allContainers.forEach((container) => {
			container.classList.remove(activeContainerClass);
		});
	};

	const resetTabs = () => {
		allTabsBtns.forEach((btnTab) => {
			btnTab.classList.remove(activeTabClass);
		});
	};

	allTabsBtns.forEach((tabBtn) => {
		tabBtn.addEventListener("click", () => {
			const data = tabBtn.getAttribute(`data-${dataName}`);
			const container = document.querySelector(
				`[data-${dataName}='${data}']${containerClass} `
			);
			resetContainers();
			resetTabs();
			container.classList.add(activeContainerClass);
			tabBtn.classList.add(activeTabClass);
		});
	});
};

const responsiveJs = (width, callback) => {
	const windowWidth = window.matchMedia(`(max-width: ${width})`);
	if (windowWidth.matches) {
		if (typeof callback === "function") {
			callback();
		}
	}
};

// message -> LIKE "ARE YOU SURE YOU WANT TO DELETE THIS USER"
// btnCancelName -> "CANCEL" OR ANY BUTTON NAME YOU'D LIKE
// btnDiscardName -> "DELET" OR "DISCARD" ANY BUTTON NAME YOU'D LIKE
// callback -> THIS WILL BE A FUNCTION THAT WILL EXECUTE WHEN YOU SELECT DELETE
const popupAreYouSure = (message, btnCancelName, btnDiscardName, callback, escape) => {

	const popupshader = document.querySelector("#confirmation_bgshade")
	const popupContainer = document.querySelector(".confirmation_contianer_popup")
	const popupMessage = document.querySelector(".confirmation_contianer_popup-message")
	const popupCancelBtn = document.querySelector("#confirmation_contianer_cancel")
	const popupDiscardBtn = document.querySelector("#confirmation_contianer_dicard")
	popupMessage.innerHTML = message ? message : "are you sure?"
	popupCancelBtn.innerHTML = btnCancelName ? btnCancelName : "Cancel"
	popupDiscardBtn.innerHTML = btnDiscardName ? btnDiscardName : "Remove"

	const closePopup = () => {
		anime({
			targets: "#confirmation_bgshade",
			opacity: [1, 0],
			duration: 150,
			easing: "easeInOutQuad",
			complete: () => {
				popupshader.style.display = "none"
			}
		})

		anime({
			targets: ".confirmation_contianer_popup",
			opacity: [1, 0],
			duration: 150,
			delay: 50,
			easing: "easeInOutQuad",
			complete: () => {
				popupContainer.style.display = "none"
			}
		})
		return popupMessage.innerHTML = ""
	}

	popupshader.style.display = "flex"
	popupContainer.style.display = "flex"

	anime({
		targets: "#confirmation_bgshade",
		opacity: [0, 1],
		duration: 150,
		easing: "easeInOutQuad",
	})
	anime({
		targets: ".confirmation_contianer_popup",
		opacity: [0, 1],
		duration: 150,
		delay: 50,
		easing: "easeInOutQuad",
	})

	// if (escape) {
	// 	document.addEventListener("keydown", (e) => {
	// 		if (e.key === "Escape") closePopup()
	// 	})
	// }

	popupCancelBtn.addEventListener("click", (e) => {
		e.preventDefault()
		closePopup()
	})
	popupDiscardBtn.addEventListener("click", (e) => {
		e.preventDefault()
		closePopup()
		if (typeof callback == "function") return callback()
	})
}
const getDisplayValue = (element) => document.defaultView.getComputedStyle(element, null).display;
const getHeightValue = (element) => document.defaultView.getComputedStyle(element, null).height;


// function to close module by esc key or close button
// 		closeBtn -> Close button selector
// 		Module -> Module Div selector
// 		validate -> should it validate if there is any empty inputs (true , false) 
const closeElement = (closeBtn, moduleContainer, validate) => {

	const selectedModal = document.querySelector(moduleContainer)
	const closeModalBtn = document.querySelector(closeBtn)
	const closeAnimation = () => {
		anime({
			targets: moduleContainer,
			top: ['0px', '-150px'],
			opacity: ['1', '0'],
			duration: 300,
			easing: "easeInOutQuad",
			// complete() {
			// 	addTeamContainer.style.display = "none"
			// 	anime({
			// 		targets: "#taskDetailsPopup_bgshade",
			// 		opacity: ['1', '0'],
			// 		duration: 200,
			// 		easing: "easeInQuad",
			// 		complete() {
			// 			backgroundShade.style.display = "none"
			// 		}
			// 	})
			// }
		})
	}
	const checkValidity = () => {
		if (validate == true) {
			let numOfEmptyInputs = 0;
			selectedModal.querySelectorAll("input").forEach((input) => {
				if (input.value.trim() == "") {
					return numOfEmptyInputs++
				}
			})
			if (numOfEmptyInputs > 0) {
				popupAreYouSure(
					"Are you sure you want to discard this driver??",
					"Cancel",
					"Discard",
					() => {
						selectedModal.style.display = "none"
					})
			}
		}
	}

	if (selectedModal.style.display != "block") {

		closeModalBtn.addEventListener("click", (e) => {
			e.preventDefault()
			// selectedModal.style.display = "none"
			closeAnimation()
		})

		document.addEventListener("keydown", (e) => {
			if (e.key == "Escape") {
				// selectedModal.style.display = "none"
				closeAnimation()
			}
		})

	}

}

const assigneTaskToDriver = async (taskId, driverId) => {
	const driver = await db.ref(`users/${uid}/drivers/${driverId}`)
	const task = await db.ref(`users/${uid}/tasks/${taskId}`)
	const TasksData = await task.once("value", (snapshot) => snapshot)

		task.update({
			status: 1,
			driverId:driverId,
			taken:true,
			whoTake:driverId

		})
	
		driver.update({
		driverStatus: -1,
		taskId: taskId
		})
		db.ref(`ongoing_tasks/${uid}/${driverId}/${taskId}`).set(TasksData.val())
		db.ref(`ongoing_tasks/${uid}/${driverId}/${taskId}`).update({
			driverId:driverId,
			taken:true,
			status: 1,
			whoTake:driverId})
			location.reload();

}

// ADD TASK FUNCTION
const addTask = () => {

	// selectors
	const creatTaskPopup = document.querySelector(".createTaskItemContainerPopup");
	const addTaskForm = document.querySelector(".createTaskItemContainer_form");
	const openTaskBtn = document.querySelector(".createTaskBtnItem");
	const closeTaskBtn = document.querySelector(".createTaskItemContainer_close");
	const addTaskBtn = document.querySelector(".createTaskItemContainer_btn");

	let numberOfErrorInInputs = 0;

	// functions
	const openAddTask = () => {
		const openAddTaskAnimation = () => {
			creatTaskPopup.style.display = "flex";
			anime({
				targets: ".createTaskItemContainerPopup",
				left: ["-100%", "0%"],
				duration: 500,
				easing: "easeInOutQuad",
			});
		};

		openTaskBtn.addEventListener("click", (e) => {
			
			e.preventDefault();
			openAddTaskAnimation();
			driverSelectOptions();
		});
	};

	const closeTaskAnimation = () => {
		anime({
			targets: ".createTaskItemContainerPopup",
			left: ["0%", "-100%"],
			duration: 500,
			easing: "easeInOutQuad",
			complete: () => {
				creatTaskPopup.style.display = "none";
				addTaskForm.reset();
			},
		});
	};

	const closeAddTask = (options) => {
		const checkBeforClose = () => {
			let numberOfEmptyInputs;
			const addTaskInputs = addTaskForm.querySelectorAll("input");
			addTaskInputs.forEach((input) => {
				if (input.value.trim()) {
					return numberOfEmptyInputs++;
				}
			});
			if (numberOfEmptyInputs > 0) {
				const popupshader = document.querySelector("#confirmation_bgshade");
				if (getDisplayValue(popupshader) !== "none") return;
				popupAreYouSure(
					"are You sure you want to discard this task",
					"Cancel",
					"discard",
					() => {
						closeTaskAnimation();
					},
					false
				);
			} else {
				closeTaskAnimation();
			}
		};

		closeTaskBtn.addEventListener("click", (e) => {
			if (getDisplayValue(creatTaskPopup) == "none") return;
			return checkBeforClose();
		});

		if (!options.doesEscKeyClose) return;
		document.addEventListener("keydown", (e) => {
			if (getDisplayValue(creatTaskPopup) == "none") return;
			if (e.key !== "Escape") return;
			return checkBeforClose();
		});
	};

	const openAndCloseOcordion = () => {
		const ocordionButtons = addTaskForm.querySelectorAll(".ocordion_btn");

		ocordionButtons.forEach((ocordionButton) => {
			const ocordionBody = ocordionButton.nextSibling.nextSibling;
			const ocordionBodyId = ocordionButton.nextSibling.nextSibling.getAttribute("id");

			const openAnimation = () => {
				const ocordionBtnIcon = ocordionButton.querySelector("svg");
				ocordionBtnIcon.classList.remove("fa-chevron-down");
				ocordionBtnIcon.classList.add("fa-chevron-up");
				ocordionBody.style.display = "grid";
				anime({
					targets: `#${ocordionBodyId}`,
					height: ["0", "325px"],
					duration: 150,
					easing: "easeInOutQuad",
				});
			};

			const closeAnimation = () => {
				const ocordionBtnIcon = ocordionButton.querySelector("svg");
				ocordionBtnIcon.classList.remove("fa-chevron-up");
				ocordionBtnIcon.classList.add("fa-chevron-down");
				anime({
					targets: `#${ocordionBodyId}`,
					height: ["325px", "0"],
					duration: 150,
					easing: "easeInOutQuad",
					complete: () => {
						ocordionBody.style.display = "none";
					},
				});
			};

			ocordionButton.addEventListener("click", () => {
				if (getDisplayValue(ocordionBody) === "none") return openAnimation();
				return closeAnimation();
			});
		});
	};

	// form functions
	const checkPhoneNumber = () => {
		const phoneNumberInputs = addTaskForm.querySelectorAll("input[type=phone]");

		const phoneValidationOptions = (e) => {
			const charCode = e.which ? e.which : e.keyCode;
			const isNumber =
				event.ctrlKey ||
				event.altKey ||
				(47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) ||
				(95 < event.keyCode && event.keyCode < 106) ||
				event.keyCode == 8 ||
				event.keyCode == 9 ||
				(event.keyCode > 34 && event.keyCode < 40) ||
				event.keyCode == 46;

			// || e.target.value.length > 11
			if (!isNumber) {
				e.preventDefault();
			} else if (e.target.value.length > 11) {
				return (e.target.value = "");
			} else {
				return (e.target.value = e.target.value.trim());
			}
		};

		phoneNumberInputs.forEach((phoneNumberInput) => {
			phoneNumberInput.addEventListener("keydown", (e) => phoneValidationOptions(e));
			phoneNumberInput.addEventListener("change", (e) => phoneValidationOptions(e));
		});
	};

	const validateAddTaskInputs = () => {
		numberOfErrorInInputs = 0
		const validatePhones = () => {
			const phoneInputs = addTaskForm.querySelectorAll("input[type=phone]");
			phoneInputs.forEach((phoneInput) => {

				const inputContainer = phoneInput.parentElement
				const input = inputContainer.querySelector("input")
				const inputIcon = inputContainer.querySelector("svg")
				const inputErrorMessage = inputContainer.querySelector("span")
				if (phoneInput.value.trim() == "" || phoneInput.value.trim().length > 11) {
					inputErrorMessage.innerHTML = ""
					input.classList.add("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.add("addTaskErrorIcon")
					inputErrorMessage.classList.add("addTaskError")
					inputErrorMessage.innerHTML = "Phone Number Is not Valid"
					return numberOfErrorInInputs++
				} else {
					input.classList.remove("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.remove("addTaskErrorIcon")
					inputErrorMessage.classList.remove("addTaskError")
					inputErrorMessage.innerHTML = ""
				}
			});
		};

		const validateEmails = () => {
			const phoneInputs = addTaskForm.querySelectorAll("input[type=email]");
			phoneInputs.forEach((phoneInput) => {
				const validateEmailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				const isEmail = phoneInput.value.match(validateEmailRegex);
				const inputContainer = phoneInput.parentElement
				const input = inputContainer.querySelector("input")
				const inputIcon = inputContainer.querySelector("svg")
				const inputErrorMessage = inputContainer.querySelector("span")
				if (!isEmail &&   phoneInput.value) {
					inputErrorMessage.innerHTML = ""
					input.classList.add("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.add("addTaskErrorIcon")
					inputErrorMessage.classList.add("addTaskError")
					inputErrorMessage.innerHTML = "Email Is not Valid"
					return numberOfErrorInInputs++
				} else {
					input.classList.remove("addTaskErrorInput")
					inputIcon.classList.remove("addTaskErrorIcon")
					inputErrorMessage.classList.remove("addTaskError")
					inputErrorMessage.innerHTML = ""
				}
			});
		};

		const validateDates = () => {
			const dateInputs = addTaskForm.querySelectorAll("input[type=datetime-local]");
			dateInputs.forEach((dateInput) => {
				const inputContainer = dateInput.parentElement
				const input = inputContainer.querySelector("input")
				const inputIcon = inputContainer.querySelector("svg")
				const inputErrorMessage = inputContainer.querySelector("span")
				const currentDate = parseInt(moment(Date.now()).add(10, 'm').format("X"))
				const selectedDate = parseInt(moment(dateInput.value, "YYYY MM DD hh:mm").format("X"))
				// if (currentDate > selectedDate) {
				// 	inputErrorMessage.innerHTML = ""
				// 	input.classList.add("addTaskErrorInput")
				// 	inputIcon.classList.add("addTaskErrorIcon")
				// 	inputErrorMessage.classList.add("addTaskError")
				// 	inputErrorMessage.innerHTML = "Plase Select a Valid Date"
				// 	return numberOfErrorInInputs++
				// }
				if (dateInput.value == "") {
					inputErrorMessage.innerHTML = ""
					input.classList.add("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.add("addTaskErrorIcon")
					inputErrorMessage.classList.add("addTaskError")
					inputErrorMessage.innerHTML = "Please Select a Date"
					return numberOfErrorInInputs++
				} else {
					input.classList.remove("addTaskErrorInput")
					inputIcon.classList.remove("addTaskErrorIcon")
					inputErrorMessage.classList.remove("addTaskError")
					inputErrorMessage.innerHTML = ""
				}
			})
		}

		const validateTextInputs = () => {
			const textInputs = addTaskForm.querySelectorAll("input[isReq=true]");
			textInputs.forEach((textInput) => {
				const inputContainer = textInput.parentElement
				const input = inputContainer.querySelector("input")
				const inputIcon = inputContainer.querySelector("svg")
				const inputErrorMessage = inputContainer.querySelector("span")
				const inputValue = textInput.value.trim()
				if (inputValue == "") {
					inputErrorMessage.innerHTML = ""
					input.classList.add("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.add("addTaskErrorIcon")
					inputErrorMessage.classList.add("addTaskError")
					inputErrorMessage.innerHTML = "this input cannot Empty "
					return numberOfErrorInInputs++
				} else {
					input.classList.remove("addTaskErrorInput")
					if (inputIcon) inputIcon.classList.remove("addTaskErrorIcon")
					inputErrorMessage.classList.remove("addTaskError")
					inputErrorMessage.innerHTML = ""
				}
			})
		}

		validateTextInputs()
		validateEmails();
		validateDates();
		validatePhones();
	};
	

	const taskObject = () => {
		const taskTypeOption = addTaskForm["taskType"];
		const taskTypeValue = taskTypeOption.options[taskTypeOption.selectedIndex].value;
		const duration = document.querySelector("#taskDurationInSec").value;
		const distance = document.querySelector("#taskDistance").value;
		const driversSelect = document.querySelector("#addTaskDrivers")
		const driver = driversSelect.options[driversSelect.selectedIndex].value
		const driverId = driversSelect.options[driversSelect.selectedIndex].dataset.id
		const driverTeam = driversSelect.options[driversSelect.selectedIndex].dataset.team
		const pickupName = addTaskForm["taskPickUpName"].value;
		const pickupNumber = addTaskForm["taskPickUpNumber"].value;
		const pickupEmail = addTaskForm["taskPickUpEmail"].value;
		const pickupOrderId = addTaskForm["taskPickUpOrderId"].value;
		const pickupAddress = addTaskForm["taskPickUpAddress"];
		const pickupAddressName = pickupAddress.value;
		const pickupAddressLng = pickupAddress.getAttribute("lng");
		const pickupAddressLat = pickupAddress.getAttribute("lat");
		const pickupDate = moment(addTaskForm["taskPickUpPickUpBefore"].value, "YYYY MM DD hh:mm").format("X")
		const pickupDescription = addTaskForm["taskPickUpDescription"].value;

		const deliverName = addTaskForm["taskDeliveryName"].value;
		const deliverNumber = addTaskForm["taskDeliveryNumber"].value;
		const deliverEmail = addTaskForm["taskDeliveryEmail"].value;
		const deliverOrderId = addTaskForm["taskDeliveryOrderId"].value;
		const deliverAddress = addTaskForm["taskDeliveryAddress"];
		const deliverAddressName = deliverAddress.value;
		const deliverLng = deliverAddress.getAttribute("lng");
		const deliverLat = deliverAddress.getAttribute("lat");
		const deliverDate = moment(addTaskForm["taskDeliveryPickUpBefore"].value, "YYYY MM DD hh:mm").format("X")
		const deliverDescription = addTaskForm["taskDeliveryDescription"].value;
		const fees = addTaskForm["taskFees"].value;

		let task = {
			type: taskTypeValue,
			estTime: duration,
			distance: distance,
			status: -1,
			pickup: {
				name: pickupName,
				email: pickupEmail,
				phone: pickupNumber,
				orderId: pickupOrderId,
				date: pickupDate,
				description: pickupDescription ? pickupDescription : "",
				address: {
					name: pickupAddressName,
					lat: pickupAddressLat,
					lng: pickupAddressLng,
				},
			},
			deliver: {
				name: deliverName,
				email: deliverEmail,
				phone: deliverNumber,
				orderId: deliverOrderId,
				date: deliverDate,
				description: deliverDescription ? deliverDescription : "",
				address: {
					name: deliverAddressName,
					lat: deliverLat,
					lng: deliverLng,
				},
			},
			fees:fees
		}
	


			let orderItems=[]
			var inputsName = document.querySelectorAll("#createTaskItemContainer_form input[name='title[]']");
			var inputsQuantity = document.querySelectorAll("#createTaskItemContainer_form input[name='quantity[]']");
			var inputsPrice = document.querySelectorAll("#createTaskItemContainer_form input[name='price[]']");
			let shipment_fees=0

			for (i = 0; i < inputsName.length; i++) 
			{

			if(inputsName[i].value &&inputsQuantity[i].value && inputsPrice[i].value)
			{
				let number=i;
			orderItems.push({id:number+1,name:inputsName[i].value,quantity:parseInt(inputsQuantity[i].value),price:inputsPrice[i].value
			})
			shipment_fees+=inputsPrice[i].value;
			}



			}
		let deliver_array=[]
		
		for( let i=1; i <document.getElementById('delivery_no_input').value;i++)
		{
 let name_deliver='deliver'+i
let  task_name="taskDeliveryName"+i
const deliverName = document.getElementById("taskDeliveryName"+i).value;
const deliverNumber = document.getElementById("taskDeliveryNumber"+i).value;
const deliverEmail =  document.getElementById("taskDeliveryEmail"+i).value;
const deliverOrderId =document.getElementById("taskDeliveryOrderId"+i).value;
const deliverAddress =document.getElementById("taskDeliveryAddress"+i);
const deliverAddressName = deliverAddress.value;
const deliverLng = deliverAddress.getAttribute("lng");
const deliverLat = deliverAddress.getAttribute("lat");
const deliverDate = moment(document.getElementById("taskDeliveryPickUpBeforeSet"+i).value, "YYYY MM DD hh:mm").format("X")
const deliverDescription = document.getElementById("taskDeliveryDescription").value;

deliver_array.push({
	name: deliverName,
	email: deliverEmail,
	phone: deliverNumber,
	orderId: deliverOrderId,
	date: deliverDate,
	description: deliverDescription ? deliverDescription : "",
	address: {
		name: deliverAddressName,
		lat: deliverLat,
		lng: deliverLng,
	}
}
)

 
}


		task.deliver_points=deliver_array
		const hasDriver = driver.trim() != "" || !!driverId || !!driverTeam

		if (hasDriver) {
			task.driverId = driverId
			task.status = 0
		}

		

		return task
	}

	const driverSelectOptions = async () => {
		const driverQuery = db.ref(`users/${uid}/drivers`)
		const snapshot = await driverQuery.once("value")
		const driversSelect = document.querySelector("#addTaskDrivers")
		driversSelect.innerHTML = ""
		driversSelect.innerHTML = `<option value="" disabled selected>Please Select A driver</option>`
		snapshot.forEach((driverSnapshot) => {
			const driver = driverSnapshot.val()
			const driverId = driverSnapshot.key
			const fristName = driver.driverFirstName
			const lastName = driver.driverLastName
			const team = driver.driverTeam.value
			
				driversSelect.innerHTML += `<option data-team=${team} data-id="${driverId}">${fristName} ${lastName}</option>`

			
		})
		if (!snapshot.val()) {
			driversSelect.innerHTML = `<option value="" disabled selected>Please Select A driver</option>`
			driversSelect.innerHTML = `<option value="" disabled selected>You Have No drivers :(</option>`
		}
	}

	const addTaskToDataBase = () => {
		db.ref(tasksQuery)
			.push(taskObject())
			.then((snapshot) => {
				snapshot.once("value", (sn) => {
					const driverId = sn.val().driverId
					const taskId = sn.key
					if (!!driverId) assigneTaskToDriver(taskId, driverId)
				})
			})
		// db.ref('data').orderByChild("phone").equalTo(taskObject().deliver.phone).on("child_added", function(snapshot) {
			
		// 	if(snapshot.val())
		// 	{
		// 		db.ref(`data/${snapshot.key}`).update({'name':taskObject().deliver.name,'email':taskObject().deliver.email,'phone':taskObject().deliver.phone})

		// 	}
		// 	else
		// 	{
		// 		db.ref(`data`).push({'name':taskObject().deliver.name,'email':taskObject().deliver.email,'phone':taskObject().deliver.phone})
	
		// 	}
		// })
		// db.ref('data').orderByChild("phone").equalTo(taskObject().pickup.phone).on("child_added", function(snapshot) {
			
		// 	if(snapshot.val())
		// 	{
		// 		db.ref(`data/${snapshot.key}`).update({'name':taskObject().pickup.name,'email':taskObject().pickup.email,'phone':taskObject().pickup.phone})

		// 	}
		// 	else
		// 	{
		// 		db.ref(`data`).push({'name':taskObject().pickup.name,'email':taskObject().pickup.email,'phone':taskObject().pickup.phone})
	
		// 	}
		// })

			//window.location.assign('index.html')
	}

	const submitAddTask = () => {
		
		validateAddTaskInputs()
		 if (numberOfErrorInInputs !== 0) return
		// if (false) return
		addTaskToDataBase()
		for( let i=1; i <document.getElementById('delivery_no_input').value;i++)
		{
 let name_deliver='deliver'+i
let  task_name="taskDeliveryName"+i
const deliverName = document.getElementById("taskDeliveryName"+i).value;
const deliverNumber = document.getElementById("taskDeliveryNumber"+i).value;
const deliverEmail =  document.getElementById("taskDeliveryEmail"+i).value;
const deliverAddress =document.getElementById("taskDeliveryAddress"+i);
const deliverAddressName = deliverAddress.value;
const deliverLng = deliverAddress.getAttribute("lng");
const deliverLat = deliverAddress.getAttribute("lat");

$.ajax({
	url: 'https://r-write.com/logistaApi/public/api/data',   
	type: 'POST',
	data:JSON.stringify({name:deliverName,phone:deliverNumber,email:deliverEmail,address:deliverAddressName,lat:deliverLat,lng:deliverLng}),
	headers: {
	'Content-Type': 'application/json',
	"Accept": 'application/json',
	}
	});



 
}
$.ajax({
	url: 'https://r-write.com/logistaApi/public/api/data',   
	type: 'POST',
	data:JSON.stringify({name:document.getElementById("taskDeliveryName").value,phone:document.getElementById("taskDeliveryNumber").value,email:document.getElementById("taskDeliveryEmail").value,address:document.getElementById("taskDeliveryAddress").value,lat:document.getElementById("taskDeliveryAddress").getAttribute('lat'),lng:document.getElementById("taskDeliveryAddress").getAttribute('lng')}),
	headers: {
	'Content-Type': 'application/json',
	"Accept": 'application/json',
	},
	success: function(json) {

	   }
	});

		$.ajax({
			url: 'https://r-write.com/logistaApi/public/api/data',   
			type: 'POST',
			data:JSON.stringify({name:document.getElementById("taskPickUpName").value,phone:document.getElementById("taskPickUpNumber").value,email:document.getElementById("taskPickUpEmail").value,address:document.getElementById("taskPickUpAddressSearchInput").value,lat:document.getElementById("taskPickUpAddressSearchInput").getAttribute('lat'),lng:document.getElementById("taskPickUpAddressSearchInput").getAttribute('lng')}),
			headers: {
			'Content-Type': 'application/json',
			"Accept": 'application/json',
			},
			success: function(json) {
				closeTaskAnimation()

			
	           }
			});
	}

	addTaskForm.addEventListener('submit', (e) => e.preventDefault())
	addTaskBtn.addEventListener('click', (e) => {
		
		e.preventDefault()
		submitAddTask()	
	
	})

	driverSelectOptions()
	 openAndCloseOcordion();
	checkPhoneNumber();
	openAddTask();
	closeAddTask({
		doesEscKeyClose: true,
	});
};

const shababTabsActivate = () => {
	tabSystem(
		".map_info-col__subhead-tasks",
		".map_info-col__containar-tabTask",
		"map_info-col__subhead-item--active",
		"map_info-col__containar-tabTask--active",
		"tasktab"
	);
	tabSystem(
		".map_info-col__subhead-agents",
		".map_info-col__containar-tabAgnet",
		"map_info-col__subhead-item--active",
		"map_info-col__containar-tabAgnet--active",
		"agentTab"
	);
}

const driverAndTaskTabsVisibility = () => {
	toggleHideAndShow(".map_info-col_collaps--tasks", ".map_tasks", "map_col--collapsed", () =>
		changeIcon(".map_info-col_icon--tasks", "fa-chevron-left", "fa-chevron-right")
	);
	toggleHideAndShow(".map_info-col_collaps--agents", ".map_agents", "map_col--collapsed", () =>
		changeIcon(".map_info-col_icon--agents", "fa-chevron-right", "fa-chevron-left")
	);
}

const navigationButton = () => {
	toggleHideAndShow(".navigation_hamburgerBtn", ".hamburger_menu", "hamburger_menu--active");
	toggleHideAndShow(".hamburger_btn-back_container", ".hamburger_menu", "hamburger_menu--active");
	toggleHideAndShow(".pickup_btn", ".pickup_contanier", "ocordion_body--active");
	// toggleHideAndShow(".dropoff_btn", ".dropoff_container", "ocordion_body--active");

	toggleHideAndShow(".notification_btn", ".notification_nav_container", "nav_popup--active");
	toggleHideAndShow(".menu_navigation_btn", ".menu_navigation_container", "nav_popup--active");
}

const navigationButtonsActivation = () => {
	toggleHideAndShow(".notification_btn", ".notification_nav_container", "nav_popup--active");
	toggleHideAndShow(".navigation_hamburgerBtn", ".hamburger_menu", "hamburger_menu--active");
	toggleHideAndShow(".hamburger_btn-back_container", ".hamburger_menu", "hamburger_menu--active");
	toggleHideAndShow(".menu_navigation_btn", ".menu_navigation_container", "nav_popup--active");

}

const hamburgerMenu = () => {
	toggleHideAndShow(".navigation_hamburgerBtn", ".hamburger_menu", "hamburger_menu--active");
	toggleHideAndShow(".hamburger_btn-back_container", ".hamburger_menu", "hamburger_menu--active");
}

const closeByEscape = (callback) => {
	document.addEventListener("keydown", (e) => {
		if (e.key == "Escape") callback()
	})
}


const closeByButton = (button, callback) => {
	button.addEventListener("click", (e) => {
		e.preventDefault()
		callback()
	})
}
function setData(event) {

	
	let id =event.target.id
	let numb = id.match(/\d/g);
	if(numb!=null)
	{
		setTimeout(function() {
		 numb = numb.join("");
		

		 let phone=document.getElementById(event.target.id).value
			$.ajax({
				url: 'https://r-write.com/logistaApi/public/api/getdata',   
				type: 'POST',
				data:JSON.stringify({phone:phone}),
				headers: {
				'Content-Type': 'application/json',
				"Accept": 'application/json',
				},
				success: function(json) {
				
			if(json.success)
			{
			
			document.getElementById("taskDeliveryName"+numb).value	=json.success.name
			document.getElementById("taskDeliveryEmail"+numb).value	=json.success.email
			document.getElementById("taskDeliveryAddress"+numb).value	=json.success.address
			document.getElementById('taskDeliveryAddress'+numb).setAttribute("lng", json.success.lat);
			document.getElementById('taskDeliveryAddress'+numb).setAttribute("lat", json.success.lng);
			
			}
			else
			{
				document.getElementById("taskDeliveryName"+numb).value	=''
				document.getElementById("taskDeliveryEmail"+numb).value	=''
				document.getElementById("taskDeliveryAddress"+numb).value	=''
				document.getElementById('taskDeliveryAddress'+numb).setAttribute("lng", '');
				document.getElementById('taskDeliveryAddress'+numb).setAttribute("lat", '');	
			}
				
			}
				});
			})
		
	}
	else
	{
	
		setTimeout(function() {
		 
			let phone=document.getElementById(event.target.id).value
			$.ajax({
				url: 'https://r-write.com/logistaApi/public/api/getdata',   
				type: 'POST',
				data:JSON.stringify({phone:phone}),
				headers: {
				'Content-Type': 'application/json',
				"Accept": 'application/json',
				},
				success: function(json) {
				
			if(json.success)
			{
			
			document.getElementById("taskDeliveryName").value	=json.success.name
			document.getElementById("taskDeliveryEmail").value	=json.success.email
			document.getElementById("taskDeliveryAddress").value	=json.success.address
			document.getElementById('taskDeliveryAddress').setAttribute("lng", json.success.lat);
			document.getElementById('taskDeliveryAddress').setAttribute("lat", json.success.lng);
			
			}
			else
			{
			document.getElementById("taskDeliveryName").value	=''
			document.getElementById("taskDeliveryEmail").value	=''
			document.getElementById("taskDeliveryAddress").value	=''
			document.getElementById('taskDeliveryAddress').setAttribute("lng", '');
			document.getElementById('taskDeliveryAddress').setAttribute("lat", '');
			}
				
			}
				});
	  
		}, 1);
	
		
	}
		}
$(document).ready(function() {
$('#taskPickUpNumber').keydown(function(){
	var timer = null;

	clearTimeout(timer); 
	timer = setTimeout(setDataPickUp, 1000)
});
// $('#taskDeliveryNumber').keydown(function(){
// 	var timer = null;

// 	clearTimeout(timer); 
// 	timer = setTimeout(setDataDeliver, 1000)
// });
function setDataPickUp() {
	let phone=document.getElementById("taskPickUpNumber").value
	$.ajax({
		url: 'https://r-write.com/logistaApi/public/api/getdata',   
		type: 'POST',
		data:JSON.stringify({phone:phone}),
		headers: {
		'Content-Type': 'application/json',
		"Accept": 'application/json',
		},
		success: function(json) {
if(json.success)
{
document.getElementById("taskPickUpName").value	=json.success.name
document.getElementById("taskPickUpEmail").value	=json.success.email
document.getElementById("taskPickUpAddressSearchInput").value	=json.success.address
document.getElementById('taskPickUpAddressSearchInput').setAttribute("lng", json.success.lat);
document.getElementById('taskPickUpAddressSearchInput').setAttribute("lat", json.success.lng);

}
else
{
	document.getElementById("taskPickUpName").value	=''
	document.getElementById("taskPickUpEmail").value	=''
	document.getElementById("taskPickUpAddressSearchInput").value	=''
	document.getElementById('taskPickUpAddressSearchInput').setAttribute("lng", '');
	document.getElementById('taskPickUpAddressSearchInput').setAttribute("lat",'');	
}
		
}
		});
}
// function setDataDeliver() {
// 	let phone=document.getElementById("taskDeliveryNumber").value

// 	db.ref('data').orderByChild("phone").equalTo(phone).on("child_added", function(snapshot) {
// 		if(snapshot.val())
// 		{
// 			document.getElementById("taskDeliveryName").value	=snapshot.val().name
// 			document.getElementById("taskDeliveryEmail").value	=snapshot.val().email
			
// 		}
// 	  });
// }
})

;
///// add  row order
$( document ).ready(function() {
	$("#addRow").click(function () {
		var inputsName = document.querySelectorAll("#createTaskItemContainer_form input[name='title[]']");
		var inputsQuantity = document.querySelectorAll("#createTaskItemContainer_form input[name='quantity[]']");
		var inputsPrice = document.querySelectorAll("#createTaskItemContainer_form input[name='price[]']");
		for (i = 0; i < inputsName.length; i++) 
		{
	if( !inputsName[i].value || !inputsQuantity[i].value || !inputsPrice[i].value)
	{
		
		const inputContainer = inputsName[i].parentElement
		const input = inputContainer.querySelector("input")
		
		const inputErrorMessage = inputContainer.querySelector("span")
		inputsName[i].classList.add("addTaskErrorInput")
		inputsQuantity[i].classList.add("addTaskErrorInput")
		inputsPrice[i].classList.add("addTaskErrorInput")
		inputErrorMessage.classList.add("addTaskError")
		inputErrorMessage.innerHTML = "Order Items is requires"
	return;
	}
	const inputContainer = inputsName[i].parentElement
	const input = inputContainer.querySelector("input")
	const inputErrorMessage = inputContainer.querySelector("span")
	inputsName[i].classList.remove("addTaskErrorInput")
	inputsQuantity[i].classList.remove("addTaskErrorInput")
	inputsPrice[i].classList.remove("addTaskErrorInput")
	inputErrorMessage.classList.remove("addTaskError")
	inputErrorMessage.innerHTML = ""
	}
		var html = '';
		html += '<div id="inputFormRow"  class="addtask-input_container">';
		html += '<div class="input-group mb-3">';
		html += '<input type="text" name="title[]" required class="form-control m-input mr-1" placeholder="Enter Product Name" autocomplete="off">';
		html += '<input type="number" name="price[]"  required class="form-control m-input mr-1" placeholder="Enter Price" autocomplete="off">';
		html += '<input type="number" name="quantity[]" required class="form-control m-input mr-1" placeholder="Enter Quantity" autocomplete="off">';
		html +='<span class="addTaskError"></span>'
		html += '<div class="input-group-append">';
		html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
		html += '</div>';
		html += '</div>';
	
		$('#newRow').append(html);
	});
	
	// remove row
	$(document).on('click', '#removeRow', function () {
		$(this).closest('#inputFormRow').remove();
	});
	});
$( document ).ready(function() {


	// document.getElementById('button').onclick = duplicate;



	var $section = $("#duplicater").clone(); 

	function duplicate() {
		var $sectionClone = $section.clone(true,true);
		$('#duplicater').append($sectionClone); 

	}

	var $template = $(".template");

var hash = 1;

let check =0;
let i=1
$(".btn-add-panel").on("click", function () {
	




    var $newPanel = $template.clone();
    $newPanel.find(".collapse").removeClass("in");
    // $newPanel.find(".accordion-toggle").removeClass("number");
    
    let inc=++hash
    $newPanel.find(".accordion-toggle").prop("href",  "#" + (inc))
    $newPanel.find(".accordion-toggle").css('display','contents') 
    $newPanel.find(".card-header").css('padding-left','20px') 

    let x='Delivery Point #<span class="number">'+inc+'</span><i class="fa fa-angle-down" style="float: right; margin-right: 5px;"></i>'
    $newPanel.find(".accordion-toggle").html(x)
             // .text(+ hash);
    // $newPanel.find(".accordion-toggle").append(+ hash)
	$newPanel.find(".panel-collapse").prop("id", hash).addClass("collapse").removeClass("in");
	// $newPanel.find('input').each(function() {
	// 	console.log($newPanel.find( this.name).attr("id"),'this.name')
	// 	this.name= this.name.replace(this.name, this.name + hash);
	// });	

	$newPanel.find("input").prop('id', function(idx, attrVal) {
        return attrVal + i;  // change the id
    }).prop('name', function(idx, attrVal) {
        return attrVal + i;  // change the name
	}).val('').end()
	$("#accordion").append($newPanel.fadeIn());
	document.getElementById('delivery_no_input').value=hash
	++i
	
if( hash !=1)
{
	
	document.getElementById('btn-remove-panel').style.display='block'
}


	initMap()

});

$(".btn-remove-panel").on("click", function () {
	var idPanel = '#' + (hash).toString();

		var $accordion = $('#accordion');
		var $collapseOne = $accordion.find(idPanel);
		$collapseOne.closest('.template').remove();
		--hash
		document.getElementById('delivery_no_input').value=hash
		if( hash ==1)
{
	
	document.getElementById('btn-remove-panel').style.display='none'
}

})
});