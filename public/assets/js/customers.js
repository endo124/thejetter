// check if user and redirect =============
if (!uid) {
    Cookies.remove("logedin");
    auth.signOut();
  }
  auth.onAuthStateChanged((user) => {
    if (!user) {
      Cookies.remove("logedin");
      Cookies.remove("uid");
      window.location.replace("signup.html");
    }
  });
  
  // ========================================
  function initMap() {
    // ADDTASK MAP
    var addTaskMaps = new google.maps.Map(document.querySelector(".createTaskItemContainer_map"), {
      zoom: 8,
      center: {
        lat: 30.0444,
        lng: 31.2357,
      },
      disableDefaultUI: true,
    });
  
    // ADD MARKS ON ADD TASKS MAP
    const getDataAndSetMark = (input, autoComplete, map, title) => {
      google.maps.event.addListener(autoComplete, "place_changed", function () {
        var place = autoComplete.getPlace().geometry.location;
        const lat = place.lat();
        const lng = place.lng();
  
        var center = new google.maps.LatLng(lat, lng);
        map.panTo(center);
  
        var addMarker = new google.maps.Marker({
          position: {
            lat,
            lng,
          },
          map,
          title,
        });
  
        input.setAttribute("lng", lng);
        input.setAttribute("lat", lat);
      });
    };
  
    // AUTOCOMPLETE MAP SEARCH INPUT FOR PICKUP INPUT
    var pickupPoint = document.querySelector("#taskPickUpAddressSearchInput");
    var autocompletePickUp = new google.maps.places.Autocomplete(pickupPoint);
    autocompletePickUp.setComponentRestrictions({
      country: ["eg"],
    });
  
    // AUTOCOMPLETE MAP SEARCH INPUT FOR DELIVERY INPUT
    var dropOffInput = document.querySelector("#taskDeliveryAddress");
    var autocompletedropOffInput = new google.maps.places.Autocomplete(dropOffInput);
  
    // RESTRICT SEARCH TO EGYPT ONLY
    autocompletedropOffInput.setComponentRestrictions({
      country: ["eg"],
    });
  
    // RUN
    getDataAndSetMark(pickupPoint, autocompletePickUp, addTaskMaps, "Pick Up point");
    getDataAndSetMark(dropOffInput, autocompletedropOffInput, addTaskMaps, "Pick Up point");
  }
  // ===============
  
  const addDriverForm = document.querySelector(".addDriver_popup-form")
  const popUpMessage = document.querySelector(".popup_message");


  
  const popUpMessgeFunction = (message, delay, status) => {
    popUpMessage.innerHTML = message;
    popUpMessage.classList.add(`popup_message--${status == 1 ? "succ" : "err"}`);
    popUpMessage.style.display = "block";
    setTimeout(() => {
      popUpMessage.style.display = "none";
      popUpMessage.innerHTML = "";
    }, delay * 1000);
  }
  
  
  
  const confirmationPopUp = document.querySelector(".confirmation_contianer_popup")
  const confirmationPopUpActiveClass = "confirmation_contianer_popup--active"
  const confirmationMessage = confirmationPopUp.querySelector(".confirmation_contianer_popup-message");
  const confirmationContianerCancel = confirmationPopUp.querySelector("#confirmation_contianer_cancel")
  const confirmationContianerDiscard = confirmationPopUp.querySelector("#confirmation_contianer_dicard")
  const addDriverPopup = document.querySelector(".addDriver_popup")
  const addDriverPopupContainer = document.querySelector(".addDriver_popup-container")
  

  const closeDriverModuleAnimation = () => {
    anime({
      opacity: ["0", "1"],
      duration: 200,
      easing: 'easeInOutQuad',
      complete() {
        addDriverPopup.style.display = "none"
      }
    })
    anime({
      targets: ".addDriver_popup-container",
      opacity: ["1", "0"],
      duration: 200,
      easing: 'easeInOutQuad',
      complete() {
        addDriverPopupContainer.style.display = "none"
      }
    })
  }
  
  const addDriver = () => {
    addDriverForm.addEventListener("submit", async (e) => {
      e.preventDefault()
  
      const driverFirstName = addDriverForm["driverFirstName"];
      const driverLastName = addDriverForm["driverLastName"];
  
      const driverEmail = addDriverForm["driverEmail"];
      const driverPhoneNumber = addDriverForm["driverPhoneNumber"];
      const driverUsername = addDriverForm["driverUsername"];
  
      const driverPassword = addDriverForm["driverPassword"];
      const driverRole = addDriverForm["driverRole"].options[addDriverForm["driverRole"].selectedIndex];
      const driverTeam = addDriverForm["driverTeam"].options[addDriverForm["driverTeam"].selectedIndex];
      const driverAddress = addDriverForm["driverAddress"];
      const driverProfileImage = addDriverForm["driver_proile_picture"];
      const transportation_type = addDriverForm["transportation_type"];
  
  
      let isExistingUsername = false
      let shortUsername = false
      let notPhoneNumber = false
      let notValidEmail = false
      let shortPassword = false
  
      let emptyinputs = 0
  
      const emptyErorrMessages = () => {
        document.querySelectorAll(".driver-add-error").forEach((input) => {
          input.innerHTML = ""
        })
      }
  
      const validateWhatsCantBeEmpty = () => {
        if (!(driverFirstName.value)) {
          const errorFirstName = driverFirstName.parentElement.querySelector(".driver-add-error")
          errorFirstName.innerHTML = "Please add Driver's First Name"
          emptyinputs = 1
        }
        if (!(driverLastName.value)) {
          const errorLastName = driverLastName.parentElement.querySelector(".driver-add-error")
          errorLastName.innerHTML = "Please add Driver's Last Name"
          emptyinputs++
        }
        if (!(driverTeam.value)) {
          const errorTeam = driverTeam.parentElement.parentElement.querySelector(".driver-add-error")
          errorTeam.innerHTML = "Please Select a team"
          emptyinputs++
        }
        if (!(driverAddress.value)) {
          const errorAddress = driverAddress.parentElement.querySelector(".driver-add-error")
          errorAddress.innerHTML = "Please add Driver Addrss"
          emptyinputs++
        }
        if (!(transportation_type.value)) {
          console.log(transportation_type.value)
          const errorTranportation = transportation_type[0].parentElement.parentElement.querySelector(".driver-add-error")
          errorTranportation.innerHTML = "Please add Trasportation type"
          emptyinputs++
        }
        if (!(driverProfileImage.value)) {
          const errorTranportation = driverProfileImage.parentElement.querySelector(".driver-add-error")
          errorTranportation.innerHTML = "Please add Driver's Image"
          emptyinputs++
        }
      }
  
      const validateUsername = async () => {
        const usernames = []
        const usernameValue = driverUsername.value
        const errorUserName = driverUsername.parentElement.querySelector(".driver-add-error")
  
        // username cant be less than 6 character
        if (usernameValue.length < 6) {
          errorUserName.innerHTML = "Please username must be 6 or more characters"
          shortUsername = true
        }
  
        // username be the same
        await db.ref(`users/${uid}/drivers`).once("value", (driversSnapshot) => {
          driversSnapshot.forEach(driverSnapshot => usernames.push(driverSnapshot.val().driverUsername));
          const sameUsername = usernames.filter((username) => username == usernameValue)
          if (sameUsername.length != 0) isExistingUsername = true
        })
      }
  
      const validateMobileNumber = () => {
        const phoneNumber = driverPhoneNumber.value
        const phoneRegex = /(01)[0-9]{9}/
        const validator = phoneNumber.match(phoneRegex)
        const errorPhone = driverPhoneNumber.parentElement.querySelector(".driver-add-error")
        if (!validator) {
          errorPhone.innerHTML = "Please add a valid phone number"
          notPhoneNumber = true
        }
      }
  
      const validatePassword = () => {
        const passwordLength = driverPassword.value.length
        if (passwordLength < 8) {
          const errorPassword = driverPassword.parentElement.querySelector(".driver-add-error")
          errorPassword.innerHTML = "password must be more than 8 characters"
          shortPassword = true
        }
      }
  
      const validateEmail = () => {
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        const email = driverEmail.value
        if (!(email.match(emailRegex))) {
          notValidEmail = true
          const errorEmail = driverEmail.parentElement.querySelector(".driver-add-error")
          errorEmail.innerHTML = "Please Write a valid Email address"
        }
      }
  
      emptyErorrMessages()
  
      validateEmail()
      validatePassword()
      validateUsername()
      validateMobileNumber()
      validateWhatsCantBeEmpty()
  
      if (isExistingUsername) {
        const errorUserName = driverUsername.parentElement.querySelector(".driver-add-error")
        errorUserName.innerHTML = "username is already taken"
      }
  
      if (emptyinputs !== 0 || isExistingUsername || shortUsername || notPhoneNumber || shortPassword || notValidEmail) return
  
      const storageRef = firebase.storage().ref()
      const file = driverProfileImage.files[0]
      const fileName = `${new Date()}_${file.name}`
      const metaData = {
        contentType: file.type,
      }
      const driverImageUrl = await storageRef
        .child(fileName)
        .put(file, metaData)
        .then(async (snapshot) => await snapshot.ref.getDownloadURL())
  
      const driverObject = {
        driverFirstName: driverFirstName.value,
        driverLastName: driverLastName.value,
        driverEmail: driverEmail.value,
        driverPhoneNumber: driverPhoneNumber.value,
        driverUsername: driverPhoneNumber.value,
        driverPassword: driverPassword.value,
        driverRole: driverRole.value,
        driverTeam: {
          value: driverTeam.value,
          name: driverTeam.innerHTML,
        },
        driverStatus: 0,
        driverAddress: driverAddress.value,
        driverTransportation: transportation_type.value,
        driverProfileImage: driverImageUrl,
        taskId: 0,
      }
      db.ref(`users/${uid}/drivers`).push(driverObject).then(() => {
        closeDriverModuleAnimation()
      })
  
    })
  }
  

  
  const closeAddDriverModule = () => {
    const closeDriverBtn = document.querySelector("#closeAddDriverModal")
    closeByButton(closeDriverBtn, closeDriverModuleAnimation)
    closeByEscape(closeDriverModuleAnimation)
  }
  
  const readTasks = () => {
    var table = $('#example').DataTable ( {
      
      dom: 'Bfrtip',
 buttons: [ 'copy', 'excel', 'pdf', 'print','csv' ],

      "searching": true,
      } );
   
      db.ref(`users/${uid}/customers`).on('value',function(CustomersSnapshot) {

        CustomersSnapshot.forEach((snapshot) => {
      var customer = snapshot.val();
     
      console.log(customer,'task')
   
     if(customer.approved==0)
     {
        var dataSet = [snapshot.key,customer.name,customer.email,customer.phone,'Approve','<a  onclick="ApproveRequest(\'' 
        + snapshot.key
        + '\',\''+'Approve'+'\')">Approve</a>'];
     }
     else
     {
        var dataSet = [snapshot.key,customer.name,customer.email,customer.phone,'Disapprove','<a  onclick="ApproveRequest(\'' 
        + snapshot.key
        + '\',\''+'Disapprove'+'\')">Disapprove</a>'];
     }
  
      
				
       table.rows.add([dataSet]).draw();

				
       
        // console.log(dataSet,'dataSet')
        

  

    })
})
   
  
  
   
  }
  


  readTasks()
 
  
  // Front-End styling
  navigationButtonsActivation()
  addTask();

  function ApproveRequest(key,type)
  {
      if(type=='Approve')
      {
        db.ref(`users/${uid}/customers/${key}`).update({
            approved: 1
        })
      }
      else
      {
        db.ref(`users/${uid}/customers/${key}`).update({
            approved: 0
        }) 
      }
      location.reload();

    
    

  }
 