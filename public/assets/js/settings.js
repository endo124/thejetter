function initMap() {
    // ADDTASK MAP
    var addTaskMaps = new google.maps.Map(document.querySelector(".createTaskItemContainer_map"), {
        zoom: 8,
        center: {
            lat: 30.0444,
            lng: 31.2357,
        },
        disableDefaultUI: true,
    });

    // ADD MARKS ON ADD TASKS MAP
    const getDataAndSetMark = (input, autoComplete, map, title) => {
        google.maps.event.addListener(autoComplete, "place_changed", function () {
            var place = autoComplete.getPlace().geometry.location;
            const lat = place.lat();
            const lng = place.lng();

            var center = new google.maps.LatLng(lat, lng);
            map.panTo(center);

            var addMarker = new google.maps.Marker({
                position: {
                    lat,
                    lng,
                },
                map,
                title,
            });

            input.setAttribute("lng", lng);
            input.setAttribute("lat", lat);
        });
    };

    // AUTOCOMPLETE MAP SEARCH INPUT FOR PICKUP INPUT
    var pickupPoint = document.querySelector("#taskPickUpAddressSearchInput");
    var autocompletePickUp = new google.maps.places.Autocomplete(pickupPoint);
    autocompletePickUp.setComponentRestrictions({
        country: ["eg"],
    });

    // AUTOCOMPLETE MAP SEARCH INPUT FOR DELIVERY INPUT
    var dropOffInput = document.querySelector("#taskDeliveryAddress");
    var autocompletedropOffInput = new google.maps.places.Autocomplete(dropOffInput);

    // RESTRICT SEARCH TO EGYPT ONLY
    autocompletedropOffInput.setComponentRestrictions({
        country: ["eg"],
    });

    // RUN
    getDataAndSetMark(pickupPoint, autocompletePickUp, addTaskMaps, "Pick Up point");
    getDataAndSetMark(dropOffInput, autocompletedropOffInput, addTaskMaps, "Pick Up point");
}
var user = firebase.auth().currentUser;

navigationButtonsActivation()
addTask();
const updateApiKeyForm = document.querySelector(".updateApiKey");
const apiKey = updateApiKeyForm["apiKey"];
const country = updateApiKeyForm["country"];
const company_name = updateApiKeyForm["company_name"];
updateApiKeyForm.addEventListener("submit", (e) => {
    e.preventDefault();
    
    if (!apiKey.value) {
      return errMessageViblity("loginErrMessage", "errorMessage--active", "Please Write Your Api Key")
    }
    if (!company_name.value) {
        return errMessageViblity("loginErrMessage", "errorMessage--active", "Please Write Company name")
      }
      if (!country.value) {
        return errMessageViblity("loginErrMessage", "errorMessage--active", "Please Write country ")
      }
      if(!apiKey.value)
      {
        db.ref(`users/${uid}/settings`).update({'api-key':'AIzaSyAvKCQ5Ync-aLf0_fZcxvMhlST6o2MMh2U'})
      }
     
    
    db.ref(`users/${uid}/settings`).update({country:country.value})
    db.ref(`users/${uid}`).update({companyName:company_name.value})
confirm('Done Update')
  });  

  $(document).ready(function() {
    const setKey = async () => {

        const apiKey = (await (db.ref(`users/${uid}/settings`).child("api-key").once("value"))).val()   
        const country = (await (db.ref(`users/${uid}/settings`).child("country").once("value"))).val() 
        const company_name = (await (db.ref(`users/${uid}`).child("companyName").once("value"))).val() 

         if(country)
         {         
            $('select').val(country).trigger('change');
            Cookies.set("country", country, {expires: 30000000});

         }
         
         if(apiKey)
         {         
            document.getElementById("apiKey").value=apiKey

         }
        

         if(company_name)
         {         
            document.getElementById("company_name").value=company_name

         }
         
         
    }
    $('#country').select2();
    setKey();
});


const addZoneSelect = async () => {

	const dbConnect = await db.ref(`users/${uid}/geoFences`)
	const zonesData = await dbConnect.once("value", (snapshot) => snapshot)
	const zoneTableEle = document.querySelector("#zones-table")
let content='';
	if(zoneTableEle)
	{
     zonesData.forEach((zoneData) => {
		const id = zoneData.key
        const zone = zoneData.val()
        let idInput=zone.name.replace(/\s/g, '')+'express';
        let idInputTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromexpress';
        let idInputTimeTo=zone.name.replace(/\s/g, '')+'TimeToexpress';
        let idInputCheck=zone.name.replace(/\s/g, '')+'check';
        let sameDayInput=zone.name.replace(/\s/g, '')+'sameDay';
        let idInputSameDayCheck=zone.name.replace(/\s/g, '')+'sameDayCheck';
        let idInputSameDayTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromsameDay';
        let idInputSameDayTimeTo=zone.name.replace(/\s/g, '')+'TimeTosameDay';
        let nextDayInput=zone.name.replace(/\s/g, '')+'nextDay';
        let idInputNextDayCheck=zone.name.replace(/\s/g, '')+'nextDayCheck';
        let idInputNextDayTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromnextDay';
        let idInputNextDayTimeTo=zone.name.replace(/\s/g, '')+'TimeTonextDay';
        let scheduledDayInput=zone.name.replace(/\s/g, '')+'scheduledDay';
        let idInputScheduledCheck=zone.name.replace(/\s/g, '')+'scheduledDayCheck';
        let idInputScheduledTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromnscheduledDay';
        let idInputScheduledTimeTo=zone.name.replace(/\s/g, '')+'TimeToscheduledDay';
        let idInputNumberTask=zone.name.replace(/\s/g, '')+'NumberTask';
    

        content += '<tr>';
        content += '<td>' + zone.name + '</td>'; 

if(zone.flags)
{
    if(zone.flags.Express)
    {
        
    /////express shipping
    content +='<td><label for='+idInputCheck+'><input checked type="checkbox" name='+idInputCheck+' id='+idInputCheck+' value="Express shopping" style="margin-right: 4px;"  />Express shopping</label><input type="number" name='+idInput+'  value='+zone.flags.Express.cost+' style="display:block"/><input type="time" id='+idInputTimeFrom+' name='+idInputTimeFrom+' value='+zone.flags.Express.From+' style="display:block"/>to<input type="time" id='+idInputTimeTo+' name='+idInputTimeTo+'  value='+zone.flags.Express.To+' style="display:block"/></td>';
    }
    else
    {
        content +='<td><label for='+idInputCheck+'><input  type="checkbox" name='+idInputCheck+' id='+idInputCheck+' value="Express shopping" style="margin-right: 4px;"  />Express shopping</label><input type="number" name='+idInput+'  value="Express shipping" style="display:block"/><input type="time" id='+idInputTimeFrom+' name='+idInputTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputTimeTo+' name='+idInputTimeTo+'  value="" style="display:block"/></td>';
 
    }
}

else
{
    content +='<td><label for='+idInputCheck+'><input  type="checkbox" name='+idInputCheck+' id='+idInputCheck+' value="Express shopping" style="margin-right: 4px;"  />Express shopping</label><input type="number" name='+idInput+'  value="Express shipping" style="display:block"/><input type="time" id='+idInputTimeFrom+' name='+idInputTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputTimeTo+' name='+idInputTimeTo+'  value="" style="display:block"/></td>';

}
if(zone.flags)
{       
if(zone.flags.SameDay)
{
/////same day shipping
        content +='<td><label for='+idInputSameDayCheck+'><input checked type="checkbox" name='+idInputSameDayCheck+' id='+idInputCheck+' value="Same Day Shopping" style="    margin-right: 4px;"  />Same Day Shopping</label><input type="number" name='+sameDayInput+'  value='+zone.flags.SameDay.cost+' style="display:block"/><input type="time" id='+idInputSameDayTimeFrom+' name='+idInputSameDayTimeFrom+' value='+zone.flags.SameDay.From+' style="display:block"/>to<input type="time" id='+idInputSameDayTimeTo+' name='+idInputSameDayTimeTo+'  value='+zone.flags.SameDay.To+' style="display:block"/></td>';

}
else
{
    content +='<td><label for='+idInputSameDayCheck+'><input type="checkbox" name='+idInputSameDayCheck+' id='+idInputCheck+' value="Same Day Shopping" style="    margin-right: 4px;"  />Same Day Shopping</label><input type="number" name='+sameDayInput+'  value="" style="display:block"/> <input type="time" id='+idInputSameDayTimeFrom+' name='+idInputSameDayTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputSameDayTimeTo+' name='+idInputSameDayTimeTo+'  value="" style="display:block"/></td>';

}
}
else
{
content +='<td><label for='+idInputSameDayCheck+'><input type="checkbox" name='+idInputSameDayCheck+' id='+idInputCheck+' value="Same Day Shopping" style="    margin-right: 4px;"  />Same Day Shopping</label><input type="number" name='+sameDayInput+'  value="" style="display:block"/><input type="time" id='+idInputSameDayTimeFrom+' name='+idInputSameDayTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputSameDayTimeTo+' name='+idInputSameDayTimeTo+'  value="" style="display:block"/></td>';

}
if(zone.flags)
{
if(zone.flags.NexDay)
{
    content +='<td><label for='+idInputNextDayCheck+'><input checked type="checkbox" name='+idInputNextDayCheck+' id='+idInputNextDayCheck+' value="Next Day Shopping" style="    margin-right: 4px;" />Next Day Shopping</label><input type="number" name='+nextDayInput+'  value='+zone.flags.NexDay.cost+' style="display:block"/><input type="time" id='+idInputNextDayTimeFrom+' name='+idInputNextDayTimeFrom+' value='+zone.flags.NexDay.From+' style="display:block"/>to<input type="time" id='+idInputNextDayTimeTo+' name='+idInputNextDayTimeTo+'  value='+zone.flags.NexDay.To+' style="display:block"/></td>';

}
else
{
    content +='<td><label for='+idInputNextDayCheck+'><input type="checkbox" name='+idInputNextDayCheck+' id='+idInputNextDayCheck+' value="Next Day Shopping" style="    margin-right: 4px;" />Next Day Shopping</label><input type="number" name='+nextDayInput+'  value="" style="display:block"/><input type="time" id='+idInputNextDayTimeFrom+' name='+idInputNextDayTimeFrom+'  value ="" style="display:block"/>to<input type="time" id='+idInputNextDayTimeTo+' name='+idInputNextDayTimeTo+'  value="" style="display:block"/></td>';
  
}
}
else
{
/////next day shipping
content +='<td><label for='+idInputNextDayCheck+'><input type="checkbox" name='+idInputNextDayCheck+' id='+idInputNextDayCheck+' value="Next Day Shopping" style="    margin-right: 4px;" />Next Day Shopping</label><input type="number" name='+nextDayInput+'  value="" style="display:block"/><input type="time" id='+idInputNextDayTimeFrom+' name='+idInputNextDayTimeFrom+'  value ="" style="display:block"/>to<input type="time" id='+idInputNextDayTimeTo+' name='+idInputNextDayTimeTo+'  value="" style="display:block"/></td>';
}
if(zone.flags)
{
if(zone.flags.Scheduled)
{
/////Scheduled shipping
content +='<td><label for='+idInputScheduledCheck+'><input checked type="checkbox" name='+idInputScheduledCheck+' id='+idInputScheduledCheck+' value="Scheduled"  />Scheduled</label><input type="number" name='+scheduledDayInput+'  value='+zone.flags.Scheduled.cost+' style="display:block"/><input type="time" id='+idInputScheduledTimeFrom+' name='+idInputScheduledTimeFrom+' value='+zone.flags.Scheduled.From+' style="display:block"/>to<input type="time" id='+idInputScheduledTimeTo+' name='+idInputScheduledTimeTo+'  value='+zone.flags.Scheduled.To+' style="display:block"/></td>';

}
else
{
    content +='<td><label for='+idInputScheduledCheck+'><input type="checkbox" name='+idInputScheduledCheck+' id='+idInputScheduledCheck+' value="Scheduled"  />Scheduled</label><input type="number" name='+scheduledDayInput+'  value="" style="display:block"/><input type="time" id='+idInputScheduledTimeFrom+' name='+idInputScheduledTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputScheduledTimeTo+' name='+idInputScheduledTimeTo+'  value="" style="display:block"/></td>';
   
}
}
else
{
    content +='<td><label for='+idInputScheduledCheck+'><input type="checkbox" name='+idInputScheduledCheck+' id='+idInputScheduledCheck+' value="Scheduled"  />Scheduled</label><input type="number" name='+scheduledDayInput+'  value="" style="display:block"/><input type="time" id='+idInputScheduledTimeFrom+' name='+idInputScheduledTimeFrom+' value="" style="display:block"/>to<input type="time" id='+idInputScheduledTimeTo+' name='+idInputScheduledTimeTo+'  value="" style="display:block"/></td>';
  
}
///limit drivers

    content +='<td><label>Task Limit Drivers </label><input type="number" name='+idInputNumberTask+' value='+parseInt(zone.limit_tasks_number)+' style="display:block"></td>';
        content += '</tr>';

        content += '</tr>';
    })
    $('#zones-table').append(content)
	// driverSelectEle.addEventListener("change", (e) => {
	// 	e.preventDefault()
	// 	const driverId = driverSelectEle.value
	// 	const taskId = driverSelectEle.options[driverSelectEle.selectedIndex].dataset.taskid;
	// 	assigneTaskToDriver(taskId, driverId)
	// 	closeTaskDetails()
	// })
	
	}
}

addZoneSelect();

const updateZoneForm = document.querySelector(".updateZone");

updateZoneForm.addEventListener("submit", (e) => {
    e.preventDefault()
    let data=$('#updateZone').serializeArray();
    const getZoneData = async () => {
        
        const dbConnect = await db.ref(`users/${uid}/geoFences`)
        const zonesData = await dbConnect.once("value", (snapshot) => snapshot)
        zonesData.forEach((zoneData) => {
            const id = zoneData.key
            const zone = zoneData.val()
            let idInput=zone.name.replace(/\s/g, '')+'express';
            let idInputTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromexpress';
            let idInputTimeTo=zone.name.replace(/\s/g, '')+'TimeToexpress';

            let idInputCheck=zone.name.replace(/\s/g, '')+'check';
            let idInputSameDayCheck=zone.name.replace(/\s/g, '')+'sameDayCheck';
            let sameDayInput=zone.name.replace(/\s/g, '')+'sameDay';
            let idInputSameDayTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromsameDay';
            let idInputSameDayTimeTo=zone.name.replace(/\s/g, '')+'TimeTosameDay';
            let nextDayInput=zone.name.replace(/\s/g, '')+'nextDay';
            let idInputNextDayCheck=zone.name.replace(/\s/g, '')+'nextDayCheck';
            let idInputNextDayTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromnextDay';
            let idInputNextDayTimeTo=zone.name.replace(/\s/g, '')+'TimeTonextDay';

            let scheduledDayInput=zone.name.replace(/\s/g, '')+'scheduledDay';
            let idInputScheduledCheck=zone.name.replace(/\s/g, '')+'scheduledDayCheck';
            let idInputScheduledTimeFrom=zone.name.replace(/\s/g, '')+'TimeFromnscheduledDay';
            let idInputScheduledTimeTo=zone.name.replace(/\s/g, '')+'TimeToscheduledDay';

            let idInputNumberTask=zone.name.replace(/\s/g, '')+'NumberTask';
            let checkedExpress=$('input[name='+idInputCheck+']:checked').val();
            let checkedSameDay=$('input[name='+idInputSameDayCheck+']:checked').val();
            let checkedNextDay=$('input[name='+idInputNextDayCheck+']:checked').val();
            let checkedScheduled=$('input[name='+idInputScheduledCheck+']:checked').val();
            let idInputChangeName=zone.name.replace(/\s/g, '')+'Name';
          for(let i=0;i<data.length;i++)
               {
    /////Express

                if(idInput ==data[i].name && data[i].value !=="" )
                {
                   

                let checked=$('input[name='+idInputCheck+']:checked').val();
                if( typeof  checked !=='undefined' && checked=='Express shopping')
                {
                    
            db.ref(`users/${uid}/geoFences/${id}/flags/Express`).update({flag:checked,cost: data[i].value})
                if(document.getElementById(idInputTimeFrom).value && document.getElementById(idInputTimeTo).value)
                {
                db.ref(`users/${uid}/geoFences/${id}/flags/Express`).update({From:document.getElementById(idInputTimeFrom).value,To: document.getElementById(idInputTimeTo).value})

                }
                }
               
                
                }
                 if( typeof checkedExpress =='undefined')
                {
                    db.ref(`users/${uid}/geoFences/${id}/flags/Express`).remove()

                   

                }

/////Same day
                 if(idInputSameDayCheck ==data[i].name && data[i].value !=="" )
                {
                   

                    let checked=$('input[name='+idInputSameDayCheck+']:checked').val();


                if( typeof  checked !=='undefined' && checked=='Same Day Shopping')
                {
                    let input=$('input[name='+sameDayInput+']').val();

              
                 db.ref(`users/${uid}/geoFences/${id}/flags/SameDay`).update({flag:checked,cost:input})

                 if(document.getElementById(idInputSameDayTimeFrom).value && document.getElementById(idInputSameDayTimeTo).value)
                {
                    db.ref(`users/${uid}/geoFences/${id}/flags/SameDay`).update({From:document.getElementById(idInputSameDayTimeFrom).value,To: document.getElementById(idInputSameDayTimeTo).value})
      
                }
                }
                
                

                }
                 if( typeof checkedSameDay =='undefined')
                {
                    db.ref(`users/${uid}/geoFences/${id}/flags/SameDay`).remove()

                   

                }
/////Next Day
                 if(idInputNextDayCheck ==data[i].name && data[i].value !=="" )
                {
                   
                    let checked=$('input[name='+idInputNextDayCheck+']:checked').val();
                if( typeof  checked !=='undefined' && checked=='Next Day Shopping')
                {
                    let input=$('input[name='+nextDayInput+']').val();

              
                 db.ref(`users/${uid}/geoFences/${id}/flags/NexDay`).update({flag:checked,cost:input})

                 if(document.getElementById(idInputNextDayTimeFrom).value && document.getElementById(idInputNextDayTimeTo).value)
                 {
                     db.ref(`users/${uid}/geoFences/${id}/flags/NexDay`).update({From:document.getElementById(idInputNextDayTimeFrom).value,To: document.getElementById(idInputNextDayTimeTo).value})
       
                 }
                }
                

                
                }
                 if(typeof checkedNextDay =='undefined' )
                {
                    db.ref(`users/${uid}/geoFences/${id}/flags/NexDay`).remove()

                   

                }
/////Scheduled
                 if(idInputScheduledCheck ==data[i].name && data[i].value !=="" )
                {
                   
                    let checked=$('input[name='+idInputScheduledCheck+']:checked').val();
                if( typeof  checked !=='undefined' && checked=='Scheduled')
                {
                    let input=$('input[name='+scheduledDayInput+']').val();
              
                   db.ref(`users/${uid}/geoFences/${id}/flags/Scheduled`).update({flag:checked,cost:input})
                   if(document.getElementById(idInputScheduledTimeFrom).value && document.getElementById(idInputScheduledTimeTo).value)
                   {
                       db.ref(`users/${uid}/geoFences/${id}/flags/Scheduled`).update({From:document.getElementById(idInputScheduledTimeFrom).value,To: document.getElementById(idInputScheduledTimeTo).value})
         
                   }
                }
                

                
                }
                 if(typeof checkedScheduled =='undefined' )
                {
                    db.ref(`users/${uid}/geoFences/${id}/flags/Scheduled`).remove()

                   

                }
    
                        }
                        let number_limit_drivers=$('input[name='+idInputNumberTask+']').val();
                        if(number_limit_drivers)
                        {
                            db.ref(`users/${uid}/geoFences/${id}`).update({limit_tasks_number:number_limit_drivers})
                            
                        }
                        else if(!number_limit_drivers)
                        {
                            db.ref(`users/${uid}/geoFences/${id}`).update({limit_tasks_number:0})


                        }
                
                        
           
        })
  
   alert("Done Adding")
        
        
    }
    
   
    getZoneData();
    
    
})

const updateFlagsData = async () => {
$('.updateFlagsButton').click(function(event) {
 let express=document.getElementById('express_flag_name')
 let sameDay=document.getElementById('same_day_flag_name')
 let nextDay=document.getElementById('next_day_flag_name')
 let secludedDay=document.getElementById('secluded_flag_name')

 if(express.value)
 {
    db.ref(`users/${uid}/settings`).update({'ExpressFlag':express.value})

    db.ref(`users/${uid}/geoFences`).on("value", (geoFenceSnapshot) => {

geoFenceSnapshot.forEach((geoFenceSnapshot) => {
    if(geoFenceSnapshot.val().flags)
    {
        if(geoFenceSnapshot.val().flags.Express)
        {

             db.ref(`users/${uid}/geoFences/${geoFenceSnapshot.key}/flags/Express`).update({'flag':express.value})

        }
    }

    })
 })
    
}
if(sameDay.value)
{
    db.ref(`users/${uid}/settings`).update({'SameDayFlag':sameDay.value})

   db.ref(`users/${uid}/geoFences`).on("value", (geoFenceSnapshot) => {

geoFenceSnapshot.forEach((geoFenceSnapshot) => {
   if(geoFenceSnapshot.val().flags)
   {
       if(geoFenceSnapshot.val().flags.SameDay)
       {

            db.ref(`users/${uid}/geoFences/${geoFenceSnapshot.key}/flags/SameDay`).update({'flag':sameDay.value})

       }
   }

   })
})
   
if(nextDay.value)
{
    db.ref(`users/${uid}/settings`).update({'NextDayFlag':nextDay.value})

   db.ref(`users/${uid}/geoFences`).on("value", (geoFenceSnapshot) => {

geoFenceSnapshot.forEach((geoFenceSnapshot) => {
   if(geoFenceSnapshot.val().flags)
   {
       if(geoFenceSnapshot.val().flags.NexDay)
       {

            db.ref(`users/${uid}/geoFences/${geoFenceSnapshot.key}/flags/NexDay`).update({'flag':nextDay.value})

       }
   }

   })
})
   
}
if(secludedDay.value)
{
    db.ref(`users/${uid}/settings`).update({'SecludedDay':secludedDay.value})

   db.ref(`users/${uid}/geoFences`).on("value", (geoFenceSnapshot) => {

geoFenceSnapshot.forEach((geoFenceSnapshot) => {
   if(geoFenceSnapshot.val().flags)
   {
       if(geoFenceSnapshot.val().flags.Scheduled)
       {

            db.ref(`users/${uid}/geoFences/${geoFenceSnapshot.key}/flags/Scheduled`).update({'flag':secludedDay.value})

       }
   }

   })
})
   
}
}
confirm('done')

})
}
updateFlagsData();

const seFlags = async () => {

    let express=document.getElementById('express_flag_name')
    let sameDay=document.getElementById('same_day_flag_name')
    let nextDay=document.getElementById('next_day_flag_name')
    let secludedDay=document.getElementById('secluded_flag_name')
    let expressFlag = (await (db.ref(`users/${uid}/settings`).child("ExpressFlag").once("value"))).val()
    if(expressFlag)
    {
        express.value=expressFlag

    }
    let nextDayFlag = (await (db.ref(`users/${uid}/settings`).child("NextDayFlag").once("value"))).val()
    if(nextDayFlag)
    {
        nextDay.value=nextDayFlag
    }
   
    let sameDayFlag = (await (db.ref(`users/${uid}/settings`).child("SameDayFlag").once("value"))).val()
    if(sameDayFlag)
    {
    sameDay.value=sameDayFlag
    }
    let secludedDayFlag = (await (db.ref(`users/${uid}/settings`).child("SecludedDay").once("value"))).val()
    if(secludedDayFlag)
    {
        secludedDay.value=secludedDayFlag

    }
}

seFlags();
$( document ).ready(function() {
	var now = new Date();
now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
document.getElementById('taskDeliveryPickUpBeforeSet').value = now.toISOString().slice(0,16);
document.getElementById('taskPickUpPickUpBeforeSet').value = now.toISOString().slice(0,16);

});