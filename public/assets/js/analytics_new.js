
const tasksInput = document.getElementById("total_order")
const total_cash = document.getElementById("total_cash")
const total_revenue = document.getElementById("total_revenue")
const total_profit =document.getElementById("total_profit")


const getTasks = async () => {
    const tasks = await db.ref(`users/${uid}/tasks`)
    tasks.on("value", async (snapshot) => {
        const completed = []
        const assigned = []
        const unAssigned = []
        let cash=0
        let total_shipment_fees=0
        snapshot.forEach((task) => {
            const taskStatus = task.val().status
            if (taskStatus === -1) completed.push(task)
            if (taskStatus === 0) assigned.push(task)
            if (taskStatus === 1) unAssigned.push(task)

            if(task.val().fees)
            {
              fees=snapshot.val().fees
              cash+=parseInt(task.val().fees)+parseInt(task.val().shipment_fees)
           
              total_shipment_fees+=parseInt(task.val().shipment_fees)
            }
        })
        let salary=0
       
        const tasksValues = await snapshot.val()
        const tasksNum = await snapshot.numChildren()
        tasksInput.innerHTML = tasksNum
        total_cash.innerHTML='EGP '+cash
        total_revenue.innerHTML='EGP' +(cash- parseInt(total_shipment_fees))
        const drivers = await db.ref(`users/${uid}/drivers`)
        drivers.on("value", async (snapshot) => {
            
           
          
            snapshot.forEach((driver) => {
                const driverData = driver.val()
    
                if(driverData.driverSalary)
                {
                    salary+=parseInt(driverData.driverSalary)
                    

                }
            })
       total_profit.innerHTML='EGP' +(cash- parseInt(salary))
        })
       
       

        // completed_tasksInput.innerHTML = completed.length
        // awaited_tasksInput.innerHTML = assigned.length
        // unAssigned_tasks.innerHTML = unAssigned.length
    })
}
getTasks()