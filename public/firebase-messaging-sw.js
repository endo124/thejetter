importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.4.2/firebase-messaging.js');


// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
var firebaseConfig = {
    apiKey: "AIzaSyBpvwKdVpQPyW4tacgKjAxvypRgCenmfsA",
    authDomain: "chat-653d2.firebaseapp.com",
    projectId: "chat-653d2",
    storageBucket: "chat-653d2.appspot.com",
    messagingSenderId: "814409154720",
    appId: "1:814409154720:web:2a6a3930c7c12671e449c5",
    measurementId: "G-HTKJWJB2CP"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();




messaging.onBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here

    const { title, body } = payload.notification;
    // const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body,
        // icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(title,
        notificationOptions);
});